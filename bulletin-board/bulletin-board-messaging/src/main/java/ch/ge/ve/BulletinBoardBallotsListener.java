/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.BallotConfirmationPublicationEvent;
import ch.ge.ve.event.BallotPublicationEvent;
import ch.ge.ve.event.ConfirmationFailedEvent;
import ch.ge.ve.event.payload.BallotConfirmationPayload;
import ch.ge.ve.event.payload.BallotPublicationPayload;
import ch.ge.ve.event.payload.ConfirmationFailedPayload;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.service.AbstractAcknowledgeableEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board ballots listener.
 */
@Component
public class BulletinBoardBallotsListener extends AbstractAcknowledgeableEventsListener {
  private static final Logger logger = LoggerFactory.getLogger(BulletinBoardBallotsListener.class);

  private final BulletinBoard bulletinBoard;

  @Autowired
  public BulletinBoardBallotsListener(BulletinBoard bulletinBoard, EventBus eventBus) {
    super(eventBus, Endpoints.BULLETIN_BOARD);
    this.bulletinBoard = bulletinBoard;
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.BALLOT_PUBLICATIONS + "-bulletinBoard", durable = "true",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                               value = Channels.BALLOT_PUBLICATIONS)
      )
  )
  public void processBallotPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                            BallotPublicationEvent event) {
    BallotPublicationPayload payload = event.getPayload();
    int voterIndex = payload.getVoterIndex();
    BallotAndQuery ballotAndQuery = payload.getBallotAndQuery();
    bulletinBoard.storeBallot(protocolId, voterIndex, ballotAndQuery);
    acknowledge(protocolId, event);
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.BALLOT_CONFIRMATION_PUBLICATIONS + "-bulletinBoard", durable = "true",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                               value = Channels.BALLOT_CONFIRMATION_PUBLICATIONS)
      )
  )
  public void processBallotConfirmationPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                        BallotConfirmationPublicationEvent event) {
    BallotConfirmationPayload payload = event.getPayload();
    int voterIndex = payload.getVoterIndex();
    Confirmation confirmation = payload.getConfirmation();
    logger.info("Ballot confirmation {} published by voter #{}", confirmation, voterIndex);
    bulletinBoard.storeConfirmation(protocolId, voterIndex, confirmation);
    acknowledge(protocolId, event);
  }


  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.CONFIRMATION_FAILED + "-bulletinBoard", durable = "true",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
          exchange = @Exchange(type = ExchangeTypes.TOPIC, durable = "true",
                               value = Channels.CONFIRMATION_FAILED)
      )
  )
  public void processBallotConfirmationFailedEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                   ConfirmationFailedEvent event) {
    ConfirmationFailedPayload payload = event.getPayload();
    int voterIndex = payload.getVoterIndex();
    logger.info("Confirmation failed published for voter #{}", voterIndex);
    acknowledge(protocolId, event);
  }
}
