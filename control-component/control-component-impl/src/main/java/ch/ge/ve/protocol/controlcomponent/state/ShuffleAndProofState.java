/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * {@code ShuffleAndProofState} is responsible to maintain the state of the shuffles and their proofs for the control
 * component across its clustered deployment.
 * <p>
 * Shuffles and their proofs are keyed by their source authority index.
 */
public class ShuffleAndProofState extends AbstractKeyedPropertyState<ShuffleAndProof> {

  ShuffleAndProofState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.SHUFFLE_AND_PROOF_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format(
        "Once the shuffle and proof have been set for authority j=%d, it can no longer be changed",
        key);
  }

  @Override
  protected TypeReference<ShuffleAndProof> getPropertyType() {
    return new TypeReference<ShuffleAndProof>() {
    };
  }
}
