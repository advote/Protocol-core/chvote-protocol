/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent;

import ch.ge.ve.protocol.controlcomponent.exception.InvalidShuffleProofRuntimeException;
import ch.ge.ve.protocol.controlcomponent.exception.SignEncryptionException;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentCachedState;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentState;
import ch.ge.ve.protocol.controlcomponent.state.ShuffleAndProofState;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.ElectionPreparationAlgorithms;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.MixingAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.PolynomialAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteCastingAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationAuthorityAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.exception.IncorrectBallotRuntimeException;
import ch.ge.ve.protocol.core.exception.InvalidElectionOfficerPublicKeyRuntimeException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.exception.ProtocolRuntimeException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.BallotEntry;
import ch.ge.ve.protocol.core.model.ConfirmationEntry;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponseAndRand;
import ch.ge.ve.protocol.core.model.PartialDecryptionsAndProof;
import ch.ge.ve.protocol.core.model.PublicConfirmationCredentialsList;
import ch.ge.ve.protocol.core.model.PublicIdentificationCredentialsList;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.Shuffle;
import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.protocol.core.model.ShufflesAndProofs;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.eventlog.state.ImmutablePropertyRuntimeException;
import ch.ge.ve.protocol.eventlog.state.VoterState;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.SecurityParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Voter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * Default implementation of the {@link ControlComponent} interface.
 */
@Component
public class DefaultControlComponent implements ControlComponent {
  private static final Logger log     = LoggerFactory.getLogger(DefaultControlComponent.class);
  private static final Logger perfLog = LoggerFactory.getLogger("PerformanceStats");

  private final int                         j;
  private final AlgorithmsSpec              algorithmsSpec;
  private final int                         batchSize;
  private final RandomGenerator             randomGenerator;
  private final ControlComponentState       state;
  private final ControlComponentCachedState cachedState;
  private final IdentificationPrivateKey    signingKey;
  private final ObjectMapper                jsonObjectMapper;
  private final Queue<Long>                 ballotVerificationTimes       = new ConcurrentLinkedQueue<>();
  private final Queue<Long>                 queryResponseTimes            = new ConcurrentLinkedQueue<>();
  private final Queue<Long>                 confirmationVerificationTimes = new ConcurrentLinkedQueue<>();
  private final Queue<Long>                 finalizationComputationTimes  = new ConcurrentLinkedQueue<>();

  private final DatabaseOptimizer databaseOptimizer;

  @Autowired
  public DefaultControlComponent(@Value("${ch.ge.ve.cc.index}") int j,
                                 @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}") int batchSize,
                                 AlgorithmsSpec algorithmsSpec,
                                 ObjectMapper jsonObjectMapper,
                                 RandomGenerator randomGenerator,
                                 ControlComponentState state,
                                 ControlComponentCachedState cachedState,
                                 @Qualifier("controlComponentSigningKey") IdentificationPrivateKey signingKey,
                                 DatabaseOptimizer databaseOptimizer) {
    this.j = j;
    this.algorithmsSpec = algorithmsSpec;
    this.batchSize = batchSize;
    this.randomGenerator = randomGenerator;
    this.state = state;
    this.cachedState = cachedState;
    this.jsonObjectMapper = jsonObjectMapper;
    this.signingKey = signingKey;
    this.databaseOptimizer = databaseOptimizer;
  }

  @Override
  public int getIndex() {
    return j;
  }

  @Override
  public void storePublicParameters(String protocolId, PublicParameters publicParameters) {
    Preconditions.checkNotNull(publicParameters, "the public parameters should not be null");
    state.getPublicParametersState().storeValueIfAbsent(protocolId, publicParameters);
  }

  @Override
  public EncryptionPublicKey generateKeys(String protocolId) {
    PublicParameters publicParameters = getPublicParameters(protocolId);

    // manage idempotency correctly here: key pair must not be regenerated even if
    // the public parameters message is received several times
    final Optional<EncryptionKeyPair> existingEncryptionKeyPair =
        state.getEncryptionKeyPairState().getValue(protocolId);
    final EncryptionKeyPair resultKeyPair = existingEncryptionKeyPair.orElseGet(
        () -> {
          EncryptionGroup encryptionGroup = publicParameters.getEncryptionGroup();
          EncryptionKeyPair newKeyPair = getKeyEstablishmentAlgorithms().generateKeyPair(encryptionGroup);
          state.getEncryptionKeyPairState().storeValueIfAbsent(protocolId, newKeyPair);
          return newKeyPair;
        }
    );
    return resultKeyPair.getPublicKey();
  }

  private KeyEstablishmentAlgorithms getKeyEstablishmentAlgorithms() {
    return new KeyEstablishmentAlgorithms(randomGenerator);
  }

  private ChannelSecurityAlgorithms getChannelSecurityAlgorithms(String protocolId) throws ChannelSecurityException {
    return new ChannelSecurityAlgorithms(algorithmsSpec,
                                         getHash(protocolId),
                                         getPublicParameters(protocolId).getIdentificationGroup(),
                                         randomGenerator);
  }

  @Override
  public PublicParameters getPublicParameters(String protocolId) {
    return cachedState.getPublicParameters(protocolId);
  }

  @Override
  public Optional<EncryptionPublicKey> storeKeyPart(String protocolId, int j, EncryptionPublicKey publicKey) {
    checkControlComponentIndex(protocolId, j);
    state.getControlComponentPublicKeysState().storeValueIfAbsent(protocolId, j, publicKey);
    return buildSystemPublicKeyIfPossible(protocolId);
  }

  @Override
  public Optional<EncryptionPublicKey> storeElectionOfficerPublicKey(String protocolId,
                                                                     EncryptionPublicKey electionOfficerPublicKey) {
    Preconditions.checkNotNull(electionOfficerPublicKey, "the election officer public key should not be null");

    // verify validity of the public key
    if (!GeneralAlgorithms
        .isMember(electionOfficerPublicKey.getPublicKey(), getPublicParameters(protocolId).getEncryptionGroup())) {
      throw new InvalidElectionOfficerPublicKeyRuntimeException();
    }

    state.getElectionOfficerPublicKeyState().storeValueIfAbsent(protocolId, electionOfficerPublicKey);
    return buildSystemPublicKeyIfPossible(protocolId);
  }

  private Optional<EncryptionPublicKey> buildSystemPublicKeyIfPossible(String protocolId) {
    if (hasReceivedAllPublicKeyParts(protocolId)) {
      log.info("Received all public key parts, building system public key");
      final EncryptionPublicKey encryptionPublicKey = buildPublicKey(protocolId);
      state.getSystemPublicKeyState().storeValueIfAbsent(protocolId, encryptionPublicKey);
    }
    return state.getSystemPublicKeyState().getValue(protocolId);
  }

  private boolean hasReceivedAllPublicKeyParts(String protocolId) {
    Optional<EncryptionPublicKey> electionOfficerPublicKey =
        state.getElectionOfficerPublicKeyState().getValue(protocolId);
    final Map<Integer, EncryptionPublicKey> controlComponentsPublicKeys =
        state.getControlComponentPublicKeysState().getMappedValues(protocolId);
    final Optional<PublicParameters> publicParameters = state.getPublicParametersState().getValue(protocolId);

    // Public parameters could not already be present: their are stored only when key generation is asked
    // using {@link ControlComponent#generateKeys}, or public key parts can be published beforehand.
    // hasReceivedAllPublicKeyParts checks for public parameters presence, and if absent, then we can
    // safely assume that all public keys were not received because the current control component's
    // public key part is issued only once {@link ControlComponent#generateKeys} is called...
    //
    return electionOfficerPublicKey.isPresent() && publicParameters.isPresent() &&
           publicParameters.map(params -> controlComponentsPublicKeys.size() == params.getS()).orElse(false);
  }

  /**
   * Creates and returns the global encryption key used to encrypt the ballots. All the key parts (control components
   * and election officer keys) must have been published before this method is called.
   *
   * @param protocolId the protocol instance id
   *
   * @return the system public key.
   */
  private EncryptionPublicKey buildPublicKey(String protocolId) {
    final EncryptionKeyPair keyPair = state.getEncryptionKeyPairState().getValue(protocolId).orElseThrow(
        () -> new IllegalStateException("Encryption key pair has not yet been published")
    );
    final Optional<EncryptionPublicKey> publicKeyOptional =
        state.getControlComponentPublicKeysState().getValue(protocolId, j);
    Preconditions
        .checkState(publicKeyOptional.isPresent() && publicKeyOptional.get().equals(keyPair.getPublicKey()),
                    "This component's key should be present on the published public keys list");

    List<EncryptionPublicKey> publicKeyParts =
        Stream.concat(state.getControlComponentPublicKeysState().getMappedValues(protocolId).values().stream(),
                      state.getElectionOfficerPublicKeyState().getValue(protocolId).map(Stream::of)
                           .orElse(Stream.empty())
        ).collect(Collectors.toList());
    return (EncryptionPublicKey) getKeyEstablishmentAlgorithms().getPublicKey(publicKeyParts);
  }

  private EncryptionPublicKey getMyPublicKey(String protocolId) {
    return state.getControlComponentPublicKeysState().getValue(protocolId, j)
                .orElseThrow(() -> new IllegalStateException("The encryption keys have not been generated yet"));
  }

  @Override
  public void storeElectionSet(String protocolId, ElectionSetWithPublicKey electionSet) {
    state.getElectionSetState().storeValueIfAbsent(protocolId, electionSet);
  }

  @Override
  public List<VoterData> generateVoterData(String protocolId, List<Voter> voters) {
    Optional<ElectionSetWithPublicKey> electionSet = state.getElectionSetState().getValue(protocolId);
    Preconditions.checkState(electionSet.isPresent(), "The election set must have been defined first");

    Stopwatch stopwatch = Stopwatch.createStarted();

    VoterState voterState = state.getVoterState();
    voterState.batchStoreIfAbsent(protocolId, voters, Voter::getVoterId);

    if (log.isDebugEnabled()) {
      log.debug("Stored voters in {} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    log.info("Authority {} generating electorate data", j);

    List<Integer> voterIds = voters.stream().map(Voter::getVoterId).collect(Collectors.toList());

    // Idempotency: do not regenerate the electorate data and return the existing one if the message
    // is received several times.
    stopwatch.reset().start();
    List<VoterData> voterDataList = state.getVoterDataState().getMappedValues(protocolId, voterIds)
                                         .values()
                                         .stream()
                                         .sorted(Comparator.comparingInt(VoterData::getVoterId))
                                         .collect(Collectors.toList());

    if (log.isDebugEnabled()) {
      log.debug("Read voters data in {} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    if (voterDataList.size() != voters.size()) {
      List<Voter> votersWithoutData = voters.stream().filter(
          v -> voterDataList.stream().noneMatch(vD -> vD.getVoterId() == v.getVoterId()))
                                            .collect(Collectors.toList());

      stopwatch.reset().start();
      ElectionPreparationAlgorithms electionPreparationAlgorithms = getElectionPreparationAlgorithms(protocolId);
      List<VoterData> newVoterData =
          electionPreparationAlgorithms.genElectorateData(votersWithoutData, electionSet.get());

      stopwatch.reset().start();
      state.getVoterDataState().batchStoreIfAbsent(protocolId, newVoterData, VoterData::getVoterId);
      voterDataList.addAll(newVoterData);
      if (log.isDebugEnabled()) {
        log.debug("Stored voters data in {} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
      }
    }
    return voterDataList;
  }

  private ElectionPreparationAlgorithms getElectionPreparationAlgorithms(String protocolId) {
    return new ElectionPreparationAlgorithms(getPublicParameters(protocolId), randomGenerator, getHash(protocolId),
                                             new PolynomialAlgorithms(randomGenerator,
                                                                      getPublicParameters(protocolId).getPrimeField()));
  }

  private ElectionSetWithPublicKey getElectionSet(String protocolId) {
    return cachedState.getElectionSetWithPublicKey(protocolId);
  }

  @Override
  public Map<PrintingAuthorityWithPublicKey, byte[]> getSignEncryptedPrivateCredentials(String protocolId, List<Integer>
      voterIds)
      throws SignEncryptionException {
    long voterCount = getElectionSet(protocolId).getVoterCount();
    Preconditions.checkState(state.getVoterState().countValues(protocolId) == voterCount,
                             "All the voters must have been generated first");
    Preconditions.checkState(state.getVoterDataState().countValues(protocolId) == voterCount,
                             "All the voter data must have been generated first");
    Preconditions.checkState(state.getPublicCredentialsState().countValues(protocolId) == voterCount,
                             "The public credentials need to have been generated first");

    StopWatch stopWatchLoadVoters = new StopWatch();
    stopWatchLoadVoters.start();
    List<Voter> voters = state.getVoterState().getMappedValues(protocolId, voterIds).values()
                              .stream().sorted(Comparator.comparingInt(Voter::getVoterId))
                              .collect(Collectors.toList());
    stopWatchLoadVoters.stop();


    final Map<String, PrintingAuthorityWithPublicKey> printingAuthoritiesByName =
        getPrintingAuthoritiesByName(protocolId);

    StopWatch stopWatchLoadVotersData = new StopWatch();
    stopWatchLoadVotersData.start();

    Map<Integer, VoterData> votersData = state.getVoterDataState().getMappedValues(protocolId, voterIds);

    Map<PrintingAuthorityWithPublicKey, List<SecretVoterData>> dataByPrintingAuthority =
        voters.stream()
              .collect(
                  Collectors.groupingBy(v -> printingAuthoritiesByName.get(v.getPrintingAuthorityName()),
                                        Collectors.mapping(
                                            v -> Optional.ofNullable(votersData.get(v.getVoterId()))
                                                         .orElseThrow(() -> new IllegalStateException(
                                                             "No voter data for voter " + v.getVoterId()))
                                                         .getSecretVoterData(), Collectors.toList())));
    stopWatchLoadVotersData.stop();

    Map<PrintingAuthorityWithPublicKey, byte[]> signEncryptedCredentialsByPrintingAuthority = new HashMap<>();
    List<String> printingAuthoritiesLogLines = new ArrayList<>();
    int secretVoterDataCount = 0;

    ChannelSecurityAlgorithms channelSecurityAlgorithms;
    try {
      channelSecurityAlgorithms = getChannelSecurityAlgorithms(protocolId);
    } catch (ChannelSecurityException e) {
      throw new SignEncryptionException("unable to create channel security algorithms", e);
    }

    for (Map.Entry<PrintingAuthorityWithPublicKey, List<SecretVoterData>> entry : dataByPrintingAuthority.entrySet()) {
      secretVoterDataCount += signAndEncryptVoterData(signEncryptedCredentialsByPrintingAuthority,
                                                      printingAuthoritiesLogLines,
                                                      channelSecurityAlgorithms,
                                                      entry);
    }

    final int min = getMinVoterId(voterIds);
    final int max = getMaxVoterId(voterIds);

    if (log.isInfoEnabled()) {
      log.info("SignEncrypt information for batch [{}-{}]:\n" +
               "- loaded {} voters in {} ms\n" +
               "- loaded {} voters data in {} ms\n" +
               "{}",
               min, max,
               voters.size(), stopWatchLoadVoters.getTotalTimeMillis(),
               votersData.size(), stopWatchLoadVotersData.getTotalTimeMillis(),
               Joiner.on("\n").join(printingAuthoritiesLogLines));
    }

    if (secretVoterDataCount != voterIds.size()) {
      throw new ProtocolRuntimeException(
          String.format("The sum (%d) of the count of voter data mapped to each printing authority " +
                        "does not match the total voter count (%d) for the batch [%d-%d]",
                        secretVoterDataCount,
                        voterIds.size(),
                        min, max));
    }

    return signEncryptedCredentialsByPrintingAuthority;
  }

  private int signAndEncryptVoterData(Map<PrintingAuthorityWithPublicKey, byte[]>
                                          signEncryptedCredentialsByPrintingAuthority,
                                      List<String> printingAuthoritiesLogLines,
                                      ChannelSecurityAlgorithms channelSecurityAlgorithms,
                                      Map.Entry<PrintingAuthorityWithPublicKey, List<SecretVoterData>> entry)
      throws SignEncryptionException {

    StringBuilder logText = new StringBuilder();

    PrintingAuthorityWithPublicKey printingAuthority = entry.getKey();
    List<SecretVoterData> secretVoterData = entry.getValue();

    StopWatch stopWatchSerialize = new StopWatch();
    stopWatchSerialize.start();
    String encodedData = serializeSecretVoterDataList(secretVoterData);
    stopWatchSerialize.stop();

    logText.append(
        String.format("- [%s]: voter data list of size %d serialized in %d ms",
                      printingAuthority.getName(),
                      secretVoterData.size(),
                      stopWatchSerialize.getTotalTimeMillis()));

    byte[] signEncryptedCreds;
    try {
      StopWatch stopWatchSignEncrypt = new StopWatch();
      stopWatchSignEncrypt.start();
      EncryptedMessageWithSignature encryptedMessageWithSignature =
          channelSecurityAlgorithms
              .signAndEncrypt(signingKey.getPrivateKey(), printingAuthority.getPublicKey(), encodedData);
      signEncryptedCreds = jsonObjectMapper.writeValueAsBytes(encryptedMessageWithSignature);
      stopWatchSignEncrypt.stop();

      logText.append(String.format(" and signEncrypted in %d ms", stopWatchSignEncrypt.getTotalTimeMillis()));

    } catch (ChannelSecurityException | JsonProcessingException e) {
      throw new SignEncryptionException("Failed to signEncrypt the data", e);
    }
    signEncryptedCredentialsByPrintingAuthority.put(printingAuthority, signEncryptedCreds);

    printingAuthoritiesLogLines.add(logText.toString());

    return secretVoterData.size();
  }

  private int getMinVoterId(List<Integer> voterIds) {
    return voterIds.stream().mapToInt(Integer::intValue).min().orElse(0);
  }

  private int getMaxVoterId(List<Integer> voterIds) {
    return voterIds.stream().mapToInt(Integer::intValue).max().orElse(Integer.MAX_VALUE);
  }

  private Map<String, PrintingAuthorityWithPublicKey> getPrintingAuthoritiesByName(String protocolId) {
    ImmutableMap.Builder<String, PrintingAuthorityWithPublicKey> builder = new ImmutableMap.Builder<>();
    List<PrintingAuthorityWithPublicKey> printingAuthorities = getElectionSet(protocolId).getPrintingAuthorities();
    Preconditions.checkState(!printingAuthorities.isEmpty(), "There should be at least one printingAuthority");
    for (PrintingAuthorityWithPublicKey printingAuthority : printingAuthorities) {
      builder.put(printingAuthority.getName(), printingAuthority);
    }
    return builder.build();
  }

  private String serializeSecretVoterDataList(List<SecretVoterData> secretVoterData) throws SignEncryptionException {
    try {
      return jsonObjectMapper.writeValueAsString(secretVoterData);
    } catch (JsonProcessingException e) {
      throw new SignEncryptionException("Failed to serialize the data", e);
    }
  }

  @Override
  public void storePublicCredentialsPart(String protocolId, int j, Map<Integer, Point> publicCredentialsPart) {
    checkControlComponentIndex(protocolId, j);
    state.getPublicCredentialsPartsState().batchStoreIfAbsent(protocolId, j, publicCredentialsPart);
  }

  @Override
  public boolean hasReceivedAllPublicCredentialParts(String protocolId) {
    final Long count = state.getPublicCredentialsPartsState().countValues(protocolId);
    return state.getPublicParametersState().getValue(protocolId)
                .map(params -> count == params.getS() * getElectionSet(protocolId).getVoterCount())
                .orElse(false);
  }

  @Override
  public void forceEventLogIndex() {
    databaseOptimizer.updateEventLogsStats();
  }

  private void checkControlComponentIndex(String protocolId, int j) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    Preconditions.checkElementIndex(j, publicParameters.getS(),
                                    "The index j should be lower than the number of authorities");
  }

  @Override
  public List<List<Integer>> getVoterIdBatches(String protocolId) {
    List<Integer> allVoterIds = state.getPublicCredentialsPartsState().getVoterIds(protocolId);

    List<List<Integer>> voterIdBatches = new ArrayList<>();
    for (int i = 0; batchSize * i < allVoterIds.size(); i++) {
      voterIdBatches.add(allVoterIds.subList(i * batchSize, Math.min((i + 1) * batchSize, allVoterIds.size())));
    }
    return voterIdBatches;
  }

  @Override
  public void buildPublicCredentials(String protocolId, List<Integer> voterIds) {
    int s = getPublicParameters(protocolId).getS();
    Map<Integer, Map<Integer, Point>> pointsByVoterIdByCCIndex =
        IntStream.range(0, s).boxed()
                 .collect(Collectors.toMap(Function.identity(),
                                           ccIndex -> state.getPublicCredentialsPartsState()
                                                           .getMappedValues(protocolId, ccIndex, voterIds)));
    if (log.isInfoEnabled()) {
      log.info(String.format("Authority %d retrieved data from the state for voter ids [%s to %s]",
                             this.j,
                             voterIds.stream().min(Ordering.natural()), voterIds.stream().max(Ordering.natural())));
    }

    Preconditions.checkState(pointsByVoterIdByCCIndex.size() == s);
    Preconditions.checkState(pointsByVoterIdByCCIndex.values().stream().allMatch(p -> p.size() == voterIds.size()));

    List<List<Point>> credentialsPartsList =
        pointsByVoterIdByCCIndex.values().stream()
                                .map(pointsByVoterId -> pointsByVoterId.entrySet().stream()
                                                                       .sorted(Map.Entry.comparingByKey())
                                                                       .map(Map.Entry::getValue)
                                                                       .collect(Collectors.toList()))
                                .collect(Collectors.toList());

    List<Point> publicCredentials =
        getElectionPreparationAlgorithms(protocolId).getPublicCredentials(credentialsPartsList);

    Map<Integer, Point> credentialsMap = IntStream.range(0, voterIds.size()).boxed()
                                                  .collect(Collectors.toMap(voterIds::get,
                                                                            publicCredentials::get));

    if (log.isInfoEnabled()) {
      log.info(String.format("Authority %d computed public credentials for voter ids [%s-%s]",
                             this.j,
                             getMinVoterId(voterIds), getMaxVoterId(voterIds)));
    }

    state.getPublicCredentialsState().batchStoreIfAbsent(protocolId, credentialsMap);
    if (log.isInfoEnabled()) {
      log.info(String.format("Authority %d stored public credentials for voter ids [%s-%s]",
                             this.j,
                             getMinVoterId(voterIds), getMaxVoterId(voterIds)));
    }
  }

  @Override
  public boolean doneBuildingPublicCredentials(String protocolId) {
    long voterCount = getElectionSet(protocolId).getVoterCount();
    return state.getPublicCredentialsState().countValues(protocolId) == voterCount;
  }

  @Override
  public List<BigInteger> generatePrimes(String protocolId) throws NotEnoughPrimesInGroupException {
    int n = getNumberOfPrimesNeeded(getElectionSet(protocolId));
    EncryptionGroup encryptionGroup = getPublicParameters(protocolId).getEncryptionGroup();
    List<BigInteger> primes = PrimesCache.populate(n, encryptionGroup).getPrimes();
    state.getPrimesState().storeValueIfAbsent(protocolId, primes);
    return primes;
  }

  @Override
  public ObliviousTransferResponse handleBallot(String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery) {
    log.info("Authority {} handling ballot", j);
    Stopwatch stopwatch = Stopwatch.createStarted();
    VoteCastingAuthorityAlgorithms localVoteCastingAuthorityAlgorithms = getVoteCastingAuthorityAlgorithms(protocolId);

    if (needToCheckBallot(protocolId, voterIndex, ballotAndQuery)) {
      PublicIdentificationCredentialsList publicIdentificationCredentialsList =
          state.getPublicCredentialsState().getPublicIdentificationCredentialsList(protocolId);
      if (!localVoteCastingAuthorityAlgorithms.checkBallot(voterIndex, ballotAndQuery, getSystemPublicKey(protocolId),
                                                           publicIdentificationCredentialsList,
                                                           state.getBallotEntriesState().getBallotList(protocolId))) {
        throw new IncorrectBallotRuntimeException(String.format("Ballot for voter %d was deemed invalid", voterIndex));
      }
      stopwatch.stop();
      ballotVerificationTimes.add(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    stopwatch.reset().start();
    VoterData voterData = state.getVoterDataState().getValue(protocolId, voterIndex)
                               .orElseThrow(() -> new IllegalStateException(
                                   String.format("No voter data found for voter %d", voterIndex)));
    ObliviousTransferResponseAndRand responseAndRand =
        getObliviousTransferResponseAndRand(protocolId, voterIndex, ballotAndQuery,
                                            localVoteCastingAuthorityAlgorithms, voterData);
    BallotEntry ballotEntry = new BallotEntry(voterIndex, ballotAndQuery, responseAndRand.getZ());
    state.getBallotEntriesState().storeValueIfAbsent(protocolId, voterIndex, ballotEntry);

    ObliviousTransferResponse beta = responseAndRand.getBeta();
    stopwatch.stop();
    queryResponseTimes.add(stopwatch.elapsed(TimeUnit.MILLISECONDS));

    return beta;
  }

  private ObliviousTransferResponseAndRand getObliviousTransferResponseAndRand(
      String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery,
      VoteCastingAuthorityAlgorithms localVoteCastingAuthorityAlgorithms, VoterData voterData) {
    ObliviousTransferResponseAndRand responseAndRand;
    Optional<BallotEntry> ballotEntry =
        state.getBallotEntriesState().getBallotList(protocolId).getBallotEntry(voterIndex);
    if (ballotEntry.isPresent()) {
      responseAndRand =
          localVoteCastingAuthorityAlgorithms
              .reComputeResponse(ballotAndQuery.getBold_a(), getSystemPublicKey(protocolId),
                                 getElectionSet(protocolId).getBold_n(), voterData.getAllowedSelections(),
                                 voterData.getRandomPoints(), ballotEntry.get().getZ());
    } else {
      responseAndRand =
          localVoteCastingAuthorityAlgorithms.genResponse(ballotAndQuery.getBold_a(),
                                                          getSystemPublicKey(protocolId),
                                                          getElectionSet(protocolId).getBold_n(),
                                                          voterData.getAllowedSelections(),
                                                          voterData.getRandomPoints());
    }
    return responseAndRand;
  }

  private boolean needToCheckBallot(String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery) {
    Optional<BallotEntry> ballotEntry =
        state.getBallotEntriesState().getBallotList(protocolId).getBallotEntry(voterIndex);
    if (ballotEntry.isPresent() && !ballotEntry.get().getAlpha().equals(ballotAndQuery)) {
      throw new ImmutablePropertyRuntimeException(
          String.format("received a new and different ballot for voter %d", voterIndex));
    }
    return !ballotEntry.isPresent();
  }

  private VoteCastingAuthorityAlgorithms getVoteCastingAuthorityAlgorithms(String protocolId) {
    ElectionSetWithPublicKey electionSet = getElectionSet(protocolId);
    return new VoteCastingAuthorityAlgorithms(getPublicParameters(protocolId), electionSet,
                                              state.getVoterState().getVoterList(protocolId),
                                              getGeneralAlgorithms(protocolId),
                                              randomGenerator, getHash(protocolId));
  }

  private GeneralAlgorithms getGeneralAlgorithms(String protocolId) {
    PublicParameters publicParameters = getPublicParameters(protocolId);
    EncryptionGroup encryptionGroup = publicParameters.getEncryptionGroup();
    return new GeneralAlgorithms(getHash(protocolId), encryptionGroup,
                                 publicParameters.getIdentificationGroup(),
                                 new PrimesCache(cachedState.getPrimes(protocolId), encryptionGroup));
  }

  private int getNumberOfPrimesNeeded(ElectionSetWithPublicKey electionSet) {
    int countingCircleCount = electionSet.getCountingCircleCount();
    return electionSet.getCandidates().size() + countingCircleCount;
  }

  private Hash getHash(String protocolId) {
    SecurityParameters securityParameters = getPublicParameters(protocolId).getSecurityParameters();
    return new Hash(algorithmsSpec.getDigestAlgorithm(),
                    algorithmsSpec.getDigestProvider(),
                    securityParameters.getUpper_l());
  }

  private EncryptionPublicKey getSystemPublicKey(String protocolId) {
    return state.getSystemPublicKeyState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("System public key has not yet been created"));
  }

  @Override
  public boolean storeConfirmationIfValid(String protocolId, Integer voterIndex, Confirmation confirmation) {
    Stopwatch stopwatch = Stopwatch.createUnstarted();
    VoteConfirmationAuthorityAlgorithms voteConfirmationAuthorityAlgorithms =
        getVoteConfirmationAuthorityAlgorithms(protocolId);

    if (!state.getBallotEntriesState().getBallotList(protocolId).getBallotEntry(voterIndex).isPresent()) {
      throw new IllegalStateException(
          String.format("Attempting to handle a confirmation before we received the ballot for voter %d", voterIndex));
    }

    // The message could be repeated, in which case it doesn't need to be verified again, or it could be a new value,
    // which is not allowed and should throw an exception
    if (needToCheckConfirmation(protocolId, voterIndex, confirmation)) {
      stopwatch.start();
      PublicConfirmationCredentialsList publicConfirmationCredentials =
          state.getPublicCredentialsState().getPublicConfirmationCredentialsList(protocolId);

      if (!voteConfirmationAuthorityAlgorithms.checkConfirmation(
          voterIndex, confirmation, publicConfirmationCredentials,
          state.getBallotEntriesState().getBallotList(protocolId),
          state.getConfirmationEntriesState().getConfirmationList(protocolId))) {
        return false;
      }
      stopwatch.stop();
      confirmationVerificationTimes.add(stopwatch.elapsed(TimeUnit.MILLISECONDS));

      ConfirmationEntry confirmationEntry = new ConfirmationEntry(voterIndex, confirmation);
      state.getConfirmationEntriesState().storeValueIfAbsent(protocolId, voterIndex, confirmationEntry);
    }
    return true;
  }


  @Override
  public FinalizationCodePart getFinalizationCodePart(String protocolId, Integer voterIndex) {
    Stopwatch stopwatch = Stopwatch.createUnstarted();
    VoteConfirmationAuthorityAlgorithms voteConfirmationAuthorityAlgorithms =
        getVoteConfirmationAuthorityAlgorithms(protocolId);
    stopwatch.start();

    VoterData voterData = state.getVoterDataState().getValue(protocolId, voterIndex)
                               .orElseThrow(() -> new IllegalStateException(
                                   String.format("No voter data found for voter %d", voterIndex)));

    FinalizationCodePart finalization =
        voteConfirmationAuthorityAlgorithms.getFinalization(voterIndex,
                                                            voterData.getRandomPoints(),
                                                            state.getBallotEntriesState().getBallotList(protocolId));
    stopwatch.stop();
    finalizationComputationTimes.add(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    return finalization;
  }

  private boolean needToCheckConfirmation(String protocolId, int voterIndex, Confirmation confirmation) {
    Optional<ConfirmationEntry> confirmationEntry =
        state.getConfirmationEntriesState().getConfirmationList(protocolId).getConfirmationEntry(voterIndex);
    if (confirmationEntry.isPresent() && !confirmationEntry.get().getGamma().equals(confirmation)) {
      throw new ImmutablePropertyRuntimeException(
          String.format("received a new and different confirmation for voter %d", voterIndex));
    }
    return !confirmationEntry.isPresent();
  }

  private VoteConfirmationAuthorityAlgorithms getVoteConfirmationAuthorityAlgorithms(String protocolId) {
    return new VoteConfirmationAuthorityAlgorithms(getPublicParameters(protocolId),
                                                   getGeneralAlgorithms(protocolId),
                                                   getVoteCastingAuthorityAlgorithms(protocolId), getHash(protocolId));
  }

  private MixingAuthorityAlgorithms getMixingAuthorityAlgorithms(String protocolId) {
    return new MixingAuthorityAlgorithms(
        getPublicParameters(protocolId), getGeneralAlgorithms(protocolId),
        getVoteConfirmationAuthorityAlgorithms(protocolId), randomGenerator);
  }

  @Override
  public ShuffleAndProof startMixing(String protocolId) {
    log.info("Authority {} started mixing", j);
    Collection<BallotEntry> ballotEntries = state.getBallotEntriesState().getMappedValues(protocolId).values();
    List<Encryption> encryptions = getMixingAuthorityAlgorithms(protocolId).getEncryptions(
        ballotEntries, state.getConfirmationEntriesState().getConfirmationList(protocolId),
        getElectionSet(protocolId).getBold_n(), getBoldW(protocolId));
    Preconditions.checkState(encryptions.size() > 1, "Not enough votes cast");
    return mix(protocolId, encryptions);
  }

  private List<Integer> getBoldW(String protocolId) {
    return state.getVoterState().getMappedValues(protocolId).values().stream()
                .sorted(Comparator.comparingInt(Voter::getVoterId))
                .map(Voter::getCountingCircle)
                .map(CountingCircle::getId)
                .collect(Collectors.toList());
  }

  @Override
  public List<BigInteger> getGenerators(String protocolId) {
    return state.getGeneratorsState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("generators must have been set first"));
  }

  @Override
  public Optional<ShuffleAndProof> storeShuffleAndProof(String protocolId, int j, ShuffleAndProof shuffleAndProof) {
    log.info("authority {} storing shuffle and proof from authority {}", this.j, j);
    ShuffleAndProofState shuffleAndProofState = state.getShuffleAndProofState();
    shuffleAndProofState.storeValueIfAbsent(protocolId, j, shuffleAndProof);

    if (j == this.j - 1) {
      final Optional<ShuffleAndProof> shuffleAndProofOptional = shuffleAndProofState.getValue(protocolId, this.j);
      return Optional.of(shuffleAndProofOptional.orElse(mixAgain(protocolId)));
    } else {
      verifyProofIfPossible(protocolId, shuffleAndProofState);
      return Optional.empty();
    }
  }

  private void verifyProofIfPossible(String protocolId, ShuffleAndProofState shuffleAndProofState) {
    Set<Integer> receivedProofsIndices = shuffleAndProofState.getMappedValues(protocolId).keySet();
    Set<Integer> verifiedShufflesIndices = state.getShuffleVerificationsState().getMappedValues(protocolId).keySet();

    Optional<Integer> proofToVerify = receivedProofsIndices.stream()
                                                           // no need to verify one's own proof
                                                           .filter(i -> i != this.j)
                                                           // filter out proofs that have already been verified
                                                           .filter(i -> !verifiedShufflesIndices.contains(i))
                                                           // ensure that we are able to verify the proof
                                                           .filter(i -> i == 0 || receivedProofsIndices.contains(i - 1))
                                                           // take the first index amongst those available
                                                           .findFirst();
    proofToVerify.ifPresent(index -> state.getShuffleVerificationsState()
                                          .storeValueIfAbsent(protocolId, index, verifyProof(protocolId, index)));
  }

  /**
   * Preforms a verification of the proof provided by the control component with the index given
   *
   * @param protocolId the protocol instance id
   * @param index      the index of the control component whose shuffle proof needs to be validated (value in 1 to s)
   */
  private boolean verifyProof(String protocolId, Integer index) {
    log.info("authority {} verifying shuffle proof from authority {}", this.j, index);
    int s = getPublicParameters(protocolId).getS();
    Preconditions.checkElementIndex(index, s);
    List<Encryption> inputEncryptions;

    Stopwatch checkShuffleWatch = Stopwatch.createStarted();
    if (Integer.valueOf(0).equals(index)) {
      // Retrieve the initial encryption list from own state
      Collection<BallotEntry> ballotEntries = state.getBallotEntriesState().getMappedValues(protocolId).values();
      inputEncryptions =
          getMixingAuthorityAlgorithms(protocolId).getEncryptions(
              ballotEntries, state.getConfirmationEntriesState().getConfirmationList(protocolId),
              getElectionSet(protocolId).getBold_n(), getBoldW(protocolId));
    } else {
      // Retrieve the result of the previous shuffle
      inputEncryptions = state.getShuffleAndProofState().getValue(protocolId, index - 1)
                              .orElseThrow(() -> new IllegalStateException(
                                  "A shuffle may not be performed unless the previous one completed"))
                              .getShuffle();
    }

    ShuffleAndProof shuffleAndProof = state.getShuffleAndProofState().getValue(protocolId, index)
                                           .orElseThrow(() -> new IllegalStateException(
                                               "this shouldn't ever happen, the state is checked beforehand"));

    boolean isProofValid = getDecryptionAuthorityAlgorithms(protocolId).checkShuffleProof(
        shuffleAndProof.getProof(), inputEncryptions, shuffleAndProof.getShuffle(), getSystemPublicKey(protocolId));
    checkShuffleWatch.stop();
    perfLog.info("Authority {} : checked shuffle proof for authority {} in {}ms",
                 j, index, checkShuffleWatch.elapsed(TimeUnit.MILLISECONDS));

    return isProofValid;
  }

  private ShuffleAndProof mixAgain(String protocolId) {
    log.info("Authority {} performing additional shuffle", j);
    Optional<ShuffleAndProof> shuffleAndProof = state.getShuffleAndProofState().getValue(protocolId, j - 1);
    Preconditions.checkState(shuffleAndProof.isPresent(),
                             "The previous control component hasn't published its shuffle yet");
    return mix(protocolId, shuffleAndProof.get().getShuffle());
  }

  private ShuffleAndProof mix(String protocolId, List<Encryption> encryptions) {
    Stopwatch shuffleWatch = Stopwatch.createStarted();
    List<BigInteger> generators = getGeneralAlgorithms(protocolId).getGenerators(encryptions.size());
    state.getGeneratorsState().storeValueIfAbsent(protocolId, generators);
    MixingAuthorityAlgorithms mixingAuthorityAlgorithms = getMixingAuthorityAlgorithms(protocolId);
    Shuffle shuffle = mixingAuthorityAlgorithms.genShuffle(encryptions, getSystemPublicKey(protocolId));
    shuffleWatch.stop();
    perfLog.info("Authority {} : shuffled in {}ms", j, shuffleWatch.elapsed(TimeUnit.MILLISECONDS));
    Stopwatch shuffleProofWatch = Stopwatch.createStarted();
    ShuffleProof shuffleProof = mixingAuthorityAlgorithms.genShuffleProof(encryptions, shuffle.getBold_e_prime(),
                                                                          shuffle.getBold_r_prime(), shuffle.getPsy(),
                                                                          getSystemPublicKey(protocolId));
    shuffleProofWatch.stop();
    perfLog.info("Authority {} : generated shuffle proof in {}ms", j,
                 shuffleProofWatch.elapsed(TimeUnit.MILLISECONDS));
    return new ShuffleAndProof(shuffle.getBold_e_prime(), shuffleProof);
  }

  @Override
  public PartialDecryptionsAndProof startPartialDecryption(String protocolId) {
    log.info("Authority {} starting decryption", j);
    ShufflesAndProofs shufflesAndProofs = getShufflesAndProofs(protocolId);
    List<List<Encryption>> shuffles = shufflesAndProofs.getShuffles();
    List<ShuffleProof> proofs = shufflesAndProofs.getShuffleProofs();
    int s = getPublicParameters(protocolId).getS();
    Preconditions.checkState(shuffles.size() == s,
                             "This may only happen during decryption time, once all the shuffles have been entered");
    Preconditions.checkState(proofs.size() == s,
                             "This may only happen during decryption time, once all the shuffles have been entered");
    DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms = getDecryptionAuthorityAlgorithms(protocolId);

    // Retrieve results of previously computed verifications, if present, compute if absent.
    Map<Integer, Boolean> proofVerifications =
        new HashMap<>(state.getShuffleVerificationsState().getMappedValues(protocolId));
    List<Boolean> proofsValidList = IntStream.range(0, s)
                                             .filter(i -> i != j) // no need to check one's own proof
                                             .parallel()
                                             .mapToObj(i -> proofVerifications
                                                 .computeIfAbsent(i, k -> verifyProofAndStoreResult(protocolId, k)))
                                             .collect(Collectors.toList());
    boolean areProofsValid = proofsValidList.size() == s - 1 &&
                             proofsValidList.stream().allMatch(i -> i);
    if (!areProofsValid) {
      throw new InvalidShuffleProofRuntimeException("At least one shuffle proof was invalid");
    }

    BigInteger privateKey = getMyPrivateKey(protocolId).getPrivateKey();
    List<Encryption> finalShuffle = shuffles.get(s - 1);

    Stopwatch decryptionWatch = Stopwatch.createStarted();
    Decryptions partialDecryptions = decryptionAuthorityAlgorithms.getPartialDecryptions(finalShuffle, privateKey);
    decryptionWatch.stop();
    perfLog.info("Authority {} : decrypted in {}ms", j, decryptionWatch.elapsed(TimeUnit.MILLISECONDS));

    BigInteger publicKey = getMyPublicKey(protocolId).getPublicKey();
    Stopwatch decryptionProofWatch = Stopwatch.createStarted();
    DecryptionProof decryptionProof =
        decryptionAuthorityAlgorithms.genDecryptionProof(privateKey, publicKey, finalShuffle, partialDecryptions);
    decryptionProofWatch.stop();
    perfLog.info("Authority {} : decryption proof in {}ms", j,
                 decryptionProofWatch.elapsed(TimeUnit.MILLISECONDS));

    return new PartialDecryptionsAndProof(partialDecryptions, decryptionProof);
  }

  private Boolean verifyProofAndStoreResult(String protocolId, Integer k) {
    boolean proofValid = this.verifyProof(protocolId, k);
    state.getShuffleVerificationsState().storeValueIfAbsent(protocolId, k, proofValid);
    return proofValid;
  }

  private ShufflesAndProofs getShufflesAndProofs(String protocolId) {
    Preconditions.checkState(
        getPublicParameters(protocolId).getS() == state.getShuffleAndProofState().getMappedValues(protocolId).size());
    final Map<Integer, ShuffleAndProof> shuffleAndProofMap =
        state.getShuffleAndProofState().getMappedValues(protocolId);
    List<List<Encryption>> shuffles =
        getValuesSortedByKey(shuffleAndProofMap).stream().map(ShuffleAndProof::getShuffle).collect(Collectors.toList());
    List<ShuffleProof> shuffleProofs =
        getValuesSortedByKey(shuffleAndProofMap).stream().map(ShuffleAndProof::getProof).collect(Collectors.toList());
    return new ShufflesAndProofs(shuffles, shuffleProofs);
  }

  private <K extends Comparable<K>, V> List<V> getValuesSortedByKey(Map<K, V> map) {
    return map.entrySet().stream()
              .sorted(Comparator.comparing(Map.Entry::getKey))
              .map(Map.Entry::getValue).collect(Collectors.toList());
  }

  private DecryptionAuthorityAlgorithms getDecryptionAuthorityAlgorithms(String protocolId) {
    return new DecryptionAuthorityAlgorithms(getPublicParameters(protocolId),
                                             getGeneralAlgorithms(protocolId),
                                             randomGenerator);
  }

  private EncryptionPrivateKey getMyPrivateKey(String protocolId) {
    final EncryptionKeyPair keyPair =
        state.getEncryptionKeyPairState().getValue(protocolId)
             .orElseThrow(() -> new IllegalStateException("Encryption key pair has not yet been published"));
    return keyPair.getPrivateKey();
  }

  @Override
  public LongSummaryStatistics getBallotVerificationStats() {
    return ballotVerificationTimes.stream().mapToLong(Long::valueOf).summaryStatistics();
  }

  @Override
  public LongSummaryStatistics getQueryResponseStats() {
    return queryResponseTimes.stream().mapToLong(Long::valueOf).summaryStatistics();
  }

  @Override
  public LongSummaryStatistics getConfirmationVerificationStats() {
    return confirmationVerificationTimes.stream().mapToLong(Long::valueOf).summaryStatistics();
  }

  @Override
  public LongSummaryStatistics getFinalizationComputationStats() {
    return finalizationComputationTimes.stream().mapToLong(Long::valueOf).summaryStatistics();
  }

  @Override
  public void cleanUp(String protocolId) {
    state.getCleanupCommand().cleanUp(protocolId);
  }
}
