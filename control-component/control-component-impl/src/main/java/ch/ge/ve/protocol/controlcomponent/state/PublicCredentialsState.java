/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.core.model.PublicConfirmationCredentialsList;
import ch.ge.ve.protocol.core.model.PublicIdentificationCredentialsList;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractBatchKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.model.Point;
import com.fasterxml.jackson.core.type.TypeReference;
import java.math.BigInteger;

/**
 * {@code PublicCredentialsState} is responsible to maintain the state of the aggregated public credentials for the
 * control component across its clustered deployment.
 */
public class PublicCredentialsState extends AbstractBatchKeyedPropertyState<Point> {
  private static final String MESSAGE_TEMPLATE =
      "Once the public credentials have been set for voter %d, they can no longer be changed";

  PublicCredentialsState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.PUBLIC_CREDENTIALS_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format(MESSAGE_TEMPLATE, key);
  }

  @Override
  protected TypeReference<Point> getPropertyType() {
    return new TypeReference<Point>() {
    };
  }

  public PublicIdentificationCredentialsList getPublicIdentificationCredentialsList(String protocolId) {
    return new DefaultPublicIdentificationCredentialsList(protocolId);
  }

  public PublicConfirmationCredentialsList getPublicConfirmationCredentialsList(String protocolId) {
    return new DefaultPublicConfirmationCredentialsList(protocolId);
  }

  private class DefaultPublicIdentificationCredentialsList implements PublicIdentificationCredentialsList {
    private final String protocolId;

    private DefaultPublicIdentificationCredentialsList(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public BigInteger get(int voterId) {
      return PublicCredentialsState.this.getValue(protocolId, voterId)
                                        .map(p -> p.x)
                                        .orElseThrow(() -> new IllegalStateException(
                                            String.format("No public credentials store for voter %d", voterId)));
    }
  }

  private class DefaultPublicConfirmationCredentialsList implements PublicConfirmationCredentialsList {
    private final String protocolId;

    private DefaultPublicConfirmationCredentialsList(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public BigInteger get(int voterId) {
      return PublicCredentialsState.this.getValue(protocolId, voterId)
                                        .map(p -> p.y)
                                        .orElseThrow(() -> new IllegalStateException(
                                            String.format("No public credentials sored for voter %d", voterId)));
    }
  }
}
