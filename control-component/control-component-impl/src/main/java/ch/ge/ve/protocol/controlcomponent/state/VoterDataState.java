/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractBatchKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.ImmutableMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * {@code VoterDataState} is responsible for maintaining the state of the voter data for the control component
 * across its clustered deployment
 */
public class VoterDataState extends AbstractBatchKeyedPropertyState<VoterData> {
  public VoterDataState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.VOTER_DATA_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format("Voter data for voter %d may not be updated once set", key);
  }

  @Override
  protected TypeReference<VoterData> getPropertyType() {
    return new TypeReference<VoterData>() {
    };
  }

  /**
   * Retrieve the stored values matching the provided keys
   *
   * @param protocolId the protocol instance id
   * @param keys       the keys relevant in this context
   *
   * @return a map of the found values, by key
   */
  @Override
  public Map<Integer, VoterData> getMappedValues(String protocolId, List<Integer> keys) {
    return ImmutableMap.copyOf(super.getEventLogQuery().getEvents(protocolId, getEventType(), keys)
                                    .stream()
                                    .collect(Collectors.toMap(PersistentEvent::getKey,
                                                              getValueExtractor())));
  }
}
