/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent;

import ch.ge.ve.protocol.controlcomponent.exception.InvalidShuffleProofRuntimeException;
import ch.ge.ve.protocol.controlcomponent.exception.SignEncryptionException;
import ch.ge.ve.protocol.core.exception.BallotNotFoundRuntimeException;
import ch.ge.ve.protocol.core.exception.IncompatibleParametersRuntimeException;
import ch.ge.ve.protocol.core.exception.IncorrectBallotRuntimeException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.PartialDecryptionsAndProof;
import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import java.math.BigInteger;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;

/**
 * This interface defines the control component API.
 */
public interface ControlComponent {
  /**
   * Returns the index of this control component.
   *
   * @return the index of this control component.
   */
  int getIndex();

  /**
   * Stores the given public parameters.
   *
   * @param protocolId       the protocol instance Id.
   * @param publicParameters the public parameters
   */
  void storePublicParameters(String protocolId, PublicParameters publicParameters);

  /**
   * Returns the election's public parameters.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the public parameters.
   *
   * @throws IllegalStateException if the election's public parameters have not yet been published.
   */
  PublicParameters getPublicParameters(String protocolId);

  /**
   * Generates the encryption/decryption keys for this control component instance and returns the public key part.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the generated encryption public key.
   *
   * @throws IllegalStateException if the election's public parameters have not yet been published.
   */
  EncryptionPublicKey generateKeys(String protocolId);

  /**
   * Stores the given encryption (public) key part.
   *
   * @param protocolId the protocol instance Id.
   * @param j          the control component's index whose key is being published.
   * @param publicKey  the encryption (public) key part.
   *
   * @return The system public key if all the public key parts have been published
   *
   * @throws NullPointerException      if {@code publicKey} is {@code null}.
   * @throws IllegalStateException     if the election event's public parameters have not yet been published.
   * @throws IndexOutOfBoundsException if {@code j} is negative or is not less than the number of control components.
   */
  Optional<EncryptionPublicKey> storeKeyPart(String protocolId, int j, EncryptionPublicKey publicKey);

  /**
   * Stores the election officer public key
   *
   * @param protocolId               the protocol instance Id.
   * @param electionOfficerPublicKey the election officer public key
   *
   * @return The system public key if all the public key parts have been published
   *
   * @throws NullPointerException  if {@code electionOfficerPublicKey} is {@code null}
   * @throws IllegalStateException if an election officer public key has already been published
   */
  Optional<EncryptionPublicKey> storeElectionOfficerPublicKey(String protocolId,
                                                              EncryptionPublicKey electionOfficerPublicKey);

  /**
   * Stores the provided electionSet
   *
   * @param protocolId  the protocol instance Id.
   * @param electionSet Election set defining the available elections
   */
  void storeElectionSet(String protocolId, ElectionSetWithPublicKey electionSet);

  /**
   * Generates the voter data for the provided voters, given the previously received electionSet
   *
   * @param protocolId the protocol instance Id.
   * @param voters     the list of voters for whom the voter data should be generated
   *
   * @return the electorate data for this control component instance.
   *
   * @throws IllegalStateException if the election's public parameters and election set have not yet been published.
   */
  List<VoterData> generateVoterData(String protocolId, List<Voter> voters);

  /**
   * Generates the list of primes used for representing the voters' choices before encrypting the ballot.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the list of the primes that may be used to represent the voters' choices
   */
  List<BigInteger> generatePrimes(String protocolId) throws NotEnoughPrimesInGroupException;

  /**
   * Stores the given public credentials part
   *
   * @param protocolId            the protocol instance Id.
   * @param j                     the index of the control component publishing the public credentials part
   * @param publicCredentialsPart the public credentials part
   */
  void storePublicCredentialsPart(String protocolId, int j, Map<Integer, Point> publicCredentialsPart);

  /**
   * Determines whether the control component has received all the required public credentials parts
   *
   * @param protocolId the protocol instance Id
   *
   * @return true if ready to build the public credentials false otherwise
   */
  boolean hasReceivedAllPublicCredentialParts(String protocolId);

  /**
   * Force the reindexing of this control components' event log, in order to improve query performance
   */
  void forceEventLogIndex();

  /**
   * Retrieves the list of the voterIds, already split in batches.
   *
   * @param protocolId the protocol instance Id
   *
   * @return all the voterIds, split in batches
   */
  List<List<Integer>> getVoterIdBatches(String protocolId);

  /**
   * Build the public credentials for the given voterIds
   *
   * @param protocolId the protocol instance Id
   * @param voterIds   the list of voterIds
   */
  void buildPublicCredentials(String protocolId, List<Integer> voterIds);

  /**
   * Determines whether this control component is done building all voter's public credentials
   *
   * @param protocolId the protocol instance Id.
   *
   * @return true if this control component has combined the public credentials for each voter for the given protocolId
   */
  boolean doneBuildingPublicCredentials(String protocolId);

  /**
   * Retrieve the voters' (private) credentials, signed with this control component's signing key, and encrypted for the
   * printing authorities. Grouped by printing authority.
   *
   * @param protocolId the protocol instance Id.
   * @param voterIds   the concerned voters' Id.
   *
   * @return for each printing authority, the signed and encrypted private credentials of the voters they are allowed to
   * print the voter certificates for
   *
   * @throws SignEncryptionException if an error occurs when attempting to signEncrypt the data
   */
  Map<PrintingAuthorityWithPublicKey, byte[]> getSignEncryptedPrivateCredentials(String protocolId,
                                                                                 List<Integer> voterIds)
      throws SignEncryptionException;

  /**
   * Processes a ballot and returns the associated oblivious transfer response.
   *
   * @param protocolId     the protocol instance Id.
   * @param voterIndex     the voter's index.
   * @param ballotAndQuery the voter's ballot and verification codes oblivious transfer query.
   *
   * @return the oblivious transfer response for the verification codes query.
   *
   * @throws NullPointerException                   if one of the arguments is {@code null}.
   * @throws IllegalStateException                  if the public credentials have not been built yet.
   * @throws IncompatibleParametersRuntimeException if there are not enough primes in the encryption group for the
   *                                                number of candidates.
   * @throws IncorrectBallotRuntimeException        if the ballot is deemed invalid.
   */
  ObliviousTransferResponse handleBallot(String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery);

  /**
   * Verifies the validity of a confirmation and store it, required before calling {@link #getFinalizationCodePart}
   *
   * @param protocolId   the protocol instance Id.
   * @param voterIndex   the voter's index.
   * @param confirmation the voter's confirmation data.
   *
   * @return true if the confirmation is correct
   *
   * @throws NullPointerException                  if one of the arguments is {@code null}.
   * @throws IllegalStateException                 if the public credentials have not been built yet.
   * @throws BallotNotFoundRuntimeException        if the ballot being confirmed can't be found.
   */
  boolean storeConfirmationIfValid(String protocolId, Integer voterIndex, Confirmation confirmation);


  /**
   * returns this control component's part of the finalization code.
   *
   * @param protocolId the protocol instance Id.
   * @param voterIndex the voter's index.
   *
   * @return the finalization code part for this control component.
   *
   * @throws NullPointerException  if one of the arguments is {@code null}.
   * @throws IllegalStateException if the public credentials have not been built yet.
   */
  FinalizationCodePart getFinalizationCodePart(String protocolId, Integer voterIndex);

  /**
   * Initiates the cryptographic shuffle process of the votes. This method shuffles the whole votes and returns the
   * result. It is meant to be called only by the control component instance initiating the shuffle, other instances
   * need to sequentially call {@link #storeShuffleAndProof(String, int, ShuffleAndProof)} ()} to keep track of the
   * shuffles.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the result of the shuffle including its validity proof.
   *
   * @throws IllegalStateException if there are not enough votes cast to initiate the shuffle.
   * @see #storeShuffleAndProof(String, int, ShuffleAndProof)
   */
  ShuffleAndProof startMixing(String protocolId);

  /**
   * Retrieves the list of generators used for creating the shuffle proof.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the list of generators used for creating the shuffle proof.
   */
  List<BigInteger> getGenerators(String protocolId);

  /**
   * Stores the provided shuffle and its proof and if this control components turn has come, performs an additional
   * cryptographic shuffle. This method uses the output of a previous shuffle made by another control component as input
   * and returns back the result of its shuffle (incl. proof), if it was performed.
   *
   * @param protocolId the protocol instance Id.
   *
   * @return if a shuffle was performed: the result of the shuffle including its validity proof otherwise {@link
   * Optional#empty()}.
   *
   * @throws IllegalStateException if the previous shuffle have not yet been published.
   */
  Optional<ShuffleAndProof> storeShuffleAndProof(String protocolId, int j, ShuffleAndProof shuffleAndProof);

  /**
   * Partially decrypts the votes after having checked shuffle proofs and returns the result (partially decrypted votes
   * and the associated decryption proof).
   *
   * @param protocolId the protocol instance Id.
   *
   * @return the decrypted votes and the associated decryption proof.
   *
   * @throws IllegalStateException               if not all the control component instance have shuffled the votes.
   * @throws InvalidShuffleProofRuntimeException if the shuffle proof is deemed invalid.
   */
  PartialDecryptionsAndProof startPartialDecryption(String protocolId);

  LongSummaryStatistics getBallotVerificationStats();

  LongSummaryStatistics getQueryResponseStats();

  LongSummaryStatistics getConfirmationVerificationStats();

  LongSummaryStatistics getFinalizationComputationStats();

  /**
   * Clean up all the events generated by the control component.
   *
   * @param protocolId the protocol instance Id.
   */
  void cleanUp(String protocolId);
}
