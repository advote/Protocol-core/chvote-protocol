/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.GeneratorsPublicationEvent;
import ch.ge.ve.event.PartialDecryptionsPublicationEvent;
import ch.ge.ve.event.ShufflePublicationEvent;
import ch.ge.ve.event.ShufflingRequestEvent;
import ch.ge.ve.event.StartPartialDecryptionEvent;
import ch.ge.ve.event.payload.GeneratorsPayload;
import ch.ge.ve.event.payload.PartialDecryptionsPayload;
import ch.ge.ve.event.payload.ShufflePayload;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.core.model.PartialDecryptionsAndProof;
import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.service.AbstractAcknowledgeableSignedEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import ch.ge.ve.service.SignatureService;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Control component shuffling events listener.
 */
@Component
public class ControlComponentShufflingListener extends AbstractAcknowledgeableSignedEventsListener {
  private static final Logger logger = LoggerFactory.getLogger(ControlComponentShufflingListener.class);

  private final int              ccIndex;
  private final ControlComponent controlComponent;

  @Autowired
  public ControlComponentShufflingListener(ControlComponent controlComponent,
                                           EventBus eventBus, SignatureService signatureService) {
    super(eventBus, Endpoints.CONTROL_COMPONENT + "-" + controlComponent.getIndex(), signatureService);
    this.controlComponent = controlComponent;
    this.ccIndex = controlComponent.getIndex();
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.SHUFFLING_REQUESTS + "-controlComponent-${ch.ge.ve.cc.index}",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
                         durable = "true"),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true", value = Channels.SHUFFLING_REQUESTS)
      )
  )
  public void processShufflingRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                           ShufflingRequestEvent event) {
    if (isFirstControlComponent()) {
      logger.info("Received shuffling request, initiating shuffle process...");
      ShuffleAndProof shuffleAndProof = controlComponent.startMixing(protocolId);
      publishShuffle(protocolId, shuffleAndProof);
      publishGenerators(protocolId, controlComponent.getGenerators(protocolId));
      acknowledge(protocolId, event);
    }
  }

  private void publishShuffle(String protocolId, ShuffleAndProof shuffleAndProof) {
    List<String> acknowledgementPartIndexes = getAcknowledgementPartIndexes(protocolId,
                                                                            Endpoints.PROTOCOL_CLIENT,
                                                                            Endpoints.BULLETIN_BOARD,
                                                                            Endpoints.CONTROL_COMPONENT);
    ShufflePayload payload = new ShufflePayload(ccIndex, shuffleAndProof);
    ShufflePublicationEvent event = new ShufflePublicationEvent(endpoint, acknowledgementPartIndexes, payload,
                                                                sign(protocolId, payload), verificationKeyHash);
    eventBus.publish(Channels.SHUFFLE_PUBLICATIONS, protocolId, endpoint, event);
  }

  private void publishGenerators(String protocolId, List<BigInteger> generators) {
    GeneratorsPayload payload = new GeneratorsPayload(ccIndex, generators);
    eventBus.publish(Channels.GENERATORS_PUBLICATIONS, protocolId, endpoint,
                     new GeneratorsPublicationEvent(endpoint, payload, sign(protocolId, payload), verificationKeyHash));
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.SHUFFLE_PUBLICATIONS + "-controlComponent-${ch.ge.ve.cc.index}",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
                         durable = "true"),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true", value = Channels.SHUFFLE_PUBLICATIONS)
      )
  )
  public void processShufflePublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                             ShufflePublicationEvent event) {
    ShufflePayload payload = event.getPayload();
    logger.info("Received shuffle from control component #{}", payload.getControlComponentIndex());
    controlComponent.storeShuffleAndProof(protocolId, payload.getControlComponentIndex(), payload.getShuffleAndProof())
                    .ifPresent(shuffleAndProof -> publishShuffle(protocolId, shuffleAndProof));
    publishGenerators(protocolId, controlComponent.getGenerators(protocolId));
    if (isLastShuffle(protocolId, event)) {
      List<String> acknowledgementPartIndexes = getAcknowledgementPartIndexes(protocolId, Endpoints.CONTROL_COMPONENT);
      eventBus.publish(Channels.START_PARTIAL_DECRYPTIONS, protocolId, endpoint,
                       new StartPartialDecryptionEvent(endpoint, acknowledgementPartIndexes));
    }
    acknowledge(protocolId, event);
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.START_PARTIAL_DECRYPTIONS + "-controlComponent-${ch.ge.ve.cc.index}",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
                         durable = "true"),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                               value = Channels.START_PARTIAL_DECRYPTIONS)
      )
  )
  public void processStartPartialDecryptionEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                 StartPartialDecryptionEvent event) {
    logger.info("Received shuffle from last control component, starting partial decryption");
    PartialDecryptionsAndProof partialDecryptionsAndProof = controlComponent.startPartialDecryption(protocolId);
    PartialDecryptionsPayload payload = new PartialDecryptionsPayload(ccIndex, partialDecryptionsAndProof);
    List<String> acknowledgementPartIndexes = getAcknowledgementPartIndexes(protocolId,
                                                                            Endpoints.PROTOCOL_CLIENT,
                                                                            Endpoints.BULLETIN_BOARD);
    eventBus.publish(Channels.PARTIAL_DECRYPTIONS_PUBLICATIONS, protocolId, endpoint,
                     new PartialDecryptionsPublicationEvent(endpoint, acknowledgementPartIndexes, payload,
                                                            sign(protocolId, payload), verificationKeyHash));
    acknowledge(protocolId, event);
  }

  private boolean isLastShuffle(String protocolId, ShufflePublicationEvent event) {
    return event.getPayload().getControlComponentIndex() == ccIndex && isLastControlComponent(protocolId);
  }

  private boolean isFirstControlComponent() {
    return ccIndex == 0;
  }

  private boolean isLastControlComponent(String protocolId) {
    return ccIndex == getCcCount(protocolId) - 1;
  }

  private int getCcCount(String protocolId) {
    return controlComponent.getPublicParameters(protocolId).getS();
  }

  private List<String> getAcknowledgementPartIndexes(String protocolId, String... endpoints) {
    return Stream.of(endpoints).flatMap(
        e -> Endpoints.CONTROL_COMPONENT.equals(e)
            ? IntStream.range(0, getCcCount(protocolId)).boxed().map(i -> Endpoints.CONTROL_COMPONENT + "-" + i)
            : Stream.of(e)
    ).collect(Collectors.toList());
  }
}
