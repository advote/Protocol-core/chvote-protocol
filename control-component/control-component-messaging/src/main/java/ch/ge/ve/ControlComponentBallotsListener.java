/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.BallotConfirmationPublicationEvent;
import ch.ge.ve.event.BallotPublicationEvent;
import ch.ge.ve.event.ConfirmationFailedEvent;
import ch.ge.ve.event.FinalizationCodePartPublicationEvent;
import ch.ge.ve.event.ObliviousTransferResponseCreationEvent;
import ch.ge.ve.event.payload.ConfirmationFailedPayload;
import ch.ge.ve.event.payload.FinalizationCodePartPayload;
import ch.ge.ve.event.payload.ObliviousTransferResponsePayload;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.Signature;
import ch.ge.ve.service.AbstractAcknowledgeableSignedEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import ch.ge.ve.service.SignatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Control component ballots listener, listens to ballot publication events and fires back oblivious transfer creation
 * events.
 */
@Component
public class ControlComponentBallotsListener extends AbstractAcknowledgeableSignedEventsListener {
  private static final Logger logger = LoggerFactory.getLogger(ControlComponentBallotsListener.class);

  private final ControlComponent controlComponent;
  private final int              ccIndex;

  @Autowired
  public ControlComponentBallotsListener(ControlComponent controlComponent, EventBus eventBus,
                                         SignatureService signatureService) {
    super(eventBus, Endpoints.CONTROL_COMPONENT + "-" + controlComponent.getIndex(), signatureService);
    this.controlComponent = controlComponent;
    this.ccIndex = controlComponent.getIndex();
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.BALLOT_PUBLICATIONS + "-controlComponent-${ch.ge.ve.cc.index}",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
                         durable = "true"),
          exchange = @Exchange(value = Channels.BALLOT_PUBLICATIONS,
                               type = ExchangeTypes.FANOUT, durable = "true")
      )
  )
  public void processBallotPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                            BallotPublicationEvent event) {
    int voterIndex = event.getPayload().getVoterIndex();
    BallotAndQuery ballotAndQuery = event.getPayload().getBallotAndQuery();
    logger.debug("Processing ballot {} (voter #{})", ballotAndQuery, voterIndex);
    ObliviousTransferResponse otResponse = controlComponent.handleBallot(protocolId, voterIndex, ballotAndQuery);
    publishObliviousTransferResponse(protocolId, voterIndex, otResponse);
    acknowledge(protocolId, event);
  }

  private void publishObliviousTransferResponse(String protocolId, int voterIndex, ObliviousTransferResponse otResponse) {
    ObliviousTransferResponsePayload payload = new ObliviousTransferResponsePayload(voterIndex, ccIndex, otResponse);
    Signature signature = sign(protocolId, payload);
    eventBus.publish(Channels.OBLIVIOUS_TRANSFER_RESPONSE_CREATIONS, protocolId, endpoint,
                     new ObliviousTransferResponseCreationEvent(endpoint, payload, signature, verificationKeyHash),
                     Integer.toString(voterIndex));
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.BALLOT_CONFIRMATION_PUBLICATIONS + "-controlComponent-${ch.ge.ve.cc.index}",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
                         durable = "true"),
          exchange = @Exchange(value = Channels.BALLOT_CONFIRMATION_PUBLICATIONS,
                               type = ExchangeTypes.FANOUT, durable = "true")
      )
  )
  public void processBallotConfirmationPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                        BallotConfirmationPublicationEvent event) {
    Integer voterIndex = event.getPayload().getVoterIndex();
    Confirmation confirmation = event.getPayload().getConfirmation();
    logger.debug("Processing ballot confirmation {} (voter #{})", confirmation, voterIndex);
    if (controlComponent.storeConfirmationIfValid(protocolId, voterIndex, confirmation)){
      FinalizationCodePart finalizationCodePart = controlComponent.getFinalizationCodePart(protocolId, voterIndex);
      logger.debug("Finalization code part generated from ballot confirmation {} (voter #{})", confirmation, voterIndex);
      publishFinalizationCodePart(protocolId, voterIndex, finalizationCodePart);
    } else {
      logger.debug("Ballot confirmation failed {} (voter #{})", confirmation, voterIndex);
      publishConfirmationFailure(protocolId, voterIndex);
    }
    acknowledge(protocolId, event);
  }

  private void publishConfirmationFailure(String protocolId, Integer voterIndex) {
    ConfirmationFailedPayload payload = new ConfirmationFailedPayload(voterIndex, ccIndex);
    Signature signature = sign(protocolId, payload);
    eventBus.publish(Channels.CONFIRMATION_FAILED, protocolId, endpoint,
                     new ConfirmationFailedEvent(endpoint, payload, signature, verificationKeyHash),
                     Integer.toString(voterIndex));
  }

  private void publishFinalizationCodePart(String protocolId, int voterIndex, FinalizationCodePart finalizationCodePart) {
    FinalizationCodePartPayload payload = new FinalizationCodePartPayload(voterIndex, ccIndex, finalizationCodePart);
    Signature signature = sign(protocolId, payload);
    eventBus.publish(Channels.FINALIZATION_CODE_PART_PUBLICATIONS, protocolId, endpoint,
                     new FinalizationCodePartPublicationEvent(endpoint, payload, signature, verificationKeyHash),
                     Integer.toString(voterIndex));
  }
}
