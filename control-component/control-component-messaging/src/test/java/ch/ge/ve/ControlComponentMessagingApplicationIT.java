/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.protocol.controlcomponent.state.ControlComponentState;
import ch.ge.ve.protocol.eventlog.state.ImmutablePropertyRuntimeException;
import ch.ge.ve.protocol.eventlog.state.PublicParametersState;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import java.util.UUID;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ControlComponentMessagingApplicationIT {
  private final static Logger log = LoggerFactory.getLogger(ControlComponentMessagingApplicationIT.class);

  @Autowired
  private ControlComponentState controlComponentState;

  private String testProtocolId = UUID.randomUUID().toString();

  @Test
  public void contextLoads() {
    log.info("Context loaded");
  }

  @After
  public void cleanUp() {
    controlComponentState.getCleanupCommand().cleanUp(testProtocolId);
  }

  @Test(expected = ImmutablePropertyRuntimeException.class)
  public void doubleInsertionOfPublicParametersShouldFail() {
    PublicParametersState publicParametersState = controlComponentState.getPublicParametersState();

    // Given: public parameters have already been inserted for a given protocol ID
    PublicParameters publicParameters = PublicParametersFactory.LEVEL_2.createPublicParameters();
    publicParametersState.storeValueIfAbsent(testProtocolId, publicParameters);

    // When: attempting to store a different set of public parameters for the same protocol ID
    PublicParameters otherPublicParameters = PublicParametersFactory.LEVEL_1.createPublicParameters();
    publicParametersState.storeValueIfAbsent(testProtocolId, otherPublicParameters);

    // Then: an exception should be thrown
  }
}
