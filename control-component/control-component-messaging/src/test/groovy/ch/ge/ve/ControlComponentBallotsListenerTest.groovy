/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve

import ch.ge.ve.event.BallotConfirmationPublicationEvent
import ch.ge.ve.event.ConfirmationFailedEvent
import ch.ge.ve.event.FinalizationCodePartPublicationEvent
import ch.ge.ve.event.payload.BallotConfirmationPayload
import ch.ge.ve.protocol.controlcomponent.ControlComponent
import ch.ge.ve.protocol.core.model.FinalizationCodePart
import ch.ge.ve.protocol.model.Confirmation
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Signature
import ch.ge.ve.service.Channels
import ch.ge.ve.service.EventBus
import ch.ge.ve.service.SignatureService
import spock.lang.Specification

class ControlComponentBallotsListenerTest extends Specification {


  def controlComponent = Mock(ControlComponent)
  def eventBus = Mock(EventBus)
  def signatureService = Mock(SignatureService)
  ControlComponentBallotsListener listener;

  void setup() {
    controlComponent.getIndex() >> 1
    signatureService.getVerificationKeyHash() >> "verificationKeyHash"
    signatureService.sign(_, _) >> new Signature(BigInteger.ONE, BigInteger.TEN)
    listener = new ControlComponentBallotsListener(controlComponent, eventBus, signatureService)


  }


  def "ProcessBallotConfirmationPublicationEvent should send finalisation part event when confirmation is correct"() {
    given:
    Confirmation confirmation = new Confirmation(BigInteger.ONE, new NonInteractiveZkp([], []))
    def event = new BallotConfirmationPublicationEvent("emiter", ["test"], new BallotConfirmationPayload(1, confirmation))
    FinalizationCodePart finalizationCodePart = new FinalizationCodePart(null, null)

    controlComponent.storeConfirmationIfValid("pid", 1, confirmation) >> true
    controlComponent.getFinalizationCodePart("pid", 1) >> finalizationCodePart


    when:
    listener.processBallotConfirmationPublicationEvent("pid", event)

    then:
    1 * eventBus.publish(Channels.FINALIZATION_CODE_PART_PUBLICATIONS, "pid", "controlComponent-1", _ as FinalizationCodePartPublicationEvent, "1");
  }


  def "ProcessBallotConfirmationPublicationEvent should send confirmation failure event when confirmation is imcorrect"() {
    given:
    Confirmation confirmation = new Confirmation(BigInteger.ONE, new NonInteractiveZkp([], []))
    def event = new BallotConfirmationPublicationEvent("emitter", ["test"], new BallotConfirmationPayload(1, confirmation))

    controlComponent.storeConfirmationIfValid("pid", 1, confirmation) >> false

    when:
    listener.processBallotConfirmationPublicationEvent("pid", event)

    then:
    0 * controlComponent.getFinalizationCodePart("pid", 1)
    1 * eventBus.publish(Channels.CONFIRMATION_FAILED, "pid", "controlComponent-1", _ as ConfirmationFailedEvent, "1");
  }
}
