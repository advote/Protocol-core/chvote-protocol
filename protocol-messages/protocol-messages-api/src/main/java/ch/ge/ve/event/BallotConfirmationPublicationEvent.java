/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.BallotConfirmationPayload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Event fired on ballot confirmation publication.
 */
public final class BallotConfirmationPublicationEvent extends AbstractAcknowledgeableMulticastEvent<BallotConfirmationPayload> {

  public BallotConfirmationPublicationEvent(String emitter, List<String> acknowledgementPartIndexes,
                                            BallotConfirmationPayload payload) {
    super(emitter, acknowledgementPartIndexes, payload);
  }

  @JsonCreator
  public BallotConfirmationPublicationEvent(@JsonProperty("eventId") String eventId,
                                            @JsonProperty("emitter") String emitter,
                                            @JsonProperty("acknowledgementPartIndexes") List<String> acknowledgementPartIndexes,
                                            @JsonProperty("payload") BallotConfirmationPayload payload) {
    super(eventId, emitter, acknowledgementPartIndexes, payload);
  }
}
