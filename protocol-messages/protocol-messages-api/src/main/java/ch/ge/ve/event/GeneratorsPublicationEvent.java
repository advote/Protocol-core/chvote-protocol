/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.GeneratorsPayload;
import ch.ge.ve.protocol.model.Signature;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Event fired on generators' publication.
 */
public final class GeneratorsPublicationEvent extends AbstractAcknowledgeableSignedEvent<GeneratorsPayload> {

  public GeneratorsPublicationEvent(String emitter, GeneratorsPayload payload,
                                    Signature signature, String verificationKeyHash) {
    super(emitter, payload, signature, verificationKeyHash);
  }

  @JsonCreator
  public GeneratorsPublicationEvent(@JsonProperty("eventId") String eventId,
                                    @JsonProperty("emitter") String emitter,
                                    @JsonProperty("payload") GeneratorsPayload payload,
                                    @JsonProperty("signature") Signature signature,
                                    @JsonProperty("verificationKeyHash") String verificationKeyHash) {
    super(eventId, emitter, payload, signature, verificationKeyHash);
  }
}
