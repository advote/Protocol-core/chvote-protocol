/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

/**
 * An unacknowledged event. An event is considered unacknowledged until the expected acknowledgement event(s) is/are
 * received by the component that sent the {@link AcknowledgeableEvent}. Implementation note: this event is mainly a
 * wrapper around the {@link AcknowledgeableEvent} waiting to be acknowledged; it carries additional information that
 * are required to capture the context in which the event has been published.
 */
public final class UnacknowledgedEvent implements Event {
  private final String               protocolId;
  private final String               emitter;
  private final String               channel;
  private final String               routingKey;
  private final AcknowledgeableEvent event;

  public UnacknowledgedEvent(String protocolId, String emitter, String channel, String routingKey, AcknowledgeableEvent event) {
    this.protocolId = protocolId;
    this.emitter = emitter;
    this.channel = channel;
    this.routingKey = routingKey;
    this.event = event;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public String getEmitter() {
    return emitter;
  }

  public String getChannel() {
    return channel;
  }

  public String getRoutingKey() {
    return routingKey;
  }

  public AcknowledgeableEvent getEvent() {
    return event;
  }
}
