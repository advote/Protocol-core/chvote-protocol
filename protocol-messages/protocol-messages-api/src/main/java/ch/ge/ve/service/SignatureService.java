/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.model.Signature;

/**
 * Events signature and verification service.
 */
public interface SignatureService {

  /**
   * @return the verification key hash corresponding to this instance's signing key
   */
  String getVerificationKeyHash();

  /**
   * Sign a given event
   *
   * @param protocolId the protocol instance's ID
   * @param payload    the payload to sign
   *
   * @return The payload's signature
   *
   * @throws ChannelSecurityException if an error occurred during the signing process
   */
  Signature sign(String protocolId, Payload payload) throws ChannelSecurityException;

  /**
   * Verifies the signature of a given payload
   *
   * @param protocolId          the protocol instance's ID
   * @param signatory           the event's signatory
   * @param verificationKeyHash the hash of the verification key to use
   * @param signature           the signature to verify
   * @param payload             the signed payload
   *
   * @return true if the signature match the emitter, false otherwise
   *
   * @throws ChannelSecurityException if an error occurred during the verification process
   */
  boolean verify(String protocolId, String signatory, String verificationKeyHash, Signature signature, Payload payload)
      throws ChannelSecurityException;
}
