/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator;

/**
 * Thrown when an {@link EventMessage} is deemed invalid.
 */
public final class InvalidEventMessageRuntimeException extends RuntimeException {

  /**
   * Creates a new {@code InvalidEventMessageRuntimeException}.
   *
   * @param message a message describing why the {@code EventMessage} has been deemed invalid.
   */
  public InvalidEventMessageRuntimeException(String message) {
    super(message);
  }

  /**
   * Creates a new {@code InvalidEventMessageRuntimeException}.
   *
   * @param cause the exception that caused this one.
   */
  public InvalidEventMessageRuntimeException(Throwable cause) {
    super(cause);
  }

  /**
   * Creates a new {@code InvalidEventMessageRuntimeException}.
   *
   * @param message a message describing why the {@code EventMessage} has been deemed invalid.
   * @param cause   the exception that caused this one.
   */
  public InvalidEventMessageRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }
}
