/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.VotersPayload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Event fired to multicast voters.
 * <p>
 * If the whole electorate is too large to be published as a single event, it may be broken into several events.
 * The only assumption in this case is that the same voter id is not given to two different voters.
 * </p>
 */
public final class VotersMulticastEvent extends AbstractAcknowledgeableMulticastEvent<VotersPayload> {

  public VotersMulticastEvent(String emitter, List<String> acknowledgementPartIndexes, VotersPayload payload) {
    super(emitter, acknowledgementPartIndexes, payload);
  }

  @JsonCreator
  public VotersMulticastEvent(@JsonProperty("eventId") String eventId,
                              @JsonProperty("emitter") String emitter,
                              @JsonProperty("acknowledgementPartIndexes") List<String> acknowledgementPartIndexes,
                              @JsonProperty("payload") VotersPayload payload) {
    super(eventId, emitter, acknowledgementPartIndexes, payload);
  }
}
