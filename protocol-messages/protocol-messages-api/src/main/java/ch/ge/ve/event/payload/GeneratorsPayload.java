/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.List;

/**
 * Payload transporting the generators used by a control component.
 */
public final class GeneratorsPayload implements ControlComponentIndexedPayload {

  private final int              controlComponentIndex;
  private final List<BigInteger> generators;

  @JsonCreator
  public GeneratorsPayload(@JsonProperty("controlComponentIndex") int controlComponentIndex,
                           @JsonProperty("generators") List<BigInteger> generators) {
    Preconditions.checkNotNull(generators, "generators may not be null");
    this.controlComponentIndex = controlComponentIndex;
    this.generators = generators;
  }

  @Override
  public int getControlComponentIndex() {
    return controlComponentIndex;
  }

  public List<BigInteger> getGenerators() {
    return generators;
  }
}
