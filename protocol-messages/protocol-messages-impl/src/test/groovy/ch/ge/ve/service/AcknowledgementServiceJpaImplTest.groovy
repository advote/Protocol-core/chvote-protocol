/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service

import ch.ge.ve.event.AbstractAcknowledgeableEvent
import ch.ge.ve.event.AbstractAcknowledgeableMulticastEvent
import ch.ge.ve.event.SimpleAcknowledgementEvent
import ch.ge.ve.event.SimpleMulticastAcknowledgementEvent
import ch.ge.ve.event.UnacknowledgedEvent
import ch.ge.ve.event.acknowledgement.entity.AcknowledgementPendingEvent
import ch.ge.ve.event.acknowledgement.repository.AcknowledgementPendingEventJpaRepository
import ch.ge.ve.event.payload.EmptyPayload
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.google.common.collect.ImmutableList
import spock.lang.Shared
import spock.lang.Specification

class AcknowledgementServiceJpaImplTest extends Specification {

  private static final String PROTOCOL_ID = "0"
  private static final String EMITTER = "test"
  private static final String CHANNEL = "test-channel"
  private static final String ROUTING_KEY = ""

  @Shared
  ObjectMapper jackson

  AcknowledgementPendingEventJpaRepository repository

  void setupSpec() {
    jackson = new ObjectMapper()
    jackson.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
  }

  void setup() {
    repository = Mock()
  }

  def "registering a simple unacknowledged event should persist a pending event"() {
    given: "a persistent acknowledgement service and an unacknowledged event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def emitter = "emitter"
    def ackableEvent = new SimpleAcknowledgeableEvent(eventId, emitter)
    def unackedEvent = new UnacknowledgedEvent(PROTOCOL_ID, EMITTER, CHANNEL, ROUTING_KEY, ackableEvent)
    AcknowledgementPendingEvent pendingEvent

    when: "the acknowledgeable event is registered"
    service.registerEvent(unackedEvent)

    then: "the service should call the repository for storing the event"
    1 * repository.save(_ as AcknowledgementPendingEvent) >> {
      AcknowledgementPendingEvent event -> pendingEvent = event
    }
    pendingEvent.protocolId == PROTOCOL_ID
    pendingEvent.routingKey == ROUTING_KEY
    pendingEvent.channel == CHANNEL
    pendingEvent.eventId == eventId
    pendingEvent.emitter == emitter
    pendingEvent.acknowledgementPartIndex == null
  }

  def "registering a multicast unacknowledged event should persist multiple pending events"() {
    given: "a persistent acknowledgement service and an unacknowledged event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def ackableEvent = new SimpleMulticastAcknowledgeableEvent("1", "test", "part1", "part2")
    def unackedEvent = new UnacknowledgedEvent(PROTOCOL_ID, EMITTER, CHANNEL, ROUTING_KEY, ackableEvent)

    when: "the acknowledgeable event is registered"
    service.registerEvent(unackedEvent)

    then: "the service should call the repository for storing the events"
    2 * repository.save(_)
  }

  def "testing acknowledgement status for an acknowledgeable event should count pending events by eventId"() {
    given: "a persistent acknowledgement service and an unacknowledged event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def ackableEvent = new SimpleMulticastAcknowledgeableEvent(eventId, "test", "part1", "part2")

    when: "the acknowledgement status is requested"
    service.isUnacknowledged(ackableEvent)

    then: "the service should call the repository to count the number of pending events"
    1 * repository.countByEventId(eventId)
  }

  def "forgetting a simple acknowledgeable event must delete the corresponding pending event from the repository"() {
    given: "a persistent acknowledgement service and an acknowledgeable event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def emitter = "emitter"
    def ackableEvent = new SimpleAcknowledgeableEvent(eventId, emitter)

    when: "the acknowledgeable event is forgetted"
    service.forget(ackableEvent)

    then: "the service should call the repository for deleting the corresponding pending event"
    1 * repository.deleteByEventIdAndAcknowledgementPartIndex(eventId, null)
  }

  def "forgetting a multicast acknowledgeable event must delete the corresponding pending events from the repository"() {
    given: "a persistent acknowledgement service and an acknowledgeable event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def emitter = "emitter"
    def partIndex1 = "part1"
    def partIndex2 = "part2"
    def ackableEvent = new SimpleMulticastAcknowledgeableEvent(eventId, emitter, partIndex1, partIndex2)

    when: "the acknowledgeable event is forgetted"
    service.forget(ackableEvent)

    then: "the service should call the repository for deleting the corresponding pending events"
    1 * repository.deleteByEventIdAndAcknowledgementPartIndex(eventId, partIndex1)
    1 * repository.deleteByEventIdAndAcknowledgementPartIndex(eventId, partIndex2)
  }

  def "forgetting all unacknowledged events for a protocol Id must delete these events"() {
    given: "a persistent acknowledgement service"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def protocolId = "1"

    when: "the acknowledgeable event is forgetted"
    service.forget(protocolId)

    then: "the service should call the repository for deleting the corresponding pending event"
    1 * repository.deleteByProtocolId(protocolId)
  }

  def "registering a simple acknowledgement event must delete the corresponding pending event from the repository"() {
    given: "a persistent acknowledgement service and an acknowledgement event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def ackEvent = new SimpleAcknowledgementEvent(eventId)

    when: "the acknowledgement event is registered"
    service.registerAcknowledgement(ackEvent)

    then: "the service should call the repository for deleting the corresponding pending event"
    1 * repository.deleteByEventIdAndAcknowledgementPartIndex(eventId, null)
  }

  def "registering a multicast acknowledgement event must delete the corresponding pending event from the repository"() {
    given: "a persistent acknowledgement service and an acknowledgement event"
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    def eventId = "1"
    def partIndex = "partIndex"
    def ackEvent = new SimpleMulticastAcknowledgementEvent(eventId, partIndex)

    when: "the acknowledgement event is registered"
    service.registerAcknowledgement(ackEvent)

    then: "the service should call the repository for deleting the corresponding pending events"
    1 * repository.deleteByEventIdAndAcknowledgementPartIndex(eventId, partIndex)
  }

  def "retrieving all unacknowledged events should return a consolidated list of pending events read from the repository"() {
    given:
    def service = new AcknowledgementServiceJpaImpl(repository, jackson)
    repository.findAll() >> { ImmutableList.of(pendingEvent("1"), pendingEvent("1"), pendingEvent("2")) }

    expect:
    service.getUnacknowledgedEvents().size() == 2
  }

  private AcknowledgementPendingEvent pendingEvent(String eventId) {
    AcknowledgementPendingEvent pendingEvent = new AcknowledgementPendingEvent()
    pendingEvent.setEventId(eventId)
    pendingEvent.setEventType(SimpleAcknowledgeableEvent.class.getName())
    pendingEvent.setEventBody("{\"eventId\": \"" + eventId + "\", \"emitter\": \"emitter\"}")
    return pendingEvent
  }

  private static final class SimpleAcknowledgeableEvent extends AbstractAcknowledgeableEvent {
    @JsonCreator
    SimpleAcknowledgeableEvent(@JsonProperty("eventId") String eventId, @JsonProperty("emitter") String emitter) {
      super(eventId, emitter, new EmptyPayload())
    }
  }

  private static final class SimpleMulticastAcknowledgeableEvent extends AbstractAcknowledgeableMulticastEvent {
    @JsonCreator
    SimpleMulticastAcknowledgeableEvent(
        @JsonProperty("eventId") String eventId, @JsonProperty("emitter") String emitter,
        @JsonProperty("acknowledgementPartIndexes") String... acknowledgementPartIndexes) {
      super(eventId, emitter, ImmutableList.copyOf(acknowledgementPartIndexes), new EmptyPayload())
    }
  }
}
