/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator

import ch.ge.ve.event.Event
import java.nio.charset.Charset
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import spock.lang.Specification

class RabbitMqEventMessageTest extends Specification {

  private static final String JSON = "application/json"
  private static final String UTF_8 = "UTF-8"
  private static final String PAYLOAD = "payload"

  def "getHeader(...) must return the requested header's value"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, ["header": "value"], PAYLOAD))

    expect:
    message.getHeader("h34d3r") == Optional.empty()
    message.getHeader("header") == Optional.of("value")
  }

  def "getRequiredHeader(...) must return the requested header's value"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, ["header": "value"], PAYLOAD))

    expect:
    message.getRequiredHeader("header") == "value"
  }

  def "getRequiredHeader(...) must throw an exception if the requested header is not found"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, ["header": "value"], PAYLOAD))

    expect:
    message.getRequiredHeader("header") == "value"

    when:
    message.getRequiredHeader("missing-header")

    then:
    def e = thrown(InvalidEventMessageRuntimeException)
    e.message.contains("Missing required header")
  }

  def "getEventClass() must return the event's class"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, [:], PAYLOAD))

    expect:
    message.getEventClass() == Event
  }

  def "getEventClass() must throw an exception if the event's class is unknown or invalid"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(typeId, JSON, UTF_8, [:], PAYLOAD))

    when:
    message.getEventClass()

    then:
    def e = thrown(InvalidEventMessageRuntimeException)
    e.message.contains(errorMessage)

    where:
    typeId      | errorMessage
    null        | "Missing type information"
    String.name | "Not an event"
    "unknown"   | "Unknown event class"
  }

  def "getContentType() must return the event's content-type"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, [:], PAYLOAD))

    expect:
    message.getContentType().toString() == JSON
  }

  def "getContentType() must throw an exception if the event's content-type is unknown or invalid"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, contentType, UTF_8, [:], PAYLOAD))

    when:
    message.getContentType()

    then:
    def e = thrown(InvalidEventMessageRuntimeException)
    e.message.contains(errorMessage)

    where:
    contentType | errorMessage
    null        | "Unknown content-type"
    "invalid"   | "Invalid content-type"
  }

  def "getEncoding() must return the event's encoding"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, UTF_8, [:], PAYLOAD))

    expect:
    message.getEncoding() == Optional.of(Charset.forName(UTF_8))
  }

  def "getEncoding() must throw an exception if the event's encoding is unsupported"() {
    given:
    def message = new RabbitMqEventMessage(createMessage(Event.name, JSON, "unsupported-encoding", [:], PAYLOAD))

    when:
    message.getEncoding()

    then:
    def e = thrown(InvalidEventMessageRuntimeException)
    e.message.contains("Unsupported encoding")
  }

  private static Message createMessage(String typeId, String contentType, String encoding,
                                       Map<String, String> headers, String payload) {
    def properties = new MessageProperties()
    properties.setHeader("__TypeId__", typeId)
    properties.setContentType(contentType)
    properties.setContentEncoding(encoding)
    headers.forEach({ k, v -> properties.setHeader(k, v) })
    return new Message(payload.getBytes(), properties)
  }
}
