/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator

import ch.ge.ve.event.Event
import java.util.regex.Pattern
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import spock.lang.Specification

class RegexEventMessageValidatorTest extends Specification {

  def validator = new RegexEventMessageValidator([
      (Event1.class): Pattern.compile("Hi\\s\\w+!"),
      (Event2.class): Pattern.compile("Hello\\s\\w+!"),
  ])

  def "it must accept events matching the expected pattern"() {
    when:
    validator.validate(message)

    then:
    noExceptionThrown()

    where:
    message << [
        createMessage("Hi world!", Event1.class),
        createMessage("Hi there!", Event1.class),
        createMessage("Hello there!", Event2.class),
        createMessage("Hello there!", Event2.class)
    ]
  }

  def "it must reject events not matching the expected pattern"() {
    when:
    validator.validate(message)

    then:
    thrown(InvalidEventMessageRuntimeException)

    where:
    message << [
        createMessage("Hi world", Event1.class),
        createMessage("hi there!", Event1.class),
        createMessage("Hello", Event2.class),
        createMessage("Bye!", Event2.class)
    ]
  }

  def "it must reject unknown events"() {
    when:
    validator.validate(createMessage("Hi!", Event3.class))

    then:
    thrown(InvalidEventMessageRuntimeException)
  }

  private static EventMessage createMessage(String body, Class<?> type = Event.class,
                                            String contentType = "application/json") {
    def encoding = "UTF-8"
    def properties = new MessageProperties()
    properties.contentEncoding = encoding
    properties.contentType = contentType
    properties.setHeader("__TypeId__", type.name)
    return new RabbitMqEventMessage(new Message(body.getBytes(encoding), properties))
  }

  private static final class Event1 implements Event {}

  private static final class Event2 implements Event {}

  private static final class Event3 implements Event {}
}
