/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.UnacknowledgedEvent;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * In-memory implementation of {@link AcknowledgementService}.
 */
public final class AcknowledgementServiceInMemoryImpl implements AcknowledgementService {

  private final ConcurrentMap<EventKey, UnacknowledgedEvent> pendingEvents;

  public AcknowledgementServiceInMemoryImpl() {
    this.pendingEvents = new ConcurrentHashMap<>();
  }

  @Override
  public void registerEvent(UnacknowledgedEvent event) {
    eventKeys(event.getEvent()).forEach(k -> pendingEvents.putIfAbsent(k, event));
  }

  @Override
  public boolean isUnacknowledged(AcknowledgeableEvent event) {
    return pendingEvents.keySet().stream().anyMatch(k -> k.eventId.equals(event.getEventId()));
  }

  @Override
  public void forget(AcknowledgeableEvent event) {
    eventKeys(event).forEach(k -> pendingEvents.keySet().remove(k));
  }

  @Override
  public void forget(String protocolId) {
    Set<EventKey> toRemove = pendingEvents.values().stream()
                                          .filter(event -> event.getProtocolId().equals(protocolId))
                                          .flatMap(event -> eventKeys(event.getEvent()))
                                          .collect(Collectors.toSet());
    pendingEvents.keySet().removeAll(toRemove);
  }

  @Override
  public void registerAcknowledgement(AcknowledgementEvent acknowledgement) {
    String partIndex = AcknowledgementEvents.getAcknowledgementPartIndex(acknowledgement).orElse(null);
    pendingEvents.keySet().remove(new EventKey(acknowledgement.getAcknowledgedEventId(), partIndex));
  }

  @Override
  public List<UnacknowledgedEvent> getUnacknowledgedEvents() {
    return pendingEvents.values().stream().distinct().collect(Collectors.toList());
  }

  private Stream<EventKey> eventKeys(AcknowledgeableEvent event) {
    return AcknowledgeableEvents.getAcknowledgementPartIndexes(event)
                                .map(partIndex -> new EventKey(event.getEventId(), partIndex));
  }

  private static final class EventKey {
    private final String eventId;
    private final String acknowledgementPartIndex;

    EventKey(String eventId, String acknowledgementPartIndex) {
      this.eventId = eventId;
      this.acknowledgementPartIndex = acknowledgementPartIndex;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      EventKey k = (EventKey) o;
      return Objects.equals(acknowledgementPartIndex, k.acknowledgementPartIndex) && Objects.equals(eventId, k.eventId);
    }

    @Override
    public int hashCode() {
      return Objects.hash(eventId);
    }
  }
}
