/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.acknowledgement.repository;

import ch.ge.ve.event.acknowledgement.entity.AcknowledgementPendingEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcknowledgementPendingEventJpaRepository extends JpaRepository<AcknowledgementPendingEvent, Long> {
  /**
   * Returns the number of pending acknowledgement for the given event ID.
   *
   * @param eventId the event ID.
   *
   * @return the number of pending acknowledgement for the given event ID.
   *
   * @throws org.springframework.dao.DataAccessException in case of data access error.
   */
  long countByEventId(String eventId);

  /**
   * Deletes the pending event matching the given event ID and part index.
   *
   * @param eventId                  the event ID.
   * @param acknowledgementPartIndex the acknowledgementPartIndex, may be {@code null}.
   *
   * @throws org.springframework.dao.DataAccessException in case of data access error.
   */
  void deleteByEventIdAndAcknowledgementPartIndex(String eventId, String acknowledgementPartIndex);

  /**
   * Deletes the pending event matching the given protocol ID.
   *
   * @param protocolId the protocol ID.
   *
   * @throws org.springframework.dao.DataAccessException in case of data access error.
   */
  void deleteByProtocolId(String protocolId);
}
