/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.exception.ProtocolRuntimeException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.SecurityParameters;
import ch.ge.ve.protocol.model.Signature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigInteger;
import java.util.Base64;
import java.util.Map;
import java.util.function.Function;

/**
 * Signature service implementation.
 */
public class SignatureServiceImpl implements SignatureService {

  private final IdentificationPrivateKey             signingKey;
  private final String                               verificationKeyHash;
  private final Map<String, Map<String, BigInteger>> verificationKeys;
  private final AlgorithmsSpec                       algorithmsSpec;
  private final RandomGenerator                      randomGenerator;
  private final ObjectMapper                         objectMapper;
  private final Function<String, PublicParameters>   publicParametersSupplier;

  public SignatureServiceImpl(String signatory,
                              IdentificationPrivateKey signingKey,
                              Map<String, Map<String, BigInteger>> verificationKeys,
                              AlgorithmsSpec algorithmsSpec,
                              RandomGenerator randomGenerator,
                              ObjectMapper objectMapper,
                              Function<String, PublicParameters> publicParametersSupplier) {
    this.verificationKeys = verificationKeys;
    this.algorithmsSpec = algorithmsSpec;
    this.randomGenerator = randomGenerator;
    this.objectMapper = objectMapper;
    this.publicParametersSupplier = publicParametersSupplier;

    // signature key initialization
    Hash hash = new Hash(algorithmsSpec.getDigestAlgorithm(), algorithmsSpec.getDigestProvider(), 32);
    KeyEstablishmentAlgorithms keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator);
    this.signingKey = signingKey;
    this.verificationKeyHash = Base64.getEncoder().encodeToString(hash.hash_L(
        keyEstablishmentAlgorithms.computePublickey(
            signingKey.getPrivateKey(),
            (IdentificationGroup) signingKey.getCyclicGroup()
        )
    ));

    // verify that this key is present in the list of verification keys
    if (!verificationKeys.containsKey(signatory)) {
      throw new ProtocolRuntimeException(String.format("No verification key found for signatory %s", signatory));
    }
    if (!this.verificationKeys.get(signatory).containsKey(this.verificationKeyHash)) {
      throw new ProtocolRuntimeException(
          String.format("No matching verification key found for signatory %s signing key", signatory));
    }
  }

  @Override
  public String getVerificationKeyHash() {
    return verificationKeyHash;
  }

  @Override
  public Signature sign(String protocolId, Payload payload) throws ChannelSecurityException {
    return getChannelSecurityAlgorithms(protocolId).genSignature(signingKey.getPrivateKey(), marshall(payload));
  }

  @Override
  public boolean verify(String protocolId, String signatory, String verificationKeyHash,
                        Signature signature, Payload payload) throws ChannelSecurityException {
    if (!verificationKeys.containsKey(signatory)) {
      throw new ChannelSecurityException(String.format("No verification key found for signatory %s", signatory));
    }
    Map<String, BigInteger> keys = verificationKeys.get(signatory);
    if (!keys.containsKey(verificationKeyHash)) {
      throw new ChannelSecurityException(String.format("Key hash %s not found in the list of %s verification keys",
                                                       verificationKeyHash, signatory));
    }
    BigInteger publicKey = keys.get(verificationKeyHash);
    return getChannelSecurityAlgorithms(protocolId).verifySignature(publicKey, signature, marshall(payload));
  }

  private ChannelSecurityAlgorithms getChannelSecurityAlgorithms(String protocolId) throws ChannelSecurityException {
    return new ChannelSecurityAlgorithms(algorithmsSpec,
                                         getHash(protocolId),
                                         publicParametersSupplier.apply(protocolId).getIdentificationGroup(),
                                         randomGenerator);
  }

  private Hash getHash(String protocolId) {
    SecurityParameters securityParameters = publicParametersSupplier.apply(protocolId).getSecurityParameters();
    return new Hash(algorithmsSpec.getDigestAlgorithm(),
                    algorithmsSpec.getDigestProvider(),
                    securityParameters.getUpper_l());
  }

  private String marshall(Payload payload) throws ChannelSecurityException {
    try {
      return objectMapper.writeValueAsString(payload);
    } catch (JsonProcessingException e) {
      throw new ChannelSecurityException("Unserializable payload", e);
    }
  }
}
