/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.UnacknowledgedEvent;
import ch.ge.ve.event.acknowledgement.entity.AcknowledgementPendingEvent;
import ch.ge.ve.event.acknowledgement.repository.AcknowledgementPendingEventJpaRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Persistent acknowledgement service implementation.
 */
public class AcknowledgementServiceJpaImpl implements AcknowledgementService {

  private final AcknowledgementPendingEventJpaRepository repository;
  private final ObjectMapper                             jackson;

  public AcknowledgementServiceJpaImpl(AcknowledgementPendingEventJpaRepository repository, ObjectMapper jackson) {
    this.repository = repository;
    this.jackson = jackson;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void registerEvent(UnacknowledgedEvent event) {
    AcknowledgeableEvent acknowledgeableEvent = event.getEvent();
    if (!isRegistered(acknowledgeableEvent)) {
      AcknowledgeableEvents.getAcknowledgementPartIndexes(acknowledgeableEvent)
                           .map(index -> createEntity(acknowledgeableEvent.getEmitter(), event.getProtocolId(),
                                                      event.getChannel(), event.getRoutingKey(),
                                                      acknowledgeableEvent.getEventId(), index, acknowledgeableEvent))
                           .forEach(this::save);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isUnacknowledged(AcknowledgeableEvent event) {
    return isRegistered(event);
  }

  @Override
  @Transactional
  public void forget(AcknowledgeableEvent event) {
    try {
      AcknowledgeableEvents.getAcknowledgementPartIndexes(event).forEach(
          partIndex -> repository.deleteByEventIdAndAcknowledgementPartIndex(event.getEventId(), partIndex));
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  @Override
  @Transactional
  public void forget(String protocolId) {
    try {
      repository.deleteByProtocolId(protocolId);
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  @Override
  @Transactional
  public void registerAcknowledgement(AcknowledgementEvent acknowledgement) {
    try {
      String partIndex = AcknowledgementEvents.getAcknowledgementPartIndex(acknowledgement).orElse(null);
      repository.deleteByEventIdAndAcknowledgementPartIndex(acknowledgement.getAcknowledgedEventId(), partIndex);
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public List<UnacknowledgedEvent> getUnacknowledgedEvents() {
    try {
      return repository.findAll().stream()
                       .collect(Collectors.groupingBy(AcknowledgementPendingEvent::getEventId))
                       .values().stream()
                       .flatMap(events -> events.stream().findFirst().map(Stream::of).orElse(Stream.empty()))
                       .map(this::toUnacknowledgedEvent)
                       .collect(Collectors.toList());
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private boolean isRegistered(AcknowledgeableEvent event) {
    try {
      return repository.countByEventId(event.getEventId()) != 0;
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private AcknowledgementPendingEvent createEntity(String emitter, String protocolId, String channel, String routingKey,
                                                   String eventId, String partIndex, AcknowledgeableEvent event) {
    AcknowledgementPendingEvent pendingEvent = new AcknowledgementPendingEvent();
    pendingEvent.setEmitter(emitter);
    pendingEvent.setProtocolId(protocolId);
    pendingEvent.setChannel(channel);
    pendingEvent.setRoutingKey(routingKey);
    pendingEvent.setEventId(eventId);
    pendingEvent.setAcknowledgementPartIndex(partIndex);
    pendingEvent.setEventType(event.getClass().getName());
    pendingEvent.setEventBody(serialize(event));
    return pendingEvent;
  }

  private void save(AcknowledgementPendingEvent entity) {
    try {
      repository.save(entity);
    } catch (DataAccessException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private UnacknowledgedEvent toUnacknowledgedEvent(AcknowledgementPendingEvent entity) {
    return new UnacknowledgedEvent(entity.getProtocolId(), entity.getEmitter(), entity.getChannel(), entity.getRoutingKey(),
                                   deserialize(entity.getEventBody(), entity.getEventType()));
  }

  private String serialize(AcknowledgeableEvent event) {
    try {
      return jackson.writeValueAsString(event);
    } catch (JsonProcessingException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private AcknowledgeableEvent deserialize(String event, String eventClass) {
    try {
      return jackson.readValue(event, readEventClass(eventClass));
    } catch (ClassNotFoundException | IOException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }

  private Class<? extends AcknowledgeableEvent> readEventClass(String eventClass) throws ClassNotFoundException {
    Class<?> cls = Class.forName(eventClass);
    if (!AcknowledgeableEvent.class.isAssignableFrom(cls)) {
      throw new EventProcessingRuntimeException("Not an acknowledgeable event class: " + eventClass);
    }
    return (Class<? extends AcknowledgeableEvent>) cls;
  }
}
