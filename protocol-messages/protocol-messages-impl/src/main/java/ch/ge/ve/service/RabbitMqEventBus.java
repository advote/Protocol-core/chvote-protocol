/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.event.UnacknowledgedEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * Event bus implementation based on RabbitMq.
 */
public final class RabbitMqEventBus implements EventBus {
  private static final Logger   logger           = LoggerFactory.getLogger(RabbitMqEventBus.class);
  private static final String   ROUTING_KEY      = "";
  private static final TimeUnit RETRY_DELAY_UNIT = TimeUnit.MILLISECONDS;

  private final RabbitTemplate           rabbitTemplate;
  private final AcknowledgementService   acknowledgementService;
  private final RetryConfiguration       retryConfiguration;
  private final ScheduledExecutorService executor;

  public RabbitMqEventBus(RabbitTemplate rabbitTemplate, AcknowledgementService acknowledgementService,
                          RetryConfiguration retryConfiguration, ScheduledExecutorService executor) {
    this.rabbitTemplate = rabbitTemplate;
    this.acknowledgementService = acknowledgementService;
    this.retryConfiguration = retryConfiguration;
    this.executor = executor;
  }

  @Override
  public void publish(String channel, String protocolId, String emitter, Event event) {
    publish(channel, protocolId, emitter, event, ROUTING_KEY);
  }

  @Override
  public void publish(String channel, String protocolId, String emitter, Event event, String routingKey) {
    if (event instanceof AcknowledgeableEvent) {
      AcknowledgeableEvent acknowledgeableEvent = (AcknowledgeableEvent) event;
      UnacknowledgedEvent unacknowledgedEvent =
          new UnacknowledgedEvent(protocolId, emitter, channel, routingKey, acknowledgeableEvent);
      acknowledgementService.registerEvent(unacknowledgedEvent);
      sendEvent(protocolId, channel, routingKey, event);
      int retryCount = 0;
      executor.schedule(() -> handleUnacknowledgedEvent(unacknowledgedEvent, retryCount),
                        retryConfiguration.computeDelayToNextRetryAttempt(retryCount), RETRY_DELAY_UNIT);
    } else {
      sendEvent(protocolId, channel, routingKey, event);
    }
  }

  @Override
  public void processAcknowledgement(AcknowledgementEvent event) {
    acknowledgementService.registerAcknowledgement(event);
  }

  @Override
  public List<UnacknowledgedEvent> getUnacknowledgedEvents() {
    return acknowledgementService.getUnacknowledgedEvents();
  }

  @Override
  public void clearUnacknowledgedEvents(String protocolId) {
    acknowledgementService.forget(protocolId);
  }

  private void handleUnacknowledgedEvent(UnacknowledgedEvent unacknowledgedEvent, int retryCount) {
    AcknowledgeableEvent event = unacknowledgedEvent.getEvent();
    if (acknowledgementService.isUnacknowledged(event)) {
      String protocolId = unacknowledgedEvent.getProtocolId();
      if (retryCount < retryConfiguration.getMaxRetryAttempts()) {
        logger.info("Event {} has not been acknowledged... Trying to publish it again", event.getEventId());
        sendEvent(protocolId, unacknowledgedEvent.getChannel(), unacknowledgedEvent.getRoutingKey(), event);
        executor.schedule(() -> handleUnacknowledgedEvent(unacknowledgedEvent, retryCount + 1),
                          retryConfiguration.computeDelayToNextRetryAttempt(retryCount + 1), RETRY_DELAY_UNIT);
      } else {
        logger.warn("All retry attempts failed for event {}... Forwarding it to the DLX", event.getEventId());
        forwardToDlx(unacknowledgedEvent, "All " + retryCount + " retry attempts failed");
      }
    }
  }

  private void forwardToDlx(UnacknowledgedEvent event, String exceptionMessage) {
    final Map<String, Object> dlxHeaders = new HashMap<>();
    dlxHeaders.put(DlxEventHeaders.ORIGINAL_CHANNEL, event.getChannel());
    dlxHeaders.put(DlxEventHeaders.ORIGINAL_ROUTING_KEY, event.getRoutingKey());
    dlxHeaders.put(DlxEventHeaders.EXCEPTION_MESSAGE, exceptionMessage);
    sendEvent(event.getProtocolId(), Channels.DLX, ROUTING_KEY, event.getEvent(), dlxHeaders);
    acknowledgementService.forget(event.getEvent());
  }

  private void sendEvent(String protocolId, String channel, String routingKey, Event event) {
    sendEvent(protocolId, channel, routingKey, event, Collections.emptyMap());
  }

  private void sendEvent(String protocolId, String channel, String routingKey,
                         Event event, Map<String, Object> additionalHeaders) {
    try {
      Map<String, Object> customHeaders = new HashMap<>(additionalHeaders);
      customHeaders.put(EventHeaders.PROTOCOL_ID, protocolId);
      customHeaders.put(EventHeaders.CREATION_DATE, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
      MessagePostProcessor postProcessor = combinePostProcessors(
          customHeadersMessagePostProcessor(customHeaders),
          loggingMessagePostProcessor(protocolId, channel, event)
      );
      rabbitTemplate.convertAndSend(channel, routingKey, event, postProcessor);
    } catch (AmqpException e) {
      logger.warn("Failed to publish event " + event, e);
      throw new EventProcessingRuntimeException(e);
    }
  }

  private MessagePostProcessor customHeadersMessagePostProcessor(Map<String, Object> customHeaders) {
    return message -> {
      message.getMessageProperties().getHeaders().putAll(customHeaders);
      return message;
    };
  }

  private MessagePostProcessor loggingMessagePostProcessor(String protocolId, String channel, Event event) {
    return message -> {
      if (logger.isDebugEnabled()) {
        logger.debug("Publishing event {} with body of size {} KiB to channel {} for protocol instance {}",
                     event, (long) message.getBody().length / 1024, channel, protocolId);
        logger.debug("Event headers: {}", message.getMessageProperties().getHeaders());
      }
      return message;
    };
  }

  private MessagePostProcessor combinePostProcessors(MessagePostProcessor... postProcessors) {
    return message -> {
      Arrays.stream(postProcessors).forEach(postProcessor -> postProcessor.postProcessMessage(message));
      return message;
    };
  }
}
