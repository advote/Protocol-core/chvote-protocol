/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.config;

import ch.ge.ve.event.Event;
import ch.ge.ve.event.SignedEvent;
import ch.ge.ve.event.validator.EventMessageValidator;
import ch.ge.ve.event.validator.InvalidEventMessageRuntimeException;
import ch.ge.ve.event.validator.RegexEventMessageValidator;
import ch.ge.ve.event.validator.SignedEventMessageValidator;
import ch.ge.ve.protocol.core.exception.ProtocolException;
import ch.ge.ve.protocol.core.exception.ProtocolRuntimeException;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.RabbitMqEventBus;
import ch.ge.ve.service.RetryConfiguration;
import ch.ge.ve.service.SignatureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.aopalliance.intercept.MethodInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.amqp.support.converter.Jackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.messaging.handler.invocation.MethodArgumentResolutionException;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RabbitMqConfiguration {
  private static final Logger log = LoggerFactory.getLogger(RabbitMqConfiguration.class);

  /**
   * Fatal listener exceptions. When a listener throws one of these exceptions (or an exception with one of these in its
   * cause chain) the message will be discarded and sent to the Dead Letter Exchange.
   */
  private static final List<Class<? extends Throwable>> FATAL_EXCEPTIONS = ImmutableList.of(
      ProtocolException.class, ProtocolRuntimeException.class, InvalidEventMessageRuntimeException.class,
      ClassCastException.class, MethodArgumentNotValidException.class, MethodArgumentResolutionException.class,
      MessageConversionException.class, org.springframework.messaging.converter.MessageConversionException.class,
      NoSuchMethodException.class);

  @Bean
  public RetryConfiguration retryConfiguration(
      @Value("${ch.ge.ve.event-bus.publisher.retry.attempts:10}") int maxRetryAttempts,
      @Value("${ch.ge.ve.event-bus.publisher.retry.delay.initial-value:30000}") long initialDelayValue,
      @Value("${ch.ge.ve.event-bus.publisher.retry.delay.maximum-value:600000}") long maximumDelayValue,
      @Value("${ch.ge.ve.event-bus.publisher.retry.delay.multiplier:2}") double delayMultiplier) {
    return new RetryConfiguration()
        .withMaxRetryAttempts(maxRetryAttempts)
        .withExponentiallyIncreasingRetryDelay(initialDelayValue, maximumDelayValue, delayMultiplier);
  }

  @Bean
  public EventBus eventBus(RabbitTemplate rabbitTemplate, AcknowledgementService acknowledgementService,
                           RetryConfiguration retryConfiguration) {
    return new RabbitMqEventBus(rabbitTemplate, acknowledgementService, retryConfiguration,
                                Executors.newSingleThreadScheduledExecutor());
  }

  @Bean
  public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
      ObjectMapper mapper,
      ConnectionFactory connectionFactory,
      RetryOperationsInterceptor retryInterceptor,
      SignatureService signatureService,
      @Value("${ch.ge.ve.listeners-concurrency-factor:4}") int concurrencyFactor) {
    int concurrency = concurrencyFactor * Runtime.getRuntime().availableProcessors();
    log.info("concurrency set at {}", concurrency);
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setMessageConverter(messageConverter(mapper));
    MethodInterceptor validationInterceptor =
        new EventMessageValidationListenerInterceptor(regexEventMessageValidator(),
                                                      signedEventMessageValidator(signatureService, mapper));
    factory.setAdviceChain(retryInterceptor, validationInterceptor);
    factory.setConcurrentConsumers(concurrency);
    return factory;
  }

  @Bean
  public MessageConverter messageConverter(ObjectMapper mapper) {
    Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter(mapper);
    converter.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.TYPE_ID);
    return converter;
  }

  @Bean
  public FanoutExchange deadLetterExchange() {
    return new FanoutExchange(Channels.DLX, true, false);
  }

  @Bean
  public RetryOperationsInterceptor retryInterceptor(
      RabbitTemplate rabbitTemplate,
      @Value("${ch.ge.ve.event-bus.listener.retry.attempts:5}") int maxRetryAttempts,
      @Value("${ch.ge.ve.event-bus.listener.retry.delay.initial-value:500}") long initialRetryDelayValue,
      @Value("${ch.ge.ve.event-bus.listener.retry.delay.maximum-value:60000}") long maximumRetryDelayValue,
      @Value("${ch.ge.ve.event-bus.listener.retry.delay.multiplier:2}") double retryDelayMultiplier) {
    Map<Class<? extends Throwable>, Boolean> fatalExceptions =
        FATAL_EXCEPTIONS.stream().collect(Collectors.toMap(Function.identity(), e -> false));
    RetryPolicy retryPolicy = new SimpleRetryPolicy(maxRetryAttempts, fatalExceptions, true, true);
    // after all retry attempts have failed, send the message to the DLX
    RepublishMessageRecoverer recoverer = new LoggingRepublishMessageRecoverer(rabbitTemplate, Channels.DLX);
    ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
    backOffPolicy.setInitialInterval(initialRetryDelayValue);
    backOffPolicy.setMaxInterval(maximumRetryDelayValue);
    backOffPolicy.setMultiplier(retryDelayMultiplier);
    RetryTemplate retryTemplate = new RetryTemplate();
    retryTemplate.setRetryPolicy(retryPolicy);
    retryTemplate.setBackOffPolicy(backOffPolicy);
    return RetryInterceptorBuilder.stateless().recoverer(recoverer).retryOperations(retryTemplate).build();
  }

  private static class LoggingRepublishMessageRecoverer extends RepublishMessageRecoverer {
    LoggingRepublishMessageRecoverer(AmqpTemplate errorTemplate, String errorExchange) {
      super(errorTemplate, errorExchange);
    }

    @Override
    public void recover(Message message, Throwable cause) {
      log.error("Message failed to be processed", cause);
      super.recover(message, cause);
    }
  }

  private EventMessageValidator regexEventMessageValidator() {
    Properties properties = readYamlResource("events-regexes.yaml");
    Map<Class<? extends Event>, Pattern> regexes = properties.stringPropertyNames().stream().collect(
        Collectors.toMap(key -> parseEventClassName(Event.class, key),
                         key -> Pattern.compile(properties.getProperty(key).trim())));
    return new RegexEventMessageValidator(regexes);
  }

  private EventMessageValidator signedEventMessageValidator(SignatureService signatureService, ObjectMapper mapper) {
    Properties properties = readYamlResource("events-signatories.yaml");
    Map<Class<? extends SignedEvent>, String> signatories = properties.stringPropertyNames().stream().collect(
        Collectors.toMap(key -> parseEventClassName(SignedEvent.class, key), key -> properties.getProperty(key).trim()));
    return new SignedEventMessageValidator(signatureService, mapper, signatories);
  }

  private Properties readYamlResource(String name) {
    YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
    yaml.setResources(new ClassPathResource(name));
    return yaml.getObject();
  }

  private <E extends Event> Class<E> parseEventClassName(Class<E> baseClass, String className) {
    try {
      Class<?> eventClass = Class.forName(className.trim());
      if (!baseClass.isAssignableFrom(eventClass)) {
        throw new IllegalStateException("Not an event class: " + className);
      }
      return (Class<E>) eventClass;
    } catch (ClassNotFoundException e) {
      throw new IllegalStateException("Unknown event class: " + className, e);
    }
  }
}
