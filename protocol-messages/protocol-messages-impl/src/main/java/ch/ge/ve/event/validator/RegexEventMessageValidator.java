/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator;

import ch.ge.ve.event.Event;
import com.google.common.collect.ImmutableMap;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * {@code EventMessageValidator} that validates the message's body against a predefined regular expression.
 */
public final class RegexEventMessageValidator implements EventMessageValidator {

  private final Map<Class<? extends Event>, Pattern> eventsRegexes;

  public RegexEventMessageValidator(Map<Class<? extends Event>, Pattern> eventsRegexes) {
    this.eventsRegexes = ImmutableMap.copyOf(eventsRegexes);
  }

  @Override
  public void validate(EventMessage message) {
    Class<? extends Event> eventClass = message.getEventClass();
    Pattern pattern = getMessageBodyPattern(eventClass);
    Charset encoding = message.getEncoding()
                              .orElseThrow(() -> new InvalidEventMessageRuntimeException("Unknown encoding"));
    String serializedEvent = new String(message.getPayload(), encoding);
    if (!pattern.matcher(serializedEvent).matches()) {
      throw new InvalidEventMessageRuntimeException(
          String.format("Message body for event %s doesn't match the expected pattern: %s", eventClass, pattern));
    }
  }

  private Pattern getMessageBodyPattern(Class<? extends Event> eventClass) {
    if (!eventsRegexes.containsKey(eventClass)) {
      throw new InvalidEventMessageRuntimeException("Unknown event class: " + eventClass);
    }
    return eventsRegexes.get(eventClass);
  }
}
