/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.acknowledgement.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing an event that has not been acknowledged yet. Note: the table name is a generic one that is
 * modified at runtime by a {@link org.hibernate.boot.model.naming.PhysicalNamingStrategy} replacing the 'XX' prefix by
 * the component prefix so that the different components using this generic implementation have their own dedicated
 * table for storing their pending events.
 */
@Entity
@Table(name = "XX_T_ACK_PENDING_EVENT",
       indexes = @Index(columnList = "eventId, acknowledgementPartIndex", unique = true)
)
// Note : allocationSize set to 1 as a workaround to a chaotic sequence caching since Hibernate 5.2.17 upgrade
//    .. but performance suffers from that, we should find a more adequate configuration
@SequenceGenerator(name = "unackedEventIdSequence", sequenceName = "XX_unackedEventIdSequence", allocationSize = 1)
public class AcknowledgementPendingEvent implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unackedEventIdSequence")
  private Long id;

  private String protocolId;

  private String emitter;

  private String routingKey;

  private String channel;

  private String eventId;

  private String acknowledgementPartIndex;

  private String eventType;

  @Lob
  private String eventBody;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public String getEmitter() {
    return emitter;
  }

  public void setEmitter(String emitter) {
    this.emitter = emitter;
  }

  public String getRoutingKey() {
    return routingKey;
  }

  public void setRoutingKey(String recipient) {
    this.routingKey = recipient;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getAcknowledgementPartIndex() {
    return acknowledgementPartIndex;
  }

  public void setAcknowledgementPartIndex(String acknowledgementPartIndex) {
    this.acknowledgementPartIndex = acknowledgementPartIndex;
  }

  public String getEventType() {
    return eventType;
  }

  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public String getEventBody() {
    return eventBody;
  }

  public void setEventBody(String eventBody) {
    this.eventBody = eventBody;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AcknowledgementPendingEvent that = (AcknowledgementPendingEvent) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
