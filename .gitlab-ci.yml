#---------------------------------------------------------------------------------------------------
# %L                                                                                               -
# protocol-core                                                                                    -
# %%                                                                                               -
# Copyright (C) 2016 - 2018 République et Canton de Genève                                         -
# %%                                                                                               -
# This program is free software: you can redistribute it and/or modify                             -
# it under the terms of the GNU Affero General Public License as published by                      -
# the Free Software Foundation, either version 3 of the License, or                                -
# (at your option) any later version.                                                              -
#                                                                                                  -
# This program is distributed in the hope that it will be useful,                                  -
# but WITHOUT ANY WARRANTY; without even the implied warranty of                                   -
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                     -
# GNU General Public License for more details.                                                     -
#                                                                                                  -
# You should have received a copy of the GNU Affero General Public License                         -
# along with this program. If not, see <http://www.gnu.org/licenses/>.                             -
# L%                                                                                               -
#---------------------------------------------------------------------------------------------------

services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay
  MAVEN_CLI_OPTS: "-Dmaven.repo.local=.m2/repository --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true --settings=./settings.xml"
  # Integration tests maven CLI options to connect to the infrastructure services
  IT_TESTS_MVN_CLI_OPTS: "-Dspring.profiles.active=system_tests"
  # RabbitMQ variables
  MB_USERNAME: "rabbitmq"
  MB_PASSWORD: "p4ssW0rd"
  # Docker publish variables
  REGISTRY_BASE: registry.gitlab.com/chvote2
  REGISTRY_INFRA: registry.gitlab.com/chvote2/infra

cache:
  key: $CI_COMMIT_REF_NAME
  paths:
    - .m2/repository/

stages:
  - build
  - deploy
  - publish

build:artifacts:
  image: $IMAGE_MVN_JDK8
  stage: build
  script:
    - CC_INDEX=0 mvn $MAVEN_CLI_OPTS $IT_TESTS_MVN_CLI_OPTS clean -pl '!protocol-docs' $SONAR_PRE_BUILD_CMD verify $SONAR_POST_BUILD_CMD
  except:
    - tags

build:deploy-artifacts:
  image: $IMAGE_MVN_JDK8
  stage: build
  script:
    - CC_INDEX=0 mvn $MAVEN_CLI_OPTS $IT_TESTS_MVN_CLI_OPTS clean -pl '!protocol-docs' $SONAR_PRE_BUILD_CMD deploy $SONAR_POST_BUILD_CMD
  only:
    - tags

build:system-tests:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_DOCKER_MVN_JDK8
  stage: build
  script:
    - MB_HOST=docker mvn $MAVEN_CLI_OPTS -P system-tests -pl '!protocol-docs' clean verify
    - docker tag chvote/bulletin-board $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$IMAGE_TAG
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$IMAGE_TAG
    - docker tag chvote/control-component $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$IMAGE_TAG
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$IMAGE_TAG
    - docker tag chvote/eventlog-database $REGISTRY_BASE/protocol-core/chvote-protocol/eventlog-database:$IMAGE_TAG
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/eventlog-database:$IMAGE_TAG
    - docker tag chvote/protocol-runner $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$IMAGE_TAG
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$IMAGE_TAG
  artifacts:
    when: always
    paths:
      - system-tests/build/docker-logs/*
    expire_in: 1 week

build:docs:
  image: $IMAGE_BUILD_DOCS
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS -f protocol-docs/pom.xml clean package
  artifacts:
    paths:
      - protocol-docs/target/generated-docs/pdf/*.pdf

deploy:deploy-to-int1:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  script:
    - cd deploy
    - echo "$GOOGLE_KEY" > key.json
    - gcloud auth activate-service-account --key-file key.json
    - gcloud config set compute/zone $K8S_COMPUTE_ZONE
    - gcloud config set project $K8S_PROJECT
    - gcloud config set container/use_client_certificate True
    - gcloud container clusters get-credentials $GCE_CLUSTER_INT1
    - kubectl delete secret registry.gitlab.com --ignore-not-found
    - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
    - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" protocol-core-deployment.yml
    - sed -i 's#CLAIM_NAME#int1-pv-claim#g' protocol-core-deployment.yml
    - kubectl apply -f protocol-core-deployment.yml -f protocol-core-service.yml
    # force image update && configuration change
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=0 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=1 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1
    - kubectl scale --replicas=0 deployment protocol-core-protocol-runner
    - kubectl scale --replicas=1 deployment protocol-core-protocol-runner
  environment:
    name: int1
    url: http://$HOSTNAME_INT1/health/bulletinboard
  only:
    variables:
      - $ORCHESTRATOR == "GCE"
    refs:
      - master

deploy:deploy-to-rec1:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  script:
    - cd deploy
    - echo "$GOOGLE_KEY" > key.json
    - gcloud auth activate-service-account --key-file key.json
    - gcloud config set compute/zone $K8S_COMPUTE_ZONE
    - gcloud config set project $K8S_PROJECT
    - gcloud config set container/use_client_certificate True
    - gcloud container clusters get-credentials $GCE_CLUSTER_REC1
    - kubectl delete secret registry.gitlab.com --ignore-not-found
    - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
    - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" protocol-core-deployment.yml
    - sed -i 's#CLAIM_NAME#rec1-pv-claim#g' protocol-core-deployment.yml
    - kubectl apply -f protocol-core-deployment.yml -f protocol-core-service.yml
    # force image update && configuration change
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=0 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=1 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1
    - kubectl scale --replicas=0 deployment protocol-core-protocol-runner
    - kubectl scale --replicas=1 deployment protocol-core-protocol-runner
  environment:
    name: rec1
    url: http://$HOSTNAME_REC1/health/bulletinboardct
  only:
    variables:
      - $ORCHESTRATOR == "GCE"
    refs:
      - master
  when: manual

deploy:deploy-to-rec2:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_GCE_SDK_KUBECTL
  stage: deploy
  script:
    - cd deploy
    - echo "$GOOGLE_KEY" > key.json
    - gcloud auth activate-service-account --key-file key.json
    - gcloud config set compute/zone $K8S_COMPUTE_ZONE
    - gcloud config set project $K8S_PROJECT
    - gcloud config set container/use_client_certificate True
    - gcloud container clusters get-credentials $GCE_CLUSTER_REC2
    - kubectl delete secret registry.gitlab.com --ignore-not-found
    - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$REGISTRY_USER --docker-password=$REGISTRY_PASSWD --docker-email=$REGISTRY_EMAIL
    - sed -i "s#IMAGE_TAG#$IMAGE_TAG#g" protocol-core-deployment.yml
    - sed -i 's#CLAIM_NAME#rec2-pv-claim#g' protocol-core-deployment.yml
    - kubectl apply -f protocol-core-deployment.yml -f protocol-core-service.yml
    # force image update && configuration change
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0-database
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1-database
    - kubectl scale --replicas=0 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=1 deployment protocol-core-rabbitmq
    - kubectl scale --replicas=0 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=1 deployment protocol-core-bulletin-board
    - kubectl scale --replicas=0 deployment protocol-core-control-component-0
    - kubectl scale --replicas=1 deployment protocol-core-control-component-0
    - kubectl scale --replicas=0 deployment protocol-core-control-component-1
    - kubectl scale --replicas=1 deployment protocol-core-control-component-1
    - kubectl scale --replicas=0 deployment protocol-core-protocol-runner
    - kubectl scale --replicas=1 deployment protocol-core-protocol-runner
  environment:
    name: rec2
    url: http://$HOSTNAME_REC2/health/bulletinboardct
  only:
    variables:
      - $ORCHESTRATOR == "GCE"
    refs:
      - master
  when: manual

publish:docker-images:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - export IMAGE_TAG=$(if [[ $CI_COMMIT_REF_NAME == "master" ]]; then echo $CI_COMMIT_SHA; else echo $CI_COMMIT_REF_NAME; fi)
  image: $IMAGE_DOCKER_MVN_JDK8
  stage: publish
  script:
    - PROJECT_VERSION=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec)
    - docker pull $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$IMAGE_TAG
    - docker tag $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$IMAGE_TAG $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$PROJECT_VERSION
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/bulletin-board:$PROJECT_VERSION
    - docker pull $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$IMAGE_TAG
    - docker tag $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$IMAGE_TAG $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$PROJECT_VERSION
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/control-component:$PROJECT_VERSION
    - docker pull $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$IMAGE_TAG
    - docker tag $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$IMAGE_TAG $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$PROJECT_VERSION
    - docker push $REGISTRY_BASE/protocol-core/chvote-protocol/protocol-runner:$PROJECT_VERSION
  only:
    - tags
