/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.ProtocolClientImpl;
import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import ch.ge.ve.protocol.client.progress.LoggingProgressTracker;
import ch.ge.ve.protocol.client.progress.LoggingProgressTrackerByCC;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriter;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriterToOutputStream;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Component that drives a {@link ProtocolClientImpl} instance through the main steps of an operation.
 * <p>
 *   <b>Important note - UNSTABLE</b> : this is currently under development, the API could be abruptly broken
 *   without notice, within the SNAPSHOT increments of the very same version of the library.
 * </p>
 */
public class ProtocolDriver {

  private final ObjectMapper   objectMapper;
  private final ProtocolClient protocolClient;
  private final int            votersBatchSize;

  /**
   * Package-private constructor - instances are meant to be obtained from {@link ProtocolDriverFactory}.
   */
  ProtocolDriver(ObjectMapper objectMapper, ProtocolClient protocolClient, int votersBatchSize) {
    this.objectMapper = objectMapper;
    this.protocolClient = protocolClient;
    this.votersBatchSize = votersBatchSize;
  }

  public String getProtocolId() {
    return protocolClient.getProtocolId();
  }

  /**
   * Initializes the Protocol system, by sending the initial inputs to it.
   *
   * @param publicParameters the public parameters
   * @param electionSet the election set with the printing authorities' public keys
   * @param electionOfficerPublicKey public key from the election officer
   */
  public void initializeProtocol(PublicParameters publicParameters, ElectionSetWithPublicKey electionSet,
                                 EncryptionPublicKey electionOfficerPublicKey) {
    protocolClient.sendPublicParameters(publicParameters);
    protocolClient.ensurePublicParametersForwardedToCCs(new LoggingProgressTracker("sendPublicParameters"));

    protocolClient.requestKeyGeneration();
    protocolClient.ensurePublicKeyPartsPublishedToBB(new LoggingProgressTracker("publishPublicKeyParts"));
    protocolClient.ensurePublicKeyPartsForwardedToCCs(new LoggingProgressTracker("storePublicKeyParts"));

    protocolClient.sendElectionSet(electionSet);
    protocolClient.ensureElectionSetForwardedToCCs(new LoggingProgressTracker("sendElectionSet"));
    protocolClient.ensurePrimesSentToBB(new LoggingProgressTracker("publishPrimes"));

    protocolClient.sendElectionOfficerPublicKey(electionOfficerPublicKey);
    protocolClient.ensureElectionOfficerPublicKeyForwardedToCCs(
        new LoggingProgressTracker("sendElectionOfficerPublicKey"));

    protocolClient.ensureControlComponentsReady(new LoggingProgressTracker("initializeControlComponents"));

    protocolClient.createHash();
    protocolClient.createGeneralAlgorithms();
    protocolClient.createChannelSecurityAlgorithms();
  }

  /**
   * Sends the voters list to the protocol.
   *
   * @param voters the eligible voters
   * @param credentialsOutputFolder path to where the private credentials files (epf) should be written
   * @return list of all created private credentials (epf) files
   */
  public List<Path> submitVoters(List<Voter> voters, Path credentialsOutputFolder) {
    List<Path> createdFiles = new ArrayList<>();

    protocolClient.sendVoters(voters, votersBatchSize);
    protocolClient.ensureVotersForwardedToCCs(new LoggingProgressTracker("sendVoters"));
    protocolClient.ensurePublicCredentialsBuiltForAllCCs(new LoggingProgressTracker("buildPublicCredentials"));
    protocolClient.requestPrivateCredentials(credentialsOutputFolder,
                                             createdFiles::add,
                                             new LoggingProgressTrackerByCC("requestPrivateCredentials"));
    protocolClient.ensurePrivateCredentialsPublished();

    return createdFiles;
  }

  /**
   * Decrypt the private credentials and write down the voting cards.
   *
   * @param credentialsOutputFolder where to find the private credentials and write the voting cards
   * @param printingAuthoritiesKeys the private keys of the printing authorities
   */
  public void readVotersPrivateCredentials(Path credentialsOutputFolder,
                                           Map<String, IdentificationPrivateKey> printingAuthoritiesKeys) {
    printingAuthoritiesKeys.forEach(protocolClient::registerPrintingAuthorityPrivateKey);
    protocolClient.decryptPrinterFiles(credentialsOutputFolder);
  }

  /**
   * Generates votes from some of the voters and send them to the protocol
   *
   * @param votesCount number of votes to cast - can not exceed the number of configured voters
   * @param credentialsOutputFolder where to find the voting cards
   * @return a summary of what happened
   */
  public GeneratedVotesData castVotes(int votesCount, Path credentialsOutputFolder) {
    final Map<Integer, VotePerformanceStats> statsMap = protocolClient.castVotes(votesCount, credentialsOutputFolder);
    final Tally expectedTally = protocolClient.getExpectedTally();
    return new GeneratedVotesData(statsMap, expectedTally);
  }

  /**
   * Request the shuffling and decryption from the protocol (control components)
   */
  public void shuffleAndGeneratePartialDecryptions() {
    protocolClient.requestShufflingAndPartialDecryption();
    protocolClient.ensureShufflesPublished(new LoggingProgressTracker("publishShuffle"));
    protocolClient.ensurePartialDecryptionsPublished(new LoggingProgressTracker("publishPartialDecryption"));
  }

  /**
   * Finish the decryption with the election-officer key then compare the obtained tally with the expected one,
   * failing if they are not the same.
   *
   * @param electionOfficerPrivateKey private key to finish decrypt the electronic ballot box
   */
  public void verifyTally(EncryptionPrivateKey electionOfficerPrivateKey) {
    boolean tallyIsOK = protocolClient.checkTally(electionOfficerPrivateKey);
    if (!tallyIsOK) {
      throw new IllegalStateException("Tally verification failed!");
    }
  }

  /**
   * Writes down the audit log file.
   *
   * @param auditLogFile the file where to write in
   * @param simulation if simulation ..?
   *
   * @throws VerificationDataWriterException if the writing of the verification data fails
   */
  public void writeAuditLog(Path auditLogFile, boolean simulation) throws VerificationDataWriterException {
    try (OutputStream outputStream = Files.newOutputStream(auditLogFile, StandardOpenOption.CREATE)) {
      ElectionVerificationDataWriter verificationDataWriter =
          new ElectionVerificationDataWriterToOutputStream(objectMapper, outputStream, simulation);
      protocolClient.writeProtocolAuditLog(verificationDataWriter);
    } catch (IOException e) {
      throw new VerificationDataWriterException(e);
    }
  }

  /**
   * Writes down the expected tally if it is available
   *
   * @param expectedTallyFile the file where to write in
   * @throws IOException if an I/O error occurs
   */
  public void writeExpectedTallyIfAvailable(Path expectedTallyFile) throws IOException {
    Tally expectedTally = protocolClient.getExpectedTally();
    if (expectedTally != null) {
      try (OutputStream out = Files.newOutputStream(expectedTallyFile)) {
        objectMapper.writeValue(out, expectedTally);
      }
    }
  }

}
