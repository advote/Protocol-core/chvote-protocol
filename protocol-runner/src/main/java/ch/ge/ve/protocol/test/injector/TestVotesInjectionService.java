/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.injector;

import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.test.GeneratedVotesData;
import ch.ge.ve.protocol.test.ProtocolDriver;
import ch.ge.ve.protocol.test.ProtocolDriverFactory;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

/**
 * A service to inject test votes in an existing protocol operation.
 */
public class TestVotesInjectionService {

  private final ProtocolDriverFactory    protocolDriverFactory;
  private final IdentificationPrivateKey bohVirtualPrinterPrivateKey;
  private final SharedFolderManager      sharedFolderManager;
  private final int                      securityLevel;
  private final int                      numberOfControlComponents;

  public TestVotesInjectionService(ProtocolDriverFactory protocolDriverFactory,
                                   IdentificationPrivateKey bohVirtualPrinterPrivateKey,
                                   SharedFolderManager sharedFolderManager,
                                   int securityLevel,
                                   int numberOfControlComponents) {
    this.protocolDriverFactory = protocolDriverFactory;
    this.bohVirtualPrinterPrivateKey = bohVirtualPrinterPrivateKey;
    this.sharedFolderManager = sharedFolderManager;
    this.securityLevel = securityLevel;
    this.numberOfControlComponents = numberOfControlComponents;
  }

  /**
   * Casts some votes - the candidates will be randomly chosen in the electionSet.
   * <p>
   *   Note : This only supports votes injection for voters managed by the virtual printer "boh-vprt".
   * </p>
   *
   * @param protocolId identifier of the protocol context
   * @param votesCount number of votes to cast
   * @return the expected tally
   */
  public Tally castRandomVotes(String protocolId, int votesCount) {
    final String virtualPrinter = "boh-vprt";
    final Path epfFolder = sharedFolderManager.getPrivateCredentialsLocation(protocolId, virtualPrinter);
    ProtocolDriver driver = protocolDriverFactory.retrieveAsReadyToCastVotes(protocolId,
                                                                             securityLevel, numberOfControlComponents);
    driver.readVotersPrivateCredentials(epfFolder,
                                        Collections.singletonMap(virtualPrinter, bohVirtualPrinterPrivateKey));
    final GeneratedVotesData votesData = driver.castVotes(votesCount, epfFolder);
    return votesData.getExpectedTally();
  }

  /**
   * Lists all existing protocol identifiers, along with a date informing about its latest activity period.
   *
   * @return a map of protocolIds - value being an approximation of the latest moment it has been used
   */
  public Map<String, LocalDateTime> listExistingProtocolIds() {
    return sharedFolderManager.findDeclaredProtocolIdentifiers();
  }

}
