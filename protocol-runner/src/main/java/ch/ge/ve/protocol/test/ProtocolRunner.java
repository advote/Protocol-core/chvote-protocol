/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.ElectionSetFactory;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import ch.ge.ve.protocol.test.reporting.FileType;
import ch.ge.ve.protocol.test.reporting.MarkdownTestReporter;
import ch.ge.ve.protocol.test.reporting.TestResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Single run of a voting protocol on an already deployed protocol system.
 */
@Component
@Profile("runner")
public class ProtocolRunner implements ApplicationRunner {
  private static final Logger log = LoggerFactory.getLogger(ProtocolRunner.class);

  private static final String PHASE_INITIALIZATION              = "Initialization";
  private static final String PHASE_GENERATE_VOTERS_CREDENTIALS = "Generate voters credentials";
  private static final String PHASE_DECRYPT_PRINTER_FILES       = "Decrypt printer files";
  private static final String PHASE_CAST_VOTES                  = "Cast votes";
  private static final String PHASE_SHUFFLE_AND_PARTIAL_DECRYPT = "Shuffle and partial decrypt";
  private static final String PHASE_TALLY                       = "Tally";
  private static final String PRINTER_FILES_SUBDIR              = "printerfiles";

  private final ProtocolDriverFactory   protocolDriverFactory;
  private final ObjectMapper            objectMapper;
  private final RandomGenerator         randomGenerator;
  private final DatabaseStatsRepository databaseStatsRepository;
  private final String                  schema;

  private PublicParameters                      publicParameters;
  private ElectionSetWithPublicKey              electionSet;
  private Map<String, IdentificationPrivateKey> printingAuthorityKeys;
  private List<Voter>                           voters;
  private int                                   votesCount;
  private ProtocolDriver                        protocolDriver;
  private EncryptionKeyPair                     electionOfficerKeyPair;
  private boolean                               hasElectionOfficerPrivateKey;
  private Path                                  outputFolder;
  private Path                                  printerFilesFolder;
  private TestResult                            testResult;

  @Autowired
  public ProtocolRunner(ProtocolDriverFactory protocolDriverFactory,
                        ObjectMapper objectMapper,
                        RandomGenerator randomGenerator,
                        DatabaseStatsRepository databaseStatsRepository,
                        @Value("${spring.datasource.username:none}") String schema) {
    this.protocolDriverFactory = protocolDriverFactory;
    this.objectMapper = objectMapper;
    this.randomGenerator = randomGenerator;
    this.databaseStatsRepository = databaseStatsRepository;
    this.schema = schema;
  }

  /**
   * Runs the test. Accepts the following arguments:
   * <ul>
   * <li>publicParameters: public parameters file, generated if not given</li>
   * <li>nbControlComponents: number of control components, mandatory if public parameters are not given</li>
   * <li>votersCount: number of voters, mandatory if election set is not given</li>
   * <li>waitTimeout: maximum waiting time (in milliseconds) at every step, default defined by an app property</li>
   * <li>securityLevel: encryption security level: 1 = 1024 bits keys, 2 = 2048 bits keys, etc. (optional, default
   * value: 1)</li>
   * <li>votesCount: number of votes to cast. Optional, if not set, votesCount = voters count</li>
   * <li>electionSet: set of election data. Must be one of the enum values of {@link ElectionSetFactory} (optional,
   * default value: SINGLE_VOTE)</li>
   * <li>electionSetFile: election set file, generated if not given</li>
   * <li>voters: voters file, generated if not given</li>
   * <li>electionOfficerPublicKey: election officer's public key, generated if not given</li>
   * <li>electionOfficerPrivateKey: election officer's private key, generated if not given</li>
   * <li>filesOutputPath: output directory for all files (optional, default value: ./target)</li>
   * </ul>
   *
   * @param args the test arguments, provided in any sequence.
   */
  @Override
  public void run(ApplicationArguments args) {
    ProtocolRunnerArguments arguments = new ProtocolRunnerArguments(args, objectMapper, randomGenerator);
    this.publicParameters = arguments.getPublicParameters();
    this.electionSet = arguments.getElectionSet();
    this.voters = arguments.getVoters();
    this.votesCount = arguments.getVotesCount().orElse(voters.size());
    this.electionOfficerKeyPair = arguments.getElectionOfficerKeyPair();
    this.hasElectionOfficerPrivateKey = this.electionOfficerKeyPair.getPrivateKey() != null;
    this.outputFolder = arguments.getOutputPath();
    ensureOutputFolderExists(outputFolder);
    this.printerFilesFolder = outputFolder.resolve(PRINTER_FILES_SUBDIR);
    this.printingAuthorityKeys = arguments.getPrintingAuthorityKeys();
    this.testResult = new TestResult(args);

    // Initialize the ProtocolDriver
    this.protocolDriver = arguments.getWaitTimeout()
                                   .map(protocolDriverFactory::withWaitTimeout)
                                   .orElse(protocolDriverFactory)
                                   .createForProtocolInitialization(arguments.getSecurityLevel(), publicParameters);

    runProtocol();
    testResult.recordSchemaSize(databaseStatsRepository.getSchemaSize(schema));
    new MarkdownTestReporter().process(testResult, outputFolder.toString());
  }

  private void ensureOutputFolderExists(Path path) {
    File f = path.toFile();
    if (!f.mkdirs()) {
      Preconditions.checkArgument(f.isDirectory(), "%s is not a directory", f);
      Preconditions.checkArgument(f.canWrite(), "Need to have write access to the output folder");
    }
  }

  private void runProtocol() {
    testResult.testStarted();
    initializeProtocol();
    generateVotersCredentials();
    decryptPrinterFiles();
    castVotes();
    shuffleAndGeneratePartialDecryptions();
    if (hasElectionOfficerPrivateKey) {
      checkTally();
    }
    testResult.testTerminated();
    writeAuditLog();
    writeExpectedTallyIfAvailable();
  }

  private void initializeProtocol() {
    testResult.phaseStarted(PHASE_INITIALIZATION);
    protocolDriver.initializeProtocol(publicParameters, electionSet, electionOfficerKeyPair.getPublicKey());
    testResult.phaseTerminated(PHASE_INITIALIZATION);
  }

  private void generateVotersCredentials() {
    testResult.phaseStarted(PHASE_GENERATE_VOTERS_CREDENTIALS);
    protocolDriver.submitVoters(voters, printerFilesFolder)
                  .forEach(path -> testResult.addFileToReport(FileType.PRINTER_FILE, path));
    testResult.phaseTerminated(PHASE_GENERATE_VOTERS_CREDENTIALS);
  }

  private void decryptPrinterFiles() {
    testResult.phaseStarted(PHASE_DECRYPT_PRINTER_FILES);
    protocolDriver.readVotersPrivateCredentials(printerFilesFolder, printingAuthorityKeys);
    testResult.phaseTerminated(PHASE_DECRYPT_PRINTER_FILES);
  }

  private void castVotes() {
    testResult.phaseStarted(PHASE_CAST_VOTES);
    final GeneratedVotesData votesData = protocolDriver.castVotes(votesCount, printerFilesFolder);
    testResult.addVoterStats(votesData.getPerformanceStats());
    testResult.phaseTerminated(PHASE_CAST_VOTES);
  }

  private void shuffleAndGeneratePartialDecryptions() {
    testResult.phaseStarted(PHASE_SHUFFLE_AND_PARTIAL_DECRYPT);
    protocolDriver.shuffleAndGeneratePartialDecryptions();
    testResult.phaseTerminated(PHASE_SHUFFLE_AND_PARTIAL_DECRYPT);
  }

  private void checkTally() {
    testResult.phaseStarted(PHASE_TALLY);
    protocolDriver.verifyTally(electionOfficerKeyPair.getPrivateKey());
    testResult.phaseTerminated(PHASE_TALLY);
  }

  private void writeAuditLog() {
    File file = outputFolder.toFile();
    int bitLength = publicParameters.getEncryptionGroup().getP().bitLength();
    Path out = Paths.get(file.getAbsolutePath(),
                         String.format("verificationData-%d-%d.json", bitLength, voters.size()))
                    .normalize();

    log.info("Election verification data is being written to {}", out);
    try {
      protocolDriver.writeAuditLog(out, hasElectionOfficerPrivateKey);
      testResult.addFileToReport(FileType.PROTOCOL_AUDIT_LOG, out);
    } catch (VerificationDataWriterException e) {
      log.error("failed to write the protocol audit log", e);
    }
  }

  private void writeExpectedTallyIfAvailable() {
    try {
      protocolDriver.writeExpectedTallyIfAvailable(outputFolder.resolve("expected-tally.json"));
    } catch (IOException e) {
      log.error("failed to write expected tally", e);
    }
  }
}
