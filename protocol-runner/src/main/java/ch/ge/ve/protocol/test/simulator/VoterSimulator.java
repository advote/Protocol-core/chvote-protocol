/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.simulator;

import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.exception.ProtocolException;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationVoterAlgorithms;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.support.model.VotingPageData;
import com.google.common.base.Preconditions;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simulation class for a Voter, with interfaces to receive the code sheet, initiate a voting client, and perform
 * selections on elections.
 */
final class VoterSimulator {
  private static final Logger logger = LoggerFactory.getLogger(VoterSimulator.class);

  private final Integer                         voterIndex;
  private final VotingClient                    votingClient;
  private final VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms;
  private final Random                          random = new SecureRandom();
  private       VotingCard                      votingCard;
  private       List<Integer>                   selections;

  VoterSimulator(Integer voterIndex, VotingClient votingClient,
                 VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms) {
    this.votingClient = votingClient;
    this.voterIndex = voterIndex;
    this.voteConfirmationVoterAlgorithms = voteConfirmationVoterAlgorithms;
  }

  void setVotingCard(VotingCard votingCard) {
    Preconditions.checkState(this.votingCard == null,
                             "The code sheet may not be updated once set (at voter %s)", voterIndex);
    Preconditions.checkArgument(votingCard.getVoter().getVoterId() == voterIndex,
                                "Voter received the wrong code lists");
    this.votingCard = votingCard;
  }

  List<Integer> vote(VotePerformanceStatsBuilder performanceStatsBuilder) {
    Preconditions.checkState(votingCard != null, "The voter needs their code sheet to vote");
    logger.info("Voter {} starting vote", voterIndex);
    VotingPageData votingPageData = votingClient.startVoteSession(voterIndex);
    selections = pickAtRandom(votingPageData.getSelectionCounts(), votingPageData.getCandidateCounts());
    logger.info("Voter {} submitting vote, selections: {}", voterIndex, selections);
    List<ObliviousTransferResponse> otResponses = votingClient.submitVote(votingCard.getUpper_x(),
                                                                          selections, performanceStatsBuilder);
    performanceStatsBuilder.verifyCodesStart();
    checkVerificationCodes(votingClient.getVerificationCodes(otResponses));
    performanceStatsBuilder.verifyCodesStop();
    return selections;
  }

  private void checkVerificationCodes(List<String> verificationCodes) {
    logger.info("Voter {} checking verification codes", voterIndex);
    if (!voteConfirmationVoterAlgorithms.checkReturnCodes(votingCard.getBold_rc(), verificationCodes, selections)) {
      throw new ProtocolException("Verification codes do not match");
    }
  }

  void confirmVote(VotePerformanceStatsBuilder performanceStatsBuilder) throws ConfirmationFailedException {
    logger.info("Voter {} confirming vote", voterIndex);
    List<FinalizationCodePart> finalizationCodeParts = votingClient.confirmVote(votingCard.getUpper_y(),
                                                                                performanceStatsBuilder);
    performanceStatsBuilder.verifyFinalizationStart();
    checkFinalizationCode(votingClient.getFinalizationCode(finalizationCodeParts));
    performanceStatsBuilder.verifyFinalizationStop();
  }

  private void checkFinalizationCode(String finalizationCode) {
    logger.info("Voter {} checking finalization code", voterIndex);
    if (!voteConfirmationVoterAlgorithms.checkFinalizationCode(votingCard.getUpper_fc(), finalizationCode)) {
      throw new ProtocolException("Finalization code does not match");
    }
    logger.info("Voter {} successfully voted", voterIndex);
  }

  private List<Integer> pickAtRandom(List<Integer> selectionCounts, List<Integer> candidateCounts) {
    Preconditions.checkArgument(selectionCounts.size() == candidateCounts.size(),
                                "The size of both lists should be identical");
    List<Integer> currentSelections = new ArrayList<>();
    int n_offset = 1;
    for (int i = 0; i < selectionCounts.size(); i++) {
      int numberOfSelections = selectionCounts.get(i);
      int numberOfCandidates = candidateCounts.get(i);
      for (int j = 0; j < numberOfSelections; j++) {
        Integer s_j;
        do {
          s_j = random.nextInt(numberOfCandidates) + n_offset;
        } while (currentSelections.contains(s_j));
        currentSelections.add(s_j);
      }
      n_offset += numberOfCandidates;
    }
    return currentSelections.stream().sorted().collect(Collectors.toList());
  }
}
