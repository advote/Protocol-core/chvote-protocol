/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.ProtocolClientImpl;
import ch.ge.ve.protocol.client.event.VoteProcessingReplyEvent;
import ch.ge.ve.protocol.client.event.VoteProcessingRequestEvent;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.model.VoteResult;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteCastingClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationVoterAlgorithms;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.test.simulator.VoteReceiverSimulator;
import ch.ge.ve.protocol.test.simulator.VoterSimulatorCoordinator;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventHeaders;
import ch.ge.ve.service.SignatureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(
            value = RabbitUtilities.VOTE_PROCESSING_REQUEST_CHANNEL,
            durable = "true"),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = RabbitUtilities.VOTE_PROCESSING_REQUEST_CHANNEL)
    )
)
@Profile("voteclient")
public class VoteSimulationListener {
  private static final Logger log = LoggerFactory.getLogger(VoteSimulationListener.class);

  private static final String DIGEST_PROVIDER  = "BC";
  private static final String DIGEST_ALGORITHM = "BLAKE2B-256";

  private final Object                                 lock                     = new Object();
  private final Map<String, VoterSimulatorCoordinator> coordinatorsByProtocolId = new HashMap<>();

  private final RabbitUtilities  rabbitUtilities;
  private final SignatureService signatureService;
  private final RandomGenerator  randomGenerator;
  private final ObjectMapper     objectMapper;
  private final int              securityLevel;
  private final int              batchSize;
  private final long             waitTimeout;

  @Autowired
  public VoteSimulationListener(RabbitUtilities rabbitUtilities, SignatureService signatureService,
                                RandomGenerator randomGenerator, ObjectMapper mapper,
                                @Value("${ch.ge.ve.security-level}") int securityLevel,
                                @Value("${chvote.protocol-runner.batch-size}") int batchSize,
                                @Value("${chvote.protocol-runner.wait-timeout}") long waitTimeout) {
    this.rabbitUtilities = rabbitUtilities;
    this.signatureService = signatureService;
    this.randomGenerator = randomGenerator;
    this.objectMapper = mapper;
    this.securityLevel = securityLevel;
    this.waitTimeout = waitTimeout;
    this.batchSize = batchSize;
  }

  @RabbitHandler
  public void handleVoteProcessingRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                               VoteProcessingRequestEvent event) throws ConfirmationFailedException {
    int voterId = event.getVotingCard().getVoter().getVoterId();
    log.info("Processing a vote for protocol [{}], voter id [{}]", protocolId, voterId);

    VoterSimulatorCoordinator voterSimulatorCoordinator =
        getVoterSimulatorCoordinator(protocolId, event.getPublicParameters(), event.getElectionSetForVerification(),
                                     event.getPublicKeyParts());

    VoteResult voteResult = voterSimulatorCoordinator.vote(event.getVotingCard());

    VoteProcessingReplyEvent replyEvent = new VoteProcessingReplyEvent(voterId, voteResult);

    rabbitUtilities
        .publish(RabbitUtilities.VOTE_PROCESSING_REPLY_CHANNEL, protocolId, Endpoints.PROTOCOL_CLIENT, replyEvent);
  }

  private VoterSimulatorCoordinator getVoterSimulatorCoordinator(String protocolId,
                                                                 PublicParameters publicParameters,
                                                                 ElectionSetForVerification electionSet,
                                                                 List<EncryptionPublicKey> publicKeyParts) {
    // First invocation may be long, but should optimize the process over time
    synchronized (lock) {
      return coordinatorsByProtocolId.computeIfAbsent(protocolId, k -> {
        log.info("Initializing VoterSimulatorCoordinator for protocol {}", protocolId);
        int nbControlComponents = publicParameters.getS();
        ProtocolClient protocolClient = new ProtocolClientImpl(securityLevel, protocolId, nbControlComponents,
                                                               rabbitUtilities, signatureService, objectMapper,
                                                               randomGenerator, waitTimeout, batchSize);
        VoteReceiverSimulator voteReceiverSimulator =
            new VoteReceiverSimulator(protocolClient, publicParameters, electionSet, publicKeyParts);

        KeyEstablishmentAlgorithms keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator);
        Hash hash = new Hash(DIGEST_ALGORITHM, DIGEST_PROVIDER, publicParameters.getSecurityParameters().getUpper_l());
        PrimesCache primesCache;
        try {
          primesCache = PrimesCache.populate(electionSet.getCandidates().size() + electionSet.getCountingCircleCount(),
                                             publicParameters.getEncryptionGroup());
        } catch (NotEnoughPrimesInGroupException e) {
          throw new SimulationException(e);
        }
        GeneralAlgorithms generalAlgorithms = new GeneralAlgorithms(hash, publicParameters.getEncryptionGroup(),
                                                                    publicParameters.getIdentificationGroup(),
                                                                    primesCache);
        VoteCastingClientAlgorithms voteCastingClientAlgorithms =
            new VoteCastingClientAlgorithms(publicParameters, generalAlgorithms, randomGenerator, hash);
        VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms =
            new VoteConfirmationClientAlgorithms(publicParameters, generalAlgorithms, randomGenerator);
        VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms = new VoteConfirmationVoterAlgorithms();

        return new VoterSimulatorCoordinator(voteReceiverSimulator, keyEstablishmentAlgorithms,
                                             voteCastingClientAlgorithms, voteConfirmationClientAlgorithms,
                                             voteConfirmationVoterAlgorithms);
      });
    }
  }

  private final class SimulationException extends RuntimeException {
    SimulationException(Throwable throwable) {
      super(throwable);
    }
  }
}
