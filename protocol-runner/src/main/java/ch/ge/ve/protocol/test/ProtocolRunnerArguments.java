/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.ElectionSetFactory;
import ch.ge.ve.protocol.simulation.model.ElectionSetAndVoters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.boot.ApplicationArguments;

final class ProtocolRunnerArguments {

  private static final String PUBLIC_PARAMETERS              = "publicParameters";
  private static final String SECURITY_LEVEL                 = "securityLevel";
  private static final String ELECTION_SET_FILE              = "electionSetFile";
  private static final String ELECTION_SET                   = "electionSet";
  private static final String VOTERS                         = "voters";
  private static final String VOTERS_COUNT                   = "votersCount";
  private static final String VOTES_COUNT                    = "votesCount";
  private static final String ELECTION_OFFICER_PUBLIC_KEY    = "electionOfficerPublicKey";
  private static final String ELECTION_OFFICER_PRIVATE_KEY   = "electionOfficerPrivateKey";
  private static final String PRINTING_AUTHORITY_PRIVATE_KEY = "printingAuthorityPrivateKey";
  private static final String NB_CONTROL_COMPONENTS          = "nbControlComponents";
  private static final String WAIT_TIMEOUT                   = "waitTimeout";
  private static final String OUTPUT_PATH                    = "filesOutputPath";
  private static final String DEFAULT_OUTPUT_PATH            = "./target";

  private final ApplicationArguments arguments;
  private final ObjectMapper         mapper;
  private final RandomGenerator      randomGenerator;

  public ProtocolRunnerArguments(ApplicationArguments arguments, ObjectMapper mapper, RandomGenerator randomGenerator) {
    this.arguments = arguments;
    this.mapper = mapper;
    this.randomGenerator = randomGenerator;
    Preconditions.checkArgument(getArgument(ELECTION_SET_FILE).isPresent() == getArgument(VOTERS).isPresent(),
                                "Voters and election set must be consistent");
    if (getArgument(ELECTION_OFFICER_PRIVATE_KEY).isPresent()) {
      Preconditions.checkArgument(getArgument(ELECTION_OFFICER_PUBLIC_KEY).isPresent(),
                                  "Missing election officer's public key");
    }
    if (getArgument(ELECTION_OFFICER_PUBLIC_KEY).isPresent()) {
      Preconditions.checkArgument(getArgument(PUBLIC_PARAMETERS).isPresent(), "Public parameters must be given");
    }
  }

  public int getSecurityLevel() {
    return getArgument(SECURITY_LEVEL).map(Integer::valueOf).orElse(1);
  }

  public PublicParameters getPublicParameters() {
    return getArgument(PUBLIC_PARAMETERS).map(v -> fromJson(Paths.get(v), PublicParameters.class))
                                         .orElseGet(this::createPublicParameters);
  }

  public ElectionSetWithPublicKey getElectionSet() {
    return getArgument(ELECTION_SET_FILE).map(v -> fromJson(Paths.get(v), ElectionSetWithPublicKey.class))
                                         .orElseGet(this::createElectionSet);
  }

  public List<Voter> getVoters() {
    TypeReference<List<Voter>> type = new TypeReference<List<Voter>>() {
    };
    return getArgument(VOTERS).map(v -> fromJson(Paths.get(v), type)).orElseGet(this::createVoters);
  }

  public Map<String, IdentificationPrivateKey> getPrintingAuthorityKeys() {
    final TypeReference<Map<String, IdentificationPrivateKey>> typeReference =
        new TypeReference<Map<String, IdentificationPrivateKey>>() { /* nothing needed */ };
    return getArgument(PRINTING_AUTHORITY_PRIVATE_KEY).map(Paths::get)
                                                      .map(path -> fromJson(path, typeReference))
                                                      .orElse(Collections.emptyMap());
  }

  public EncryptionKeyPair getElectionOfficerKeyPair() {
    Optional<String> electionOfficerPublicKey = getArgument(ELECTION_OFFICER_PUBLIC_KEY);
    Optional<String> electionOfficerPrivateKey = getArgument(ELECTION_OFFICER_PRIVATE_KEY);
    if (electionOfficerPublicKey.isPresent()) {
      EncryptionPublicKey publicKey = fromJson(Paths.get(electionOfficerPublicKey.get()), EncryptionPublicKey.class);
      EncryptionPrivateKey privateKey =
          electionOfficerPrivateKey.map(v -> fromJson(Paths.get(v), EncryptionPrivateKey.class))
                                   .orElse(null);
      return new EncryptionKeyPair(publicKey, privateKey);
    } else {
      return generateElectionOfficerKeyPair();
    }
  }

  public Path getOutputPath() {
    return Paths.get(getArgument(OUTPUT_PATH).orElse(DEFAULT_OUTPUT_PATH));
  }

  public Optional<Integer> getVotesCount() {
    return getArgument(VOTES_COUNT).map(Integer::valueOf);
  }

  public Optional<Long> getWaitTimeout() {
    return getArgument(WAIT_TIMEOUT).map(Long::valueOf);
  }

  private Optional<String> getArgument(String name) {
    return Optional.ofNullable(arguments.getOptionValues(name))
                   .flatMap(values -> values.stream().findFirst());
  }

  private String getMandatoryArgument(String name) {
    return getArgument(name).orElseThrow(() -> new IllegalArgumentException("Expected argument: --" + name));
  }

  private <T> T fromJson(Path path, TypeReference<T> type) {
    try (InputStream in = Files.newInputStream(path)) {
      return mapper.readValue(in, type);
    } catch (IOException e) {
      throw new IllegalArgumentException("Can't read " + path, e);
    }
  }

  private <T> T fromJson(Path path, Class<T> type) {
    try (InputStream in = Files.newInputStream(path)) {
      return mapper.readValue(in, type);
    } catch (IOException e) {
      throw new IllegalArgumentException("Can't read " + path, e);
    }
  }

  private PublicParameters createPublicParameters() {
    PublicParametersFactory securityLevel = getArgument(SECURITY_LEVEL).map(Integer::valueOf)
                                                                       .map(PublicParametersFactory::forLevel)
                                                                       .orElse(PublicParametersFactory.LEVEL_1);
    int nbControlComponents = Integer.parseInt(getMandatoryArgument(NB_CONTROL_COMPONENTS));
    return securityLevel.createPublicParameters(nbControlComponents);
  }

  private ElectionSetWithPublicKey createElectionSet() {
    return createElectionSetAndVoters().getElectionSet();
  }

  private List<Voter> createVoters() {
    return createElectionSetAndVoters().getVoters();
  }

  private ElectionSetAndVoters createElectionSetAndVoters() {
    ElectionSetFactory factory = getArgument(ELECTION_SET).map(ElectionSetFactory::valueOf)
                                                          .orElse(ElectionSetFactory.SINGLE_VOTE);
    int votersCount = Integer.parseInt(getMandatoryArgument(VOTERS_COUNT));
    return factory.createElectionSetAndVoters(votersCount, mapper, getSecurityLevel());
  }

  private EncryptionKeyPair generateElectionOfficerKeyPair() {
    KeyEstablishmentAlgorithms keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator);
    return keyEstablishmentAlgorithms.generateKeyPair(getPublicParameters().getEncryptionGroup());
  }
}
