/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.ProtocolClientImpl;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.service.SignatureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * A factory to obtain {@link ProtocolDriver} instances, in pre-defined state.
 */
@Service
public class ProtocolDriverFactory {

  private final ObjectMapper     objectMapper;
  private final RabbitUtilities  rabbitUtilities;
  private final SignatureService signatureService;
  private final RandomGenerator  randomGenerator;
  private final long             waitTimeout;
  private final int              votersBatchSize;

  @Autowired
  public ProtocolDriverFactory(ObjectMapper objectMapper,
                               RabbitUtilities rabbitUtilities,
                               SignatureService signatureService,
                               RandomGenerator randomGenerator,
                               @Value("${chvote.protocol-runner.wait-timeout}") long waitTimeout,
                               @Value("${chvote.protocol-runner.batch-size}") int votersBatchSize) {
    this.objectMapper = objectMapper;
    this.rabbitUtilities = rabbitUtilities;
    this.signatureService = signatureService;
    this.randomGenerator = randomGenerator;
    this.waitTimeout = waitTimeout;
    this.votersBatchSize = votersBatchSize;
  }

  /**
   * Get a copy of this factory with the same attributes but a modified "waitTimeout".
   * <p>
   * As the factory is a container singleton, its state is immutable, "waitTimeout" being the default
   * configured from "application.yml". This allows customization.
   * </p>
   *
   * @param timeout the waiting timeout - in milliseconds
   *
   * @return a new instance of {@code ProtocolDriverFactory}, similar to this one but with a different "waitTimeout"
   */
  public ProtocolDriverFactory withWaitTimeout(long timeout) {
    return new ProtocolDriverFactory(objectMapper, rabbitUtilities, signatureService,
                                     randomGenerator, timeout, votersBatchSize);
  }

  /**
   * Creates a {@link ProtocolDriver} in order to operate a new protocol instance.
   * <p>
   * First expected call is
   * {@link ProtocolDriver#initializeProtocol(PublicParameters, ElectionSetWithPublicKey, EncryptionPublicKey)}.
   * </p>
   *
   * @param securityLevel    security level of the protocol to create
   * @param publicParameters the public parameters to declare to the protocol system
   *
   * @return an instance of {@code ProtocolDriver}, ready to start a brand new protocol operation
   */
  public ProtocolDriver createForProtocolInitialization(int securityLevel, PublicParameters publicParameters) {
    final int nbControlComponents = publicParameters.getS();
    final String protocolId = UUID.randomUUID().toString();
    ProtocolClient
        protocolClient = new ProtocolClientImpl(securityLevel, protocolId, nbControlComponents, rabbitUtilities,
                                                signatureService, objectMapper, randomGenerator, waitTimeout,
                                                votersBatchSize);
    return new ProtocolDriver(objectMapper, protocolClient, votersBatchSize);
  }

  /**
   * Creates a {@link ProtocolDriver} in order to operate an <em>already existing</em> protocol instance.
   * <p>
   * No call to
   * {@link ProtocolDriver#initializeProtocol(PublicParameters, ElectionSetWithPublicKey, EncryptionPublicKey)}
   * is expected : this instance of the protocol has already been initialize, all initialization data is requested
   * from the protocol system.
   * </p>
   *
   * @param protocolId          identifier of the protocol instance to operate
   * @param securityLevel       the security level this protocol instance had been initialized with
   * @param nbControlComponents the number of control component running this protocol instance
   *
   * @return an instance of {@code ProtocolDriver} that is aware of this protocol instance's initializing parameters
   */
  public ProtocolDriver retrieveAsReadyToCastVotes(String protocolId, int securityLevel, int nbControlComponents) {
    ProtocolClient
        protocolClient = new ProtocolClientImpl(securityLevel, protocolId, nbControlComponents, rabbitUtilities,
                                                signatureService, objectMapper, randomGenerator, waitTimeout,
                                                votersBatchSize);
    protocolClient.reinitFromProtocolState();
    return new ProtocolDriver(objectMapper, protocolClient, votersBatchSize);
  }
}
