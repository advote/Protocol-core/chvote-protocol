/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.injector;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An (internal) shared-folder manager that mimics {@code OutputFilesManager} used by the PACT.
 * <p>
 *   This helps us integrate in a PACT-managed environment. We are thereby able to determine the currently
 *   running protocol instances, and access the private credentials (EPF) requested by the PACT.
 * </p>
 */
class SharedFolderManager {

  private static final ZoneId ZONE_ECT = ZoneId.of("Europe/Paris");
  private static final String PROTOCOL_ROOT = "protocol";

  private final Path rootFolder;

  public SharedFolderManager(Path rootFolder) {
    this.rootFolder = rootFolder;
  }

  /**
   * Determines the ids of running protocol instances, based on a shared-folder analysis.
   *
   * @return a map containing all protocol identifiers - the value being the time of last modification
   */
  public Map<String, LocalDateTime> findDeclaredProtocolIdentifiers() {
    try (Stream<Path> children = Files.list(rootFolder.resolve(PROTOCOL_ROOT))) {
      return children.filter(Files::isDirectory)
                     .collect(Collectors.toMap(
                         p -> p.getFileName().toString(),
                         SharedFolderManager::lastModifiedTime
                     ));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private static LocalDateTime lastModifiedTime(Path path) {
    try {
      final Instant instant = Files.getLastModifiedTime(path).toInstant();
      return LocalDateTime.ofInstant(instant, ZONE_ECT);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Determines the path where the private credentials files (epf) of a specific protocol instance, and for a specific
   * printer, reside.
   *
   * @param protocolId identifier of the protocol "owning" the credentials
   * @param printerName name of the printer that manages the voters
   *
   * @return the path to the parent folder containing epf files
   * @throws IllegalArgumentException if no directory exists for the given protocolId
   */
  public Path getPrivateCredentialsLocation(String protocolId, String printerName) {
    // Tree structure comes from PACT: see PrinterArchiveGenerator
    final Path printersFolder = rootFolder.resolve(PROTOCOL_ROOT).resolve(protocolId).resolve("printerFiles");
    try {
      Path epfLocation = printersFolder.resolve(String.format("printer-archive-%s",
                                                              URLEncoder.encode(printerName, "UTF-8"))
      ).normalize();

      if (Files.notExists(epfLocation)) {
        throw new IllegalArgumentException(String.format("No directory found for printer \"%s\" and protocolId = %s",
                                                         printerName, protocolId));
      }
      return epfLocation;

    } catch (UnsupportedEncodingException e) {
      throw new AssertionError("Failed to encode printing authority name in UTF-8", e);
    }
  }
}
