/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Oracle Database implementation, uses Oracle specific SQL queries
 */
@Profile({"runner & oracle"})
@Service
public class DatabaseStatsRepositoryOracleImpl implements DatabaseStatsRepository {
  private final JdbcTemplate jdbcTemplate;

  public DatabaseStatsRepositoryOracleImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public long getSchemaSize(String schema) {
    Long schemaSize = jdbcTemplate.queryForObject(
        "SELECT owner,sum(bytes) AS schema_size FROM dba_segments WHERE owner=? GROUP BY owner",
        new Object[]{schema},
        (resultSet, i) -> resultSet.getLong("schema_size")
    );
    return schemaSize != null ? schemaSize : 0;
  }
}
