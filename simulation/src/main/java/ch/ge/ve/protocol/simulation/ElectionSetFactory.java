/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.exception.SimulationRuntimeException;
import ch.ge.ve.protocol.simulation.model.ElectionSetAndVoters;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public enum ElectionSetFactory {
  SINGLE_VOTE {
    @Override
    public ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper, int securityLevel) {
      List<CountingCircle> countingCircles = createCountingCircles(votersCount);
      List<PrintingAuthorityWithPublicKey> printingAuthorities =
          createPrintingAuthorities(votersCount, mapper, securityLevel);
      List<Voter> voters = IntStream.range(0, votersCount)
                                    .mapToObj(i -> new Voter(i, Integer.toString(i),
                                                             countingCircles.get(i % countingCircles.size()),
                                                             printingAuthorities.get(i % printingAuthorities.size())
                                                                                .getName(),
                                                             CANTON)).collect(Collectors.toList());
      Election cantonalVotation1 = new Election("CAN1", 3, 1, CANTON);
      List<Election> elections = Collections.singletonList(cantonalVotation1);
      List<Candidate> candidates = IntStream.range(0, cantonalVotation1.getNumberOfCandidates())
                                            .mapToObj(
                                                i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                            .collect(Collectors.toList());
      ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, printingAuthorities,
                                                                          voters.size(),
                                                                          ElectionSetFactory
                                                                              .countCountingCircles(voters));
      return new ElectionSetAndVoters(electionSet, voters);
    }
  },
  SIMPLE_SAMPLE {
    @Override
    public ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper, int securityLevel) {
      List<CountingCircle> countingCircles = createCountingCircles(votersCount);
      List<PrintingAuthorityWithPublicKey> printingAuthorities =
          createPrintingAuthorities(votersCount, mapper, securityLevel);
      Stream<Voter> voters = Stream.concat(
          IntStream.range(0, votersCount / 10)
                   .mapToObj(i -> new Voter(i, Integer.toString(i), countingCircles.get(i % countingCircles.size()),
                                            printingAuthorities.get(i % printingAuthorities.size()).getName(), CANTON,
                                            MUNICIPALITY_1)),
          IntStream.range(votersCount / 10, votersCount)
                   .mapToObj(i -> new Voter(i, Integer.toString(i), countingCircles.get(i % countingCircles.size()),
                                            printingAuthorities.get(i % printingAuthorities.size()).getName(),
                                            CANTON)));
      Election cantonalVotation1 = new Election("CAN1", 3, 1, CANTON);
      Election cantonalVotation2 = new Election("CAN2", 3, 1, CANTON);
      Election municipalElection = new Election("MUN", 10, 2, MUNICIPALITY_1);
      List<Election> elections = Arrays.asList(cantonalVotation1, cantonalVotation2, municipalElection);
      int numberOfCandidates = cantonalVotation1.getNumberOfCandidates() + cantonalVotation2.getNumberOfCandidates()
                               + municipalElection.getNumberOfCandidates();
      List<Candidate> candidates = IntStream.range(0, numberOfCandidates)
                                            .mapToObj(
                                                i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                            .collect(Collectors.toList());
      List<Voter> voterList = voters.collect(Collectors.toList());
      ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, printingAuthorities,
                                                                          voterList.size(),
                                                                          ElectionSetFactory
                                                                              .countCountingCircles(voterList));
      return new ElectionSetAndVoters(electionSet, voterList);
    }
  },
  MUNICIPAL {
    @Override
    public ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper, int securityLevel) {
      List<CountingCircle> countingCircles = createCountingCircles(votersCount);
      List<PrintingAuthorityWithPublicKey> printingAuthorities =
          createPrintingAuthorities(votersCount, mapper, securityLevel);
      Stream<Voter> voters = Stream.concat(
          IntStream.range(0, votersCount / 10)
                   .mapToObj(i -> new Voter(i, Integer.toString(i), countingCircles.get(i % countingCircles.size()),
                                            printingAuthorities.get(i % printingAuthorities.size()).getName(),
                                            MUNICIPALITY_1)),
          IntStream.range(votersCount / 10, votersCount)
                   .mapToObj(i -> new Voter(i, Integer.toString(i), countingCircles.get(i % countingCircles.size()),
                                            printingAuthorities.get(i % printingAuthorities.size()).getName(),
                                            MUNICIPALITY_2)));
      Election municipalElection1 = new Election("MUN1", 500, 3, MUNICIPALITY_1);
      Election municipalElection2 = new Election("MUN2", 6, 3, MUNICIPALITY_2);
      List<Election> elections = Arrays.asList(municipalElection1, municipalElection2);
      int numberOfCandidates = municipalElection1.getNumberOfCandidates() + municipalElection2.getNumberOfCandidates();
      List<Candidate> candidates = IntStream.range(0, numberOfCandidates)
                                            .mapToObj(
                                                i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                            .collect(Collectors.toList());
      List<Voter> voterList = voters.collect(Collectors.toList());
      ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, printingAuthorities,
                                                                          voterList.size(),
                                                                          ElectionSetFactory
                                                                              .countCountingCircles(voterList));
      return new ElectionSetAndVoters(electionSet, voterList);
    }
  },
  GC_CE {
    @Override
    public ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper, int securityLevel) {
      List<CountingCircle> countingCircles = createCountingCircles(votersCount);
      List<PrintingAuthorityWithPublicKey> printingAuthorities =
          createPrintingAuthorities(votersCount, mapper, securityLevel);
      List<Voter> voters = IntStream.range(0, votersCount)
                                    .mapToObj(i -> new Voter(i, Integer.toString(i),
                                                             countingCircles.get(i % countingCircles.size()),
                                                             printingAuthorities.get(i % printingAuthorities.size())
                                                                                .getName(),
                                                             CANTON))
                                    .collect(Collectors.toList());
      Election ce_election = new Election("CE", 40, 7, CANTON);
      Election gc_election = new Election("GC", 730, 100, CANTON);
      List<Election> elections = Arrays.asList(ce_election, gc_election);
      int numberOfCandidates = ce_election.getNumberOfCandidates() + gc_election.getNumberOfCandidates();
      List<Candidate> candidates = IntStream.range(0, numberOfCandidates)
                                            .mapToObj(
                                                i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                            .collect(Collectors.toList());
      ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, printingAuthorities,
                                                                          voters.size(),
                                                                          ElectionSetFactory
                                                                              .countCountingCircles(voters));
      return new ElectionSetAndVoters(electionSet, voters);
    }
  },
  EXTREME {
    @Override
    public ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper, int securityLevel) {
      List<CountingCircle> countingCircles =
          IntStream.range(0, 380)
                   .mapToObj(i -> new CountingCircle(i,
                                                     String.format("CountingCircle%d", i),
                                                     String.format("CountingCircleName%d", i)))
                   .collect(Collectors.toList());
      List<PrintingAuthorityWithPublicKey> printingAuthorities =
          createPrintingAuthorities(votersCount, mapper, securityLevel);
      List<Voter> voters = IntStream.range(0, votersCount)
                                    .mapToObj(i -> new Voter(i, Integer.toString(i),
                                                             countingCircles.get(i % countingCircles.size()),
                                                             printingAuthorities.get(i % printingAuthorities.size())
                                                                                .getName(),
                                                             CANTON))
                                    .collect(Collectors.toList());
      Election vote1 = new Election("VOTE1", 3, 1, CANTON);
      Election vote2 = new Election("VOTE2", 3, 1, CANTON);
      Election vote3 = new Election("VOTE3", 3, 1, CANTON);
      Election vote4 = new Election("VOTE4", 3, 1, CANTON);
      Election vote5 = new Election("VOTE5", 3, 1, CANTON);
      Election vote6 = new Election("VOTE6", 3, 1, CANTON);
      Election ce_election = new Election("CE", 80, 7, CANTON);
      Election gc_election = new Election("GC", 1000, 100, CANTON);
      Election additional_election_1 = new Election("EL1", 36, 5, CANTON);
      Election additional_election_2 = new Election("EL2", 36, 5, CANTON);
      List<Election> elections = Arrays.asList(vote1, vote2, vote3, vote4, vote5, vote6, ce_election, gc_election,
                                               additional_election_1, additional_election_2);
      int numberOfCandidates = elections.stream().mapToInt(Election::getNumberOfCandidates).sum();
      List<Candidate> candidates =
          IntStream.range(0, numberOfCandidates)
                   .mapToObj(i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                   .collect(Collectors.toList());
      ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, printingAuthorities,
                                                                          voters.size(),
                                                                          ElectionSetFactory
                                                                              .countCountingCircles(voters));
      return new ElectionSetAndVoters(electionSet, voters);
    }
  };

  private static final DomainOfInfluence CANTON                         = new DomainOfInfluence("canton");
  private static final DomainOfInfluence MUNICIPALITY_1                 = new DomainOfInfluence("municipality1");
  private static final DomainOfInfluence MUNICIPALITY_2                 = new DomainOfInfluence("municipality2");
  private static final String            CANDIDATE_DESCRIPTION_TEMPLATE = "candidate %d";

  private static List<CountingCircle> createCountingCircles(int votersCount) {
    return IntStream.range(0, Math.max(1, (int) Math.log10(votersCount)))
                    .mapToObj(i -> new CountingCircle(i,
                                                      String.format("CountingCircle%d", i),
                                                      String.format("CountingCircleName%d", i)))
                    .collect(Collectors.toList());
  }

  private static List<PrintingAuthorityWithPublicKey> createPrintingAuthorities(int votersCount,
                                                                                ObjectMapper mapper,
                                                                                int securityLevel) {
    return IntStream.rangeClosed(0, Math.min((int) Math.log10(votersCount), 3))
                    .mapToObj(i -> {
                      try {
                        final String printingAuthorityName = String.format("PrintingAuthority%d", i);
                        return new PrintingAuthorityWithPublicKey(printingAuthorityName,
                                                                  readPrintingAuthorityVerificationKey(mapper,
                                                                                                       securityLevel, i,
                                                                                                       printingAuthorityName));
                      } catch (IOException e) {
                        throw new SimulationRuntimeException("Failed to load the printing authority's public key", e);
                      }
                    }).collect(Collectors.toList());
  }

  private static int countCountingCircles(List<Voter> voters) {
    return Math.toIntExact(voters.stream().map(Voter::getCountingCircle).distinct().count());
  }

  public abstract ElectionSetAndVoters createElectionSetAndVoters(int votersCount, ObjectMapper mapper,
                                                                  int securityLevel);


  private static BigInteger readPrintingAuthorityVerificationKey(ObjectMapper objectMapper,
                                                                 int securityLevel,
                                                                 int printingAuthorityIndex,
                                                                 String printingAuthorityName) throws IOException {
    TypeReference<Map<String, List<VerificationKey>>> typeRef =
        new TypeReference<Map<String, List<VerificationKey>>>() {
        };

    Map<String, List<VerificationKey>> rawKeys = objectMapper.readValue(
        ElectionSetFactory.class.getResourceAsStream(
            String.format("/level%d/pa%d-e.pk", securityLevel, printingAuthorityIndex)
        ),
        typeRef
    );

    return rawKeys.get(printingAuthorityName).get(0).getPublicKey();
  }

  private static final class VerificationKey {

    private final String     keyId;
    private final BigInteger publicKey;

    @JsonCreator
    public VerificationKey(@JsonProperty("keyId") String keyId,
                           @JsonProperty("publicKey") BigInteger publicKey) {
      this.keyId = keyId;
      this.publicKey = publicKey;
    }

    public String getKeyId() {
      return keyId;
    }

    public BigInteger getPublicKey() {
      return publicKey;
    }
  }
}
