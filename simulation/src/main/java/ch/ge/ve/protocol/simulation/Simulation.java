/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.config.JacksonConfiguration;
import ch.ge.ve.config.VerificationKeysConfiguration;
import ch.ge.ve.protocol.bulletinboard.DefaultBulletinBoard;
import ch.ge.ve.protocol.bulletinboard.state.BulletinBoardState;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.controlcomponent.DefaultControlComponent;
import ch.ge.ve.protocol.controlcomponent.exception.SignEncryptionException;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentCachedState;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentState;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.algorithm.VoteCastingClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationVoterAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.PartialDecryptionsAndProof;
import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.eventlog.state.CleanupCommand;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.ElectionVerificationData;
import ch.ge.ve.protocol.model.ElectionVerificationDataWithTally;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthority;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.eventlog.EventLogCommandInMemoryImpl;
import ch.ge.ve.protocol.simulation.eventlog.EventLogQueryInMemoryImpl;
import ch.ge.ve.protocol.simulation.exception.IncorrectTallyException;
import ch.ge.ve.protocol.simulation.exception.InvalidDecryptionProofException;
import ch.ge.ve.protocol.simulation.exception.SimulationException;
import ch.ge.ve.protocol.simulation.exception.SimulationRuntimeException;
import ch.ge.ve.protocol.simulation.model.ElectionSetAndVoters;
import ch.ge.ve.protocol.simulation.model.TallyDecryptionAndProof;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Simulation class...
 */
public class Simulation {
  private static final String PRINTER_FILE_NAME_FORMAT = "printer_file_%s_CC%d.epf";
  private static final String OUTPUT_FOLDER_PATH       = System.getProperty("OUTPUT_FOLDER", ".");
  private static final String PROTOCOL_ID              = "0";
  private static final Logger log                      = LoggerFactory.getLogger(Simulation.class);
  private static final Logger perfLog                  = LoggerFactory.getLogger("PerformanceStats");
  private static final String ALGO_KEYSPEC_ALGORITHM   = "AES";
  private static final String ALGO_CIPHER_PROVIDER     = "BC";
  private static final String ALGO_CIPHER_ALGORITHM    = "AES/CTR/NoPadding";
  private static final String ALGO_DIGEST_PROVIDER     = "BC";
  private static final String ALGO_DIGEST_ALGORITHM    = "BLAKE2B-256";
  private static final String ALGO_RNG_PROVIDER        = "SUN";
  private static final String ALGO_RNG_ALGORITHM       = "SHA1PRNG";

  private final int                                                             securityLevel;
  private final AlgorithmsSpec                                                  algorithmsSpec;
  private final ElectionSetFactory                                              electionSetFactory;
  private final int                                                             votersCount;
  private final RandomGenerator                                                 randomGenerator;
  private final PerformanceStats                                                performanceStats              =
      new PerformanceStats();
  private       PublicParameters                                                publicParameters;
  private       ElectionSetWithPublicKey                                        electionSet;
  private       DefaultBulletinBoard                                            bulletinBoard;
  private       KeyEstablishmentAlgorithms                                      keyEstablishmentAlgorithms;
  private       ObjectMapper                                                    jsonObjectMapper;
  private       Hash                                                            hash;
  private       GeneralAlgorithms                                               generalAlgorithms;
  private       ChannelSecurityAlgorithms                                       channelSecurityAlgorithms;
  private       VotingCardPreparationAlgorithms                                 votingCardPreparationAlgorithms;
  private       List<ControlComponent>                                          controlComponents;
  private       List<BigInteger>                                                controlComponentsPublicKeys   =
      new ArrayList<>();
  private       Map<PrintingAuthorityWithPublicKey, PrintingAuthoritySimulator> printingAuthoritySimulatorMap =
      new HashMap<>();
  private       VoteCastingClientAlgorithms                                     voteCastingClientAlgorithms;
  private       VoteConfirmationClientAlgorithms                                voteConfirmationClientAlgorithms;
  private       VoteConfirmationVoterAlgorithms                                 voteConfirmationVoterAlgorithms;
  private       List<VoterSimulator>                                            voterSimulators               =
      new ArrayList<>();
  private       DecryptionAuthorityAlgorithms                                   decryptionAuthorityAlgorithms;
  private       TallyingAuthoritiesAlgorithm                                    tallyingAuthoritiesAlgorithm;
  private       ElectionAdministrationSimulator                                 electionAdministrationSimulator;
  private       List<Voter>                                                     voters;
  private       Map<String, Map<String, BigInteger>>                            verificationKeys;

  public Simulation(int level, int votersCount, ElectionSetFactory electionSetFactory) {
    this.securityLevel = level;
    this.randomGenerator = RandomGenerator.create(ALGO_RNG_ALGORITHM, ALGO_RNG_PROVIDER);
    this.votersCount = votersCount;
    this.electionSetFactory = electionSetFactory;
    this.algorithmsSpec = new AlgorithmsSpec(
        ALGO_KEYSPEC_ALGORITHM,
        ALGO_CIPHER_PROVIDER,
        ALGO_CIPHER_ALGORITHM,
        ALGO_DIGEST_PROVIDER,
        ALGO_DIGEST_ALGORITHM,
        ALGO_RNG_PROVIDER,
        ALGO_RNG_ALGORITHM
    );

    initializeSettings(level);
    createComponents();
 }

  public static void main(String[] args) throws SimulationException {
    log.info("Starting simulation");

    int level = 1;
    int votersCount = 100;
    ElectionSetFactory electionSetFactory = ElectionSetFactory.SIMPLE_SAMPLE;
    if (args.length >= 1) {
      level = Integer.parseInt(args[0]);
    }
    if (args.length >= 2) {
      electionSetFactory = ElectionSetFactory.valueOf(args[1]);
    }
    if (args.length >= 3) {
      votersCount = Integer.parseInt(args[2]);
    }


    Security.addProvider(new BouncyCastleProvider());
    Simulation simulation = new Simulation(level, votersCount, electionSetFactory);
    simulation.run();
  }

  public void run() throws SimulationException {
    runInitialisation();

    runCodeSheets();

    Tally expectedTally = runVoting();

    runMixing();

    runDecryption();

    Tally tally = runTally();

    if (tally.equals(expectedTally)) {
      log.info("Vote simulation successful");
    } else {
      log.error("Vote simulation failed");
      throw new IncorrectTallyException("The tally doesn't match the expected result");
    }

    final List<Integer> voterIds = IntStream.range(0, votersCount).boxed().collect(Collectors.toList());

    final int s = publicParameters.getS();
    final Map<Integer, List<Point>> publicCredentials =
        IntStream.range(0, s).boxed()
                 .collect(Collectors.toMap(
                     Function.identity(),
                     j -> bulletinBoard.getPublicCredentialsParts(PROTOCOL_ID, j, voterIds)
                                       .entrySet().stream()
                                       .sorted(Map.Entry.comparingByKey())
                                       .map(Map.Entry::getValue).collect(Collectors.toList())));
    final Map<Integer, List<Encryption>> shuffles =
        IntStream.range(0, s).boxed()
                 .collect(Collectors.toMap(Function.identity(), j -> bulletinBoard.getShuffle(PROTOCOL_ID, j)));
    final Map<Integer, ShuffleProof> shuffleProofs =
        IntStream.range(0, s).boxed()
                 .collect(Collectors.toMap(Function.identity(), j -> bulletinBoard.getShuffleProof(PROTOCOL_ID, j)));

    final Map<Integer, Decryptions> partialDecryptions =
        IntStream.range(0, s + 1).boxed()
                 .collect(Collectors.toMap(Function.identity(), j -> bulletinBoard.getDecryptions(PROTOCOL_ID, j)));
    final Map<Integer, DecryptionProof> decryptionProofs =
        IntStream.range(0, s + 1).boxed()
                 .collect(Collectors.toMap(Function.identity(), j -> bulletinBoard.getDecryptionProof(PROTOCOL_ID, j)));

    ElectionVerificationData electionVerificationData = new ElectionVerificationDataWithTally(
        bulletinBoard.getPublicParameters(PROTOCOL_ID),
        bulletinBoard.getElectionSetForVerification(PROTOCOL_ID),
        bulletinBoard.getPrimes(PROTOCOL_ID),
        bulletinBoard.getGenerators(PROTOCOL_ID),
        bulletinBoard.getPublicKeyMap(PROTOCOL_ID),
        bulletinBoard.getElectionOfficerPublicKey(PROTOCOL_ID),
        publicCredentials,
        bulletinBoard.getBallots(PROTOCOL_ID, voterIds),
        bulletinBoard.getConfirmations(PROTOCOL_ID, voterIds),
        shuffles,
        shuffleProofs,
        partialDecryptions,
        decryptionProofs,
        tally);

    int bitLength = publicParameters.getEncryptionGroup().getP().bitLength();
    Path outputPath = Paths.get(".",
                                String.format("verificationData-%d-%s-%d.json",
                                              bitLength, electionSetFactory.name(), voters.size()))
                           .normalize();

    ElectionVerificationDataWriter electionVerificationDataWriter =
        new ElectionVerificationDataWriter(jsonObjectMapper);
    electionVerificationDataWriter.writeElectionVerificationData(electionVerificationData, outputPath);

    performanceStats.stop(PerformanceStats.totalSimulation);

    performanceStats.logStatSummary();
  }

  private Tally runTally() throws InvalidDecryptionProofException {
    log.info("checking tally prerequisites");
    performanceStats.start(PerformanceStats.tallyingPrereqs);
    TallyDecryptionAndProof tallyDecryptionAndProof = electionAdministrationSimulator.getTallyDecryptionAndProof();
    int electionOfficerAuthorityIndex = publicParameters.getS();

    Decryptions partialDecryption = tallyDecryptionAndProof.getPartialDecryption();
    DecryptionProof decryptionProof = tallyDecryptionAndProof.getDecryptionProof();
    Tally tally = tallyDecryptionAndProof.getTally();

    bulletinBoard.storePartialDecryptionsAndProof(PROTOCOL_ID, electionOfficerAuthorityIndex,
                                                  partialDecryption, decryptionProof);
    bulletinBoard.storeTally(PROTOCOL_ID, tally);
    BigInteger electionOfficerPublicKey =
        bulletinBoard.getPublicKeyParts(PROTOCOL_ID).get(electionOfficerAuthorityIndex).getPublicKey();
    List<Encryption> finalShuffle = bulletinBoard.getShufflesAndProofs(PROTOCOL_ID)
                                                 .getShuffles()
                                                 .get(publicParameters.getS() - 1);
    boolean isProofValid = tallyingAuthoritiesAlgorithm.checkDecryptionProof(decryptionProof, electionOfficerPublicKey,
                                                                             finalShuffle, partialDecryption);
    if (!isProofValid) {
      throw new InvalidDecryptionProofException("An invalid decryption proof was found");
    }
    performanceStats.stop(PerformanceStats.tallyingPrereqs);

    log.info("tallying votes");
    performanceStats.start(PerformanceStats.tallying);
    performanceStats.stop(PerformanceStats.tallying);

    log.info("Tally is: {}", tally);
    return tally;
  }

  private void runDecryption() {
    log.info("starting decryption");
    performanceStats.start(PerformanceStats.decryption);
    boolean decryptionSuccess = controlComponents.parallelStream().allMatch(aS -> {
      PartialDecryptionsAndProof partialDecryptionsAndProof = aS.startPartialDecryption(PROTOCOL_ID);
      bulletinBoard.storePartialDecryptionsAndProof(PROTOCOL_ID, aS.getIndex(),
                                                    partialDecryptionsAndProof.getDecryptions(),
                                                    partialDecryptionsAndProof.getProof());
      return true;
    });
    performanceStats.stop(PerformanceStats.decryption);
    log.info("decryptionSuccess: {}", decryptionSuccess);
  }

  private void runMixing() {
    log.info("starting the mixing");
    performanceStats.start(PerformanceStats.mixing);
    ShuffleAndProof previousMix = controlComponents.get(0).startMixing(PROTOCOL_ID);
    for (int j = 0; j < publicParameters.getS(); j++) {
      bulletinBoard.storeShuffleAndProof(PROTOCOL_ID, j, previousMix.getShuffle(), previousMix.getProof());
      final ShuffleAndProof innerPreviousMix = previousMix;
      final int ccIndex = j;

      List<Optional<ShuffleAndProof>> nextShuffleAndProofOptionals =
          controlComponents.parallelStream()
                           .map(cc -> cc.storeShuffleAndProof(PROTOCOL_ID, ccIndex, innerPreviousMix))
                           .collect(Collectors.toList());
      final Optional<ShuffleAndProof> previousMixOptional = nextShuffleAndProofOptionals.stream()
                                                                                        .flatMap(
                                                                                            op -> op.map(Stream::of)
                                                                                                    .orElseGet(
                                                                                                        Stream::empty))
                                                                                        .findFirst();
      if (previousMixOptional.isPresent()) {
        previousMix = previousMixOptional.get();
      }
    }
    bulletinBoard.storeGenerators(PROTOCOL_ID, controlComponents.get(0).getGenerators(PROTOCOL_ID));
    performanceStats.stop(PerformanceStats.mixing);
  }

  private Tally runVoting() {
    log.info("stating the voting phase");
    performanceStats.start(PerformanceStats.votingPhase);
    Map<CountingCircle, List<Integer>> votes =
        voterSimulators.parallelStream().collect(Collectors.toMap(
            simulator -> getCountingCircle(simulator.getVoterIndex()),
            VoterSimulator::vote,
            (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList())
        ));
    performanceStats.stop(PerformanceStats.votingPhase);
    Map<CountingCircle, Map<Integer, Long>> expectedVotesCountMap =
        votes.entrySet().parallelStream()
             .collect(Collectors.toMap(
                 Map.Entry::getKey,
                 e -> {
                   Map<Integer, Long> expectedVoteCounts = new HashMap<>();
                   e.getValue().forEach(i -> expectedVoteCounts.compute(i - 1, (k, v) -> (v == null) ? 1 : v + 1));
                   return expectedVoteCounts;
                 }));
    Map<CountingCircle, List<Long>> expectedTally =
        expectedVotesCountMap.entrySet().parallelStream()
                             .collect(Collectors.toMap(
                                 Map.Entry::getKey,
                                 e -> IntStream.range(0, electionSet.getCandidates().size())
                                               .mapToObj(i -> e.getValue().computeIfAbsent(i, k -> 0L))
                                               .collect(Collectors.toList())));
    log.info("Expected results are: {}", expectedTally);
    return new Tally(expectedTally);
  }

  private CountingCircle getCountingCircle(int voterIndex) {
    return voters.get(voterIndex).getCountingCircle();
  }

  private void runCodeSheets() {
    log.info("printing code sheets...");
    performanceStats.start(PerformanceStats.printingCodeSheets);
    List<Map<PrintingAuthorityWithPublicKey, byte[]>> list = controlComponents.stream().map(c -> {
      try {
        return c.getSignEncryptedPrivateCredentials(PROTOCOL_ID, voters.stream().map(Voter::getVoterId).collect(
            Collectors.toList()));
      } catch (SignEncryptionException e) {
        throw new SimulationRuntimeException("Failed to retrieved the signEncrypted credentials", e);
      }
    }).collect(Collectors.toList());
    for (Map.Entry<PrintingAuthorityWithPublicKey, PrintingAuthoritySimulator> entry : printingAuthoritySimulatorMap
        .entrySet()) {
      List<byte[]> printingAuthorityVoterCredentials =
          list.stream().map(m -> m.computeIfAbsent(entry.getKey(), k -> new byte[0])).collect(Collectors.toList());
      for (int i = 0; i < printingAuthorityVoterCredentials.size(); i++) {
        savePrinterFile(entry.getKey().getName(), i, printingAuthorityVoterCredentials.get(i));
      }
      entry.getValue().print(printingAuthorityVoterCredentials);
    }
    performanceStats.stop(PerformanceStats.printingCodeSheets);
  }

  private void savePrinterFile(String printingAuthorityName, int controlComponentIndex, byte[] printerData) {
    String printerFileName = String.format(PRINTER_FILE_NAME_FORMAT, printingAuthorityName, controlComponentIndex);
    Path outputPath = Paths.get(OUTPUT_FOLDER_PATH, printerFileName).normalize();
    try {
      Files.write(outputPath, printerData);
      log.info("Printer file written at {}", outputPath);
    } catch (IOException e) {
      throw new SimulationRuntimeException("Failed to write printer file " + outputPath, e);
    }
  }


  private void runInitialisation() {
    publishPublicParameters();
    runKeyGeneration();
    publishElectionSet();
    runCredentialsGeneration();
  }

  private void publishPublicParameters() {
    log.info("publishing public parameters");
    performanceStats.start(PerformanceStats.publishingParameters);
    bulletinBoard.storePublicParameters(PROTOCOL_ID, publicParameters);
    controlComponents.parallelStream().forEach(cc -> cc.storePublicParameters(PROTOCOL_ID, publicParameters));
    performanceStats.stop(PerformanceStats.publishingParameters);
  }

  private void runKeyGeneration() {
    log.info("generating authorities keys");
    performanceStats.start(PerformanceStats.keyGeneration);
    // parallelStream.forEach returns early, this is a workaround so that the call returns once all items have been
    // processed
    long keyGenerationCount =
        controlComponents.parallelStream()
                         .peek(cc -> {
                           EncryptionPublicKey pk = cc.generateKeys(PROTOCOL_ID);
                           bulletinBoard.storeKeyPart(PROTOCOL_ID, cc.getIndex(), pk);
                           controlComponents.parallelStream().forEach(
                               controlComponent -> controlComponent.storeKeyPart(PROTOCOL_ID, cc.getIndex(), pk));
                         })
                         .count();

    assert (keyGenerationCount == publicParameters.getS());

    log.info("generating election officer key");
    electionAdministrationSimulator.generateElectionOfficerKey();
    EncryptionPublicKey electionOfficerPublicKey = electionAdministrationSimulator.getElectionOfficerPublicKey();
    bulletinBoard.storeElectionOfficerPublicKey(PROTOCOL_ID, electionOfficerPublicKey);
    controlComponents.parallelStream()
                     .forEach(cc -> cc.storeElectionOfficerPublicKey(PROTOCOL_ID, electionOfficerPublicKey));
    performanceStats.stop(PerformanceStats.keyGeneration);
    log.info("keyGenerationSuccess");
  }

  private void publishElectionSet() {
    log.info("publishing election set");
    performanceStats.start(PerformanceStats.publishElectionSet);
    bulletinBoard.storeElectionSet(PROTOCOL_ID, electionSet);
    controlComponents.parallelStream()
                     .forEach(cc -> cc.storeElectionSet(PROTOCOL_ID, electionSet));
    performanceStats.stop(PerformanceStats.publishElectionSet);
  }

  private void runCredentialsGeneration() {
    log.info("generating electorate data and public credentials");
    performanceStats.start(PerformanceStats.generatingElectoralData);
    List<BigInteger> primes = null;
    for (int j = 0; j < controlComponents.size(); j++) {
      int ccIndex = j;
      ControlComponent controlComponent = controlComponents.get(j);
      List<VoterData> electorateData = controlComponent.generateVoterData(PROTOCOL_ID, voters);
      Map<Integer, Point> points = electorateData.stream()
                                                 .collect(Collectors.toMap(
                                                     VoterData::getVoterId,
                                                     VoterData::getPublicVoterData
                                                 ));
      bulletinBoard.storePublicCredentials(PROTOCOL_ID, j, points);
      controlComponents.parallelStream()
                       .forEach(cc -> cc.storePublicCredentialsPart(PROTOCOL_ID, ccIndex, points));
      try {
        primes = controlComponent.generatePrimes(PROTOCOL_ID);
      } catch (NotEnoughPrimesInGroupException e) {
        throw new SimulationRuntimeException("Failed to generate the primes", e);
      }
    }
    controlComponents.parallelStream().forEach(cc -> cc
        .buildPublicCredentials(PROTOCOL_ID, voters.stream().map(Voter::getVoterId).collect(Collectors.toList()))
    );
    bulletinBoard.storePrimes(PROTOCOL_ID, primes);
    performanceStats.stop(PerformanceStats.generatingElectoralData);
  }

  private void createComponents() {
    log.info("creating components");
    createUtilities();
    createAlgorithms();
    createServices();
    createSimulators();
    log.info("created components");
  }

  private void createSimulators() {
    log.info("creating simulators");
    List<PrintingAuthorityWithPublicKey> printingAuthorities = electionSet.getPrintingAuthorities();
    IntStream.range(0, printingAuthorities.size()).parallel().forEach(i -> {
      try {
        // TODO: protect key with a password
        IdentificationPrivateKey decryptionKey = jsonObjectMapper.readValue(
            Simulation.class.getResourceAsStream(String.format("/level%d/pa%d-e.sk", securityLevel, i)),
            IdentificationPrivateKey.class);

        PrintingAuthorityWithPublicKey printingAuthority = printingAuthorities.get(i);
        PrintingAuthoritySimulator simulator =
            new PrintingAuthoritySimulator(PROTOCOL_ID, decryptionKey, bulletinBoard,
                                           votingCardPreparationAlgorithms, channelSecurityAlgorithms,
                                           jsonObjectMapper);
        simulator.setControlComponentPublicKeys(controlComponentsPublicKeys);
        printingAuthoritySimulatorMap.put(printingAuthority, simulator);
      } catch (IOException e) {
        throw new SimulationRuntimeException("Failed to load printing authorities private keys", e);
      }
    });

    VoteReceiverSimulator voteReceiverSimulator =
        new VoteReceiverSimulator(PROTOCOL_ID, bulletinBoard, controlComponents,
                                  publicParameters, electionSet, voters);
    Map<PrintingAuthority, List<Voter>> votersByPrintingAuthority =
        voters.stream().collect(Collectors.groupingBy(
            v -> printingAuthorities.stream()
                                    .filter(p -> p.getName().equals(v.getPrintingAuthorityName()))
                                    .findFirst()
                                    .orElseThrow(
                                        () -> new SimulationRuntimeException("no matching printing authority"))));

    for (Map.Entry<PrintingAuthority, List<Voter>> entry : votersByPrintingAuthority.entrySet()) {
      List<VoterSimulator> paVoterSimulators = entry.getValue().stream().map(v -> {
        VotingClient votingClient = new VotingClient(voteReceiverSimulator, keyEstablishmentAlgorithms,
                                                     voteCastingClientAlgorithms, voteConfirmationClientAlgorithms);
        return new VoterSimulator(v.getVoterId(), votingClient, voteConfirmationVoterAlgorithms);
      }).collect(Collectors.toList());
      final PrintingAuthorityWithPublicKey key = (PrintingAuthorityWithPublicKey) entry.getKey();

      printingAuthoritySimulatorMap.get(key).setVoterSimulators(paVoterSimulators);
      voterSimulators.addAll(paVoterSimulators);
    }

    electionAdministrationSimulator = new ElectionAdministrationSimulator(PROTOCOL_ID,
                                                                          electionSet.getCandidates().size(),
                                                                          bulletinBoard, generalAlgorithms,
                                                                          keyEstablishmentAlgorithms,
                                                                          tallyingAuthoritiesAlgorithm,
                                                                          decryptionAuthorityAlgorithms, electionSet,
                                                                          voters);
    log.info("all simulators created");
  }

  private void createServices() {
    log.info("creating services");
    final JsonConverter jsonConverter = new JsonConverter(jsonObjectMapper);

    final EventLogCommandInMemoryImpl bbEventLogCommand = new EventLogCommandInMemoryImpl();
    final EventLogQueryInMemoryImpl bbEventLogQuery = new EventLogQueryInMemoryImpl(bbEventLogCommand);
    bulletinBoard = createBulletinBoard(bbEventLogCommand, bbEventLogQuery, jsonConverter);

    controlComponents = IntStream.range(0, publicParameters.getS())
                                 .mapToObj(j -> {
                                   try {

                                     final Map<String, BigInteger> ccKeys = verificationKeys.get(String.format("cc%d", j));
                                     controlComponentsPublicKeys.add(ccKeys.get(ccKeys.keySet().iterator().next()));

                                     IdentificationPrivateKey signingKey = jsonObjectMapper.readValue(
                                         Simulation.class.getResourceAsStream(
                                             String.format("/level%d/cc%d.1-s.sk", securityLevel, j)),
                                         IdentificationPrivateKey.class
                                     );

                                     final EventLogCommandInMemoryImpl ccEventLogCommand =
                                         new EventLogCommandInMemoryImpl();
                                     final EventLogQueryInMemoryImpl ccEventLogQuery =
                                         new EventLogQueryInMemoryImpl(ccEventLogCommand);

                                     final ControlComponentState state =
                                         new ControlComponentState(ccEventLogCommand, ccEventLogQuery, jsonConverter,
                                                                   new CleanupCommand(ccEventLogCommand));
                                     return new DefaultControlComponent(j,
                                                                        25,
                                                                        algorithmsSpec,
                                                                        jsonObjectMapper,
                                                                        randomGenerator,
                                                                        state,
                                                                        new ControlComponentCachedState(state),
                                                                        signingKey,
                                                                        () -> {
                                                                          // do nothing
                                                                        });
                                   } catch (IOException e) {
                                     throw new SimulationRuntimeException(
                                         String
                                             .format("Failed to load the control components certificates / keys (%d)",
                                                     j + 1), e);
                                   }
                                 })
                                 .collect(Collectors.toList());
    log.info("created all services");
  }

  private DefaultBulletinBoard createBulletinBoard(EventLogCommandInMemoryImpl eventLogCommand,
                                                   EventLogQueryInMemoryImpl eventLogQuery,
                                                   JsonConverter jsonConverter) {
    return new DefaultBulletinBoard(new BulletinBoardState(eventLogCommand, eventLogQuery, jsonConverter,
                                                           new CleanupCommand(eventLogCommand)));
  }

  private void createAlgorithms() {
    log.info("instantiating algorithms classes");
    int countingCicleCount = electionSet.getCountingCircleCount();
    try {
      PrimesCache primesCache = PrimesCache.populate(electionSet.getCandidates().size() + countingCicleCount,
                                                     publicParameters.getEncryptionGroup());
      generalAlgorithms = new GeneralAlgorithms(hash, publicParameters.getEncryptionGroup(),
                                                publicParameters.getIdentificationGroup(), primesCache);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new SimulationRuntimeException("Failed to create general algorithms", e);
    }

    try {
      channelSecurityAlgorithms =
          new ChannelSecurityAlgorithms(algorithmsSpec,
                                        hash,
                                        publicParameters.getIdentificationGroup(),
                                        randomGenerator);
    } catch (ChannelSecurityException e) {
      throw new SimulationRuntimeException("Failed to create channel security algorithms", e);
    }

    keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator);
    votingCardPreparationAlgorithms = new VotingCardPreparationAlgorithms(publicParameters);
    voteCastingClientAlgorithms =
        new VoteCastingClientAlgorithms(publicParameters, generalAlgorithms, randomGenerator, hash);
    voteConfirmationClientAlgorithms =
        new VoteConfirmationClientAlgorithms(publicParameters, generalAlgorithms, randomGenerator);
    voteConfirmationVoterAlgorithms = new VoteConfirmationVoterAlgorithms();
    decryptionAuthorityAlgorithms =
        new DecryptionAuthorityAlgorithms(publicParameters, generalAlgorithms, randomGenerator);
    tallyingAuthoritiesAlgorithm = new TallyingAuthoritiesAlgorithm(publicParameters, generalAlgorithms);
    log.info("instantiated all algorithm classes");
  }

  private void createUtilities() {
    hash = new Hash(algorithmsSpec.getDigestAlgorithm(),
                    algorithmsSpec.getDigestProvider(),
                    publicParameters.getSecurityParameters().getUpper_l());
  }

  private void createJsonObjectMapper() {
    JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();
    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    builder.featuresToEnable(SerializationFeature.INDENT_OUTPUT);
    jacksonConfiguration.protocolJacksonCustomizer().customize(builder);
    jsonObjectMapper = builder.build();
  }

  private void initializeSettings(int level) {
    performanceStats.start(PerformanceStats.totalSimulation);
    log.info("Initializing settings");
    createJsonObjectMapper();
    performanceStats.start(PerformanceStats.creatingPublicParameters);
    createPublicParameters(level);
    performanceStats.stop(PerformanceStats.creatingPublicParameters);
    performanceStats.start(PerformanceStats.creatingElectionSet);
    ElectionSetAndVoters electionSetAndVoters =
        electionSetFactory.createElectionSetAndVoters(votersCount, jsonObjectMapper, level);
    electionSet = electionSetAndVoters.getElectionSet();
    voters = electionSetAndVoters.getVoters();
    performanceStats.stop(PerformanceStats.creatingElectionSet);
    // load verification keys
    VerificationKeysConfiguration verificationKeysConfiguration = new VerificationKeysConfiguration();
    this.verificationKeys = verificationKeysConfiguration.verificationKeys(securityLevel, algorithmsSpec, jsonObjectMapper);
    log.info("Settings initialized");
  }

  private void createPublicParameters(int level) {
    log.info("creating public parameters...");
    publicParameters = PublicParametersFactory.forLevel(level).createPublicParameters();
    log.info("public parameters created");
  }

  private class PerformanceStats {
    private static final String CLIENT                   = "client";
    private static final String SERVER                   = "server";
    private static final String creatingPublicParameters = "creating public parameters";
    private static final String creatingElectionSet      = "creating election set";
    private static final String publishingParameters     = "publishing parameters";
    private static final String keyGeneration            = "key generation";
    private static final String publishElectionSet       = "publish election set";
    private static final String generatingElectoralData  = "generating electoral data";
    private static final String printingCodeSheets       = "printing code sheets";
    private static final String votingPhase              = "voting phase";
    private static final String mixing                   = "mixing";
    private static final String decryption               = "decryption";
    private static final String tallyingPrereqs          = "tallying prerequisites";
    private static final String tallying                 = "tallying";
    private static final String totalSimulation          = "total simulation time";

    private final Map<String, Stopwatch> stopwatches = new HashMap<>();

    void start(String name) {
      stopwatches.compute(name, (k, v) -> (v == null) ? Stopwatch.createStarted() : v.start());
    }

    void stop(String name) {
      stopwatches.compute(name, (k, v) -> (v == null) ? Stopwatch.createUnstarted() : v.stop());
    }

    private long getElapsed(String name, TimeUnit timeUnit) {
      return stopwatches.compute(name, (k, v) -> (v == null) ? Stopwatch.createUnstarted() : v).elapsed(timeUnit);
    }

    void logStatSummary() {
      List<String> elements = Arrays.asList(creatingPublicParameters, creatingElectionSet, publishingParameters,
                                            keyGeneration, publishElectionSet,
                                            generatingElectoralData,
                                            printingCodeSheets, votingPhase, mixing, decryption,
                                            tallyingPrereqs, tallying, totalSimulation);
      if (perfLog.isInfoEnabled()) {
        perfLog.info("##### Performance statistics");
        perfLog.info("");
        perfLog.info("- using LibGMP: {}", BigIntegerArithmetic.isGmpLoaded());
        perfLog.info("- length of p: {}", publicParameters.getEncryptionGroup().getP().bitLength());
        perfLog.info("- number of voters: {}", voters.size());
        List<String> electionDescriptions = electionSet.getElections().stream()
                                                       .map(e -> e.getNumberOfSelections() + "-out-of-" + e
                                                           .getNumberOfCandidates())
                                                       .collect(Collectors.toList());
        perfLog.info("- elections: {}", Joiner.on(", ").join(electionDescriptions));
        perfLog.info("");
        perfLog.info(String.format("| %-30s | %15s |", "Step name", "Time taken (ms)"));
        perfLog.info(String.format("| %1$.30s | %1$.14s: |", Strings.repeat("-", 30)));
        elements
            .forEach(k -> perfLog.info(String.format("| %-30s | %,15d |", k, getElapsed(k, TimeUnit.MILLISECONDS))));

        perfLog.info("");
      }

      List<DefaultControlComponent> defaultAuthorities = controlComponents.stream()
                                                                          .map(a -> ((DefaultControlComponent) a))
                                                                          .collect(Collectors.toList());
      List<VotingClient> defaultVotingClients = voterSimulators.stream()
                                                               .map(VoterSimulator::getVotingClient)
                                                               .collect(Collectors.toList());
      logDetailedStats(defaultAuthorities, defaultVotingClients);
    }

    private void logDetailedStats(List<DefaultControlComponent> defaultAuthorities, List<VotingClient> votingClients) {
      LongSummaryStatistics voteEncodingStats = computeStats(votingClients,
                                                             VotingClient.Stats::getVoteEncodingTime);
      LongSummaryStatistics ballotVerificationStats = combineStatistics(defaultAuthorities,
                                                                        DefaultControlComponent::getBallotVerificationStats);
      LongSummaryStatistics queryResponseStats = combineStatistics(defaultAuthorities,
                                                                   DefaultControlComponent::getQueryResponseStats);
      LongSummaryStatistics verificationCodesComputationStats = computeStats(votingClients,
                                                                             VotingClient
                                                                                 .Stats::getVerificationCodesComputationTime);
      LongSummaryStatistics confirmationEncodingStats = computeStats(votingClients,
                                                                     VotingClient.Stats::getConfirmationEncodingTime);
      LongSummaryStatistics confirmationVerificationStats = combineStatistics(defaultAuthorities,
                                                                              DefaultControlComponent::getConfirmationVerificationStats);
      LongSummaryStatistics finalizationComputationStats = combineStatistics(defaultAuthorities,
                                                                             DefaultControlComponent::getFinalizationComputationStats);
      LongSummaryStatistics finalizationCodeComputationStats = computeStats(votingClients,
                                                                            VotingClient
                                                                                .Stats::getFinalizationCodeComputationTime);

      if (perfLog.isInfoEnabled()) {
        perfLog.info("###### Voting phase details");
        perfLog.info(String
                         .format("| %-30s | %-20s | %15s | %15s | %15s | %15s |", "Step name", "Performed by",
                                 "Total time",
                                 "Min", "Avg", "Max"));
        perfLog.info(String.format("| %1$.30s | %1$.20s | %1$.14s: | %1$.14s: | %1$.14s: | %1$.14s: |",
                                   Strings.repeat("-", 30)));
      }
      logStats("vote encoding", CLIENT, voteEncodingStats);
      logStats("ballot verification", SERVER, ballotVerificationStats);
      logStats("query response", SERVER, queryResponseStats);
      logStats("verification codes computation", CLIENT, verificationCodesComputationStats);
      logStats("confirmation encoding", CLIENT, confirmationEncodingStats);
      logStats("confirmation verification", SERVER, confirmationVerificationStats);
      logStats("finalization code parts", SERVER, finalizationComputationStats);
      logStats("finalization code computation", CLIENT, finalizationCodeComputationStats);
    }

    private void logStats(String stepName, String performedBy, LongSummaryStatistics stats) {
      if (perfLog.isInfoEnabled()) {
        perfLog.info(String.format("| %-30s | %-20s | %,15d | %,15d | %,15.2f | %,15d |",
                                   stepName,
                                   performedBy,
                                   stats.getSum(), stats.getMin(), stats.getAverage(), stats.getMax()));
      }
    }

    private LongSummaryStatistics computeStats(List<VotingClient> votingClients, ToLongFunction<VotingClient.Stats>
        getVoteEncodingTime) {
      return votingClients.stream().map(VotingClient::getStats).mapToLong(getVoteEncodingTime).summaryStatistics();
    }

    private LongSummaryStatistics combineStatistics(List<DefaultControlComponent> defaultAuthorities,
                                                    Function<DefaultControlComponent, LongSummaryStatistics>
                                                        extractionFunction) {
      return defaultAuthorities.stream().map(extractionFunction)
                               .reduce(new LongSummaryStatistics(), combineLongStats());
    }

    private BinaryOperator<LongSummaryStatistics> combineLongStats() {
      return (a, b) -> {
        LongSummaryStatistics result = new LongSummaryStatistics();
        result.combine(a);
        result.combine(b);
        return result;
      };
    }
  }
}
