/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation.model;

import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.Tally;

/**
 * This model class is used to combine the tally with the proofs of the validity of the decryptions (by counting circle)
 * using the election officer key.
 */
public final class TallyDecryptionAndProof {
  private final Tally           tally;
  private final Decryptions     partialDecryption;
  private final DecryptionProof decryptionProof;

  public TallyDecryptionAndProof(Tally tally, Decryptions partialDecryption, DecryptionProof decryptionProof) {
    this.tally = tally;
    this.partialDecryption = partialDecryption;
    this.decryptionProof = decryptionProof;
  }

  public Tally getTally() {
    return tally;
  }

  public Decryptions getPartialDecryption() {
    return partialDecryption;
  }

  public DecryptionProof getDecryptionProof() {
    return decryptionProof;
  }
}
