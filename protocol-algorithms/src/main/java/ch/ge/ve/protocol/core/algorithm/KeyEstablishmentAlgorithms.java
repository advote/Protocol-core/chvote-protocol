/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;

import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.IdentificationKeyPair;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.CyclicGroup;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.IdentificationPublicKey;
import ch.ge.ve.protocol.model.PublicKey;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.security.KeyPair;
import java.util.List;

/**
 * Algorithms used during the key establishment part of the election preparation phase
 */
public class KeyEstablishmentAlgorithms {
  private final RandomGenerator randomGenerator;

  public KeyEstablishmentAlgorithms(RandomGenerator randomGenerator) {
    this.randomGenerator = randomGenerator;
  }

  /**
   * Algorithm 7.15: genKeyPair with an encryption group
   *
   * @param eg the encryption group for which we need a {@link KeyPair}
   *
   * @return a newly, randomly generated EncryptionKeyPair
   */
  public EncryptionKeyPair generateKeyPair(EncryptionGroup eg) {
    BigInteger sk = randomGenerator.randomInZq(eg.getQ());
    BigInteger pk = modExp(eg.getG(), sk, eg.getP());

    return new EncryptionKeyPair(new EncryptionPublicKey(pk, eg), new EncryptionPrivateKey(sk, eg));
  }

  /**
   * Algorithm 7.15: genKeyPair with an identification group
   *
   * @param ig the identification group for which we need a {@link KeyPair}
   *
   * @return a newly, randomly generated SignatureKeyPair
   */
  public IdentificationKeyPair generateKeyPair(IdentificationGroup ig) {
    BigInteger sk = randomGenerator.randomInZq(ig.getQ_hat());
    BigInteger pk = computePublickey(sk, ig);

    return new IdentificationKeyPair(new IdentificationPublicKey(pk, ig), new IdentificationPrivateKey(sk, ig));
  }

  /**
   * Compute the public key corresponding to the given private key and identification group
   *
   * @param sk the concerned private key
   * @param ig the concerned identification group
   *
   * @return the corresponding public key
   */
  public BigInteger computePublickey(BigInteger sk, IdentificationGroup ig) {
    return modExp(ig.getG_hat(), sk, ig.getP_hat());
  }

  /**
   * Algorithm 7.16: GetPublicKey
   *
   * @param publicKeys the set of public key shares that should be combined
   *
   * @return the combined public key
   */
  public PublicKey getPublicKey(List<? extends PublicKey> publicKeys) {
    BigInteger publicKey = BigInteger.ONE;
    CyclicGroup cg = publicKeys.get(0).getCyclicGroup();
    Preconditions.checkArgument(publicKeys.stream().allMatch(pk -> pk.getCyclicGroup().equals(cg)),
                                "All of the public keys should be defined within the same cyclic group");

    for (PublicKey key : publicKeys) {
      publicKey = publicKey.multiply(key.getPublicKey()).mod(cg.getModulus());
    }

    if (cg instanceof EncryptionGroup) {
      return new EncryptionPublicKey(publicKey, (EncryptionGroup) cg);
    } else {
      return new IdentificationPublicKey(publicKey, (IdentificationGroup) cg);
    }
  }
}
