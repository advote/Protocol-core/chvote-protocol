/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;

import ch.ge.ve.protocol.core.exception.IncompatibleParametersRuntimeException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.BallotList;
import ch.ge.ve.protocol.core.model.CiphertextMatrixBuilder;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponseAndRand;
import ch.ge.ve.protocol.core.model.PublicIdentificationCredentialsList;
import ch.ge.ve.protocol.core.model.VoterList;
import ch.ge.ve.protocol.core.support.ByteArrayUtils;
import ch.ge.ve.protocol.core.support.Conversion;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.BigIntPair;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.NonInteractiveZkp;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthority;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Algorithms related to the vote casting phase, performed by the authorities
 */
public class VoteCastingAuthorityAlgorithms {
  private final PublicParameters                         publicParameters;
  private final ElectionSet<? extends PrintingAuthority> electionSet;
  private final VoterList                                voterList;
  private final GeneralAlgorithms                        generalAlgorithms;
  private final RandomGenerator                          randomGenerator;
  private final Hash                                     hash;
  private final Conversion conversion = new Conversion();

  public VoteCastingAuthorityAlgorithms(PublicParameters publicParameters,
                                        ElectionSet<? extends PrintingAuthority> electionSet,
                                        VoterList voterList, GeneralAlgorithms generalAlgorithms,
                                        RandomGenerator randomGenerator,
                                        Hash hash) {
    this.publicParameters = publicParameters;
    this.electionSet = electionSet;
    this.voterList = voterList;
    this.generalAlgorithms = generalAlgorithms;
    this.randomGenerator = randomGenerator;
    this.hash = hash;
  }

  /**
   * Algorithm 7.22: CheckBallot
   * <p>
   * The additional parameters bold_upper_k defined in the specification is implied by the electionSet provided to this
   * object's constructor (it is the same for every execution of this algorithm for a given protocol instance).
   *
   * @param i          the voter index
   * @param alpha      the submitted ballot, including the oblivious transfer query
   * @param pk         the encryption public key
   * @param bold_x_hat the vector of public voter credentials
   * @param upper_b    the current ballot list
   *
   * @return true if the ballot was valid
   */
  public boolean checkBallot(Integer i, BallotAndQuery alpha, EncryptionPublicKey pk,
                             PublicIdentificationCredentialsList bold_x_hat, BallotList upper_b) {
    Preconditions.checkNotNull(i);
    Preconditions.checkNotNull(alpha);
    List<BigIntPair> bold_a = alpha.getBold_a();
    Preconditions.checkNotNull(bold_a);
    Preconditions.checkArgument(
        bold_a.stream().flatMap(a -> Stream.of(a.getLeft(), a.getRight())).allMatch(generalAlgorithms::isMember),
        "All of the a_j_{1,2}'s must be members of G_q");

    int numberOfSelections = bold_a.size();
    Preconditions.checkArgument(numberOfSelections > 0);
    Voter voter = voterList.getVoter(i).orElseThrow(() -> new IllegalStateException("No voter found for id " + i));
    int k_i = electionSet.getElections().stream().filter(e -> electionSet.isEligible(voter, e))
                         .mapToInt(Election::getNumberOfSelections).sum();
    Preconditions.checkArgument(numberOfSelections == k_i,
                                "A voter may not submit more than his allowed number of selections");
    Preconditions.checkNotNull(pk);
    Preconditions.checkNotNull(bold_x_hat);
    Preconditions.checkNotNull(upper_b);

    BigInteger x_hat_i = bold_x_hat.get(i);

    if (!hasBallot(i, upper_b) && alpha.getX_hat().compareTo(x_hat_i) == 0) {
      return checkBallotProof(alpha.getPi(), alpha.getX_hat(), bold_a, pk);
    }
    return false;
  }

  /**
   * Algorithm 7.23: HasBallot
   *
   * @param i       the voter index
   * @param upper_b the current ballot list
   *
   * @return true if any ballot in the list matches the given voter index, false otherwise
   */
  public boolean hasBallot(Integer i, BallotList upper_b) {
    Preconditions.checkNotNull(i);
    Preconditions.checkNotNull(upper_b);

    return upper_b.containsBallot(i);
  }

  /**
   * Algorithm 7.24: CheckBallotProof
   *
   * @param pi     the proof
   * @param x_hat  public voting credential
   * @param bold_a the oblivious transfer query containing the encrypted vote
   * @param pk     the encryption public key
   *
   * @return true if the proof is valid, false otherwise
   */
  public boolean checkBallotProof(NonInteractiveZkp pi, BigInteger x_hat,
                                  List<BigIntPair> bold_a, EncryptionPublicKey pk) {
    Preconditions.checkNotNull(pi);
    List<BigInteger> t = pi.getT();
    Preconditions.checkNotNull(t);
    List<BigInteger> s = pi.getS();
    Preconditions.checkNotNull(s);
    Preconditions.checkNotNull(x_hat);
    Preconditions.checkNotNull(pk);
    Preconditions.checkNotNull(pk.getPublicKey());
    Preconditions.checkArgument(t.size() == 3, "t contains three elements");
    Preconditions.checkArgument(generalAlgorithms.isMember_G_q_hat(t.get(0)),
                                "t_1 must be in G_q_hat");
    Preconditions.checkArgument(generalAlgorithms.isMember(t.get(1)),
                                "t_2 must be in G_q");
    Preconditions.checkArgument(generalAlgorithms.isMember(t.get(2)),
                                "t_3 must be in G_q");

    Preconditions.checkArgument(s.size() == 3, "s contains three elements");
    BigInteger s_1 = s.get(0);
    BigInteger s_2 = s.get(1);
    BigInteger s_3 = s.get(2);
    Preconditions.checkArgument(generalAlgorithms.isInZ_q_hat(s_1), "s_1 must be in Z_q_hat");
    Preconditions.checkArgument(generalAlgorithms.isMember(s_2), "s_2 must be in G_q");
    Preconditions.checkArgument(generalAlgorithms.isInZ_q(s_3), "s_3 must be in Z_q");

    Preconditions.checkArgument(pk.getCyclicGroup().equals(publicParameters.getEncryptionGroup()));

    BigInteger p = publicParameters.getEncryptionGroup().getP();
    BigInteger g = publicParameters.getEncryptionGroup().getG();
    BigInteger p_hat = publicParameters.getIdentificationGroup().getP_hat();
    BigInteger g_hat = publicParameters.getIdentificationGroup().getG_hat();
    int tau = publicParameters.getSecurityParameters().getTau();

    Object[] y = new Object[]{x_hat, bold_a};
    BigInteger[] t_array = new BigInteger[3];
    t.toArray(t_array);
    BigInteger c = generalAlgorithms.getNIZKPChallenge(y, t_array, tau);

    Function<BigIntPair, BigInteger> left = BigIntPair::getLeft;
    Function<BigIntPair, BigInteger> right = BigIntPair::getRight;

    BigInteger a_1 = bold_a.stream().map(left).reduce(BigInteger.ONE, BigInteger::multiply).mod(p);
    BigInteger a_2 = bold_a.stream().map(right).reduce(BigInteger.ONE, BigInteger::multiply).mod(p);

    BigInteger t_prime_1 = modExp(x_hat, c.negate(), p_hat).multiply(modExp(g_hat, s_1, p_hat)).mod(p_hat);
    BigInteger t_prime_2 = modExp(a_1, c.negate(), p).multiply(s_2).multiply(modExp(pk.getPublicKey(), s_3, p)).mod(p);
    BigInteger t_prime_3 = modExp(a_2, c.negate(), p).multiply(modExp(g, s_3, p)).mod(p);

    return t_array[0].compareTo(t_prime_1) == 0
           && t_array[1].compareTo(t_prime_2) == 0
           && t_array[2].compareTo(t_prime_3) == 0;
  }

  /**
   * Algorithm 7.25: GenResponse
   *
   * @param bold_a         the vector of the queries
   * @param pk             the encryption public key
   * @param bold_n         the vector of number of candidates per election
   * @param bold_K_i       the vector of number of selections per election for current voter
   * @param upper_bold_p_i the vector of points per candidate for current voter
   *
   * @return the OT response, along with the randomness used
   *
   * @throws IncompatibleParametersRuntimeException if not enough primes exist in the encryption group for the number of
   *                                                candidates
   */
  public ObliviousTransferResponseAndRand genResponse(List<BigIntPair> bold_a,
                                                      EncryptionPublicKey pk,
                                                      List<Integer> bold_n,
                                                      List<Integer> bold_K_i,
                                                      List<Point> upper_bold_p_i) {
    BigInteger q = publicParameters.getEncryptionGroup().getQ();

    BigIntPair z = new BigIntPair(randomGenerator.randomInZq(q), randomGenerator.randomInZq(q));
    return computeResponse(bold_a, pk, bold_n, bold_K_i, upper_bold_p_i, z);
  }

  /**
   * This method was added for idempotency.
   * If we receive an identical ballot twice it might simply be a sign of a communication error, and therefore the
   * exact same response as the first time should be generated and sent!
   *
   * @param bold_a         the vector of the queries
   * @param pk             the encryption public key
   * @param bold_n         the vector of number of candidates per election
   * @param bold_K_i       the vector of number of selections per election for current voter
   * @param upper_bold_p_i the vector of points per candidate for current voter
   * @param z              the randomness used for the previous iteration
   *
   * @return the OT response, along with the randomness used (the same z that was passed as a parameter)
   *
   * @throws IncompatibleParametersRuntimeException if not enough primes exist in the encryption group for the number of
   *                                                candidates
   */
  public ObliviousTransferResponseAndRand reComputeResponse(List<BigIntPair> bold_a,
                                                            EncryptionPublicKey pk,
                                                            List<Integer> bold_n,
                                                            List<Integer> bold_K_i,
                                                            List<Point> upper_bold_p_i,
                                                            BigIntPair z) {
    return computeResponse(bold_a, pk, bold_n, bold_K_i, upper_bold_p_i, z);
  }

  private ObliviousTransferResponseAndRand computeResponse(List<BigIntPair> bold_a,
                                                           EncryptionPublicKey pk,
                                                           List<Integer> bold_n,
                                                           List<Integer> bold_K_i,
                                                           List<Point> upper_bold_p_i,
                                                           BigIntPair z) {
    Preconditions.checkArgument(bold_a.stream()
                                      .flatMap(a -> Stream.of(a.getLeft(), a.getRight()))
                                      .allMatch(generalAlgorithms::isMember),
                                "All queries a_i must be in G_q^2");
    Preconditions.checkArgument(pk.getPublicKey().compareTo(BigInteger.ONE) != 0,
                                "The encryption key may not be 1");
    Preconditions.checkArgument(generalAlgorithms.isMember(pk.getPublicKey()),
                                "The public key must be a member of G_q");

    BigInteger p_prime = publicParameters.getPrimeField().getP_prime();

    Preconditions.checkArgument(upper_bold_p_i.stream().filter(Objects::nonNull)
                                              .allMatch(point -> BigInteger.ZERO.compareTo(point.x) <= 0
                                                                 && point.x.compareTo(p_prime) < 0
                                                                 && BigInteger.ZERO.compareTo(point.y) <= 0
                                                                 && point.y.compareTo(p_prime) < 0),
                                "All points' coordinates must be in Z_p_prime");
    Preconditions.checkArgument(!bold_K_i.isEmpty());
    final int t = bold_K_i.size();

    final int n = bold_n.stream().reduce((a, b) -> a + b).orElse(0);
    Preconditions.checkArgument(!upper_bold_p_i.isEmpty());
    Preconditions.checkArgument(upper_bold_p_i.size() == n);

    final int k_sum = bold_K_i.stream().reduce((a, b) -> a + b).orElse(0);
    Preconditions.checkArgument(bold_a.size() == k_sum);

    BigInteger p = publicParameters.getEncryptionGroup().getP();
    BigInteger g = publicParameters.getEncryptionGroup().getG();
    int upper_l_m = publicParameters.getUpper_l_m();

    byte[][] upper_m = new byte[n][];

    for (int j = 0; j < n; j++) { // iterate over the candidates
      Point p_i_j = upper_bold_p_i.get(j);
      //noinspection SuspiciousNameCombination
      if (p_i_j != null) {
        upper_m[j] = ByteArrayUtils.concatenate(conversion.toByteArray(p_i_j.x, upper_l_m / 2),
                                                conversion.toByteArray(p_i_j.y, upper_l_m / 2));
      } else {
        upper_m[j] = new byte[0];
      }
    }

    List<BigInteger> bold_beta = new ArrayList<>();

    BigInteger z_1 = z.getLeft();
    BigInteger z_2 = z.getRight();
    List<BigInteger> bold_b = bold_a.stream().map(a -> {
      BigInteger beta_j = randomGenerator.randomInGq(publicParameters.getEncryptionGroup());
      bold_beta.add(beta_j);
      return modExp(a.getLeft(), z_1, p)
          .multiply(modExp(a.getRight(), z_2, p))
          .multiply(beta_j).mod(p);
    }).collect(Collectors.toList());

    int l_m = (int) Math.ceil((double) upper_l_m / publicParameters.getSecurityParameters().getUpper_l());

    List<BigInteger> bold_p;
    try {
      bold_p = generalAlgorithms.getPrimes(n);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new IncompatibleParametersRuntimeException(e);
    }

    CiphertextMatrixBuilder bold_c_builder = new CiphertextMatrixBuilder();
    int n_prime = 0;
    int k_prime = 0;

    for (int l = 0; l < t; l++) { // iterate over the elections
      int k_l = bold_K_i.get(l); // the number of selections for this voter for the current election
      Integer n_l = bold_n.get(l); // the number of candidates for the current election
      for (int i = n_prime; i < n_prime + n_l; i++) { // iterate over the candidates for the election
        BigInteger k_i = modExp(bold_p.get(i), z_1, p);
        for (int j = k_prime; j < k_prime + k_l;
             j++) { // iterates over the allowed selections for this voter for the current election
          BigInteger k_ij = k_i.multiply(bold_beta.get(j)).mod(p);
          byte[] upper_k_ij = ByteArrayUtils.truncate(
              IntStream.rangeClosed(1, l_m).mapToObj(c -> hash.recHash_L(k_ij, BigInteger.valueOf(c)))
                       .reduce(ByteArrayUtils::concatenate)
                       .orElse(new byte[0]),
              upper_l_m);
          bold_c_builder.add(i, j, ByteArrayUtils.xor(upper_m[i], upper_k_ij));
        }
      }
      n_prime += n_l;
      k_prime += k_l;
    }

    BigInteger d = modExp(pk.getPublicKey(), z_1, p)
        .multiply(modExp(g, z_2, p))
        .mod(p);
    ObliviousTransferResponse beta = new ObliviousTransferResponse(bold_b, bold_c_builder.build(), d);
    return new ObliviousTransferResponseAndRand(beta, new BigIntPair(z_1, z_2));
  }
}
