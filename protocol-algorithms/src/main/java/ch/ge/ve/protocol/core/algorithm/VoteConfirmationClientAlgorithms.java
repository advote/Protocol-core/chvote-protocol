/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;
import static java.math.BigInteger.ZERO;
import static java.util.Collections.singletonList;

import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.support.ByteArrayUtils;
import ch.ge.ve.protocol.core.support.Conversion;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.NonInteractiveZkp;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

/**
 * Algorithms for the vote confirmation phase, on the voting client's side
 */
public class VoteConfirmationClientAlgorithms {
  private static final String ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME =
      "All points' coordinates must be in Z_p_prime";

  private final PublicParameters  publicParameters;
  private final RandomGenerator   randomGenerator;
  private final GeneralAlgorithms generalAlgorithms;
  private final Conversion conversion = new Conversion();

  public VoteConfirmationClientAlgorithms(PublicParameters publicParameters, GeneralAlgorithms generalAlgorithms,
                                          RandomGenerator randomGenerator) {
    this.publicParameters = publicParameters;
    this.randomGenerator = randomGenerator;
    this.generalAlgorithms = generalAlgorithms;
  }

  /**
   * Algorithm 7.30: GenConfirmation
   *
   * @param upper_y      the confirmation code
   * @param bold_upper_p the <tt>s</tt> per <tt>k</tt> point matrix, where k = sum(bold_k) and s is the number of
   *                     authorities
   *
   * @return the public confirmation y_hat and the proof of knowledge of the secret confirmation y
   */
  public Confirmation genConfirmation(String upper_y, List<List<Point>> bold_upper_p) {
    Preconditions.checkNotNull(upper_y);
    Preconditions.checkNotNull(bold_upper_p);
    Preconditions.checkArgument(bold_upper_p.size() == publicParameters.getS(),
                                "There should be one list of points per authority");

    BigInteger p_prime = publicParameters.getPrimeField().getP_prime();
    Preconditions.checkArgument(bold_upper_p.stream().flatMap(Collection::stream)
                                            .allMatch(point -> BigInteger.ZERO.compareTo(point.x) <= 0
                                                               && point.x.compareTo(p_prime) < 0
                                                               && BigInteger.ZERO.compareTo(point.y) <= 0
                                                               && point.y.compareTo(p_prime) < 0),
                                ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME);
    Preconditions.checkArgument(bold_upper_p.size() == publicParameters.getS());

    BigInteger p_hat = publicParameters.getIdentificationGroup().getP_hat();
    BigInteger q_hat = publicParameters.getIdentificationGroup().getQ_hat();
    BigInteger g_hat = publicParameters.getIdentificationGroup().getG_hat();

    BigInteger y = conversion.toInteger(upper_y, publicParameters.getUpper_a_y()).mod(q_hat);
    BigInteger y_prime = bold_upper_p.stream()
                                     .map(this::getValue)
                                     .reduce(BigInteger::add).orElse(ZERO)
                                     .mod(q_hat);

    BigInteger y_hat = modExp(g_hat, y.add(y_prime).mod(q_hat), p_hat);
    NonInteractiveZkp pi = genConfirmationProof(y, y_hat, y_prime);

    return new Confirmation(y_hat, pi);
  }

  /**
   * Algorithm 7.31: GetValue
   *
   * @param bold_p a list of <tt>k</tt> points defining the polynomial <tt>A(X)</tt> of degree <tt>k - 1</tt>
   *
   * @return the interpolated value <tt>y = A(0)</tt>
   */
  public BigInteger getValue(List<Point> bold_p) {
    Preconditions.checkNotNull(bold_p);
    BigInteger p_prime = publicParameters.getPrimeField().getP_prime();
    Preconditions.checkArgument(bold_p.stream()
                                      .allMatch(point -> BigInteger.ZERO.compareTo(point.x) <= 0
                                                         && point.x.compareTo(p_prime) < 0
                                                         && BigInteger.ZERO.compareTo(point.y) <= 0
                                                         && point.y.compareTo(p_prime) < 0),
                                ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME);

    BigInteger y = ZERO;
    for (int i = 0; i < bold_p.size(); i++) {
      BigInteger n = BigInteger.ONE;
      BigInteger d = BigInteger.ONE;
      for (int j = 0; j < bold_p.size(); j++) {
        if (i != j) {
          BigInteger x_i = bold_p.get(i).x;
          BigInteger x_j = bold_p.get(j).x;

          n = n.multiply(x_j).mod(p_prime);
          d = d.multiply(x_j.subtract(x_i)).mod(p_prime);
        }
      }
      BigInteger y_i = bold_p.get(i).y;

      y = y.add(y_i.multiply(n.multiply(d.modInverse(p_prime)))).mod(p_prime);
    }

    return y;
  }

  /**
   * Algorithm 7.32: GenConfirmationProof
   *
   * @param y       the secret confirmation credential
   * @param y_hat   the public confirmation credential
   * @param y_prime the secret vote validity credential
   *
   * @return a proof of knowledge of the secret confirmation credential
   */
  public NonInteractiveZkp genConfirmationProof(BigInteger y, BigInteger y_hat, BigInteger y_prime) {
    BigInteger p_hat = publicParameters.getIdentificationGroup().getP_hat();
    BigInteger q_hat = publicParameters.getIdentificationGroup().getQ_hat();
    BigInteger g_hat = publicParameters.getIdentificationGroup().getG_hat();
    int tau = publicParameters.getSecurityParameters().getTau();

    //noinspection SuspiciousNameCombination
    Preconditions.checkArgument(generalAlgorithms.isInZ_q_hat(y), "y must be in Z_q_hat");
    //noinspection SuspiciousNameCombination
    Preconditions.checkArgument(generalAlgorithms.isMember_G_q_hat(y_hat),
                                "y_hat must be in G_q_hat");

    BigInteger omega = randomGenerator.randomInZq(q_hat);

    BigInteger t = modExp(g_hat, omega, p_hat);
    BigInteger[] bold_v = new BigInteger[]{y_hat};
    BigInteger[] bold_t = new BigInteger[]{t};
    BigInteger c = generalAlgorithms.getNIZKPChallenge(bold_v, bold_t, tau);
    BigInteger s = omega.add(c.multiply(y.add(y_prime))).mod(q_hat);
    return new NonInteractiveZkp(singletonList(t), singletonList(s));
  }

  /**
   * Algorithm 7.37: GetFinalizationCode
   *
   * @param bold_delta the finalization code parts received from the authorities
   *
   * @return the combined finalization code
   */
  public String getFinalizationCode(List<FinalizationCodePart> bold_delta) {
    Preconditions.checkArgument(bold_delta.size() == publicParameters.getS());
    return bold_delta.stream()
                     .map(FinalizationCodePart::getUpper_f) // extract F_j
                     .reduce(ByteArrayUtils::xor) // xor over j
                     .map(b -> conversion.toString(b, publicParameters.getUpper_a_f()))
                     .orElse("");
  }
}
