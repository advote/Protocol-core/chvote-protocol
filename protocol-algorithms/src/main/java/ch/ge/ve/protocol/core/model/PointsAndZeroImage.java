/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.Point;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Model class combining a list of points and y-image of 0 for a polynomial
 */
public final class PointsAndZeroImage {
  private final List<Point> points;
  private final BigInteger  y_prime;

  public PointsAndZeroImage(List<Point> points, BigInteger y_prime) {
    this.points = Collections.unmodifiableList(new ArrayList<>(points));
    this.y_prime = y_prime;
  }

  public List<Point> getPoints() {
    return points;
  }

  public BigInteger getY_prime() {
    return y_prime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PointsAndZeroImage that = (PointsAndZeroImage) o;
    return Objects.equals(points, that.points)
           && y_prime.compareTo(that.y_prime) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(points, y_prime);
  }
}
