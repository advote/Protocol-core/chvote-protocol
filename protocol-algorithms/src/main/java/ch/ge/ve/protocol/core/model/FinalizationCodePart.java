/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.BigIntPair;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.Objects;

/**
 * Part of the finalization code generated by one authority
 */
public final class FinalizationCodePart {
  private final byte[]           upper_f;
  private final BigIntPair z;

  @JsonCreator
  public FinalizationCodePart(@JsonProperty("upper_f") byte[] upper_f,
                              @JsonProperty("z") BigIntPair z) {
    this.upper_f = upper_f;
    this.z = z;
  }

  public byte[] getUpper_f() {
    return upper_f;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FinalizationCodePart that = (FinalizationCodePart) o;
    return Arrays.equals(upper_f, that.upper_f)
           && Objects.equals(z, that.z);
  }

  @Override
  public int hashCode() {
    return Objects.hash(upper_f, z);
  }
}
