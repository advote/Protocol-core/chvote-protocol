/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

/**
 * Class holding the algorithms global specifications.
 */
public class AlgorithmsSpec {
  private final String keySpecAlgorithm;
  private final String cipherProvider;
  private final String cipherAlgorithm;
  private final String digestProvider;
  private final String digestAlgorithm;
  private final String randomGeneratorProvider;
  private final String randomGeneratorAlgorithm;

  public AlgorithmsSpec(String keySpecAlgorithm,
                        String cipherProvider,
                        String cipherAlgorithm,
                        String digestProvider,
                        String digestAlgorithm,
                        String randomGeneratorProvider,
                        String randomGeneratorAlgorithm) {
    this.keySpecAlgorithm = keySpecAlgorithm;
    this.cipherProvider = cipherProvider;
    this.cipherAlgorithm = cipherAlgorithm;
    this.digestProvider = digestProvider;
    this.digestAlgorithm = digestAlgorithm;
    this.randomGeneratorProvider = randomGeneratorProvider;
    this.randomGeneratorAlgorithm = randomGeneratorAlgorithm;
  }

  public String getKeySpecAlgorithm() {
    return keySpecAlgorithm;
  }

  public String getCipherProvider() {
    return cipherProvider;
  }

  public String getCipherAlgorithm() {
    return cipherAlgorithm;
  }

  public String getDigestProvider() {
    return digestProvider;
  }

  public String getDigestAlgorithm() {
    return digestAlgorithm;
  }

  public String getRandomGeneratorProvider() {
    return randomGeneratorProvider;
  }

  public String getRandomGeneratorAlgorithm() {
    return randomGeneratorAlgorithm;
  }
}
