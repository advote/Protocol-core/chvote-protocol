/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This model class represents in a more compact way the two-dimensional sparse matrix containing the ciphertexts for
 * the oblivious transfer response, as described in Section <tt>5.3.3 Simultaneous Oblivious Transfers</tt>.
 */
public class CiphertextMatrix {
  private final Map<Integer, CiphertextArray> valuesMatrix;

  @JsonCreator
  public CiphertextMatrix(@JsonProperty("valuesMap") Map<Integer, Map<Integer, byte[]>> valuesMap) {
    valuesMatrix = valuesMap.entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry::getKey,
                                                      entry -> new CiphertextArray(entry.getValue())));
  }

  /**
   * Retrieves the byte array for the provided coordinates
   *
   * @param i the first coordinate
   * @param j the second coordinate
   *
   * @return the byte array at the provided coordintates
   *
   * @throws IllegalArgumentException if no byte array exists at the given position
   */
  public byte[] get(int i, int j) {
    Preconditions.checkArgument(valuesMatrix.containsKey(i), "no values for index i = {}", i);
    return valuesMatrix.get(i).get(j);
  }

  @SuppressWarnings("WeakerAccess") // used by Json serialization
  public Map<Integer, Map<Integer, byte[]>> getValuesMap() {
    return valuesMatrix.entrySet().stream()
                       .collect(Collectors.toMap(Map.Entry::getKey,
                                                 entry -> entry.getValue().getValuesMap()));
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
                      .add("valuesMatrix", valuesMatrix)
                      .toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CiphertextMatrix that = (CiphertextMatrix) o;
    return Objects.equals(valuesMatrix, that.valuesMatrix);
  }

  @Override
  public int hashCode() {
    return Objects.hash(valuesMatrix);
  }

  private static final class CiphertextArray {
    private final Map<Integer, byte[]> ciphertextMap;

    private CiphertextArray(Map<Integer, byte[]> valuesMap) {
      ciphertextMap = valuesMap.entrySet().stream()
                               .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private byte[] get(int j) {
      Preconditions.checkArgument(ciphertextMap.containsKey(j), "no value for for index j = {}", j);
      return ciphertextMap.get(j);
    }

    private Map<Integer, byte[]> getValuesMap() {
      return ciphertextMap.entrySet().stream()
                          .collect(Collectors.toMap(Map.Entry::getKey,
                                                    Map.Entry::getValue));
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      CiphertextArray that = (CiphertextArray) o;
      if (!ciphertextMap.keySet().equals(that.ciphertextMap.keySet())) {
        return false;
      }
      return ciphertextMap.keySet().stream().allMatch(
          k -> Arrays.equals(ciphertextMap.get(k), that.ciphertextMap.get(k))
      );
    }

    @Override
    public int hashCode() {
      return Objects.hash(ciphertextMap);
    }

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
                        .add("ciphertextMap", ciphertextMap)
                        .toString();
    }
  }

}
