/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;

import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.support.Conversion;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.EncryptedMessage;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.Signature;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class regroups the algorithms necessary to ensure secure messages transfer between the different channels and
 * described in Section 7.6 of the specification.
 */
public class ChannelSecurityAlgorithms {

  private static final Charset CHARSET = Charset.forName("UTF-8");

  private final Hash                hash;
  private final Hash                hash32;
  private final IdentificationGroup identificationGroup;
  private final RandomGenerator     randomGenerator;
  private final Conversion          conversion = new Conversion();

  private final String       keySpecAlgorithm;
  private final Cipher       cipher;
  private final SecureRandom secureRandom;

  /**
   * Constructor, defines all collaborators
   *
   * @param algorithmsSpec      the algorithm base specifications
   * @param hash                the hash implementation
   * @param identificationGroup the identification group
   * @param randomGenerator     the random generator
   *
   * @throws ChannelSecurityException if the AES algorithm could not be initialized
   */
  public ChannelSecurityAlgorithms(AlgorithmsSpec algorithmsSpec,
                                   Hash hash,
                                   IdentificationGroup identificationGroup,
                                   RandomGenerator randomGenerator) throws ChannelSecurityException {
    this.keySpecAlgorithm = algorithmsSpec.getKeySpecAlgorithm();
    this.hash = hash;
    this.hash32 = new Hash(algorithmsSpec.getDigestAlgorithm(), algorithmsSpec.getDigestProvider(), 32);
    this.identificationGroup = identificationGroup;
    this.randomGenerator = randomGenerator;

    try {
      // initialize AES cipher
      secureRandom = new SecureRandom();
      cipher = Cipher.getInstance(algorithmsSpec.getCipherAlgorithm(), algorithmsSpec.getCipherProvider());
    } catch (Exception e) {
      throw new ChannelSecurityException("error during cipher init", e);
    }
  }

  /**
   * Algorithms 7.54: GenSignature
   *
   * @param sk the private signature key
   * @param m  the JSON representation of the message to sign
   *
   * @return the message's signature
   */
  public Signature genSignature(BigInteger sk, String m) {
    Preconditions
        .checkArgument(GeneralAlgorithms.isInZ_q_hat(sk, identificationGroup), "sk must be in Z_q_hat");

    BigInteger c = BigInteger.ZERO;
    BigInteger s = BigInteger.ZERO;
    final BigInteger g_hat = identificationGroup.getG_hat();
    final BigInteger q_hat = identificationGroup.getQ_hat();
    final BigInteger p_hat = identificationGroup.getP_hat();

    while (BigInteger.ZERO.compareTo(c) == 0 || BigInteger.ZERO.compareTo(s) == 0) {
      BigInteger r = randomGenerator.randomInZq(q_hat);
      BigInteger t = modExp(g_hat, r, p_hat);
      c = conversion.toInteger(hash.recHash_L(t, m)).mod(q_hat);
      s = r.subtract(c.multiply(sk)).mod(q_hat);
    }
    return new Signature(c, s);
  }

  /**
   * Algorithms 7.55: VerifySignature
   *
   * @param pk  the public verification key
   * @param tau the message's signature
   * @param m   the JSON representation of the message to verify
   *
   * @return true if the message is verified, false otherwise
   */
  public boolean verifySignature(BigInteger pk, Signature tau, Object m) {
    final BigInteger c = tau.getC();
    final BigInteger s = tau.getS();
    Preconditions
        .checkArgument(GeneralAlgorithms.isMember(pk, identificationGroup), "pk must be in G_q_hat");
    Preconditions.checkArgument(GeneralAlgorithms.isInZ_q_hat(c, identificationGroup), "c must be in Z_q_hat");
    Preconditions.checkArgument(GeneralAlgorithms.isInZ_q_hat(s, identificationGroup), "s must be in Z_q_hat");

    final BigInteger g_hat = identificationGroup.getG_hat();
    final BigInteger q_hat = identificationGroup.getQ_hat();
    final BigInteger p_hat = identificationGroup.getP_hat();

    BigInteger t_prime = modExp(g_hat, s, p_hat).multiply(modExp(pk, c, p_hat)).mod(p_hat);
    BigInteger c_prime = conversion.toInteger(hash.recHash_L(t_prime, m)).mod(q_hat);

    return c.compareTo(c_prime) == 0;
  }

  /**
   * Algorithms 7.56: GenCiphertext
   *
   * @param pk the public key
   * @param m  the JSON representation of the message to encrypt
   *
   * @return the message's cipher
   *
   * @throws ChannelSecurityException if an error occurred during encryption
   */
  public EncryptedMessage genCiphertext(BigInteger pk, String m) throws ChannelSecurityException {
    Preconditions
        .checkArgument(GeneralAlgorithms.isMember(pk, identificationGroup), "pk must be in G_q_hat");

    final BigInteger q_hat = identificationGroup.getQ_hat();
    final BigInteger p_hat = identificationGroup.getP_hat();
    final BigInteger g_hat = identificationGroup.getG_hat();

    BigInteger r = randomGenerator.randomInZq(q_hat);
    byte[] k = hash32.hash_L(modExp(pk, r, p_hat));
    BigInteger c1 = modExp(g_hat, r, p_hat);

    // even though we could use the same IV for all (since we use a different key each time), it is preferable and
    // more standard to generate a random IV each time
    byte[] iv = new byte[cipher.getBlockSize()];
    secureRandom.nextBytes(iv);

    byte[] c2 = encrypt(k, iv, m.getBytes(CHARSET));

    return new EncryptedMessage(c1, c2, iv);
  }

  /**
   * Algorithms 7.57: GetPlaintext
   *
   * @param sk the private key
   * @param c  the message's cipher
   *
   * @return the decrypted message
   *
   * @throws ChannelSecurityException if an error occurred during encryption
   */
  public String getPlaintext(BigInteger sk, EncryptedMessage c)
      throws ChannelSecurityException {
    Preconditions
        .checkArgument(GeneralAlgorithms.isInZ_q_hat(sk, identificationGroup), "sk must be in Z_q_hat");
    Preconditions.checkArgument(GeneralAlgorithms.isMember(c.getC1(), identificationGroup), "c1 must be in G_q_hat");

    final BigInteger p_hat = identificationGroup.getP_hat();

    byte[] k = hash32.hash_L(modExp(c.getC1(), sk, p_hat));
    return new String(decrypt(k, c.getIv(), c.getC2()), CHARSET);
  }

  /**
   * Sign and encrypt a given message.
   *
   * @param sk the private signature key
   * @param pk the public encryption key
   * @param m  the message to sign and then encrypt
   *
   * @return the message's cipher with its signature
   *
   * @throws ChannelSecurityException if an error occurred during encryption
   */
  public EncryptedMessageWithSignature signAndEncrypt(BigInteger sk, BigInteger pk, String m)
      throws ChannelSecurityException {
    final Signature signature = genSignature(sk, m);
    final EncryptedMessage ciphertext = genCiphertext(pk, m);
    return new EncryptedMessageWithSignature(ciphertext, signature);
  }

  /**
   * Decrypt and verify a given cipher.
   *
   * @param sk                  the private encryption key
   * @param pk                  the public signature key
   * @param cipherWithSignature the cipher to decrypt and verify
   *
   * @return the decrypted message
   *
   * @throws ChannelSecurityException if an error occurred during decryption or if signature could not be verified
   */
  public String decryptAndVerify(BigInteger sk, BigInteger pk, EncryptedMessageWithSignature
      cipherWithSignature)
      throws ChannelSecurityException {
    String decrypted = getPlaintext(sk, cipherWithSignature.getEncryptedMessage());
    if (!verifySignature(pk, cipherWithSignature.getSignature(), decrypted)) {
      throw new ChannelSecurityException("signature could not be verified");
    }
    return decrypted;
  }

  private byte[] encrypt(byte[] key, byte[] iv, byte[] toEncrypt) throws ChannelSecurityException {
    Key keySpec = new SecretKeySpec(key, keySpecAlgorithm);
    try {
      cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
      return cipher.doFinal(toEncrypt);
    } catch (Exception e) {
      throw new ChannelSecurityException("error during encryption", e);
    }
  }

  private byte[] decrypt(byte[] key, byte[] iv, byte[] encrypted) throws ChannelSecurityException {
    Key keySpec = new SecretKeySpec(key, keySpecAlgorithm);
    try {
      cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
      return cipher.doFinal(encrypted);
    } catch (Exception e) {
      throw new ChannelSecurityException("error during decryption", e);
    }
  }
}
