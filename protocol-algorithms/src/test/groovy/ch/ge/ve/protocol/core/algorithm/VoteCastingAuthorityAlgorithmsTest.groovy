/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.NINE
import static ch.ge.ve.protocol.core.support.BigIntegers.SIX
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.core.support.BigIntegers.TWO
import static java.math.BigInteger.ONE
import static java.math.BigInteger.ZERO

import ch.ge.ve.protocol.core.exception.IncompatibleParametersRuntimeException
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException
import ch.ge.ve.protocol.core.model.BallotList
import ch.ge.ve.protocol.core.model.CiphertextMatrix
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse
import ch.ge.ve.protocol.core.model.ObliviousTransferResponseAndRand
import ch.ge.ve.protocol.core.model.PublicIdentificationCredentialsList
import ch.ge.ve.protocol.core.model.VoterList
import ch.ge.ve.protocol.core.support.Hash
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.BallotAndQuery
import ch.ge.ve.protocol.model.BigIntPair
import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.DomainOfInfluence
import ch.ge.ve.protocol.model.Election
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.protocol.model.PrimeField
import ch.ge.ve.protocol.model.PrintingAuthority
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.model.SecurityParameters
import ch.ge.ve.protocol.model.Voter
import spock.lang.Specification

/**
 * Tests on the vote casting algorithms on the authority side
 */
class VoteCastingAuthorityAlgorithmsTest extends Specification {
  def defaultAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_".toCharArray() as List<Character>
  EncryptionGroup encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)
  IdentificationGroup identificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE)
  SecurityParameters securityParameters = new SecurityParameters(1, 1, 2, 0.99)
  PrimeField primeField = new PrimeField(ELEVEN)
  PublicParameters publicParameters = new PublicParameters(
      securityParameters, encryptionGroup, identificationGroup, primeField,
      FIVE, defaultAlphabet, FIVE, defaultAlphabet,
      defaultAlphabet, 2, defaultAlphabet, 2, 4, 5
  )
  GeneralAlgorithms generalAlgorithms = Mock()
  RandomGenerator randomGenerator = Mock()
  Hash hash = Mock()
  String printingAuthorityName = "default"
  PrintingAuthority printingAuthority = Mock()

  VoteCastingAuthorityAlgorithms voteCastingAuthority

  void setup() {
    def domainOfInfluence = new DomainOfInfluence("test")
    def countingCircle1 = new CountingCircle(1, "CountingCircle1", "CountingCircleName1")
    Voter voter0 = new Voter(0, "1", countingCircle1, printingAuthorityName, domainOfInfluence)
    Voter voter1 = new Voter(1, "2", countingCircle1, printingAuthorityName, domainOfInfluence)
    def voters = [voter0, voter1]
    VoterList voterList = new VoterList() {
      @Override
      Optional<Voter> getVoter(int voterIndex) {
        Optional.of(voters.get(voterIndex))
      }
    }
    Election election = new Election("E", 4, 2, domainOfInfluence)
    def candidates = [
        new Candidate(""),
        new Candidate(""),
        new Candidate(""),
        new Candidate("")
    ]
    printingAuthority.name >> printingAuthorityName
    def electionSet = new ElectionSetWithPublicKey([election], candidates, [printingAuthority], 2L, 1)

    voteCastingAuthority = new VoteCastingAuthorityAlgorithms(publicParameters, electionSet, voterList, generalAlgorithms, randomGenerator, hash)
  }

  def "checkBallot should correctly check the ballot"() {
    given:
    def encryptionKey = new EncryptionPublicKey(THREE, encryptionGroup)
    def ballotList = Mock(BallotList)
    ballotList.containsBallot(1) >> true
    ballotList.containsBallot(3) >> true
    def publicCredentials = Mock(PublicIdentificationCredentialsList)
    publicCredentials.get(0) >> ONE
    publicCredentials.get(1) >> FOUR
    generalAlgorithms.getNIZKPChallenge([ONE, bold_a] as Object[], t as BigInteger[], 1) >> c

    and: "the expected preconditions checks"
    generalAlgorithms.isMember(THREE) >> true
    generalAlgorithms.isMember(FOUR) >> true
    generalAlgorithms.isMember(FIVE) >> true
    generalAlgorithms.isMember(NINE) >> true
    generalAlgorithms.isMember_G_q_hat(FIVE) >> true
    generalAlgorithms.isInZ_q(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < encryptionGroup.q }
    generalAlgorithms.isInZ_q_hat(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < identificationGroup.q_hat }

    expect:
    voteCastingAuthority.checkBallot(
        i,
        new BallotAndQuery(
            x_hat,
            bold_a,
            new NonInteractiveZkp(t, s)
        ),
        encryptionKey,
        publicCredentials,
        ballotList
    ) == result

    where: "a valid dataset taken from VoteCastingClientAlgorithmsTest and some invalid ones"
    i | x_hat | bold_a                                                   | c   | t                   | s                  || result
    0 | ONE   | [new BigIntPair(FIVE, NINE), new BigIntPair(FOUR, FOUR)] | ONE | [FIVE, FOUR, THREE] | [THREE, FOUR, TWO] || true
    1 | ONE   | [new BigIntPair(FIVE, NINE), new BigIntPair(FOUR, FOUR)] | ONE | [FIVE, FOUR, THREE] | [THREE, FOUR, TWO] || false
    0 | ONE   | [new BigIntPair(FIVE, NINE), new BigIntPair(FOUR, FOUR)] | ONE | [FIVE, FOUR, NINE]  | [THREE, FOUR, TWO] || false
  }

  def "hasBallot should detect if a BallotEntry list contains a given voter index"() {
    given: "a ballot list"
    def ballotList = Mock(BallotList)
    ballotList.containsBallot(1) >> true
    ballotList.containsBallot(3) >> true
    ballotList.containsBallot(45) >> true

    expect: "the call to hasBallot to have the expected result"
    result == voteCastingAuthority.hasBallot(i, ballotList)

    where: "the values for i and the result are as follows"
    i  || result
    1  || true
    2  || false
    3  || true
    4  || false
    44 || false
    45 || true
    46 || false
  }

  def "checkBallotProof should verify the validity of a provided proof"() {
    given: "a fixed encryption key and challenge"
    def encryptionKey = new EncryptionPublicKey(THREE, encryptionGroup)
    generalAlgorithms.getNIZKPChallenge(_ as Object[], t as BigInteger[], 1) >> c

    and: "the expected preconditions checks"
    generalAlgorithms.isMember(THREE) >> true
    generalAlgorithms.isMember(FOUR) >> true
    generalAlgorithms.isMember(NINE) >> true
    generalAlgorithms.isMember_G_q_hat(FIVE) >> true
    generalAlgorithms.isMember_G_q_hat(NINE) >> true
    generalAlgorithms.isInZ_q(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < encryptionGroup.q }
    generalAlgorithms.isInZ_q_hat(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < identificationGroup.q_hat }

    expect: "the verification of the Proof to have the expected result"
    result == voteCastingAuthority.checkBallotProof(new NonInteractiveZkp(t, s), x_hat, [new BigIntPair(a_1, a_2)], encryptionKey)

    where: "the values are taken from the following table"
    t                   | s                    | x_hat | a_1  | a_2   | c    || result
    [FIVE, FOUR, THREE] | [THREE, NINE, ZERO]  | ONE   | NINE | THREE | FOUR || true // values from genBallotProof
    // test
    [NINE, FOUR, THREE] | [THREE, NINE, ZERO]  | ONE   | NINE | THREE | FOUR || false
    [FIVE, FOUR, THREE] | [THREE, NINE, THREE] | ONE   | NINE | THREE | FOUR || false
  }

  def "genResponse should generate a valid response to an OT query"() {
    given: "a fixed encryption key and challenge"
    def encryptionKey = new EncryptionPublicKey(THREE, encryptionGroup)
    List<Integer> candidatesNumberVector = [3]
    List<List<Integer>> selectionsMatrix = [[1], [1]]
    List<List<Point>> pointMatrix = [
        [   // voter1
            new Point(ONE, SIX),
            new Point(FOUR, SIX),
            new Point(THREE, SIX)
        ],
        [   // voter2
            new Point(TWO, THREE),
            new Point(FIVE, THREE),
            new Point(ONE, THREE)
        ]
    ]
    and: "some known randomess"
    randomGenerator.randomInZq(_) >>> [z_1, z_2]
    randomGenerator.randomInGq(encryptionGroup) >>> bold_beta
    and: "known primes"
    generalAlgorithms.getPrimes(3) >> [TWO, THREE, FIVE]
    and: "some hash values"
    hash.recHash_L(_) >>> [
        [0x00, 0x10], // l = 1
        [0x20, 0x30], // l = 2
        [0x40, 0x50] // l = 3
    ]

    and: "the expected preconditions checks"
    generalAlgorithms.isMember(THREE) >> true
    generalAlgorithms.isMember(FOUR) >> true
    generalAlgorithms.isMember(FIVE) >> true
    generalAlgorithms.isMember(NINE) >> true

    expect: "the generated response should match the expected values"
    voteCastingAuthority.genResponse(bold_a, encryptionKey, candidatesNumberVector,
        selectionsMatrix[i], pointMatrix[i]) ==
        new ObliviousTransferResponseAndRand(new ObliviousTransferResponse(
            bold_b, new CiphertextMatrix(bold_c), d
        ), new BigIntPair(z_1, z_2))

    where: "the input / output values are"
    i | bold_a                        | z_1   | z_2  | bold_beta || bold_b | bold_c                                                                                           | d
    0 | [new BigIntPair(FOUR, THREE)] | THREE | FOUR | [THREE]   || [NINE] | [0: [0: [0x01, 0x16] as byte[]], 1: [0: [0x24, 0x36] as byte[]], 2: [0: [0x43, 0x56] as byte[]]] | NINE
    1 | [new BigIntPair(FIVE, NINE)]  | TWO   | ONE  | [THREE]   || [FOUR] | [0: [0: [0x02, 0x13] as byte[]], 1: [0: [0x25, 0x33] as byte[]], 2: [0: [0x41, 0x53] as byte[]]] | FIVE

    // i = 0 => b_1 = a_1,1 ^ z_1 * a_1,2 ^ z_2 * beta_1 mod p = 4^3*3^4*3 mod 11 = 9
    // i = 0 => d = pk ^ z_1 * g ^ z_2 mod p = 3^3*3^4 mod 11 = 9
    // i = 1 => b_1 = a_1,1 ^ z_1 * a_1,2 ^ z_2 * beta_1 mod p = 5^2*9^1*3 mod 11 = 4
    // i = 1 => d = pk ^ z_1 * g ^ z_2 mod p = 3^2*3^1 mod 11 = 5
  }

  def "genResponse should fail if the group is too small"() {
    given: "a fixed encryption key and challenge"
    def pk = new EncryptionPublicKey(THREE, encryptionGroup)
    List<Integer> candidatesNumberVector = [3]
    List<Integer> selections = [1]
    List<Point> pointsVector = [
        new Point(ONE, SIX),
        new Point(FOUR, SIX),
        new Point(THREE, SIX)
    ]
    and: "some known randomess"
    randomGenerator.randomInZq(_) >>> [ONE, TWO]
    randomGenerator.randomInGq(_) >> NINE
    and: "failure to get enough primes"
    generalAlgorithms.getPrimes(3) >> {
      args -> throw new NotEnoughPrimesInGroupException("not enough of them")
    }

    and: "the expected preconditions checks"
    generalAlgorithms.isMember(THREE) >> true
    generalAlgorithms.isMember(FOUR) >> true

    when: "an attempt is made at generating a response"
    voteCastingAuthority.genResponse([new BigIntPair(THREE, FOUR)], pk, candidatesNumberVector, selections, pointsVector)

    then:
    thrown(IncompatibleParametersRuntimeException)
  }
}
