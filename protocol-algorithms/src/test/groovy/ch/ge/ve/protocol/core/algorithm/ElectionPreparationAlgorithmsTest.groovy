/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm

import static ch.ge.ve.protocol.core.support.BigIntegers.EIGHT
import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.NINE
import static ch.ge.ve.protocol.core.support.BigIntegers.SEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.SIX
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.core.support.BigIntegers.TWO
import static java.math.BigInteger.ONE
import static java.math.BigInteger.ZERO

import ch.ge.ve.protocol.core.model.PointsAndZeroImage
import ch.ge.ve.protocol.core.model.SecretVoterData
import ch.ge.ve.protocol.core.model.VoterData
import ch.ge.ve.protocol.core.support.Hash
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.DomainOfInfluence
import ch.ge.ve.protocol.model.Election
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.protocol.model.PrimeField
import ch.ge.ve.protocol.model.PrintingAuthority
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.model.SecurityParameters
import ch.ge.ve.protocol.model.Voter
import spock.lang.Specification

/**
 * Tests on the algorithms performed during election preparation
 */
class ElectionPreparationAlgorithmsTest extends Specification {
  // Mocks
  Hash hash = Mock()
  RandomGenerator randomGenerator = Mock()
  PrintingAuthority printingAuthority = Mock()
  PolynomialAlgorithms polynomialAlgorithms = Mock()
  String printingAuthorityName = "default"
  IdentificationGroup identificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE)
  PrimeField primeField = new PrimeField(ELEVEN)
  PublicParameters publicParameters = new PublicParameters(
          new SecurityParameters(1, 1, 1, 0.9),
          new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR),
          identificationGroup,
          primeField,
          THREE,
          ['a', 'b'] as List<Character>,
          THREE,
          ['a', 'b'] as List<Character>,
          ['a', 'b'] as List<Character>,
          1,
          ['a', 'b'] as List<Character>,
          1,
          4,
          4
  )

  ElectionPreparationAlgorithms electionPreparation

  void setup() {
    electionPreparation = new ElectionPreparationAlgorithms(publicParameters, randomGenerator, hash, polynomialAlgorithms)
    printingAuthority.name >> printingAuthorityName
  }

  def "genElectorateData should generate the expected electorate data"() {
    given: "two elections"
    DomainOfInfluence doi1 = new DomainOfInfluence("test 1")
    DomainOfInfluence doi2 = new DomainOfInfluence("test 2")
    and: "the first election is a 1-out-of-3 election (typical referendum)"
    Election election1 = new Election("E1", 3, 1, doi1)
    and: "the second election is a 2-out-of-4 election"
    Election election2 = new Election("E2", 4, 2, doi2)

    and: "two voters where both are eligible for the first election and only the first is eligible for the second election"
    CountingCircle countingCircle1 = new CountingCircle(1, "CountingCircle1", "CountingCircleName1")
    Voter voter1 = new Voter(1, "1", countingCircle1, printingAuthorityName, doi1, doi2)
    Voter voter2 = new Voter(2, "2", countingCircle1, printingAuthorityName, doi1)
    def voters = [voter1, voter2]

    and: "the matching number of candidates"
    Candidate candidate1 = new Candidate("e1 c1")
    Candidate candidate2 = new Candidate("e1 c2")
    Candidate candidate3 = new Candidate("e1 c3")
    Candidate candidate4 = new Candidate("e2 c1")
    Candidate candidate5 = new Candidate("e2 c2")
    Candidate candidate6 = new Candidate("e2 c3")
    Candidate candidate7 = new Candidate("e2 c4")
    def candidates = [
            candidate1,
            candidate2,
            candidate3,
            candidate4,
            candidate5,
            candidate6,
            candidate7
    ]
    def elections = [election1, election2]

    and: "the corresponding election set"
    ElectionSetWithPublicKey electionSet = new ElectionSetWithPublicKey(elections, candidates, [printingAuthority], 2L, 1)

    and: "a provided polynomial for voter 1"
    // polynomial is 2x² + x + 2
    polynomialAlgorithms.genPoints([3, 4], [true, true], 3) >> new PointsAndZeroImage([
            new Point(SIX, THREE), new Point(ONE, FIVE), new Point(TWO, ONE), // points for the first election
            new Point(NINE, EIGHT), new Point(FIVE, TWO), new Point(FOUR, FIVE), new Point(THREE, ONE) // 2nd election
    ], TWO)

    and: "a provided polynomial for voter 2"
    // polynomial is 3
    polynomialAlgorithms.genPoints([3, 4], [true, false], 1) >> new PointsAndZeroImage([
            new Point(FIVE, THREE), new Point(FOUR, THREE), new Point(THREE, THREE), // point for the first election
            null, null, null, null // 2nd election - non eligible
    ], THREE)

    and: "the following pre-established 'random' value"
    randomGenerator.randomInZq(_ as BigInteger) >> ONE

    and: "the following computed hashes"
    hash.recHash_L(_) >> ([0x0C] as byte[])

    when: "the electorate data is generated"
    def electorateData = electionPreparation.genElectorateData(voters, electionSet)

    then: "the voter data should have th expected value for voter 1"
    electorateData[0] == new VoterData(
            voter1.voterId,
            new SecretVoterData(voter1,
                    ONE,
                    ONE,
                    [0x0C] as byte[],
                    [[0x0C], [0x0C], [0x0C], [0x0C], [0x0C], [0x0C], [0x0C]] as byte[][]),
            new Point(THREE, FIVE), // voter 1 -- x = 3^1 % 11 = 3; y_exp = (1 + 2) % 5 = 3; y_hat = 3^3 % 11 = 5
            [
                    new Point(SIX, THREE), new Point(ONE, FIVE), new Point(TWO, ONE), // points for the first election
                    new Point(NINE, EIGHT), new Point(FIVE, TWO), new Point(FOUR, FIVE), new Point(THREE, ONE) // 2nd election
            ],
            [1, 2]
    )

    and: "the voter data should have the expected values for voter 2"
    electorateData[1] == new VoterData(
            voter2.voterId,
            new SecretVoterData(voter2,
                    ONE,
                    ONE,
                    [0x0C] as byte[],
                    [[0x0C], [0x0C], [0x0C], [], [], [], []] as byte[][]),
            new Point(THREE, FOUR), // voter 2 -- x = 3^1 % 11 = 3; y_exp = (1 + 3) % 5 = 4; y_hat = 3^4 % 11 = 4
            [
                    new Point(FIVE, THREE), new Point(FOUR, THREE), new Point(THREE, THREE), // point for the first election
                    null, null, null, null // 2nd election
            ],
            [1, 0]
    )
  }

  def "genSecretVoterData should generate the expected private voter data"() {
    given:
    Point point1 = new Point(ONE, ZERO)
    Point point2 = new Point(ZERO, ONE)
    hash.recHash_L([point1, point2] as Object[]) >> ([0x03] as byte[])
    hash.recHash_L(point1) >> ([0x05] as byte[])
    hash.recHash_L(point2) >> ([0x07] as byte[])
    randomGenerator.randomInZq(_) >>> [FIVE, THREE]
    CountingCircle countingCircle1 = new CountingCircle(1, "CountingCircle1", "CountingCircleName1")
    DomainOfInfluence doi1 = new DomainOfInfluence("test 1")
    def voter = new Voter(1, "1", countingCircle1, printingAuthorityName, doi1)

    when:
    def secretData = electionPreparation.genSecretVoterData(voter, [point1, point2])

    then:
    secretData.x == FIVE
    secretData.y == THREE
    secretData.f == ([0x03] as byte[])
    secretData.rc == ([[0x05], [0x07]] as byte[][])
  }

  def "genPublicVoterData should generate the expected public voter data"() {
    expect:
    electionPreparation.getPublicVoterData(x, y, y_prime) == point

    where:
    x     | y   | y_prime | hashed                 || point
    THREE | TWO | FIVE    | [0x13] as byte[]       || new Point(FIVE, NINE) // x_hat = 3^3 % 11 = 5; y = (2 + 5) % 5 = 2 --> y_hat = 3^2 mod 11 = 9
    SIX   | ONE | TWO     | [0x10, 0x30] as byte[] || new Point(THREE, FIVE) // x_hat = 3^6 % 11 = 3; y = 1 + 2 % 5 = 3 --> y_hat = 3^3 mod 11 = 5
  }

  def "getPublicCredentials should combine the public data from the different authorities"() {
    given:
    def upper_d_hat = [
            [ // authority 1
              new Point(THREE, FOUR),
              new Point(ONE, TWO)
            ], [ // authority 2
                 new Point(FIVE, ONE),
                 new Point(ONE, TWO)
            ], [ // authority 3
                 new Point(THREE, FIVE),
                 new Point(ONE, TWO)
            ], [ // authority 4
                 new Point(FOUR, TWO),
                 new Point(ONE, TWO)
            ]]

    when:
    def credentials = electionPreparation.getPublicCredentials(upper_d_hat)

    then:
    credentials == [
            new Point(FOUR, SEVEN),
            new Point(ONE, FIVE)
    ]
  }
}
