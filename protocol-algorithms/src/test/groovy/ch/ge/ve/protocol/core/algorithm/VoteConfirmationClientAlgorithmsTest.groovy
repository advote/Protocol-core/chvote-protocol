/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.NINE
import static ch.ge.ve.protocol.core.support.BigIntegers.SEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.SIX
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.core.support.BigIntegers.TWO
import static java.math.BigInteger.ONE
import static java.math.BigInteger.ZERO

import ch.ge.ve.protocol.core.model.FinalizationCodePart
import ch.ge.ve.protocol.core.support.Hash
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.BigIntPair
import ch.ge.ve.protocol.model.Confirmation
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.protocol.model.PrimeField
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.model.SecurityParameters
import spock.lang.Specification

/**
 * Tests on the vote confirmation algorithms performed by the voting client
 */
class VoteConfirmationClientAlgorithmsTest extends Specification {
  def defaultAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_".toCharArray() as List<Character>
  EncryptionGroup encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)
  IdentificationGroup identificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE)
  SecurityParameters securityParameters = new SecurityParameters(1, 1, 2, 0.99)
  PrimeField primeField = new PrimeField(SEVEN)
  PublicParameters publicParameters = new PublicParameters(
      securityParameters, encryptionGroup, identificationGroup, primeField,
      FIVE, defaultAlphabet, FIVE, defaultAlphabet,
      defaultAlphabet, 2, defaultAlphabet, 2, 4, 5
  )
  RandomGenerator randomGenerator = Mock()
  GeneralAlgorithms generalAlgorithms = Mock()
  Hash hash = Mock()

  VoteConfirmationClientAlgorithms voteConfirmationClient

  void setup() {
    voteConfirmationClient = new VoteConfirmationClientAlgorithms(publicParameters, generalAlgorithms, randomGenerator)
  }

  def "genConfirmation should generate the expected confirmation"() {
    given: "a given set of parameters"
    def confirmationCode = "cA" // ToInteger(Y) = 154
    def upper_bold_p = [
        [new Point(FOUR, TWO)],
        [new Point(THREE, ONE)],
        [new Point(FIVE, ZERO)],
        [new Point(SIX, THREE)]
    ]

    and: "known collaborators responses"
    randomGenerator.randomInZq(FIVE) >> THREE // called by GenConfirmationProof - omega
    generalAlgorithms.getNIZKPChallenge(_ as BigInteger[], _ as BigInteger[], 1) >> ONE // c

    and: "the expected preconditions checks"
    generalAlgorithms.isMember_G_q_hat(ONE) >> true
    generalAlgorithms.isInZ_q_hat(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < identificationGroup.q_hat }

    // y = 154 mod 5 = 4
    // y' = getValue(upper_bold_p) = 1
    // y_hat = g_hat ^ (y + y') mod p_hat = 3 ^ 5 mod 11 = 1
    // t = g_hat ^ omega mod p_hat = 3 ^ 3 mod 11 = 5
    // s = omega + c * (y + y') mod q_hat = 3 + 1 * (4 + 1) mod 5 = 3
    expect:
    voteConfirmationClient.genConfirmation(confirmationCode, upper_bold_p) ==
        new Confirmation(ONE, new NonInteractiveZkp([FIVE], [THREE]))
  }

  def "getValue should correctly interpolate the value for A(0)"() {
    expect:
    voteConfirmationClient.getValue(points) == y

    where:
    points                                        || y
    [new Point(SIX, ONE)]                         || ONE
    [new Point(THREE, TWO), new Point(FIVE, ONE)] || ZERO // performed algorithm by hand, on paper.
  }

  def "genConfirmationProof should generate a valid proof of knowledge for y"() {
    given: "a known random omega"
    randomGenerator.randomInZq(FIVE) >> FOUR // omega

    and: "a known challenge value"
    // t = g_hat ^ omega mod p_hat = 3 ^ 4 mod 11 = 4
    generalAlgorithms.getNIZKPChallenge([NINE] as BigInteger[], [FOUR] as BigInteger[], 1) >> ONE

    and: "the expected preconditions checks"
    generalAlgorithms.isMember_G_q_hat(NINE) >> true
    generalAlgorithms.isInZ_q_hat(_ as BigInteger) >> { BigInteger x -> 0 <= x && x < identificationGroup.q_hat }

    expect: "the generated proof to have the expected value"
    // s = omega + c * (y + y_prime) mod q_hat = 4 + 1 * (2 + 4) mod 5 = 0
    voteConfirmationClient.genConfirmationProof(TWO, NINE, FOUR) == new NonInteractiveZkp([FOUR], [ZERO])
  }

  def "getFinalizationCode should correctly combine the given finalization code parts"() {
    expect:
    voteConfirmationClient.getFinalizationCode([
        new FinalizationCodePart([0xDE, 0xAD] as byte[], new BigIntPair(ONE, ZERO)),
        new FinalizationCodePart([0xBE, 0xEF] as byte[], new BigIntPair(ONE, ZERO)),
        new FinalizationCodePart([0x01, 0x10] as byte[], new BigIntPair(ONE, ZERO)),
        new FinalizationCodePart([0xFA, 0xCE] as byte[], new BigIntPair(ONE, ZERO))
    ]) == "jUC" // [0x9B, 0x9C] -> 39836
  }
}
