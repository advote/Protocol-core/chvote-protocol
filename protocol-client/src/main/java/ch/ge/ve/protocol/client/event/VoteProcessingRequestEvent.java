/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.event;

import ch.ge.ve.event.Event;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.util.List;

public class VoteProcessingRequestEvent implements Event {
  private final PublicParameters           publicParameters;
  private final ElectionSetForVerification electionSetForVerification;
  private final List<EncryptionPublicKey>  publicKeyParts;
  private final VotingCard                 votingCard;

  @JsonCreator
  public VoteProcessingRequestEvent(@JsonProperty("publicParameters") PublicParameters publicParameters,
                                    @JsonProperty(
                                        "electionSetForVerification") ElectionSetForVerification
                                        electionSetForVerification,
                                    @JsonProperty("publicKeyParts") List<EncryptionPublicKey> publicKeyParts,
                                    @JsonProperty("votingCard") VotingCard votingCard) {
    this.publicParameters = publicParameters;
    this.electionSetForVerification = electionSetForVerification;
    this.publicKeyParts = ImmutableList.copyOf(publicKeyParts);
    this.votingCard = votingCard;
  }

  public PublicParameters getPublicParameters() {
    return publicParameters;
  }

  public ElectionSetForVerification getElectionSetForVerification() {
    return electionSetForVerification;
  }

  public List<EncryptionPublicKey> getPublicKeyParts() {
    return publicKeyParts;
  }

  public VotingCard getVotingCard() {
    return votingCard;
  }
}
