/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client;

import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOTS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DECRYPTION_PROOFS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PARTIAL_DECRYPTIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLES_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLE_PROOFS_RECEIVED;

import ch.ge.ve.config.VerificationKeysConfiguration;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.BallotConfirmationPublicationEvent;
import ch.ge.ve.event.BallotPublicationEvent;
import ch.ge.ve.event.BuildPublicCredentialsFinishedEvent;
import ch.ge.ve.event.CleanupRequestEvent;
import ch.ge.ve.event.CleanupResponseEvent;
import ch.ge.ve.event.ConfirmationFailedEvent;
import ch.ge.ve.event.ControlComponentInitializedPublicationEvent;
import ch.ge.ve.event.ControlComponentPublicKeyMulticastEvent;
import ch.ge.ve.event.ControlComponentPublicKeyPublicationEvent;
import ch.ge.ve.event.ElectionOfficerPublicKeyMulticastEvent;
import ch.ge.ve.event.ElectionOfficerPublicKeyPublicationEvent;
import ch.ge.ve.event.ElectionSetMulticastEvent;
import ch.ge.ve.event.ElectionSetPublicationEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.event.FinalizationCodePartPublicationEvent;
import ch.ge.ve.event.KeyGenerationRequestEvent;
import ch.ge.ve.event.ObliviousTransferResponseCreationEvent;
import ch.ge.ve.event.PartialDecryptionsPublicationEvent;
import ch.ge.ve.event.PrimesPublicationEvent;
import ch.ge.ve.event.PublicParametersMulticastEvent;
import ch.ge.ve.event.PublicParametersPublicationEvent;
import ch.ge.ve.event.ShufflePublicationEvent;
import ch.ge.ve.event.ShufflingRequestEvent;
import ch.ge.ve.event.SignEncryptedPrivateCredentialsRequestEvent;
import ch.ge.ve.event.SignEncryptedPrivateCredentialsResponseEvent;
import ch.ge.ve.event.UnacknowledgedEvent;
import ch.ge.ve.event.VotersMulticastEvent;
import ch.ge.ve.event.VotersPublicationEvent;
import ch.ge.ve.event.VotersPublishedEvent;
import ch.ge.ve.event.VotesCastCountRequestEvent;
import ch.ge.ve.event.VotesCastCountResponseEvent;
import ch.ge.ve.event.payload.BallotConfirmationPayload;
import ch.ge.ve.event.payload.BallotPublicationPayload;
import ch.ge.ve.event.payload.ElectionOfficerKeyPayload;
import ch.ge.ve.event.payload.ElectionSetPayload;
import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.event.payload.PublicParametersPayload;
import ch.ge.ve.event.payload.VoterIdsPayload;
import ch.ge.ve.event.payload.VotersPayload;
import ch.ge.ve.event.verification.VerificationBallotKeysPublicationEvent;
import ch.ge.ve.event.verification.VerificationBallotKeysRequestEvent;
import ch.ge.ve.event.verification.VerificationBallotsPublicationEvent;
import ch.ge.ve.event.verification.VerificationBallotsRequestEvent;
import ch.ge.ve.event.verification.VerificationConfirmationKeysPublicationEvent;
import ch.ge.ve.event.verification.VerificationConfirmationKeysRequestEvent;
import ch.ge.ve.event.verification.VerificationConfirmationsPublicationEvent;
import ch.ge.ve.event.verification.VerificationConfirmationsRequestEvent;
import ch.ge.ve.event.verification.VerificationDecryptionProofPublicationEvent;
import ch.ge.ve.event.verification.VerificationDecryptionProofRequestEvent;
import ch.ge.ve.event.verification.VerificationElectionOfficerKeyPublicationEvent;
import ch.ge.ve.event.verification.VerificationElectionOfficerKeyRequestEvent;
import ch.ge.ve.event.verification.VerificationElectionSetPublicationEvent;
import ch.ge.ve.event.verification.VerificationElectionSetRequestEvent;
import ch.ge.ve.event.verification.VerificationGeneratorsPublicationEvent;
import ch.ge.ve.event.verification.VerificationGeneratorsRequestEvent;
import ch.ge.ve.event.verification.VerificationPartialDecryptionsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPartialDecryptionsRequestEvent;
import ch.ge.ve.event.verification.VerificationPrimesPublicationEvent;
import ch.ge.ve.event.verification.VerificationPrimesRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicCredentialPartsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicCredentialPartsRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicKeyPartsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicKeyPartsRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicParametersPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicParametersRequestEvent;
import ch.ge.ve.event.verification.VerificationShuffleProofPublicationEvent;
import ch.ge.ve.event.verification.VerificationShuffleProofRequestEvent;
import ch.ge.ve.event.verification.VerificationShufflePublicationEvent;
import ch.ge.ve.event.verification.VerificationShuffleRequestEvent;
import ch.ge.ve.filenamer.EpfFileName;
import ch.ge.ve.protocol.client.event.VoteProcessingReplyEvent;
import ch.ge.ve.protocol.client.event.VoteProcessingRequestEvent;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.exception.ProtocolException;
import ch.ge.ve.protocol.client.message.MessageHandlerCountMessagesImpl;
import ch.ge.ve.protocol.client.message.api.MessageHandler;
import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import ch.ge.ve.protocol.client.model.VoteResult;
import ch.ge.ve.protocol.client.progress.api.ProgressTracker;
import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.exception.EncryptionColouringRuntimeException;
import ch.ge.ve.protocol.core.exception.InvalidElectionOfficerPublicKeyRuntimeException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.MoreCollectors;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PrintingAuthority;
import ch.ge.ve.protocol.model.PrintingAuthorityForVerification;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Signature;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriter;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.SignatureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.LongSupplier;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Protocol client handling the nominal protocol scenario.
 * <p>
 * Order of call is of prime importance. The public methods are ordered in the expected call order.
 */
public class ProtocolClientImpl implements ProtocolClient {
  private static final Logger log                        = LoggerFactory.getLogger(ProtocolClientImpl.class);
  private static final int    THROTTLING_BUFFER_MIN_SIZE = 500;
  private static final String VOTING_CARDS_SUBDIR        = "votingcards";
  private static final String ALGO_KEYSPEC_ALGORITHM     = "AES";
  private static final String ALGO_CIPHER_PROVIDER       = "BC";
  private static final String ALGO_CIPHER_ALGORITHM      = "AES/CTR/NoPadding";
  private static final String ALGO_DIGEST_PROVIDER       = "BC";
  private static final String ALGO_DIGEST_ALGORITHM      = "BLAKE2B-256";
  private static final String ALGO_RNG_PROVIDER          = "SUN";
  private static final String ALGO_RNG_ALGORITHM         = "SHA1PRNG";

  private final int                                   securityLevel;
  private final AlgorithmsSpec                        algorithmsSpec;
  private final String                                protocolId;
  private final RabbitUtilities                       rabbitUtilities;
  private final SignatureService                      signatureService;
  private final ObjectMapper                          objectMapper;
  private final int                                   nbControlComponents;
  private final List<EncryptionPublicKey>             publicKeyParts;
  private final Map<String, IdentificationPrivateKey> printingAuthoritiesPrivateKeysByName;
  private final RandomGenerator                       randomGenerator;
  private final int                                   batchSize;
  private final List<Encryption>                      finalShuffle;
  private final List<Decryptions>                     controlComponentsPartialDecryptions;
  private final long                                  waitTimeout;
  private final List<String>                          ccPartIndexes;

  private Tally                                                        expectedTally;
  private PublicParameters                                             publicParameters;
  private Hash                                                         hash;
  private GeneralAlgorithms                                            generalAlgorithms;
  private ChannelSecurityAlgorithms                                    channelSecurityAlgorithms;
  private ElectionSet<?>                                               electionSet;
  private MessageHandler<SignEncryptedPrivateCredentialsResponseEvent> privateCredentialsResponseEventMessageHandler;
  private MessageHandler<ControlComponentPublicKeyPublicationEvent>    publicKeyPubEventHandler;
  private MessageHandler<ShufflePublicationEvent>                      shufflePublicationsHandler;
  private MessageHandler<PartialDecryptionsPublicationEvent>           partialDecryptionsPublicationsHandler;
  private MessageHandler<PublicParametersMulticastEvent>               publicParamsPubEventHandler;
  private MessageHandler<ControlComponentPublicKeyMulticastEvent>      publicKeyForwardPubEventHandler;
  private MessageHandler<ElectionOfficerPublicKeyMulticastEvent>       electionOfficerKeyPubEventHandler;
  private MessageHandler<ControlComponentInitializedPublicationEvent>  controlComponentInitializedEventHandler;
  private MessageHandler<ElectionSetMulticastEvent>                    electionSetPubEventHandler;
  private MessageHandler<BuildPublicCredentialsFinishedEvent>          finishedBuildingPublicCredentialsHandler;
  private Decryptions                                                  electionOfficerDecryptions;
  private EncryptionPublicKey                                          electionOfficerPublicKey;
  private DecryptionProof                                              electionOfficerDecryptionProof;
  private Tally                                                        actualTally;
  private MessageHandler<PrimesPublicationEvent>                       primesPublicationEventMessageHandler;
  private MessageHandler<VotersMulticastEvent>                         votersPublicationHandler;
  private List<Voter>                                                  voters;
  private Map<String, Map<String, BigInteger>>                         verificationKeys;

  /**
   * @param securityLevel       the protocol's security level
   * @param protocolId          the protocol instance id
   * @param nbControlComponents number of deployed control components
   * @param objectMapper        jackson object mapper for json serialisation
   * @param rabbitUtilities     the rabbit utilities
   * @param signatureService    the signature service
   * @param randomGenerator     cryptographically secured random generator
   * @param waitTimeout         the wait timeout
   * @param batchSize           the batch size
   */
  public ProtocolClientImpl(int securityLevel,
                            String protocolId,
                            int nbControlComponents, RabbitUtilities rabbitUtilities,
                            SignatureService signatureService,
                            ObjectMapper objectMapper,
                            RandomGenerator randomGenerator,
                            long waitTimeout,
                            int batchSize) {
    this.securityLevel = securityLevel;
    this.protocolId = protocolId;
    this.rabbitUtilities = rabbitUtilities;
    this.signatureService = signatureService;
    this.objectMapper = objectMapper;
    this.nbControlComponents = nbControlComponents;
    this.randomGenerator = randomGenerator;
    this.waitTimeout = waitTimeout;
    this.batchSize = batchSize;
    this.publicKeyParts = new CopyOnWriteArrayList<>();
    this.printingAuthoritiesPrivateKeysByName = new ConcurrentHashMap<>();
    this.finalShuffle = new CopyOnWriteArrayList<>();
    this.controlComponentsPartialDecryptions = new CopyOnWriteArrayList<>();
    this.ccPartIndexes = ImmutableList.copyOf(
        IntStream.range(0, nbControlComponents).boxed()
                 .map(i -> Endpoints.CONTROL_COMPONENT + "-" + i)
                 .collect(Collectors.toList()));

    this.algorithmsSpec = new AlgorithmsSpec(
        ALGO_KEYSPEC_ALGORITHM,
        ALGO_CIPHER_PROVIDER,
        ALGO_CIPHER_ALGORITHM,
        ALGO_DIGEST_PROVIDER,
        ALGO_DIGEST_ALGORITHM,
        ALGO_RNG_PROVIDER,
        ALGO_RNG_ALGORITHM
    );

    rabbitUtilities.createDefaultMessageHandlerForRoutingKey(
        Channels.ACK,
        protocolId,
        Endpoints.PROTOCOL_CLIENT,
        AcknowledgementEvent.class,
        waitTimeout,
        rabbitUtilities::processAcknowledgement
    );

    // load verification keys
    VerificationKeysConfiguration verificationKeysConfiguration = new VerificationKeysConfiguration();
    this.verificationKeys = verificationKeysConfiguration.verificationKeys(securityLevel, algorithmsSpec, objectMapper);
  }

  @Override
  public MessageHandler<Event> createDeadLetterHandler(Class eventClass) {
    return rabbitUtilities.createDefaultMessageHandler(
        Channels.DLX,
        protocolId,
        eventClass,
        false,
        waitTimeout);
  }

  private <T extends Event> void ensureTaskHasBeenCompleted(MessageHandler<T> messageHandler, ProgressTracker
      tracker, long total) {
    ensureTaskHasBeenCompleted(messageHandler, messageHandler::getMessagesCount, tracker, total);
  }

  private <T extends Event> void ensureTaskHasBeenCompleted(MessageHandler<T> messageHandler,
                                                            LongSupplier done,
                                                            ProgressTracker tracker,
                                                            long total) {

    tracker.updateProgress(0, total);

    messageHandler.waitUntil(() -> {
      long doneUnits = done.getAsLong();
      tracker.updateProgress(doneUnits, total);
      return doneUnits == total;
    });
  }

  @Override
  public String getProtocolId() {
    return protocolId;
  }

  @Override
  public void registerPrintingAuthorityPrivateKey(String authorityName, IdentificationPrivateKey privateKey) {
    this.printingAuthoritiesPrivateKeysByName.put(authorityName, privateKey);
  }

  @Override
  public void setPublicParameters(PublicParameters publicParameters) {
    this.publicParameters = publicParameters;
  }

  @Override
  public PublicParameters sendPublicParameters(PublicParametersFactory securityLevel) {
    PublicParameters parameters = securityLevel.createPublicParameters(nbControlComponents);
    sendPublicParameters(parameters);
    return parameters;
  }

  @Override
  public void sendPublicParameters(PublicParameters publicParameters) {
    log.info("Sending public parameters");
    this.publicParameters = publicParameters;
    final PublicParametersPublicationEvent publicParametersPublicationEvent =
        new PublicParametersPublicationEvent(Endpoints.PROTOCOL_CLIENT, new PublicParametersPayload(publicParameters));
    publicParamsPubEventHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId,
                                                    PublicParametersMulticastEvent.class, false, waitTimeout);
    rabbitUtilities.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT,
                            publicParametersPublicationEvent);
  }

  @Override
  public void ensurePublicParametersForwardedToCCs(ProgressTracker tracker) {
    log.info("Waiting for public parameters to be forwarded to the control components");
    ensureTaskHasBeenCompleted(publicParamsPubEventHandler, tracker, 1);
    publicParamsPubEventHandler.stop();
  }

  @Override
  public void requestKeyGeneration() {
    log.info("Request key generation");

    // given a message handler to collect control components public key parts publications
    publicKeyPubEventHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.BULLETIN_BOARD_INITIALIZATION,
                                                    protocolId, ControlComponentPublicKeyPublicationEvent.class,
                                                    false, waitTimeout);

    // and: a message handler to collect bulletin board public key parts forwarding
    publicKeyForwardPubEventHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId,
                                                    ControlComponentPublicKeyMulticastEvent.class, false, waitTimeout);

    // when: requesting key generation
    KeyGenerationRequestEvent keyGenerationRequestEvent =
        new KeyGenerationRequestEvent(Endpoints.PROTOCOL_CLIENT, ccPartIndexes);
    rabbitUtilities.publish(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT,
                            keyGenerationRequestEvent);
  }

  @Override
  public void ensurePublicKeyPartsPublishedToBB(ProgressTracker tracker) {
    log.info("Waiting for the public key parts to be published to the bulletin board");
    ensureTaskHasBeenCompleted(publicKeyPubEventHandler, tracker, nbControlComponents);
    storePublicKeyParts();
    publicKeyPubEventHandler.stop();
  }

  private void storePublicKeyParts() {
    Comparator<ControlComponentPublicKeyPublicationEvent> byControlComponentIndex =
        Comparator.comparing(e -> e.getPayload().getControlComponentIndex());
    List<EncryptionPublicKey> controlComponentKeys =
        publicKeyPubEventHandler.getEvents().stream().sorted(byControlComponentIndex)
                                .map(e -> e.getPayload().getEncryptionPublicKey())
                                .collect(Collectors.toList());
    publicKeyParts.addAll(controlComponentKeys);
  }

  @Override
  public void ensurePublicKeyPartsForwardedToCCs(ProgressTracker tracker) {
    log.info("Waiting for the public key parts to be forwarded to the control components");
    ensureTaskHasBeenCompleted(publicKeyForwardPubEventHandler, tracker, nbControlComponents);
    publicKeyForwardPubEventHandler.stop();
  }

  @Override
  public void sendElectionOfficerPublicKey(EncryptionPublicKey publicKey) {
    log.info("Send the election officer public key");

    // given: the election officer public key
    if (!GeneralAlgorithms.isMember(publicKey.getPublicKey(), publicParameters.getEncryptionGroup())) {
      throw new InvalidElectionOfficerPublicKeyRuntimeException();
    }
    electionOfficerPublicKey = publicKey;

    publicKeyParts.add(electionOfficerPublicKey);
    ElectionOfficerPublicKeyPublicationEvent electionOfficerPublicKeyPublicationEvent =
        new ElectionOfficerPublicKeyPublicationEvent(Endpoints.PROTOCOL_CLIENT,
                                                     new ElectionOfficerKeyPayload(electionOfficerPublicKey));

    // and: a message handler to collect bulletin board responses
    electionOfficerKeyPubEventHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZATION,
                                                    protocolId,
                                                    ElectionOfficerPublicKeyMulticastEvent.class,
                                                    event -> rabbitUtilities.acknowledge(event, protocolId),
                                                    false,
                                                    waitTimeout);

    // and: a message handler to collect the control components readiness events
    controlComponentInitializedEventHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZED,
                                                    protocolId,
                                                    ControlComponentInitializedPublicationEvent.class,
                                                    event -> rabbitUtilities.acknowledge(event, protocolId),
                                                    true,
                                                    waitTimeout);

    // when the election officer public key is published
    rabbitUtilities
        .publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT,
                 electionOfficerPublicKeyPublicationEvent);
  }

  @Override
  public void ensureElectionOfficerPublicKeyForwardedToCCs(ProgressTracker tracker) {
    log.info("Wait for the election officer public key to be forwarded to the control components");
    ensureTaskHasBeenCompleted(electionOfficerKeyPubEventHandler, tracker, 1);
    electionOfficerKeyPubEventHandler.stop();
  }

  @Override
  public void ensureControlComponentsReady(ProgressTracker tracker) {
    log.info("Waiting for the control components to have computed the system public key");
    ensureTaskHasBeenCompleted(controlComponentInitializedEventHandler, tracker, nbControlComponents);
    controlComponentInitializedEventHandler.stop();
  }

  @Override
  public void reinitFromProtocolState() {
    // PublicParameters sent
    this.publicParameters = fetchPublicParameters();

    // recover: requestKeyGeneration
    this.publicKeyParts.clear();
    final EncryptionGroup encryptionGroup = publicParameters.getEncryptionGroup();
    fetchPublicKeyParts().entrySet().stream()
                         .sorted(Comparator.comparing(Map.Entry::getKey))
                         .map(Map.Entry::getValue)
                         .forEach(publicKeyParts::add);

    // ElectionSet sent
    this.electionSet = fetchElectionSet();

    // ElectionOfficerPublicKey sent
    this.electionOfficerPublicKey = fetchElectionOfficerKey();
    this.publicKeyParts.add(electionOfficerPublicKey);

    // init hash &co (as in initialization)
    createHash();
    createGeneralAlgorithms(new PrimesCache(fetchPrimes(), encryptionGroup));
    createChannelSecurityAlgorithms();
  }

  @Override
  public void sendElectionSet(ElectionSetWithPublicKey electionSet) {
    log.info("Send the election set");
    this.electionSet = electionSet;

    // given: some election set
    ElectionSetPayload payload = new ElectionSetPayload(electionSet);

    ElectionSetPublicationEvent electionSetPublicationEvent =
        new ElectionSetPublicationEvent(Endpoints.PROTOCOL_CLIENT, payload, sign(payload),
                                        signatureService.getVerificationKeyHash());

    // and: a message handler to collect the election set sent to the the control components
    electionSetPubEventHandler =
        rabbitUtilities
            .createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId,
                                         ElectionSetMulticastEvent.class, false, waitTimeout);

    // and: a message handler to collect the primes publication events
    primesPublicationEventMessageHandler =
        rabbitUtilities
            .createDefaultMessageHandler(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId,
                                         PrimesPublicationEvent.class, false, waitTimeout);

    // when: the election set is submitted to the protocol
    rabbitUtilities.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT,
                            electionSetPublicationEvent);
  }

  private Signature sign(Payload payload) {
    try {
      return signatureService.sign(protocolId, payload);
    } catch (ChannelSecurityException e) {
      throw new ProtocolException(e);
    }
  }

  @Override
  public void ensureElectionSetForwardedToCCs(ProgressTracker tracker) {
    log.info("Wait for the election set to be forwarded to the control components");
    ensureTaskHasBeenCompleted(electionSetPubEventHandler, tracker, 1);
    electionSetPubEventHandler.stop();
  }

  @Override
  public void sendVoters(List<Voter> voters, int batchSize) {
    Preconditions.checkArgument(
        voters.size() == electionSet.getVoterCount(),
        "The number of voters must match the declared number of voters in the election set");
    log.info("Sending the voters.");
    this.voters = voters;

    // given: a message handler to collect the public credentials parts sent back to the control components
    finishedBuildingPublicCredentialsHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.CONTROL_COMPONENT_BUILD_CREDS,
                                                    protocolId, BuildPublicCredentialsFinishedEvent.class,
                                                    true, waitTimeout);

    // and: a message handler to collect the election set sent to the the control components
    votersPublicationHandler =
        rabbitUtilities
            .createDefaultMessageHandler(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId,
                                         VotersMulticastEvent.class, false, waitTimeout);

    // when: the voters are sent to the bulletin board
    IntStream.rangeClosed(0, voters.size() / batchSize).forEachOrdered(i -> {
      int fromIncl = i * batchSize;
      int toExcl = Math.min((i + 1) * batchSize, voters.size());
      VotersPayload payload = new VotersPayload(voters.subList(fromIncl, toExcl));
      VotersPublicationEvent votersPublicationEvent = new VotersPublicationEvent(Endpoints.PROTOCOL_CLIENT, payload);
      rabbitUtilities.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT,
                              votersPublicationEvent);
    });

    final VotersPublishedEvent event = new VotersPublishedEvent(Endpoints.PROTOCOL_CLIENT, ccPartIndexes);
    rabbitUtilities.publish(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId, Endpoints.PROTOCOL_CLIENT, event);
  }

  @Override
  public void ensureVotersForwardedToCCs(ProgressTracker tracker) {
    log.info("Wait for the voters to be forwarded to the control components");
    ensureTaskHasBeenCompleted(
        votersPublicationHandler,
        () -> votersPublicationHandler.getEvents().stream()
                                      .mapToLong(event -> event.getPayload().getVoters().size()).sum(),
        tracker,
        electionSet.getVoterCount());
    votersPublicationHandler.stop();
  }

  @Override
  public void ensurePublicCredentialsBuiltForAllCCs(ProgressTracker tracker) {
    log.info("Wait for the control components to signal they finished building the public credentials");
    ensureTaskHasBeenCompleted(
        finishedBuildingPublicCredentialsHandler,
        () -> Math.toIntExact(
            finishedBuildingPublicCredentialsHandler.getEvents().stream()
                                                    .map(BuildPublicCredentialsFinishedEvent::getCcIndex)
                                                    .distinct()
                                                    .count()),
        tracker,
        nbControlComponents);
    finishedBuildingPublicCredentialsHandler.stop();
  }

  @Override
  public void ensurePrimesSentToBB(ProgressTracker tracker) {
    log.info("Wait for the primes to be sent to the bulletin board");
    ensureTaskHasBeenCompleted(primesPublicationEventMessageHandler, tracker, nbControlComponents);
    primesPublicationEventMessageHandler.stop();
  }

  @Override
  public void requestPrivateCredentials(Path outputFolder,
                                        Consumer<Path> printerFileConsumer,
                                        ProgressTrackerByCC tracker) {
    log.info("Request the private credentials");

    // given: a valid output folder
    createValidOutputFolder(outputFolder);

    // and: a message handler to collect responses and store them into printer files
    privateCredentialsResponseEventMessageHandler = new MessageHandlerCountMessagesImpl<>(
        protocolId,
        SignEncryptedPrivateCredentialsResponseEvent.class,
        rabbitUtilities.getMessageConverter(),
        savePrinterFilesForMessage(outputFolder, printerFileConsumer, tracker),
        waitTimeout);

    rabbitUtilities.bindHandler(Channels.SIGN_ENCRYPTED_PRIVATE_CREDENTIALS,
                                privateCredentialsResponseEventMessageHandler,
                                true,
                                2 * Runtime.getRuntime().availableProcessors());

    final List<List<Integer>> voterIdSlices =
        Lists.partition(IntStream.range(0, voters.size()).boxed().collect(Collectors.toList()), batchSize);

    final int totalSliceCount = voterIdSlices.size();

    int privateCredentialsRequestsSentCount = 0;
    while (privateCredentialsRequestsSentCount < totalSliceCount) {
      final int newRequestsSentCount =
          Math.min(privateCredentialsRequestsSentCount + THROTTLING_BUFFER_MIN_SIZE, totalSliceCount);

      // when: the private credentials requests are sent to the protocol
      voterIdSlices.subList(privateCredentialsRequestsSentCount, newRequestsSentCount).forEach(voterIdSlice -> {
        VoterIdsPayload payload = new VoterIdsPayload(voterIdSlice);
        SignEncryptedPrivateCredentialsRequestEvent signEncryptedPrivateCredentialsRequestEvent =
            new SignEncryptedPrivateCredentialsRequestEvent(Endpoints.PROTOCOL_CLIENT, ccPartIndexes, payload);
        rabbitUtilities.publish(Channels.CONTROL_COMPONENT_INITIALIZATION,
                                protocolId,
                                Endpoints.PROTOCOL_CLIENT,
                                signEncryptedPrivateCredentialsRequestEvent);
      });

      privateCredentialsRequestsSentCount = newRequestsSentCount;
      privateCredentialsResponseEventMessageHandler.waitUntil(
          () -> newRequestsSentCount - privateCredentialsResponseEventMessageHandler.getMessagesCount() <
                THROTTLING_BUFFER_MIN_SIZE);
    }
  }

  private void createValidOutputFolder(Path outputFolder) {
    try {
      Files.createDirectories(outputFolder, (FileAttribute[]) new FileAttribute[]{});
    } catch (IOException e) {
      throw new IllegalStateException("Error caught while making sure that directory [" + outputFolder + "] exists", e);
    }

    File file = outputFolder.toFile();
    Preconditions.checkArgument(file.isDirectory(), outputFolder.toString() + " is not a directory");
    Preconditions.checkArgument(file.canWrite(), "Need to have write access to the output folder");
  }

  private Consumer<SignEncryptedPrivateCredentialsResponseEvent> savePrinterFilesForMessage(
      Path outputFolder, Consumer<Path> printerFileConsumer, ProgressTrackerByCC tracker) {

    return message -> {
      int controlComponentIndex = message.getControlComponentIndex();
      final String qualifier = message.getQualifier();

      log.info("Processing private credentials for CC index: [{}] and qualifier: [{}]",
               controlComponentIndex,
               qualifier);
      message.getPrivateCredentialsByPrintingAuthority()
             .forEach(savePrinterFile(outputFolder, controlComponentIndex, printerFileConsumer, qualifier));
      rabbitUtilities.processAcknowledgement(message);

      tracker.incrementProgress(controlComponentIndex, (int) Math.ceil(voters.size() / ((double) batchSize)));
    };
  }

  @Override
  public void ensurePrivateCredentialsPublished() {
    log.info("Wait for the private credentials to be published");

    int expectedMessagesByCC = (int) Math.ceil(voters.size() / ((double) batchSize));

    privateCredentialsResponseEventMessageHandler.waitUntil(
        () -> privateCredentialsResponseEventMessageHandler
                  .getMessagesCount() == nbControlComponents * expectedMessagesByCC);

    privateCredentialsResponseEventMessageHandler.stop();
    log.info("All the private credentials have been received");
  }

  @Override
  public int decryptPrinterFiles(Path printerFilesFolder) {
    log.info("Start decrypting the printer files");
    Map<String, PrintingAuthoritySimulator> printingAuthoritySimulators = createPrintingAuthoritySimulators();

    Map<String, Map<String, Map<String, Path>>> printerFileNameByCcIndexAndQualifierAndAuthorityName =
        indexPrinterFiles(printerFilesFolder);

    Path votingCardsPath = printerFilesFolder.resolve(VOTING_CARDS_SUBDIR);
    votingCardsPath.toFile().mkdir();

    AtomicInteger votingCardsCount = new AtomicInteger(0);

    printerFileNameByCcIndexAndQualifierAndAuthorityName.entrySet().parallelStream().flatMap(entry -> {
      PrintingAuthoritySimulator printingAuthoritySimulator = printingAuthoritySimulators.get(entry.getKey());
      return printingAuthoritySimulator.print(entry.getValue());
    }).forEach(votingCard -> {
      try (OutputStream outputStream = Files
          .newOutputStream(votingCardsPath.resolve(String.format("%d.json", votingCard.getVoter().getVoterId())))) {
        objectMapper.writeValue(outputStream, votingCard);
        votingCardsCount.incrementAndGet();
      } catch (IOException e) {
        throw new ProtocolException("couldn't write voting card", e);
      }
    });

    log.info("{} voting cards written to {}", votingCardsCount, votingCardsPath);
    return votingCardsCount.get();
  }

  private Map<String, Map<String, Map<String, Path>>> indexPrinterFiles(Path printerFilesFolder) {
    Map<String, Map<String, Map<String, Path>>> printerFileNameByCcIndexAndQualifierAndAuthorityName = new HashMap<>();
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(printerFilesFolder, "*.epf")) {
      stream.forEach(path -> {
        try {
          EpfFileName epfFileName = EpfFileName.parse(path);
          String printingAutorityName = epfFileName.getPrintingAuthorityName();
          String ccIndex = Integer.toString(epfFileName.getControlComponentIndex());
          String qualifier = epfFileName.getQualifier();

          Map<String, Map<String, Path>> printerFileNameByCcIndexAndQualifier =
              printerFileNameByCcIndexAndQualifierAndAuthorityName
                  .computeIfAbsent(printingAutorityName, s -> new HashMap<>());

          Map<String, Path> printerFileNameByCcIndex =
              printerFileNameByCcIndexAndQualifier.computeIfAbsent(qualifier, s -> new HashMap<>());

          printerFileNameByCcIndex.put(ccIndex, path);

        } catch (IllegalArgumentException e) {
          throw new IllegalStateException("Invalid epf file name: " + path, e);
        }
      });
    } catch (IOException e) {
      throw new IllegalArgumentException("Incorrect printer files folder: " + printerFilesFolder, e);
    }
    return printerFileNameByCcIndexAndQualifierAndAuthorityName;
  }

  @Override
  public List<ObliviousTransferResponse> publishBallot(int voterIndex, BallotAndQuery ballotAndQuery) {
    Preconditions.checkNotNull(ballotAndQuery, "ballotAndQuery must not be null");
    MessageHandler<ObliviousTransferResponseCreationEvent> handler =
        rabbitUtilities.createDefaultMessageHandlerForRoutingKey(
            Channels.OBLIVIOUS_TRANSFER_RESPONSE_CREATIONS,
            protocolId,
            Integer.toString(voterIndex),
            ObliviousTransferResponseCreationEvent.class,
            waitTimeout,
            event -> rabbitUtilities.acknowledge(event, protocolId));
    List<String> acknowledgementParts = ImmutableList.<String>builder().add(Endpoints.BULLETIN_BOARD)
                                                                       .addAll(ccPartIndexes).build();
    BallotPublicationPayload payload = new BallotPublicationPayload(voterIndex, ballotAndQuery);
    rabbitUtilities.publish(Channels.BALLOT_PUBLICATIONS, protocolId, Endpoints.PROTOCOL_CLIENT,
                            new BallotPublicationEvent(Endpoints.PROTOCOL_CLIENT, acknowledgementParts, payload));
    return collectControlComponentsResponses(handler,
                                             e -> e.getPayload().getControlComponentIndex(),
                                             e -> e.getPayload().getObliviousTransferResponse());
  }

  @Override
  public List<FinalizationCodePart> submitConfirmation(int voterIndex, Confirmation confirmation)
      throws ConfirmationFailedException {
    MessageHandler<FinalizationCodePartPublicationEvent> mainHandler =
        rabbitUtilities.createDefaultMessageHandlerForRoutingKey(
            Channels.FINALIZATION_CODE_PART_PUBLICATIONS,
            protocolId,
            Integer.toString(voterIndex),
            FinalizationCodePartPublicationEvent.class,
            waitTimeout,
            event -> rabbitUtilities.acknowledge(event, protocolId));

    MessageHandler<ConfirmationFailedEvent> confirmationErrorHandler =
        rabbitUtilities.createDefaultMessageHandlerForRoutingKey(
            Channels.CONFIRMATION_FAILED,
            protocolId,
            Integer.toString(voterIndex),
            ConfirmationFailedEvent.class,
            waitTimeout,
            event ->
                rabbitUtilities.acknowledge(event, protocolId)
        );

    CompletableFuture<List<FinalizationCodePart>> finalizationCodeParts = new CompletableFuture<>();
    mainHandler.postProcessEvent(event -> {
      List<FinalizationCodePart> finalizationCodePartsReceived = mainHandler
          .getEvents().stream().map(e -> e.getPayload().getFinalizationCodePart()).distinct()
          .collect(Collectors.toList());

      if (finalizationCodePartsReceived.size() == nbControlComponents) {
        finalizationCodeParts.complete(finalizationCodePartsReceived);
      }
    });


    confirmationErrorHandler.postProcessEvent(
        event -> finalizationCodeParts.completeExceptionally(new ConfirmationFailedException())
    );

    List<String> acknowledgementParts = ImmutableList.<String>builder().add(Endpoints.BULLETIN_BOARD)
                                                                       .addAll(ccPartIndexes).build();
    BallotConfirmationPayload payload = new BallotConfirmationPayload(voterIndex, confirmation);
    rabbitUtilities.publish(Channels.BALLOT_CONFIRMATION_PUBLICATIONS, protocolId, Endpoints.PROTOCOL_CLIENT,
                            new BallotConfirmationPublicationEvent(Endpoints.PROTOCOL_CLIENT, acknowledgementParts, payload));

    try {
      return finalizationCodeParts.get(waitTimeout, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new IllegalStateException("wait interrupted", e);
    } catch (ExecutionException e) {
      log.debug("Confirmation failed for voter " + voterIndex, e);
      if (e.getCause() instanceof ConfirmationFailedException) {
        throw (ConfirmationFailedException) e.getCause();
      } else {
        throw new ProtocolException(e.getCause());
      }
    } catch (TimeoutException e) {
      throw new IllegalStateException("wait timeout", e);
    } finally {
      mainHandler.stop();
      confirmationErrorHandler.stop();
    }
  }

  private <E extends Event, T> List<T> collectControlComponentsResponses(MessageHandler<E> handler,
                                                                         Function<E, Integer> ccIndexExtractor,
                                                                         Function<E, T> payloadExtractor) {
    try {
      handler.waitUntil(
          () -> handler.getEvents().stream().map(ccIndexExtractor).distinct().count() == nbControlComponents
      );
      HashSet<Integer> seen = new HashSet<>();
      return handler.getEvents().stream()
                    .filter(e -> seen.add(ccIndexExtractor.apply(e)))
                    .sorted(Comparator.comparing(ccIndexExtractor))
                    .map(payloadExtractor)
                    .collect(Collectors.toList());
    } finally {
      handler.stop();
    }
  }

  @Override
  public Map<Integer, VotePerformanceStats> castVotes(int votesCount, Path printerFilesFolder) {
    log.info("Start to publish the ballots");
    final Path votingCardsPath = printerFilesFolder.resolve(VOTING_CARDS_SUBDIR);
    final List<String> votingCards = listVotingCards(votingCardsPath);
    final long availableVoters = votingCards.size();
    Preconditions.checkArgument(votesCount <= availableVoters,
                                "Number of votes to cast cannot exceed the number of available voters (%s > %s)",
                                votesCount, availableVoters);
    // when the voters cast their votes
    final Map<Integer, CountingCircle> ccPerVoterId = new HashMap<>();
    final Map<CountingCircle, List<Long>> expectedResult = new HashMap<>();
    final Map<Integer, VotePerformanceStats> performanceStatsMap = new HashMap<>();

    MessageHandler<VoteProcessingReplyEvent> voteProcessingRepliesHandler =
        rabbitUtilities.createDefaultMessageHandler(RabbitUtilities.VOTE_PROCESSING_REPLY_CHANNEL, protocolId,
                                                    VoteProcessingReplyEvent.class,
                                                    reply -> {
                                                      int voterId = reply.getVoterId();
                                                      VoteResult voteResult = reply.getVoteResult();
                                                      addVoteToExpectedResult(voteResult.getVoteCast(),
                                                                              ccPerVoterId.get(voterId),
                                                                              expectedResult);
                                                      performanceStatsMap
                                                          .put(voterId, voteResult.getVotePerformanceStats());
                                                    },
                                                    true,
                                                    waitTimeout);

    int voteRequestsCount = 0;
    while (voteRequestsCount < votesCount) {
      final int newVoteRequestsCount = Math.min(votesCount, voteRequestsCount + THROTTLING_BUFFER_MIN_SIZE);
      log.debug("Sending vote processing requests for voters [{}, {})", voteRequestsCount, newVoteRequestsCount);

      for (String cardName : votingCards.subList(voteRequestsCount, newVoteRequestsCount)) {
        VotingCard votingCard = loadVotingCard(votingCardsPath, cardName);
        ccPerVoterId.put(votingCard.getVoter().getVoterId(), votingCard.getVoter().getCountingCircle());

        VoteProcessingRequestEvent voteProcessingRequestEvent =
            new VoteProcessingRequestEvent(publicParameters, electionSetForVerification(), publicKeyParts, votingCard);
        rabbitUtilities.publish(RabbitUtilities.VOTE_PROCESSING_REQUEST_CHANNEL, protocolId, Endpoints.PROTOCOL_CLIENT,
                                voteProcessingRequestEvent);
      }
      voteRequestsCount = newVoteRequestsCount;
      log.info("Sent {} votes out of {}", voteRequestsCount, votesCount);

      voteProcessingRepliesHandler
          .waitUntil(() -> newVoteRequestsCount - voteProcessingRepliesHandler
              .getMessagesCount() <= THROTTLING_BUFFER_MIN_SIZE);
    }

    voteProcessingRepliesHandler.waitUntil(() -> voteProcessingRepliesHandler.getMessagesCount() == votesCount);
    voteProcessingRepliesHandler.stop();

    expectedTally = new Tally(expectedResult);
    log.info("Ballots published");
    return performanceStatsMap;
  }

  private static List<String> listVotingCards(Path votingCardsPath) {
    try (Stream<Path> directoryStream = Files.list(votingCardsPath)) {
      return directoryStream.map(path -> path.getFileName().toString())
                            .filter(name -> name.endsWith(".json"))
                            .sorted()
                            .collect(Collectors.toList());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private ElectionSetForVerification electionSetForVerification() {
    if (electionSet instanceof ElectionSetForVerification) {
      return (ElectionSetForVerification) electionSet;
    }

    // if we have the voters list we can define it ourselves -- or we fetch it from the protocol
    if (voters != null) {
      List<PrintingAuthorityForVerification> printingAuthoritiesForVerification =
          electionSet.getPrintingAuthorities().stream()
                     .map(pA -> new PrintingAuthorityForVerification(
                         pA.getName()))
                     .collect(Collectors.toList());

      return new ElectionSetForVerification(
          electionSet.getElections(),
          electionSet.getCandidates(),
          printingAuthoritiesForVerification,
          electionSet.getVoterCount(),
          electionSet.getCountingCircleCount(),
          voters.stream().sorted(Comparator.comparingInt(Voter::getVoterId))
                .mapToInt(v -> v.getCountingCircle().getId())
                .toArray());
    } else {
      return fetchElectionSet();
    }
  }

  private VotingCard loadVotingCard(Path votingCardsFolderPath, String cardName) {
    Path votingCardPath = votingCardsFolderPath.resolve(cardName);
    try (InputStream is = Files.newInputStream(votingCardPath)) {
      return objectMapper.readValue(is, VotingCard.class);
    } catch (IOException e) {
      throw new IllegalArgumentException("cannot read voting card: " + votingCardPath, e);
    }
  }

  @Override
  public long getVotesCastCount() {
    MessageHandler<VotesCastCountResponseEvent> handler =
        rabbitUtilities.createDefaultMessageHandler(Channels.VOTES_CAST_COUNT_RESPONSE, protocolId,
                                                    VotesCastCountResponseEvent.class, true, waitTimeout);
    rabbitUtilities.publish(Channels.VOTES_CAST_COUNT_REQUEST, protocolId,
                            Endpoints.PROTOCOL_CLIENT, new VotesCastCountRequestEvent());
    try {
      handler.waitUntil(() -> handler.getMessagesCount() > 0);
      return handler.getEvents().stream().findFirst()
                    .map(VotesCastCountResponseEvent::getVotesCastCount)
                    .orElseThrow(() -> new IllegalStateException("Votes cast count can't be retrieved"));
    } finally {
      handler.stop();
    }
  }

  @Override
  public void requestShufflingAndPartialDecryption() {
    log.info("Request shuffling and partial decryption");

    // given: the end of the voting phase
    // and: a message handler to collect shuffles and their associated proofs
    shufflePublicationsHandler =
        rabbitUtilities
            .createDefaultMessageHandler(Channels.SHUFFLE_PUBLICATIONS,
                                         protocolId,
                                         ShufflePublicationEvent.class,
                                         event -> rabbitUtilities.acknowledge(event, protocolId),
                                         false,
                                         waitTimeout);

    // and: a message handler to collect partials decryptionsList and their associated proofs
    partialDecryptionsPublicationsHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.PARTIAL_DECRYPTIONS_PUBLICATIONS,
                                                    protocolId,
                                                    PartialDecryptionsPublicationEvent.class,
                                                    event -> rabbitUtilities.acknowledge(event, protocolId),
                                                    false,
                                                    waitTimeout);

    // when: the shuffling is requested
    rabbitUtilities
        .publish(Channels.SHUFFLING_REQUESTS, protocolId, Endpoints.PROTOCOL_CLIENT,
                 new ShufflingRequestEvent(Endpoints.PROTOCOL_CLIENT));
  }

  @Override
  public void ensureShufflesPublished(ProgressTracker tracker) {
    log.info("Waiting for the shuffles to have been published");
    ensureTaskHasBeenCompleted(shufflePublicationsHandler, tracker, nbControlComponents);
    storeFinalShuffle();
    shufflePublicationsHandler.stop();
  }

  @Override
  public void ensurePartialDecryptionsPublished(ProgressTracker tracker) {
    log.info("Waiting for the partial decryptions to have been published");
    ensureTaskHasBeenCompleted(partialDecryptionsPublicationsHandler, tracker, nbControlComponents);
    storePartialDecryptionsAndProofs();
    partialDecryptionsPublicationsHandler.stop();
  }

  private void storeFinalShuffle() {
    Comparator<ShufflePublicationEvent> byCCIndex =
        Comparator.comparing(e -> e.getPayload().getControlComponentIndex());

    List<ShufflePublicationEvent> shufflePublicationEvents = shufflePublicationsHandler.getEvents();
    finalShuffle.addAll(shufflePublicationEvents.stream()
                                                .sorted(byCCIndex)
                                                .collect(Collectors.toList())
                                                .get(nbControlComponents - 1).getPayload().getShuffleAndProof().getShuffle());
  }

  private void storePartialDecryptionsAndProofs() {
    Comparator<PartialDecryptionsPublicationEvent> byCCIndex =
        Comparator.comparing(e -> e.getPayload().getControlComponentIndex());

    List<PartialDecryptionsPublicationEvent> messages = partialDecryptionsPublicationsHandler.getEvents();

    controlComponentsPartialDecryptions.addAll(
        messages.stream()
                .sorted(byCCIndex)
                .map(decryptionAndProof -> decryptionAndProof.getPayload().getPartialDecryptionsAndProof().getDecryptions())
                .collect(Collectors.toList())
    );
  }

  @Override
  public boolean checkTally(EncryptionPrivateKey electionOfficerPrivateKey) {
    log.info("Checking tally");

    // when: the election authority performs their own decryption
    DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms = createDecryptionAuthorityAlgorithms();
    electionOfficerDecryptions = decryptionAuthorityAlgorithms
        .getPartialDecryptions(finalShuffle, electionOfficerPrivateKey.getPrivateKey());

    electionOfficerDecryptionProof = decryptionAuthorityAlgorithms.genDecryptionProof(
        electionOfficerPrivateKey.getPrivateKey(),
        electionOfficerPublicKey.getPublicKey(),
        finalShuffle,
        electionOfficerDecryptions);

    // and: combines the decryptions with the control components decryptions
    TallyingAuthoritiesAlgorithm tallyingAuthoritiesAlgorithms = createTallyingAuthoritiesAlgorithms();
    List<Decryptions> partialDecryptions = new ArrayList<>(controlComponentsPartialDecryptions);
    partialDecryptions.add(electionOfficerDecryptions);
    Decryptions decryptions = tallyingAuthoritiesAlgorithms.getDecryptions(finalShuffle, partialDecryptions);

    // and: retrieves the counting circle colouring from the decrypted votes
    int candidateCount = electionSet.getCandidates().size();
    int countingCircleCount = electionSet.getCountingCircleCount();

    final List<BigInteger> primes;
    try {
      primes = generalAlgorithms.getPrimes(candidateCount + countingCircleCount);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new IllegalStateException("Cannot generate primes", e);
    }

    Map<BigInteger, CountingCircle> countingCircleByColour = voters
        .stream()
        .map(Voter::getCountingCircle)
        .distinct()
        .collect(Collectors.toMap(
            countingCircle -> primes.get(candidateCount + countingCircle.getId()),
            countingCircle -> countingCircle)
        );

    Map<CountingCircle, Decryptions> decryptionsByCountingCircle = decryptions
        .getDecryptionsList()
        .parallelStream()
        .collect(
            Collectors.groupingBy(
                extractCountingCircleFromDecryption(countingCircleByColour),
                MoreCollectors.toDecryptions()));

    // and: runs the tallying for each counting circle
    Map<CountingCircle, List<List<Boolean>>> votes = decryptionsByCountingCircle.entrySet().parallelStream().collect(
        Collectors.toMap(
            Map.Entry::getKey,
            e -> tallyingAuthoritiesAlgorithms.getVotes(e.getValue(), candidateCount)
        )
    );

    Map<CountingCircle, List<Long>> finalTally = votes
        .entrySet().parallelStream()
        .collect(
            Collectors.toMap(
                Map.Entry::getKey,
                toVoteCountByCandidate(candidateCount)
            ));

    // then: the computed tally should be equal to the expected tally
    actualTally = new Tally(finalTally);
    return expectedTally.equals(actualTally);
  }

  private Function<Map.Entry<CountingCircle, List<List<Boolean>>>, List<Long>> toVoteCountByCandidate(
      int candidateCount
  ) {
    return e -> IntStream
        .range(0, candidateCount)
        .mapToLong(
            i -> e.getValue()
                  .parallelStream()
                  .filter(vote -> vote.get(i))
                  .count()
        )
        .boxed()
        .collect(Collectors.toList());
  }

  private Function<BigInteger, CountingCircle> extractCountingCircleFromDecryption(
      Map<BigInteger, CountingCircle> countingCircleByColour
  ) {
    return decryption ->
        countingCircleByColour
            .entrySet()
            .parallelStream()
            .filter(e -> decryption.mod(e.getKey()).equals(BigInteger.ZERO))
            .findFirst()
            .map(Map.Entry::getValue)
            .orElseThrow(
                () -> new EncryptionColouringRuntimeException("Failed to retrieve color from ballot")
            );
  }

  @Override
  public void writeProtocolAuditLog(ElectionVerificationDataWriter verificationDataWriter)
      throws VerificationDataWriterException {
    log.info("Request the election verification data, step by step");

    verificationDataWriter.initialize();

    final PublicParameters remotePublicParameters = fetchPublicParameters();
    final ElectionSetForVerification remoteElectionSet = fetchElectionSet();
    verificationDataWriter.writePublicParameters(remotePublicParameters);
    verificationDataWriter.writeElectionSet(remoteElectionSet);
    verificationDataWriter.writePrimes(fetchPrimes());
    verificationDataWriter.writeGenerators(fetchGenerators());
    verificationDataWriter.writePublicKeyParts(fetchPublicKeyParts());
    verificationDataWriter.writeElectionOfficerPublicKey(fetchElectionOfficerKey());

    final int s = remotePublicParameters.getS();
    // Too many places where we use List or Map at this stage to get rid of the requirement that
    // voter count is an integer. This will need to be upgraded if we ever go beyond 2 million voters on a single
    // protocol instance
    writePublicCredentialParts(s, Math.toIntExact(remoteElectionSet.getVoterCount()), verificationDataWriter);

    writeBallots(verificationDataWriter);
    writeConfirmations(verificationDataWriter);

    writeShuffles(s, verificationDataWriter);
    writeShuffleProofs(s, verificationDataWriter);
    writePartialDecryptions(s, verificationDataWriter);
    writeDecryptionProofs(s, verificationDataWriter);

    if (actualTally != null) {
      // we only have the actual tally if the election officer private key has been set (simulation mode).
      verificationDataWriter.writeTally(actualTally);
    }
    verificationDataWriter.writeEnd();
  }

  @Override
  public PublicParameters fetchPublicParameters() {
    return requestWaitAndGetValue(VerificationPublicParametersPublicationEvent.class,
                                  VerificationPublicParametersPublicationEvent::getPublicParameters,
                                  new VerificationPublicParametersRequestEvent());
  }

  @Override
  public ElectionSetForVerification fetchElectionSet() {
    return requestWaitAndGetValue(VerificationElectionSetPublicationEvent.class,
                                  VerificationElectionSetPublicationEvent::getElectionSet,
                                  new VerificationElectionSetRequestEvent());
  }

  @Override
  public List<BigInteger> fetchPrimes() {
    return requestWaitAndGetValue(VerificationPrimesPublicationEvent.class,
                                  VerificationPrimesPublicationEvent::getPrimes,
                                  new VerificationPrimesRequestEvent());
  }

  private List<BigInteger> fetchGenerators() {
    return requestWaitAndGetValue(VerificationGeneratorsPublicationEvent.class,
                                  VerificationGeneratorsPublicationEvent::getGenerators,
                                  new VerificationGeneratorsRequestEvent());
  }

  @Override
  public Map<Integer, EncryptionPublicKey> fetchPublicKeyParts() {
    return requestWaitAndGetValue(VerificationPublicKeyPartsPublicationEvent.class,
                                  VerificationPublicKeyPartsPublicationEvent::getPublicKeyParts,
                                  new VerificationPublicKeyPartsRequestEvent());
  }

  @Override
  public EncryptionPublicKey fetchElectionOfficerKey() {
    return requestWaitAndGetValue(VerificationElectionOfficerKeyPublicationEvent.class,
                                  VerificationElectionOfficerKeyPublicationEvent::getElectionOfficerKey,
                                  new VerificationElectionOfficerKeyRequestEvent());
  }

  private void writePublicCredentialParts(int s, int voterCount,
                                          ElectionVerificationDataWriter verificationDataWriter) {
    Consumer<VerificationPublicCredentialPartsPublicationEvent> receiver =
        event -> verificationDataWriter.addPublicCredentials(event.getCcIndex(), event.getPublicCredentialsByVoterId());
    MessageHandler<VerificationPublicCredentialPartsPublicationEvent> messageHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION,
                                                    protocolId, VerificationPublicCredentialPartsPublicationEvent.class,
                                                    receiver, false, waitTimeout);

    final List<List<Integer>> voterIdPartitions =
        Lists.partition(IntStream.range(0, voterCount).boxed().collect(Collectors.toList()), batchSize);
    for (List<Integer> voterIdPartition : voterIdPartitions) {
      for (int i = 0; i < s; i++) {
        final VerificationPublicCredentialPartsRequestEvent requestEvent =
            new VerificationPublicCredentialPartsRequestEvent(i, voterIdPartition);
        rabbitUtilities
            .publish(Channels.ELECTION_VERIFICATION_DATA_REQUEST, protocolId, Endpoints.PROTOCOL_CLIENT, requestEvent);
      }
    }

    messageHandler.waitUntil(
        () -> ElectionVerificationDataWriter.State.PUBLIC_CREDENTIALS_WRITTEN == verificationDataWriter.getState());
    messageHandler.stop();
  }

  private void writeBallots(ElectionVerificationDataWriter verificationDataWriter) {
    getKeyListAndRequestByBlocks(
        VerificationBallotKeysPublicationEvent.class,
        VerificationBallotKeysPublicationEvent::getBallotKeys,
        verificationDataWriter::setExpectedBallotKeys,
        new VerificationBallotKeysRequestEvent(),
        VerificationBallotsPublicationEvent.class,
        VerificationBallotsPublicationEvent::getBallots,
        verificationDataWriter::addBallots,
        VerificationBallotsRequestEvent::new,
        () -> {
          log.debug("checked, currently in state {}", verificationDataWriter.getState());
          return BALLOTS_RECEIVED.equals(verificationDataWriter.getState());
        });
  }

  private void writeConfirmations(ElectionVerificationDataWriter verificationDataWriter) {
    getKeyListAndRequestByBlocks(
        VerificationConfirmationKeysPublicationEvent.class,
        VerificationConfirmationKeysPublicationEvent::getConfirmationKeys,
        verificationDataWriter::setExpectedConfirmationKeys,
        new VerificationConfirmationKeysRequestEvent(),
        VerificationConfirmationsPublicationEvent.class,
        VerificationConfirmationsPublicationEvent::getConfirmations,
        verificationDataWriter::addConfirmations,
        VerificationConfirmationsRequestEvent::new,
        () -> CONFIRMATIONS_RECEIVED.equals(verificationDataWriter.getState()));
  }

  private void writeShuffles(int s, ElectionVerificationDataWriter verificationDataWriter) {
    requestByCcWaitAndWriteValues(s,
                                  VerificationShufflePublicationEvent.class,
                                  VerificationShufflePublicationEvent::getCcIndex,
                                  VerificationShufflePublicationEvent::getShuffle,
                                  VerificationShuffleRequestEvent::new,
                                  verificationDataWriter::addShuffle,
                                  () -> SHUFFLES_RECEIVED.equals(verificationDataWriter.getState()));
  }

  private void writeShuffleProofs(int s, ElectionVerificationDataWriter verificationDataWriter) {
    requestByCcWaitAndWriteValues(s,
                                  VerificationShuffleProofPublicationEvent.class,
                                  VerificationShuffleProofPublicationEvent::getCcIndex,
                                  VerificationShuffleProofPublicationEvent::getShuffleProof,
                                  VerificationShuffleProofRequestEvent::new,
                                  verificationDataWriter::addShuffleProof,
                                  () -> SHUFFLE_PROOFS_RECEIVED.equals(verificationDataWriter.getState()));
  }

  private void writePartialDecryptions(int s, ElectionVerificationDataWriter verificationDataWriter) {
    if (electionOfficerDecryptions != null) {
      // we only have the final decryption if the election officer private key has been set (simulation mode).
      verificationDataWriter.addPartialDecryptions(s, electionOfficerDecryptions);
    }
    requestByCcWaitAndWriteValues(s,
                                  VerificationPartialDecryptionsPublicationEvent.class,
                                  VerificationPartialDecryptionsPublicationEvent::getCcIndex,
                                  VerificationPartialDecryptionsPublicationEvent::getDecryptions,
                                  VerificationPartialDecryptionsRequestEvent::new,
                                  verificationDataWriter::addPartialDecryptions,
                                  () -> PARTIAL_DECRYPTIONS_RECEIVED.equals(verificationDataWriter.getState()));
  }

  private void writeDecryptionProofs(int s, ElectionVerificationDataWriter verificationDataWriter) {
    if (electionOfficerDecryptionProof != null) {
      // we only have the final decryption's proof if the election officer private key has been set (simulation mode).
      verificationDataWriter.addDecryptionProof(s, electionOfficerDecryptionProof);
    }

    requestByCcWaitAndWriteValues(s,
                                  VerificationDecryptionProofPublicationEvent.class,
                                  VerificationDecryptionProofPublicationEvent::getCcIndex,
                                  VerificationDecryptionProofPublicationEvent::getDecryptionProof,
                                  VerificationDecryptionProofRequestEvent::new,
                                  verificationDataWriter::addDecryptionProof,
                                  () -> DECRYPTION_PROOFS_RECEIVED.equals(verificationDataWriter.getState()));
  }

  private <KR extends Event, KP extends Event, R extends Event, P extends Event, T>
  void getKeyListAndRequestByBlocks(
      Class<KP> keysPublicationType,
      Function<KP, List<Integer>> keysExtractor,
      Consumer<List<Integer>> expectedKeysCallback,
      KR keysRequestEvent,
      Class<P> publicationEventType,
      Function<P, Map<Integer, T>> valuesExtractor,
      Consumer<Map<Integer, T>> valuesCallback,
      Function<List<Integer>, R> requestBuilder,
      BooleanSupplier doneCheck) {
    final List<Integer> keys = requestWaitAndGetValue(keysPublicationType, keysExtractor, keysRequestEvent);
    expectedKeysCallback.accept(keys);

    Consumer<P> receiver = event -> valuesCallback.accept(valuesExtractor.apply(event));

    MessageHandler<P> messageHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION,
                                                    protocolId, publicationEventType, receiver, false, waitTimeout);

    for (List<Integer> keysPartition : Lists.partition(keys, batchSize)) {
      rabbitUtilities.publish(Channels.ELECTION_VERIFICATION_DATA_REQUEST, protocolId, Endpoints.PROTOCOL_CLIENT,
                              requestBuilder.apply(keysPartition));
    }
    messageHandler.waitUntil(doneCheck);
    messageHandler.stop();
  }

  private <R extends Event, P extends Event, T> T requestWaitAndGetValue(
      Class<P> publicationEventType, Function<P, T> valueExtractor, R requestEvent) {
    final AtomicReference<T> atomicReference = new AtomicReference<>();
    final Consumer<P> receiver = event -> atomicReference.set(valueExtractor.apply(event));

    final MessageHandler<P> messageHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION,
                                                    protocolId, publicationEventType,
                                                    receiver, true, waitTimeout);
    rabbitUtilities
        .publish(Channels.ELECTION_VERIFICATION_DATA_REQUEST, protocolId, Endpoints.PROTOCOL_CLIENT, requestEvent);
    messageHandler.waitUntil(() -> atomicReference.get() != null);
    messageHandler.stop();

    return atomicReference.get();
  }

  private <R extends Event, P extends Event, T> void requestByCcWaitAndWriteValues(
      int s, Class<P> publicationEventType,
      ToIntFunction<P> keyExtractor,
      Function<P, T> valueExtractor,
      IntFunction<R> requestBuilder,
      BiConsumer<Integer, T> valuePublicationCallback,
      BooleanSupplier doneCheck) {
    Consumer<P> receiver = event -> valuePublicationCallback.accept(keyExtractor.applyAsInt(event),
                                                                    valueExtractor.apply(event));
    MessageHandler<P> messageHandler =
        rabbitUtilities.createDefaultMessageHandler(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION,
                                                    protocolId, publicationEventType,
                                                    receiver, false, waitTimeout);

    for (int i = 0; i < s; i++) {
      rabbitUtilities.publish(Channels.ELECTION_VERIFICATION_DATA_REQUEST, protocolId, Endpoints.PROTOCOL_CLIENT,
                              requestBuilder.apply(i));
    }
    messageHandler.waitUntil(doneCheck);
    messageHandler.stop();
  }

  @Override
  public void requestCleanup() {
    log.info("Request cleanup");
    MessageHandler<CleanupResponseEvent> handler =
        rabbitUtilities.createDefaultMessageHandlerForRoutingKey(
            Channels.ACK,
            protocolId,
            Endpoints.PROTOCOL_CLIENT,
            CleanupResponseEvent.class,
            waitTimeout,
            e -> rabbitUtilities.clearUnacknowledgedEvents(protocolId));

    ImmutableList<String> acknowledgementPartIndexes = FluentIterable.of(Endpoints.BULLETIN_BOARD)
                                                                     .append(ccPartIndexes).toList();
    CleanupRequestEvent cleanupRequest = new CleanupRequestEvent(Endpoints.PROTOCOL_CLIENT, acknowledgementPartIndexes);
    rabbitUtilities.publish(Channels.PROTOCOL_INSTANCE_CLEANUP, protocolId, Endpoints.PROTOCOL_CLIENT, cleanupRequest);

    try {
      handler.waitUntil(() -> handler.getMessagesCount() > 0);
    } finally {
      handler.stop();
    }
  }

  @Override
  public void createHash() {
    hash = new Hash(algorithmsSpec.getDigestAlgorithm(),
                    algorithmsSpec.getDigestProvider(),
                    publicParameters.getSecurityParameters().getUpper_l());
  }

  @Override
  public void createGeneralAlgorithms() {
    int countingCircleCount = electionSet.getCountingCircleCount();
    try {
      PrimesCache primesCache = PrimesCache.populate(electionSet.getCandidates().size() + countingCircleCount,
                                                     publicParameters.getEncryptionGroup());
      createGeneralAlgorithms(primesCache);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new IllegalStateException("Cannot instantiate general algorithms", e);
    }
  }

  private void createGeneralAlgorithms(PrimesCache primesCache) {
    this.generalAlgorithms = new GeneralAlgorithms(hash, publicParameters.getEncryptionGroup(),
                                              publicParameters.getIdentificationGroup(), primesCache);
  }

  @Override
  public void createChannelSecurityAlgorithms() {
    try {
      channelSecurityAlgorithms = new ChannelSecurityAlgorithms(algorithmsSpec,
                                                                hash,
                                                                publicParameters.getIdentificationGroup(),
                                                                randomGenerator);
    } catch (ChannelSecurityException e) {
      throw new IllegalStateException("Cannot instantiate channel security algorithms", e);
    }
  }

  @Override
  public Tally getExpectedTally() {
    return expectedTally;
  }

  private VotingCardPreparationAlgorithms createVotingCardPreparationAlgorithms() {
    return new VotingCardPreparationAlgorithms(publicParameters);
  }

  private DecryptionAuthorityAlgorithms createDecryptionAuthorityAlgorithms() {
    return new DecryptionAuthorityAlgorithms(publicParameters, generalAlgorithms, randomGenerator);
  }

  private TallyingAuthoritiesAlgorithm createTallyingAuthoritiesAlgorithms() {
    return new TallyingAuthoritiesAlgorithm(publicParameters, generalAlgorithms);
  }

  private BiConsumer<PrintingAuthorityWithPublicKey, byte[]> savePrinterFile(Path outputFolder,
                                                                             int controlComponentIndex,
                                                                             Consumer<Path> printerFileConsumer,
                                                                             String qualifier) {
    return (key, value) -> {
      String printerFileName = new EpfFileName(key.getName(), controlComponentIndex, qualifier).toString();
      Path outputPath = outputFolder.resolve(printerFileName).normalize();
      try {
        Files.write(outputPath, value);
        printerFileConsumer.accept(outputPath);
      } catch (IOException e) {
        throw new ProtocolException("Failed to write printer file " + outputPath, e);
      }
    };
  }

  private Map<String, PrintingAuthoritySimulator> createPrintingAuthoritySimulators() {
    VotingCardPreparationAlgorithms votingCardPreparationAlgorithms = createVotingCardPreparationAlgorithms();
    Map<String, PrintingAuthoritySimulator> printingAuthoritySimulators = new HashMap<>();


    for (int i = 0; i < electionSet.getPrintingAuthorities().size(); i++) {
      PrintingAuthority authority = electionSet.getPrintingAuthorities().get(i);
      final IdentificationPrivateKey decryptionKey = loadPrivateKey(authority.getName(), securityLevel, i);

      PrintingAuthoritySimulator simulator = new PrintingAuthoritySimulator(
          publicParameters, electionSet, decryptionKey,
          votingCardPreparationAlgorithms, channelSecurityAlgorithms, objectMapper);

      simulator.setControlComponentPublicKeys(getControlComponentPublicKeys());
      printingAuthoritySimulators.put(authority.getName(), simulator);
    }

    return printingAuthoritySimulators;
  }

  private IdentificationPrivateKey loadPrivateKey(String authorityName, int securityLevel, int index) {
    // if a resource is explicitly specified we use it, otherwise we use the fixture by security-level / index
    IdentificationPrivateKey privateKey = printingAuthoritiesPrivateKeysByName.get(authorityName);

    if (privateKey == null) {
      final String defaultFileName = String.format("/level%d/pa%d-e.sk", securityLevel, index);
      try (InputStream inputStream = getResourceStream(defaultFileName)) {
        // TODO: protect key with a password
        privateKey =  objectMapper.readValue(inputStream, IdentificationPrivateKey.class);
      } catch (IOException e) {
        throw new IllegalStateException("Cannot read printing authority private key from file " + defaultFileName, e);
      }
    }

    return privateKey;
  }

  private List<BigInteger> getControlComponentPublicKeys() {
    return IntStream.range(0, nbControlComponents)
                    .mapToObj(getControlComponentPublicKey())
                    .collect(Collectors.toList());
  }

  private IntFunction<BigInteger> getControlComponentPublicKey() {
    return i -> {
      final Map<String, BigInteger> ccKeys = verificationKeys.get(String.format("cc%d", i));
      return ccKeys.get(ccKeys.keySet().iterator().next());
    };
  }

  private InputStream getResourceStream(String path) {
    return getClass().getResourceAsStream(path);
  }

  // Method implementation is not thread-safe, so needing a global synchronization.
  private synchronized void addVoteToExpectedResult(List<Integer> vote, CountingCircle countingCircle,
                                                    Map<CountingCircle, List<Long>> expectedResult) {
    List<Long> countingCircleResults = expectedResult
        .computeIfAbsent(countingCircle,
                         cc -> IntStream.range(0, electionSet.getCandidates().size())
                                        .mapToObj(i -> 0L).collect(Collectors.toList())
        );


    vote.forEach(
        candidateIndex -> countingCircleResults.set(
            candidateIndex - 1,
            countingCircleResults.get(candidateIndex - 1) + 1L
        )
    );
  }

  @Override
  public void ensureAllSentEventsHaveBeenAcknowledged() {
    Stopwatch stopwatch = Stopwatch.createStarted();
    while (!rabbitUtilities.getUnacknowledgedEvents().isEmpty() && stopwatch.elapsed().toMillis() < waitTimeout) {
      try {
        Thread.sleep(1000L);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
    List<UnacknowledgedEvent> unacknowledgedEvents = rabbitUtilities.getUnacknowledgedEvents();
    if (!unacknowledgedEvents.isEmpty()) {
      throw new IllegalStateException("There are unacknowledged events: " + unacknowledgedEvents);
    }
  }
}
