/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.progress.api;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A progress tracker strategy that tracks the progress of a task for each involved control component.
 */
public abstract class ProgressTrackerByCC {
  private Map<Integer, Integer> done = new ConcurrentHashMap<>();

  /**
   * Increment by 1 the number of done units of the given control component identified by its index.
   *
   * @param ccIndex the index of the control component which progress will be incremented.
   * @param total   the total number of units to track.
   */
  public final void incrementProgress(int ccIndex, int total) {
    Integer newDoneValue = done.compute(ccIndex, (key, oldValue) -> (oldValue != null) ? oldValue + 1 : 1);
    incrementProgress(ccIndex, newDoneValue, total);
  }

  /**
   * Update the progress
   *
   * @param ccIndex the index of the control component which progress will be updated.
   * @param done    the number of done units.
   * @param total   the number of total units.
   */
  protected abstract void incrementProgress(int ccIndex, int done, int total);
}
