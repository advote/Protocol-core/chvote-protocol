/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.utils;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgeableMulticastEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.event.SimpleAcknowledgementEvent;
import ch.ge.ve.event.SimpleMulticastAcknowledgementEvent;
import ch.ge.ve.event.UnacknowledgedEvent;
import ch.ge.ve.protocol.client.message.MessageHandlerStoreInMemoryImpl;
import ch.ge.ve.protocol.client.message.api.MessageHandler;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;

public class RabbitUtilities {

  public static final String VOTE_PROCESSING_REQUEST_CHANNEL = "voteProcessingRequests";
  public static final String VOTE_PROCESSING_REPLY_CHANNEL   = "voteProcessingReplies";

  private final EventBus         eventBus;
  private final RabbitAdmin      rabbitAdmin;
  private final MessageConverter messageConverter;

  public RabbitUtilities(EventBus eventBus, RabbitAdmin rabbitAdmin, MessageConverter messageConverter) {
    this.eventBus = eventBus;
    this.rabbitAdmin = rabbitAdmin;
    this.messageConverter = messageConverter;
  }

  public <T extends Event> MessageHandler<T> createDefaultMessageHandler(String channel, String protocolId, Class<T>
      messageType,
                                                                         boolean createChannel, long waitTimeout) {
    return createDefaultMessageHandler(channel, protocolId, messageType, noOpConsumer(), createChannel, waitTimeout);
  }

  public <T extends Event> MessageHandler<T> createDefaultMessageHandler(String channel, String protocolId, Class<T>
      messageType,
                                                                         Consumer<T> messageConsumer, boolean
                                                                             createExchange,
                                                                         long waitTimeout) {
    MessageHandler<T> handler = new MessageHandlerStoreInMemoryImpl<>(protocolId, messageType, messageConverter,
                                                                      messageConsumer, waitTimeout);
    bindHandler(channel, handler, createExchange, 1);
    return handler;
  }

  public <T extends Event> void bindHandler(String channel, MessageHandler<T> handler, boolean createChannel,
                                            int concurrentConsumers) {
    if (createChannel) {
      rabbitAdmin.declareExchange(new FanoutExchange(channel, true, false));
    }
    Queue testQueue = createQueue();
    Binding binding = new Binding(testQueue.getName(), Binding.DestinationType.QUEUE, channel, "", new HashMap<>());
    rabbitAdmin.declareBinding(binding);
    handler.setContainer(createMessageContainer(testQueue, handler, concurrentConsumers));
  }

  private <T extends Event> MessageListenerContainer createMessageContainer(Queue testQueue, MessageHandler<T> handler,
                                                                            int concurrentConsumers) {
    MessageListenerAdapter listener = new MessageListenerAdapter(handler, (MessageConverter) null);
    ConnectionFactory connectionFactory = rabbitAdmin.getRabbitTemplate().getConnectionFactory();
    SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer(connectionFactory);
    messageListenerContainer.setConcurrentConsumers(concurrentConsumers);
    messageListenerContainer.setMessageListener(listener);
    messageListenerContainer.setQueueNames(testQueue.getName());
    messageListenerContainer.setDefaultRequeueRejected(false);
    messageListenerContainer.start();
    return messageListenerContainer;
  }

  public <T extends Event> MessageHandler<T> createDefaultMessageHandlerForRoutingKey(String channel, String protocolId,
                                                                                      String routingKey, Class<T>
                                                                                          messageType,
                                                                                      long waitTimeout, Consumer<T>
                                                                                          messageConsumer) {
    rabbitAdmin.declareExchange(new TopicExchange(channel, true, false));
    Queue testQueue = createQueue();
    Binding binding = new Binding(testQueue.getName(), Binding.DestinationType.QUEUE, channel,
                                  routingKey, new HashMap<>());
    rabbitAdmin.declareBinding(binding);
    MessageHandler<T> handler =
        new MessageHandlerStoreInMemoryImpl<>(protocolId, messageType, messageConverter,
                                              messageConsumer == null ? noOpConsumer() : messageConsumer,
                                              waitTimeout);
    handler.setContainer(createMessageContainer(testQueue, handler, 1));
    return handler;
  }

  private Queue createQueue() {
    Queue queue = QueueBuilder.nonDurable() // won't survive a server restart
                              .autoDelete() // will be automatically deleted when no longer used
                              .exclusive()  // will only be used by the declarer's connection
                              .withArgument("x-dead-letter-exchange", Channels.DLX)
                              .build();
    rabbitAdmin.declareQueue(queue);
    return queue;
  }

  public void publish(String channel, String protocolId, String emitter, Event event) {
    eventBus.publish(channel, protocolId, emitter, event);
  }

  public void publish(String channel, String protocolId, String emitter, Event event, String routingKey) {
    eventBus.publish(channel, protocolId, emitter, event, routingKey);
  }

  public void processAcknowledgement(AcknowledgementEvent event) {
    eventBus.processAcknowledgement(event);
  }

  public void clearUnacknowledgedEvents(String protocolId) {
    eventBus.clearUnacknowledgedEvents(protocolId);
  }

  public void acknowledge(AcknowledgeableMulticastEvent event, String protocolId) {
    AcknowledgementEvent acknowledgement = new SimpleMulticastAcknowledgementEvent(
        event.getEventId(), Endpoints.PROTOCOL_CLIENT);
    publish(Channels.ACK, protocolId, event.getEmitter(), acknowledgement, event.getEmitter());
  }

  public void acknowledge(AcknowledgeableEvent event, String protocolId) {
    AcknowledgementEvent acknowledgement = new SimpleAcknowledgementEvent(event.getEventId());
    publish(Channels.ACK, protocolId, event.getEmitter(), acknowledgement, event.getEmitter());
  }

  public List<UnacknowledgedEvent> getUnacknowledgedEvents() {
    return eventBus.getUnacknowledgedEvents();
  }

  public MessageConverter getMessageConverter() {
    return messageConverter;
  }

  private <T> Consumer<T> noOpConsumer() {
    return t -> {
      // empty by design: NoOp
    };
  }
}
