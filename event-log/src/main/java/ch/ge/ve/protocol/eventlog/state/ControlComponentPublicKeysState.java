/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * {@code ControlComponentPublicKeysState} is responsible to maintain the state of the public key parts for components
 * across their clustered deployment.
 * <p>
 * Control component public keys are keyed by their source authority index.
 */
public class ControlComponentPublicKeysState extends AbstractKeyedPropertyState<EncryptionPublicKey> {

  public ControlComponentPublicKeysState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter
      converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.CC_PUBLIC_KEY_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format(
        "Once the control component public key has been set for authority j=%d, it can no longer be changed",
        key);
  }

  @Override
  protected TypeReference<EncryptionPublicKey> getPropertyType() {
    return new TypeReference<EncryptionPublicKey>() {
    };
  }
}
