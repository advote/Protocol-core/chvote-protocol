/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.entity;

/**
 * The types of events representing the state of the bulletin board
 */
public enum EventType {
  PUBLIC_PARAMETERS_ADDED,
  ENCRYPTION_KEY_PAIR_ADDED,
  ELECTION_OFFICER_PUBLIC_KEY_ADDED,
  SYSTEM_PUBLIC_KEY_ADDED,
  ELECTION_SET_ADDED,
  VOTER_ADDED(true),
  PRIMES_ADDED,
  CC_PUBLIC_KEY_ADDED(true),
  VOTER_DATA_ADDED(true),
  PUBLIC_CREDENTIALS_PART_ADDED(true),
  PUBLIC_CREDENTIALS_ADDED(true),
  BALLOT_AND_QUERY_ADDED(true),
  BALLOT_ENTRY_ADDED(true),
  CONFIRMATION_ADDED(true),
  CONFIRMATION_ENTRY_ADDED(true),
  GENERATORS_ADDED,
  SHUFFLES_ADDED(true),
  SHUFFLE_PROOFS_ADDED(true),
  SHUFFLE_AND_PROOF_ADDED(true),
  SHUFFLE_VERIFICATION_ADDED(true),
  DECRYPTIONS_ADDED(true),
  DECRYPTION_PROOFS_ADDED(true),
  TALLY_ADDED;

  private final boolean keyedProperty;

  EventType() {
    this.keyedProperty = false;
  }

  EventType(boolean keyedProperty) {
    this.keyedProperty = keyedProperty;
  }

  /**
   * A state property is keyed if its values are mapped to different keys. However for a given key, only one value is
   * allowed.
   * <p>
   * Examples of keys are voter id or authority index.
   *
   * @return true if the property is mapped by a key
   */
  public boolean isKeyedProperty() {
    return keyedProperty;
  }
}
