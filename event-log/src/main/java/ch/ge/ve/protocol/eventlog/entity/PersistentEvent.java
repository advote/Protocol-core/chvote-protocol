/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity for the event log persistence.
 *
 * The table name is a generic one that is modified at runtime (prefix XX replaced by the component prefix) by a
 * {@link org.hibernate.boot.model.naming.PhysicalNamingStrategy} so that the different components using this generic
 * implementation can have their own dedicated table for storing their events.
 */
@Entity
@Table(name = "XX_T_EVENTLOG",
       uniqueConstraints = @UniqueConstraint(columnNames = {"protocolId", "type", "key", "secondaryKey"}),
       indexes = {@Index(columnList = "protocolId, type"), @Index(columnList = "protocolId, type, key")})
// Note : allocationSize set to 1 as a workaround to a chaotic sequence caching since Hibernate 5.2.17 upgrade
//    .. but performance suffers from that, we should find a more adequate configuration
@SequenceGenerator(name = "eventIdSeq", sequenceName = "XX_EventIdSequence", allocationSize = 1)
public class PersistentEvent implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventIdSeq")
  private long id;

  private String protocolId;

  @Enumerated(EnumType.STRING)
  private EventType type;

  private Integer key;

  private Integer secondaryKey;

  @Lob
  private String jsonPayload;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getProtocolId() {
    return protocolId;
  }

  public void setProtocolId(String protocolId) {
    this.protocolId = protocolId;
  }

  public EventType getType() {
    return type;
  }

  public void setType(EventType type) {
    this.type = type;
  }

  public String getJsonPayload() {
    return jsonPayload;
  }

  public void setJsonPayload(String payload) {
    this.jsonPayload = payload;
  }

  public Integer getKey() {
    return key;
  }

  public void setKey(Integer j) {
    this.key = j;
  }

  public Integer getSecondaryKey() {
    return secondaryKey;
  }

  public void setSecondaryKey(Integer secondaryKey) {
    this.secondaryKey = secondaryKey;
  }
}

