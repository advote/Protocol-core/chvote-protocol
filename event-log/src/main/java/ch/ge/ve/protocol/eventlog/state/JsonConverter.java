/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.stereotype.Component;

/**
 * Utility class to serialize/deserialize object attributes into/from a json string representation.
 */
@Component
public class JsonConverter {

  private final ObjectMapper objectMapper;

  public JsonConverter(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * Serializes an object into json
   *
   * @param object object to be serialized
   *
   * @return the json representation, or null if <code>object</code> is null
   */
  public String serialize(Object object) {
    if (object == null) {
      return null;
    } else {
      try {
        return objectMapper.writeValueAsString(object);
      } catch (JsonProcessingException e) {
        throw new JsonSerializationRuntimeException("Could not serialize " + object, e);
      }
    }
  }

  /**
   * Deserializes a json representation into the <code>targetType</code>
   *
   * @param databaseValue json string representation of the object in database
   * @param targetType   type into which the json is to be deserialized
   *
   * @return the deserialized object, or null if <code>databaseValue</code> is null
   *
   * @throws JsonSerializationRuntimeException if the json representation can't be deserialized into the target
   *                                                    class
   */
  public <T> T deserialize(String databaseValue, TypeReference<T> targetType) {
    if (databaseValue == null) {
      return null;
    } else {
      try {
        return objectMapper.readValue(databaseValue, targetType);
      } catch (IOException e) {
        throw new JsonSerializationRuntimeException("Could not deserialize " + databaseValue, e);
      }
    }
  }
}
