/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

/**
 *
 */
class JsonConverterTest extends Specification {
    def converter = new JsonConverter(new ObjectMapper())

    def "serialize an object"() {
        given:
        def object = new MyObject()
        object.setI(1)
        object.setS("test object")

        when:
        def jsonColumnValue = converter.serialize(object)

        then:
        jsonColumnValue == '{"i":1,"s":"test object"}'
    }

    def "deserialize an object"() {
        given:
        def jsonString = '{"i":1,"s":"test object"}'

        when:
        def object = converter.deserialize(jsonString, new TypeReference<MyObject>() {})

        then:
        object.getI() == 1
        object.getS() == "test object"
    }

    def "deserializing an object from a wrong representation should throw a runtime exception"() {
        given:
        def jsonString = '{"i":1,"t":"test object"}'

        when:
        converter.deserialize(jsonString, new TypeReference<MyObject>() {})

        then:
        thrown JsonSerializationRuntimeException
    }

    static class MyObject {
        private int i
        private String s

        MyObject() {
        }

        int getI() {
            return i
        }

        void setI(int i) {
            this.i = i
        }

        String getS() {
            return s
        }

        void setS(String s) {
            this.s = s
        }
    }
}
