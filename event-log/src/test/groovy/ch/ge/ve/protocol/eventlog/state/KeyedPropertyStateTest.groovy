/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.eventlog.entity.EventType.CC_PUBLIC_KEY_ADDED

import ch.ge.ve.protocol.eventlog.entity.EventType
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent
import ch.ge.ve.protocol.eventlog.service.EventLogCommand
import ch.ge.ve.protocol.eventlog.service.EventLogQuery
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.support.PublicParametersFactory
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.dao.DataIntegrityViolationException
import spock.lang.Shared
import spock.lang.Specification

class KeyedPropertyStateTest extends Specification {

    private static final String PROTOCOL_ID = "0"

    EncryptionGroup encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)

    @Shared
    PublicParameters level1PublicParameters

    @Shared
    JsonConverter jsonConverter

    EventLogCommand eventLogCommand
    EventLogQuery eventLogQuery

    void setupSpec() {
        level1PublicParameters = PublicParametersFactory.LEVEL_1.createPublicParameters()
        def mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        jsonConverter = new JsonConverter(mapper)
    }

    void setup() {
        eventLogCommand = Mock()
        eventLogQuery = Mock()
    }

    def "adding public key parts on two nodes should be idempotent"() {

        given: "a control component public key part"
        def expectedPublicKeyPart = new EncryptionPublicKey(THREE, encryptionGroup)

        and: "a fresh bulletin board state on a first node"
        def eventLogCommand1 = Mock(EventLogCommand.class)
        def eventLogQuery1 = Mock(EventLogQuery.class)
        def publicKeyPartsState1 = new TestKeyedPropertyState(eventLogCommand1, eventLogQuery1, jsonConverter)

        and: "a fresh bulletin board state on a second node"
        def eventLogCommand2 = Mock(EventLogCommand.class)
        def eventLogQuery2 = Mock(EventLogQuery.class)
        def publicKeyPartsState2 = new TestKeyedPropertyState(eventLogCommand2, eventLogQuery2, jsonConverter)

        and: "the future persisted event"
        PersistentEvent pkpAdded1

        when: "the public key part is stored on the first node"
        publicKeyPartsState1.storeValueIfAbsent(PROTOCOL_ID, 0, expectedPublicKeyPart)

        then: "the state of the first node should call for storing the event"
        0 * eventLogQuery1.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED, 0) >> []
        1 * eventLogCommand1.append({ it.type == CC_PUBLIC_KEY_ADDED }) >> {
            PersistentEvent e -> pkpAdded1 = e
        }

        when: "they are stored on the second node"
        publicKeyPartsState2.storeValueIfAbsent(PROTOCOL_ID, 0, expectedPublicKeyPart)

        then: "the state of the second node should fail when attempting to store the key"
        1 * eventLogCommand2.append({ it.type == CC_PUBLIC_KEY_ADDED }) >> {
            throw new DataIntegrityViolationException("unique constraint violated")
        }
        and: "the previous value should be retrieved for comparison"
        1 * eventLogQuery2.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED, 0) >> [pkpAdded1]

        when: "getting all the authorities key parts on both nodes"
        def publicKeyParts1 = publicKeyPartsState1.getMappedValues(PROTOCOL_ID)
        def publicKeyParts2 = publicKeyPartsState2.getMappedValues(PROTOCOL_ID)

        then: "the two states should return the same parameters"
        1 * eventLogQuery1.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED) >> [pkpAdded1]
        publicKeyParts1[0] == expectedPublicKeyPart

        1 * eventLogQuery2.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED) >> [pkpAdded1]
        publicKeyParts2[0] == expectedPublicKeyPart
    }

    def "adding different control component key part should throw an exception"() {
        given: "a control component public key already defined for cc#1"
        def state = new TestKeyedPropertyState(eventLogCommand, eventLogQuery, jsonConverter)
        def pkpAdded
        eventLogQuery.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED, 1) >> []
        eventLogCommand.append({ it.type == CC_PUBLIC_KEY_ADDED }) >> {
            PersistentEvent e -> pkpAdded = e
        }
        state.storeValueIfAbsent(PROTOCOL_ID, 1, new EncryptionPublicKey(THREE, encryptionGroup))

        when: "a new public key is to be stored for cc#1 and the command throws a DataIntegrityViolationException"
        eventLogCommand.append({ it.type == CC_PUBLIC_KEY_ADDED }) >> {
            throw new DataIntegrityViolationException("unique constraint violated")
        }
        state.storeValueIfAbsent(PROTOCOL_ID, 1, new EncryptionPublicKey(FIVE, encryptionGroup))

        then: "an exception should be thrown because state is immutable"
        1 * eventLogQuery.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED, 1) >> [pkpAdded]
        thrown(ImmutablePropertyRuntimeException.class)
    }

    def "having an inconsistent event store should throw an exception"() {
        given: "a state backed by an event store with two CC_PUBLIC_KEY_ADDED events"
        def state = new TestKeyedPropertyState(eventLogCommand, eventLogQuery, jsonConverter)

        def event1 = new PersistentEvent()
        event1.setId(1)
        event1.setProtocolId(PROTOCOL_ID)
        event1.setType(CC_PUBLIC_KEY_ADDED)
        event1.setKey(1)
        event1.setJsonPayload("{}")

        def event2 = new PersistentEvent()
        event2.setId(2)
        event2.setProtocolId(PROTOCOL_ID)
        event2.setType(CC_PUBLIC_KEY_ADDED)
        event2.setKey(1)
        event2.setJsonPayload("{}")

        eventLogQuery.getEvents(PROTOCOL_ID, CC_PUBLIC_KEY_ADDED) >> [event1, event2]

        when: "state value is queried"
        state.getMappedValues(PROTOCOL_ID)

        then: "an exception should be thrown"
        thrown(IllegalStateException.class)
    }

    private static class TestKeyedPropertyState extends AbstractKeyedPropertyState<EncryptionPublicKey> {

        TestKeyedPropertyState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
            super(eventLogCommand, eventLogQuery, converter)
        }

        @Override
        protected EventType getEventType() {
            return CC_PUBLIC_KEY_ADDED
        }

        @Override
        protected String createImmutabilityMessage(int key) {
            return "Public key part cannot be changed once set for an authority"
        }

        @Override
        protected TypeReference<EncryptionPublicKey> getPropertyType() {
            return new TypeReference<EncryptionPublicKey>() {}
        }
    }
}
