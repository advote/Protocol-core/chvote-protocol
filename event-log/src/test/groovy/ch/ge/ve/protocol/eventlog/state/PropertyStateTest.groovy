/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state

import static ch.ge.ve.protocol.eventlog.entity.EventType.PUBLIC_PARAMETERS_ADDED

import ch.ge.ve.protocol.eventlog.entity.EventType
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent
import ch.ge.ve.protocol.eventlog.service.EventLogCommand
import ch.ge.ve.protocol.eventlog.service.EventLogQuery
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.support.PublicParametersFactory
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.dao.DataIntegrityViolationException
import spock.lang.Shared
import spock.lang.Specification

class PropertyStateTest extends Specification {

    private static final String PROTOCOL_ID = "0"

    @Shared
    PublicParameters level1PublicParameters

    @Shared
    JsonConverter jsonConverter

    EventLogCommand eventLogCommand
    EventLogQuery eventLogQuery

    void setupSpec() {
        level1PublicParameters = PublicParametersFactory.LEVEL_1.createPublicParameters()
        def mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        jsonConverter = new JsonConverter(mapper)
    }

    void setup() {
        eventLogCommand = Mock()
        eventLogQuery = Mock()
    }

    def "adding public parameters for the first time should update the bulletin board state and call persistence"() {
        given:
        def state = new TestPropertyState(eventLogCommand, eventLogQuery, jsonConverter)
        PersistentEvent publicParametersAdded

        when: "the public parameters are stored for the first time to the state"
        state.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        then: "the state should call the event log for storing the event"
        1 * eventLogCommand.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            PersistentEvent e -> publicParametersAdded = e
        }

        when: "getting the public parameters state"
        def publicParameters = state.getValue(PROTOCOL_ID).get()

        then:
        1 * eventLogQuery.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded]
        publicParameters == level1PublicParameters
    }

    def "adding public parameters should be idempotent"() {
        given:
        def state = new TestPropertyState(eventLogCommand, eventLogQuery, jsonConverter)
        PersistentEvent publicParametersAdded1

        when: "the public parameters are stored for the first time to the state"
        state.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        then: "the state should call the event log for storing the event"
        1 * eventLogCommand.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            PersistentEvent e -> publicParametersAdded1 = e
        }

        when: "the public parameters are stored a second time into the state"
        state.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        then: "a DataIntegrityViolationException is thrown"
        1 * eventLogCommand.append(_ as PersistentEvent) >> {
            throw new DataIntegrityViolationException("unique constraint violated")
        }
        and: "the previous value is retrieved to be compared with the new one"
        1 * eventLogQuery.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded1]

        when:
        def publicParameters = state.getValue(PROTOCOL_ID).get()

        then: "the state should return the same parameters"
        1 * eventLogQuery.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded1]
        publicParameters == level1PublicParameters
    }

    def "adding public parameters on two nodes should be idempotent"() {
        given: "a fresh bulletin board state on one node"
        def eventLogCommand1 = Mock(EventLogCommand.class)
        def eventLogQuery1 = Mock(EventLogQuery.class)
        def state1 = new TestPropertyState(eventLogCommand1, eventLogQuery1, jsonConverter)
        PersistentEvent publicParametersAdded1

        and: "fresh bulletin board state on a second node"
        def eventLogCommand2 = Mock(EventLogCommand.class)
        def eventLogQuery2 = Mock(EventLogQuery.class)
        def state2 = new TestPropertyState(eventLogCommand2, eventLogQuery2, jsonConverter)

        when: "they are stored on the first node"
        state1.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        then: "the state of the first node should call for storing the event, without reading the previous value"
        1 * eventLogCommand1.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            PersistentEvent e -> publicParametersAdded1 = e
        }
        0 * eventLogQuery1.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED)

        when: "they are stored on the second node"
        state2.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        then: "the state of the second node should fail when storing the event"
        1 * eventLogCommand2.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            throw new DataIntegrityViolationException("unique constaint violated")
        }
        and: "the previous value should be retrieved for comparison"
        1 * eventLogQuery2.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded1]


        when:
        def publicParameters1 = state1.getValue(PROTOCOL_ID, ).get()
        def publicParameters2 = state2.getValue(PROTOCOL_ID, ).get()

        then: "the two states should return the same parameters"
        1 * eventLogQuery1.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded1]
        publicParameters1 == level1PublicParameters

        1 * eventLogQuery2.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded1]
        publicParameters2 == level1PublicParameters
    }

    def "adding different public parameters should throw an exception"() {
        given: "a public parameter state already defined"
        def state = new TestPropertyState(eventLogCommand, eventLogQuery, jsonConverter)
        def publicParametersAdded
        eventLogCommand.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            PersistentEvent e -> publicParametersAdded = e
        }
        state.storeValueIfAbsent(PROTOCOL_ID, level1PublicParameters)

        when: "new public parameters are to be stored"
        state.storeValueIfAbsent(PROTOCOL_ID, PublicParametersFactory.LEVEL_2.createPublicParameters())

        then: "an exception should be thrown because state is immutable"
        1 * eventLogCommand.append({ it.type == PUBLIC_PARAMETERS_ADDED }) >> {
            throw new DataIntegrityViolationException("unique constraint violated")
        }
        1 * eventLogQuery.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [publicParametersAdded]
        thrown(ImmutablePropertyRuntimeException.class)
    }

    def "having an inconsistent event store should throw an exception"() {
        given: "a state backed by an event store with two PUBLIC_PARAMETERS_ADDED events"
        def state = new TestPropertyState(eventLogCommand, eventLogQuery, jsonConverter)

        def event1 = new PersistentEvent()
        event1.setId(1)
        event1.setProtocolId(PROTOCOL_ID)
        event1.setType(PUBLIC_PARAMETERS_ADDED)
        event1.setJsonPayload("{}")

        def event2 = new PersistentEvent()
        event2.setId(2)
        event2.setProtocolId(PROTOCOL_ID)
        event2.setType(PUBLIC_PARAMETERS_ADDED)
        event2.setJsonPayload("{}")

        eventLogQuery.getEvents(PROTOCOL_ID, PUBLIC_PARAMETERS_ADDED) >> [event1, event2]

        when: "state value is queried"
        state.getValue(PROTOCOL_ID)

        then: "an exception should be thrown"
        thrown(IllegalStateException.class)
    }

    private static class TestPropertyState extends AbstractPropertyState<PublicParameters> {

        TestPropertyState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
            super(eventLogCommand, eventLogQuery, converter)
        }

        @Override
        protected EventType getEventType() {
            return PUBLIC_PARAMETERS_ADDED
        }

        @Override
        protected String createImmutabilityMessage() {
            return "Public parameters cannot be changed once set"
        }

        @Override
        protected TypeReference<PublicParameters> getPropertyType() {
            return new TypeReference<PublicParameters>() {}
        }
    }
}
