#Test Execution Results

##Parameters
  --host=ve-proto-1

  --port=8100

  --username=rabbitmq

  --nbControlComponents=4

  --votersCount=10000

  --votesCount=1800

  --waitTimeout=2400000

  --securityLevel=2

  --electionSet=SIMPLE_SAMPLE

  --filesOutputPath=run-8_CPUs-SIMPLE_SAMPLE-1800-10000

##Execution Times
Began at Nov 3, 2017 10:45:02 AM.

Lasted 00:07:05.825.

Phase                                 | Duration     
:-------------------------------------|-------------:
Initialization|00:02:12.656
Create voters and decrypt printer files|00:00:10.908
Cast votes|00:01:41.767
Shuffle and partial decrypt|00:02:25.612
Tally|00:00:34.880

Voter step in vote casting phase      | Total duration | Average duration | Min duration | Max duration   
:-------------------------------------|---------------:|-----------------:|-------------:|---------------:
Prepare ballot (client side)|00:03:07.154|00:00:00.103|00:00:00.063|00:00:00.563
Publish ballot|00:30:56.504|00:00:01.031|00:00:00.173|00:01:10.058
Check verification codes (client side)|00:03:16.755|00:00:00.109|00:00:00.063|00:00:00.404
Publish confirmation|00:23:52.992|00:00:00.796|00:00:00.042|00:01:07.428
Check finalization code (client side)|00:00:02.702|00:00:00.001|00:00:00.000|00:00:00.115

## Generated files

### Printer files
File name                        | Size              
:------------------------------- | -----------------:
printer_file_PrintingAuthority2_CC3.epf|3 MB
printer_file_PrintingAuthority1_CC3.epf|3 MB
printer_file_PrintingAuthority0_CC3.epf|3 MB
printer_file_PrintingAuthority3_CC3.epf|3 MB
printer_file_PrintingAuthority2_CC0.epf|3 MB
printer_file_PrintingAuthority1_CC0.epf|3 MB
printer_file_PrintingAuthority0_CC0.epf|3 MB
printer_file_PrintingAuthority3_CC0.epf|3 MB
printer_file_PrintingAuthority2_CC2.epf|3 MB
printer_file_PrintingAuthority1_CC2.epf|3 MB
printer_file_PrintingAuthority0_CC2.epf|3 MB
printer_file_PrintingAuthority3_CC2.epf|3 MB
printer_file_PrintingAuthority2_CC1.epf|3 MB
printer_file_PrintingAuthority1_CC1.epf|3 MB
printer_file_PrintingAuthority0_CC1.epf|3 MB
printer_file_PrintingAuthority3_CC1.epf|3 MB

### Protocol audit log file
File name                        | Size              
:------------------------------- | -----------------:
verificationData-2048-SIMPLE_SAMPLE-10000.json|105 MB

## Database Volumetrics
Schema size after test: 1 GB.
