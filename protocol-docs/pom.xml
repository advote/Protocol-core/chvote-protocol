<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ protocol-core
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>ch.ge.ve</groupId>
        <artifactId>protocol-core</artifactId>
        <version>1.0.16</version>
    </parent>

    <artifactId>protocol-docs</artifactId>
    <packaging>pom</packaging>

    <name>protocol-docs</name>
    <description>CHVote protocol documentation</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <asciidoctor.maven.plugin.version>1.5.7.1</asciidoctor.maven.plugin.version>
        <asciidoctorj.pdf.version>1.5.0-alpha.16</asciidoctorj.pdf.version>
        <plantuml-maven-plugin.version>1.2</plantuml-maven-plugin.version>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>com.github.jeluard</groupId>
                <artifactId>plantuml-maven-plugin</artifactId>
                <version>${plantuml-maven-plugin.version}</version>
                <configuration>
                    <sourceFiles>
                        <directory>${basedir}/src/main/diagrams</directory>
                        <includes>
                            <include>**/*.puml</include>
                        </includes>
                    </sourceFiles>
                    <outputDirectory>${basedir}/src/main/images/generated</outputDirectory>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.plantuml</groupId>
                        <artifactId>plantuml</artifactId>
                        <version>8059</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>generate-diagrams</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy-asciidoc-resources</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>src/main/images</directory>
                                    <includes>
                                        <include>**/*.jpg</include>
                                        <include>**/*.png</include>
                                        <include>**/*.svg</include>
                                    </includes>
                                </resource>
                            </resources>
                            <outputDirectory>target/generated-docs/images</outputDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-asciidoc-icons</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>src/main/icons</directory>
                                    <includes>
                                        <include>**/*.png</include>
                                    </includes>
                                </resource>
                            </resources>
                            <outputDirectory>target/generated-docs/icons</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoctor-maven-plugin</artifactId>
                <version>${asciidoctor.maven.plugin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>org.asciidoctor</groupId>
                        <artifactId>asciidoctorj-pdf</artifactId>
                        <version>${asciidoctorj.pdf.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>generate-pdf-doc</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/generated-docs/pdf</outputDirectory>
                            <preserveDirectories>true</preserveDirectories>
                            <relativeBaseDir>true</relativeBaseDir>
                            <backend>pdf</backend>
                            <sourceHighlighter>coderay</sourceHighlighter>
                            <attributes>
                                <icons>font</icons>
                                <pagenums/>
                                <toc/>
                                <idprefix/>
                                <idseparator>-</idseparator>
                            </attributes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
