/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOTS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOT_KEYS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATION_KEYS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DECRYPTION_PROOFS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DONE;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_OFFICER_KEY_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_SET_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.GENERATORS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.INITIALIZED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PARTIAL_DECRYPTIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PRIMES_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_CREDENTIALS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_KEY_PARTS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_PARAMETERS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLES_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLE_PROOFS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.TALLY_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.UNINITIALIZED;
import static ch.ge.ve.protocol.support.WriterUtils.FIELD_SEPARATOR;
import static ch.ge.ve.protocol.support.WriterUtils.fieldKey;

import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.base.Suppliers;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Writes the election verification data to the provided output stream.
 * <p>
 * Multiple runs for different outputs should use different instances.
 */
public class ElectionVerificationDataWriterToOutputStream implements ElectionVerificationDataWriter {
  private static final Logger log =
      LoggerFactory.getLogger(ElectionVerificationDataWriterToOutputStream.class);
  private final ObjectMapper                                             objectMapper;
  private final Supplier<PublicCredentialsWriter>                        publicCredentialsWriterSupplier;
  private final Supplier<ValuesByAuthorityIndexWriter<List<Encryption>>> shuffleWriterSupplier;
  private final Supplier<ValuesByAuthorityIndexWriter<ShuffleProof>>     shuffleProofWriterSupplier;
  private final Supplier<ValuesByAuthorityIndexWriter<Decryptions>>      partialDecryptionsWriterSupplier;
  private final Supplier<ValuesByAuthorityIndexWriter<DecryptionProof>>  decryptionProofsWriterSupplier;
  private final BufferedWriter                                           bufferedWriter;
  private final boolean                                                  hasTally;
  private       IntegerMapWriter<BallotAndQuery>                         ballotWriter;
  private       IntegerMapWriter<Confirmation>                           confirmationWriter;
  private int   ccCount    = 0;
  private int   voterCount = 0;
  private State state      = UNINITIALIZED;

  public ElectionVerificationDataWriterToOutputStream(ObjectMapper objectMapper, OutputStream outputStream,
                                                      boolean simulation) {
    this.objectMapper = objectMapper;
    try {
      bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      throw new VerificationDataWriterRuntimeException(e);
    }
    publicCredentialsWriterSupplier =
        Suppliers.memoize(() -> new PublicCredentialsWriter(objectMapper, bufferedWriter, ccCount, voterCount));
    shuffleWriterSupplier =
        Suppliers.memoize(() -> new ValuesByAuthorityIndexWriter<>(objectMapper, bufferedWriter,
                                                                   "shuffles", ccCount, true));
    shuffleProofWriterSupplier =
        Suppliers.memoize(
            () -> new ValuesByAuthorityIndexWriter<>(objectMapper, bufferedWriter, "shuffleProofs", ccCount, true));
    partialDecryptionsWriterSupplier =
        Suppliers.memoize(
            () -> new ValuesByAuthorityIndexWriter<>(objectMapper, bufferedWriter, "partialDecryptions",
                                                     simulation ? ccCount + 1 : ccCount, true));
    decryptionProofsWriterSupplier =
        Suppliers.memoize(
            () -> new ValuesByAuthorityIndexWriter<>(objectMapper, bufferedWriter, "decryptionProofs",
                                                     simulation ? ccCount + 1 : ccCount, simulation));
    this.hasTally = simulation;
  }

  @Override
  public State getState() {
    return state;
  }

  @Override
  public void initialize() throws VerificationDataWriterException {
    log.debug("Initializing the output");
    Preconditions.checkState(UNINITIALIZED.equals(state));
    try {
      bufferedWriter.append(String.format("{%n"));
      bufferedWriter.flush();
    } catch (IOException e) {
      log.error("Writing to output stream failed", e);
      throw new VerificationDataWriterException(e);
    }
    state = INITIALIZED;
  }

  @Override
  public void writePublicParameters(PublicParameters publicParameters) throws VerificationDataWriterException {
    log.debug("writing public parameters");
    Preconditions.checkState(INITIALIZED.equals(state));
    writeValue("publicParameters", publicParameters);

    ccCount = publicParameters.getS();
    state = PUBLIC_PARAMETERS_WRITTEN;
  }

  @Override
  public void writeElectionSet(ElectionSetForVerification electionSet) throws VerificationDataWriterException {
    log.debug("writing election set");
    Preconditions.checkState(PUBLIC_PARAMETERS_WRITTEN.equals(state));
    writeValue("electionSet", electionSet);
    // Too many places where we use List or Map at this stage to get rid of the requirement that
    // voter count is an integer. This will need to be upgraded if we ever go beyond 2 million voters on a single
    // protocol instance
    voterCount = Math.toIntExact(electionSet.getVoterCount());
    state = ELECTION_SET_WRITTEN;
  }

  @Override
  public void writePrimes(List<BigInteger> primes) throws VerificationDataWriterException {
    log.debug("writing primes");
    Preconditions.checkState(ELECTION_SET_WRITTEN.equals(state));
    writeValue("primes", primes);
    state = PRIMES_WRITTEN;
  }

  @Override
  public void writeGenerators(List<BigInteger> generators) throws VerificationDataWriterException {
    log.debug("writing generators");
    Preconditions.checkState(PRIMES_WRITTEN.equals(state));
    writeValue("generators", generators);
    state = GENERATORS_WRITTEN;
  }

  @Override
  public void writePublicKeyParts(Map<Integer, EncryptionPublicKey> publicKeyParts)
      throws VerificationDataWriterException {
    log.debug("writing public key parts");
    Preconditions.checkState(GENERATORS_WRITTEN.equals(state));
    writeValue("publicKeyParts", publicKeyParts);
    state = PUBLIC_KEY_PARTS_WRITTEN;
  }

  @Override
  public void writeElectionOfficerPublicKey(EncryptionPublicKey electionOfficerKey)
      throws VerificationDataWriterException {
    log.debug("writing election officer public key");
    Preconditions.checkState(PUBLIC_KEY_PARTS_WRITTEN.equals(state));
    writeValue("electionOfficerPublicKey", electionOfficerKey);
    state = ELECTION_OFFICER_KEY_WRITTEN;
  }

  @Override
  public void addPublicCredentials(int ccIndex, Map<Integer, Point> publicCredentialsByVoterId) {
    log.debug("received public credentials");
    Preconditions.checkState(ELECTION_OFFICER_KEY_WRITTEN.equals(state));
    final PublicCredentialsWriter publicCredentialsWriter = publicCredentialsWriterSupplier.get();

    publicCredentialsWriter.publishItems(ccIndex, publicCredentialsByVoterId);

    flushWriter();

    if (publicCredentialsWriter.done()) {
      state = PUBLIC_CREDENTIALS_WRITTEN;
    }
  }

  @Override
  public void setExpectedBallotKeys(List<Integer> ballotKeys) {
    log.debug("received expected ballot keys");
    Preconditions.checkState(PUBLIC_CREDENTIALS_WRITTEN.equals(state));
    ballotWriter = new IntegerMapWriter<>(objectMapper, bufferedWriter, "ballots", ballotKeys, true);

    state = BALLOT_KEYS_RECEIVED;
  }

  @Override
  public void addBallots(Map<Integer, BallotAndQuery> ballots) {
    log.debug("received ballots");
    Preconditions.checkState(BALLOT_KEYS_RECEIVED.equals(state));
    ballotWriter.publishValues(ballots);

    flushWriter();

    if (ballotWriter.done()) {
      state = BALLOTS_RECEIVED;
    }
  }

  @Override
  public void setExpectedConfirmationKeys(List<Integer> confirmationKeys) {
    log.debug("received confirmation keys");
    Preconditions.checkState(BALLOTS_RECEIVED.equals(state));
    confirmationWriter = new IntegerMapWriter<>(objectMapper, bufferedWriter, "confirmations", confirmationKeys, true);

    state = CONFIRMATION_KEYS_RECEIVED;
  }

  @Override
  public void addConfirmations(Map<Integer, Confirmation> confirmations) {
    log.debug("received confirmations");
    Preconditions.checkState(CONFIRMATION_KEYS_RECEIVED.equals(state));
    confirmationWriter.publishValues(confirmations);

    flushWriter();

    if (confirmationWriter.done()) {
      state = CONFIRMATIONS_RECEIVED;
    }
  }

  @Override
  public void addShuffle(Integer ccIndex, List<Encryption> shuffle) {
    log.debug("received shuffle");
    Preconditions.checkState(CONFIRMATIONS_RECEIVED.equals(state));
    final ValuesByAuthorityIndexWriter<List<Encryption>> shuffleWriter = shuffleWriterSupplier.get();
    shuffleWriter.publishValue(ccIndex, shuffle);

    flushWriter();

    if (shuffleWriter.done()) {
      state = SHUFFLES_RECEIVED;
    }
  }

  @Override
  public void addShuffleProof(Integer ccIndex, ShuffleProof shuffleProof) {
    log.debug("received shuffle proof");
    Preconditions.checkState(SHUFFLES_RECEIVED.equals(state));
    final ValuesByAuthorityIndexWriter<ShuffleProof> shuffleProofWriter =
        shuffleProofWriterSupplier.get();
    shuffleProofWriter.publishValue(ccIndex, shuffleProof);

    flushWriter();

    if (shuffleProofWriter.done()) {
      state = SHUFFLE_PROOFS_RECEIVED;
    }
  }

  @Override
  public void addPartialDecryptions(Integer authorityIndex, Decryptions partialDecryptions) {
    log.debug("received partial decryption");
    Preconditions.checkState(SHUFFLE_PROOFS_RECEIVED.equals(state));
    final ValuesByAuthorityIndexWriter<Decryptions> partialDecryptionsWriter =
        partialDecryptionsWriterSupplier.get();
    partialDecryptionsWriter.publishValue(authorityIndex, partialDecryptions);

    flushWriter();

    if (partialDecryptionsWriter.done()) {
      state = PARTIAL_DECRYPTIONS_RECEIVED;
    }
  }

  @Override
  public void addDecryptionProof(Integer authorityIndex, DecryptionProof decryptionProof) {
    log.debug("received decryption proof");
    Preconditions.checkState(PARTIAL_DECRYPTIONS_RECEIVED.equals(state));
    final ValuesByAuthorityIndexWriter<DecryptionProof> decryptionProofsWriter =
        decryptionProofsWriterSupplier.get();
    decryptionProofsWriter.publishValue(authorityIndex, decryptionProof);

    flushWriter();

    if (decryptionProofsWriter.done()) {
      state = DECRYPTION_PROOFS_RECEIVED;
    }
  }

  @Override
  public void writeTally(Tally tally) throws VerificationDataWriterException {
    log.debug("reveived tally");
    Preconditions.checkState(DECRYPTION_PROOFS_RECEIVED.equals(state));
    try {
      bufferedWriter.append(fieldKey("tally", false));
      bufferedWriter.append(objectMapper.writeValueAsString(tally));
      bufferedWriter.append(String.format("%n"));
      bufferedWriter.flush();
    } catch (IOException e) {
      log.error("failed to write tally", e);
      throw new VerificationDataWriterException(e);
    }

    state = TALLY_WRITTEN;
  }

  @Override
  public void writeEnd() throws VerificationDataWriterException {
    log.debug("writing the end of the file");
    State expectedState = hasTally ? TALLY_WRITTEN : DECRYPTION_PROOFS_RECEIVED;
    Preconditions.checkState(expectedState.equals(state));

    try {
      bufferedWriter.append(String.format("}%n"));
      bufferedWriter.close();
    } catch (IOException e) {
      log.error("failed to finalize writer", e);
      throw new VerificationDataWriterException(e);
    }
    state = DONE;
  }

  private void writeValue(String fieldName, Object value) throws VerificationDataWriterException {
    try {
      bufferedWriter.append(fieldKey(fieldName, false));
      bufferedWriter.append(objectMapper.writeValueAsString(value));
      bufferedWriter.append(FIELD_SEPARATOR);
      bufferedWriter.flush();
    } catch (IOException e) {
      log.error("Writing to output stream failed", e);
      throw new VerificationDataWriterException(e);
    }
  }

  private void flushWriter() {
    try {
      bufferedWriter.flush();
    } catch (IOException e) {
      throw new VerificationDataWriterRuntimeException(e);
    }
  }
}
