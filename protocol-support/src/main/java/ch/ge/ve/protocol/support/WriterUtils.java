/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import java.nio.charset.Charset;

public final class WriterUtils {
  static final Charset CHARSET         = Charset.forName("UTF-8");
  static final String  FIELD_SEPARATOR = String.format(",%n");
  static final String  LIST_SEPARATOR  = ", ";

  private WriterUtils() {
    // See "Effective Java" (Item 4: Enforce noninstantiability with a private constructor)
    throw new AssertionError("This class is not meant to be instantiated");
  }

  public static String fieldKey(String fieldKey, boolean openField) {
    String template = openField ? "\"%s\": {%n" : "\"%s\": ";
    return String.format(template, fieldKey);
  }
}
