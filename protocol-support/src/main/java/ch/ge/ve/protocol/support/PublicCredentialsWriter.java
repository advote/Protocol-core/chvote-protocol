/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import static ch.ge.ve.protocol.support.WriterUtils.LIST_SEPARATOR;
import static ch.ge.ve.protocol.support.WriterUtils.fieldKey;

import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class PublicCredentialsWriter implements Closeable {
  private static final Logger log = LoggerFactory.getLogger(PublicCredentialsWriter.class);

  /**
   * Map instance used as buffer holding the data that has been received but not yet written
   */
  private final Map<Integer, Map<Integer, Point>> publicCredentialsMap;
  /**
   * Mutex holder for synchronized blocs
   */
  private final Object mutex = new Object();
  private final int          controlComponentsCount;
  private final Writer       writer;
  private final int          voterCount;
  private final ObjectMapper objectMapper;

  private final boolean writeFieldName;
  private       int     currentCcIndex    = 0;
  private       int     currentVoterIndex = 0;

  PublicCredentialsWriter(ObjectMapper objectMapper, Writer writer, int controlComponentsCount, int voterCount) {
    this(objectMapper, writer, controlComponentsCount, voterCount, true);
  }

  PublicCredentialsWriter(ObjectMapper objectMapper, Writer writer, int controlComponentsCount, int voterCount,
                          boolean writeFieldName) {
    this.writer = writer;
    this.voterCount = voterCount;
    this.objectMapper = objectMapper;
    this.publicCredentialsMap = new HashMap<>();
    // Initialize the buffer with thread safe maps
    // Thread safety is only needed for the nested maps, since the first level will never be updated
    this.controlComponentsCount = controlComponentsCount;
    for (int i = 0; i < this.controlComponentsCount; i++) {
      publicCredentialsMap.put(i, new ConcurrentHashMap<>());
    }
    this.writeFieldName = writeFieldName;
  }

  public void publishItems(int ccIndex, Map<Integer, Point> values) {
    Preconditions.checkElementIndex(ccIndex, controlComponentsCount);
    publicCredentialsMap.get(ccIndex).putAll(values);
    writeItems();
  }

  public boolean done() {
    return currentCcIndex >= controlComponentsCount;
  }

  private void writeItems() {
    while (writeNextItem()) {
      log.debug("Wrote public credentials part");
    }
  }

  private boolean writeNextItem() {
    boolean callAgain = false;
    synchronized (mutex) {
      try {
        if (publicCredentialsMap.get(currentCcIndex).containsKey(currentVoterIndex)) {
          // then the next entry is available!
          writeFieldDeclaration();
          // call to Map#remove, to cleanup and free memory as we go...
          final Point currentPoint = publicCredentialsMap.get(currentCcIndex).remove(currentVoterIndex++);
          writer.append(objectMapper.writeValueAsString(currentPoint));
          closeOpenItems();
          if (currentCcIndex == controlComponentsCount) {
            // Need to write the end of field for the public credentials
            writeEnd();
          } else {
            callAgain = true;
          }
        }
      } catch (IOException e) {
        log.error("Failed to write public credentials entry");
        throw new VerificationDataWriterRuntimeException(e);
      }
    }
    return callAgain;
  }

  private void writeEnd() throws IOException {
    writer.append("}");
    if (writeFieldName) { // we write the separator only if we have written the fieldname
      writer.append(String.format(",%n"));
    }
  }

  private void writeFieldDeclaration() throws IOException {
    if (currentCcIndex == 0 && currentVoterIndex == 0) {
      // Need to write the field declaration for the whole block
      writer.append(writeFieldName ? fieldKey("publicCredentials", true) : "{");
    }
    if (currentVoterIndex == 0) {
      // Need to write the field declaration for the current list
      writer.append(String.format("\"%d\": [%n", currentCcIndex));
    }
  }

  private void closeOpenItems() throws IOException {
    if (currentVoterIndex == voterCount) {
      // Need to write the end of current list
      currentVoterIndex = 0;
      String template = ++currentCcIndex < controlComponentsCount ? "],%n" : "]%n";
      // String.format converts %n into a system dependent newline...
      //noinspection RedundantStringFormatCall
      writer.append(String.format(template));
    } else {
      writer.append(LIST_SEPARATOR);
    }
  }

  @Override
  public void close() throws IOException {
    writer.close();
  }
}
