/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.PrimeField;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.SecurityParameters;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public enum PublicParametersFactory {

  LEVEL_0 {
    @Override
    public PublicParameters createPublicParameters(int numberOfControlComponent) {
      SecurityParameters securityParameters = new SecurityParameters(3, 3, 1, 0.9);

      EncryptionGroup encryptionGroup = new EncryptionGroup(TestingConstants.p_RC0e,
                                                            TestingConstants.q_RC0e,
                                                            TestingConstants.g_RC0e,
                                                            TestingConstants.h_RC0e);
      IdentificationGroup identificationGroup = new IdentificationGroup(TestingConstants.p_hat_RC0s,
                                                                        TestingConstants.q_hat_RC0s,
                                                                        TestingConstants.g_hat_RC0s);
      PrimeField primeField = new PrimeField(TestingConstants.p_prime_RC0);

      return new PublicParameters(securityParameters, encryptionGroup, identificationGroup, primeField,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  defaultAlphabet, 1,
                                  defaultAlphabet, 1,
                                  numberOfControlComponent, 3);
    }
  },
  LEVEL_1 {
    @Override
    public PublicParameters createPublicParameters(int numberOfControlComponent) {
      SecurityParameters securityParameters = new SecurityParameters(80, 80, 20, 0.999);

      EncryptionGroup encryptionGroup = new EncryptionGroup(TestingConstants.p_RC1e,
                                                            TestingConstants.q_RC1e,
                                                            TestingConstants.g_RC1e,
                                                            TestingConstants.h_RC1e);
      IdentificationGroup identificationGroup = new IdentificationGroup(TestingConstants.p_hat_RC1s,
                                                                        TestingConstants.q_hat_RC1s,
                                                                        TestingConstants.g_hat_RC1s);
      PrimeField primeField = new PrimeField(TestingConstants.p_prime_RC1);

      return new PublicParameters(securityParameters, encryptionGroup, identificationGroup, primeField,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  defaultAlphabet, 2,
                                  defaultAlphabet, 2,
                                  numberOfControlComponent, TestingConstants.default_n_max);
    }
  },
  LEVEL_2 {
    @Override
    public PublicParameters createPublicParameters(int numberOfControlComponent) {
      SecurityParameters securityParameters = new SecurityParameters(112, 112, 28, 0.999);

      EncryptionGroup encryptionGroup = new EncryptionGroup(TestingConstants.p_RC2e,
                                                            TestingConstants.q_RC2e,
                                                            TestingConstants.g_RC2e,
                                                            TestingConstants.h_RC2e);
      IdentificationGroup identificationGroup = new IdentificationGroup(TestingConstants.p_hat_RC2s,
                                                                        TestingConstants.q_hat_RC2s,
                                                                        TestingConstants.g_hat_RC2s);
      PrimeField primeField = new PrimeField(TestingConstants.p_prime_RC2);

      return new PublicParameters(securityParameters, encryptionGroup, identificationGroup, primeField,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  defaultAlphabet, 2,
                                  defaultAlphabet, 2,
                                  numberOfControlComponent, TestingConstants.default_n_max);
    }

  },
  LEVEL_3 {
    @Override
    public PublicParameters createPublicParameters(int numberOfControlComponent) {
      SecurityParameters securityParameters = new SecurityParameters(128, 128, 32, 0.999);

      EncryptionGroup encryptionGroup = new EncryptionGroup(TestingConstants.p_RC3e,
                                                            TestingConstants.q_RC3e,
                                                            TestingConstants.g_RC3e,
                                                            TestingConstants.h_RC3e);
      IdentificationGroup identificationGroup = new IdentificationGroup(TestingConstants.p_hat_RC3s,
                                                                        TestingConstants.q_hat_RC3s,
                                                                        TestingConstants.g_hat_RC3s);
      PrimeField primeField = new PrimeField(TestingConstants.p_prime_RC3);

      return new PublicParameters(securityParameters, encryptionGroup, identificationGroup, primeField,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  identificationGroup.getQ_hat(), defaultAlphabet,
                                  defaultAlphabet, 2,
                                  defaultAlphabet, 2,
                                  numberOfControlComponent, TestingConstants.default_n_max);
    }
  };

  private static final List<Character> defaultAlphabet = getDefaultAlphabet();

  private static List<Character> getDefaultAlphabet() {
    char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_".toCharArray();
    List<Character> alphabet = new ArrayList<>();
    for (char c : chars) {
      alphabet.add(c);
    }
    return alphabet;
  }

  public static PublicParametersFactory forLevel(int level) {
    return PublicParametersFactory.valueOf("LEVEL_" + level);
  }

  public PublicParameters createPublicParameters() {
    return createPublicParameters(4);
  }

  public abstract PublicParameters createPublicParameters(int numberOfControlComponent);
}
