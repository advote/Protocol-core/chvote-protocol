/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Writer;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ValuesByAuthorityIndexWriter<T> extends IntegerMapWriter<T> {
  public ValuesByAuthorityIndexWriter(ObjectMapper objectMapper, Writer writer, String fieldName,
                                      int authoritiesCount, boolean writeNextFieldSeparator) {
    super(objectMapper, writer, fieldName,
          IntStream.range(0, authoritiesCount).boxed().collect(Collectors.toList()),
          writeNextFieldSeparator);
  }

  public void publishValue(int authorityIndex, T value) {
    super.publishValues(Collections.singletonMap(authorityIndex, value));
  }
}
